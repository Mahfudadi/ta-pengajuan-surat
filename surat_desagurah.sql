/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.4.11-MariaDB : Database - surat_desagurah
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`surat_desagurah` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `surat_desagurah`;

/*Table structure for table `surat_biodata` */

DROP TABLE IF EXISTS `surat_biodata`;

CREATE TABLE `surat_biodata` (
  `id_surat` varchar(30) NOT NULL,
  `id_status` varchar(10) DEFAULT NULL,
  `id_user` varchar(20) DEFAULT NULL,
  `no_surat` varchar(30) DEFAULT NULL,
  `tgl_surat` datetime DEFAULT NULL,
  `hub_keluarga` varchar(50) DEFAULT NULL,
  `nik_ibu` int(30) DEFAULT NULL,
  `nama_ibu` varchar(100) DEFAULT NULL,
  `nik_ayah` int(30) DEFAULT NULL,
  `nama_ayah` varchar(100) DEFAULT NULL,
  `no_paspor` varchar(50) DEFAULT NULL,
  `tenggang_paspor` date DEFAULT NULL,
  `no_akta_kelahiran` varchar(50) DEFAULT NULL,
  `no_akta_kawin` varchar(50) DEFAULT NULL,
  `tgl_kawin` date DEFAULT NULL,
  `no_akta_cerai` varchar(50) DEFAULT NULL,
  `tgl_cerai` date DEFAULT NULL,
  `alamat_sekarang` varchar(255) DEFAULT NULL,
  `rt_sekarang` varchar(10) DEFAULT NULL,
  `rw_sekarang` varchar(10) DEFAULT NULL,
  `dusun_sekarang` varchar(20) DEFAULT NULL,
  `ket_tolak` varchar(255) DEFAULT NULL,
  `penolak` varchar(30) DEFAULT NULL,
  `scan_kk` varchar(100) DEFAULT NULL,
  `scan_ektp` varchar(100) DEFAULT NULL,
  `scan_pajak` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_surat`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `surat_biodata` */

insert  into `surat_biodata`(`id_surat`,`id_status`,`id_user`,`no_surat`,`tgl_surat`,`hub_keluarga`,`nik_ibu`,`nama_ibu`,`nik_ayah`,`nama_ayah`,`no_paspor`,`tenggang_paspor`,`no_akta_kelahiran`,`no_akta_kawin`,`tgl_kawin`,`no_akta_cerai`,`tgl_cerai`,`alamat_sekarang`,`rt_sekarang`,`rw_sekarang`,`dusun_sekarang`,`ket_tolak`,`penolak`,`scan_kk`,`scan_ektp`,`scan_pajak`) values 
('biodata0001','6','Wrg01','144/0007/418.69.10/2021','2020-02-07 09:40:54','Anak',2147483647,'Ngasinah',2147483647,'Tukiran','',NULL,'','',NULL,NULL,NULL,'Jalan Jagalan, RT.03 RW.03','01','02','Gurah 2',NULL,NULL,'8b28bdd668ee08fce9c4860d1c7a7e4b.jpg','a4efb1d069506623a6b87c97dc66f3e0.jpg',NULL),
('biodata0002','1','Wrg10','144/0002/418.69.10/2021','2021-03-24 07:00:00','Anak',2147483647,'Srianani',2147483647,'Suyatno','',NULL,'230987890','',NULL,NULL,NULL,'-','-','-','-','Persyaratan Tidak Lengkap',NULL,'da3d079a020a6ffe2e0a371618fe2769.jpg','316e014901955b452e2dd3f0e4a9b7d3.jpg',NULL),
('biodata0003','1','Wrg04','144/0003/418.69.10/2021','2021-07-06 08:07:03','Anak',2147483647,'Nina',2147483647,'Suryanto','587489',NULL,'-','-',NULL,NULL,NULL,'Jagalan','-','-','-',NULL,NULL,'7251991ca61aabf0948d00b965672594.jpg','d14ec28792b3ce868a14c69eaaa23cf5.jpg',NULL),
('biodata0004','1','Wrg05','144/0004/418.69.10/2021','2021-07-07 08:12:03','Anak',2147483647,'Munawaroh',2147483647,'Tukiran','',NULL,'','',NULL,NULL,NULL,'Jalan Kawi','01','02','Jagalan',NULL,NULL,'219726c7df2604b8a5cd6867eb1552a0.jpg','3da477cfd094e031f5b0ac08ee3f754b.jpg',NULL),
('biodata0005','1','Wrg09','144/0005/418.69.10/2021','2021-07-07 08:22:50','Anak',2147483647,'Siti Munawaroh',2147483647,'Saekodin','',NULL,'','',NULL,NULL,NULL,'Jln. Pondok Ds. Gurah','01','01','Gurah 2',NULL,NULL,'a457fb47efe6bae05564ff007d17200d.jpg','d9e2af16742820c2099d9ea33061394c.jpg',NULL),
('biodata0006','7','Wrg08','144/0006/418.69.10/2021','2021-07-07 08:55:53','Anak',2147483647,'Siti Munawaroh',2147483647,'Saekodin','',NULL,'','',NULL,NULL,NULL,'Jalan Jagalan, RT. 03 RW. 03','03','03','Gurah 2','Data tidak Lengkap',NULL,'4691b29d0171adb836979c97b9ecfc01.jpg','2e4963688fd2cf65c7a78091b478be37.jpg',NULL);

/*Table structure for table `surat_domisili` */

DROP TABLE IF EXISTS `surat_domisili`;

CREATE TABLE `surat_domisili` (
  `id_surat` varchar(30) NOT NULL,
  `id_user` varchar(20) DEFAULT NULL,
  `id_status` varchar(10) DEFAULT NULL,
  `no_surat` varchar(30) DEFAULT NULL,
  `tgl_surat` datetime DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `ket_tolak` varchar(255) DEFAULT NULL,
  `penolak` varchar(30) DEFAULT NULL,
  `scan_kk` varchar(100) DEFAULT NULL,
  `scan_ektp` varchar(100) DEFAULT NULL,
  `scan_pajak` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_surat`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `surat_domisili` */

insert  into `surat_domisili`(`id_surat`,`id_user`,`id_status`,`no_surat`,`tgl_surat`,`keterangan`,`ket_tolak`,`penolak`,`scan_kk`,`scan_ektp`,`scan_pajak`) values 
('domisili0001','Wrg01','1','145/0001/418.69.10/2021','2021-06-24 07:16:05','Untuk Keperluan Kerja',NULL,NULL,'8431db3ba6edf74702e8bf5db373e4c2.jpg','ebdfb596794b9c90eaa61de071a630ca.jpg',NULL),
('domisili0002','Wrg07','6','145/0002/418.69.10/2021','2021-06-24 07:18:10','Untuk Keperluan Kerja','',NULL,'8bb89726e38c8346e118e98611bce607.jpg','8f80c2a003855e7c233a454a543edbc7.jpg',NULL),
('domisili0003','Wrg04','1','145/0003/418.69.10/2021','2021-07-06 08:02:03','Pekerjaan',NULL,NULL,'65d5bb823efec225400b8cd01e6908b3.jpg','d0d61f14c178b055ee28ecbe1c32f43d.jpg',NULL),
('domisili0004','Wrg05','1','145/0004/418.69.10/2021','2021-07-07 08:07:02','Pekerjaan',NULL,NULL,'89c4ae7b6b3a911a3f304b1d1543c71d.jpg','46aa794df6b7e717c7c6bc6bd8b147fd.jpg',NULL),
('domisili0005','Wrg09','1','145/0005/418.69.10/2021','2021-07-07 12:06:03','',NULL,NULL,'37f775687d29eae8dfbca8f8ada572c7.jpg','50c19e23981587600dd8f6ea99fb12b8.jpg',NULL),
('domisili0006','Wrg10','1','145/0006/418.69.10/2021','2021-07-07 12:09:07','Pengantar SKCK',NULL,NULL,'57b2d87d158e388ab43dd5c3afb8eb4b.jpg','31dea90e597fbf89ba7cdeb062c781d5.jpg',NULL);

/*Table structure for table `surat_ijin_keramaian` */

DROP TABLE IF EXISTS `surat_ijin_keramaian`;

CREATE TABLE `surat_ijin_keramaian` (
  `id_surat` varchar(30) NOT NULL,
  `id_user` varchar(30) DEFAULT NULL,
  `id_status` varchar(10) DEFAULT NULL,
  `no_surat` varchar(30) DEFAULT NULL,
  `tgl_surat` datetime DEFAULT NULL,
  `jenis_kegiatan` varchar(200) DEFAULT NULL,
  `penanggungjawab` varchar(50) DEFAULT NULL,
  `ket_tolak` varchar(255) DEFAULT NULL,
  `penolak` varchar(30) DEFAULT NULL,
  `scan_kk` varchar(100) DEFAULT NULL,
  `scan_ektp` varchar(100) DEFAULT NULL,
  `scan_pajak` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_surat`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `surat_ijin_keramaian` */

insert  into `surat_ijin_keramaian`(`id_surat`,`id_user`,`id_status`,`no_surat`,`tgl_surat`,`jenis_kegiatan`,`penanggungjawab`,`ket_tolak`,`penolak`,`scan_kk`,`scan_ektp`,`scan_pajak`) values 
('keramaian0001','Wrg06','7','300/0001/418.69.10/2021','2021-06-24 07:07:07','17 Agustus','Julianto','data tidak lengkap',NULL,'04afd390b48075421c306ec152775c76.jpg','80000b076787d5a1076f192a44ea6939.jpg',NULL),
('keramaian0002','Wrg04','1','300/0002/418.69.10/2021','2021-06-24 07:08:09','Pernikahan','Anang',NULL,NULL,'f16e469fb5040a860ea3bc3347945b8e.jpg','f6ffc0d07ed49aba470ed9c611eebb63.jpg',NULL),
('keramaian0003','Wrg03','1','300/0003/418.69.10/2021','2021-07-06 07:19:11','17 Agustus','Anang',NULL,NULL,'d7a8fdcab3cdcd6a7b1b911e72f02e1a.jpg','0eea299ea250f2924ddf3792f0220b40.jpg',NULL),
('keramaian0004','Wrg05','1','300/0004/418.69.10/2021','2021-07-07 08:05:01','Pernikahan','Suryanto',NULL,NULL,'7c403b55f3689cfbac64b57473aa45db.jpg','508a62369d38bf82fbcb37c647c04f0a.jpg',NULL),
('keramaian0005','Wrg09','1','300/0005/418.69.10/2021','2021-07-07 08:12:26','Hajatan','Joko',NULL,NULL,'4178ba0ff60c85dcf82d62b2c474116f.jpg','0e0d45cb04a55423aaa0aad17f954e9e.jpg',NULL),
('keramaian0006','Wrg10','1','300/0006/418.69.10/2021','2021-07-07 08:59:49','Konser Amal','Kavita Salsa',NULL,NULL,'7ff9feaec33960746cc740ffc6ac8911.jpg','008961c002b127cfd2ecca3f40afbeec.jpg',NULL);

/*Table structure for table `surat_kelahiran` */

DROP TABLE IF EXISTS `surat_kelahiran`;

CREATE TABLE `surat_kelahiran` (
  `id_surat` varchar(30) NOT NULL,
  `id_user` varchar(20) DEFAULT NULL,
  `id_status` varchar(10) DEFAULT NULL,
  `no_surat` varchar(30) DEFAULT NULL,
  `tgl_surat` datetime DEFAULT NULL,
  `kepala_keluarga` varchar(50) DEFAULT NULL,
  `no_kk_keluarga` varchar(30) DEFAULT NULL,
  `nama_bayi` varchar(255) NOT NULL,
  `jenkel_bayi` varchar(20) DEFAULT NULL,
  `tempat_dilahirkan` varchar(50) DEFAULT NULL,
  `tempat_kelahiran` varchar(50) DEFAULT NULL,
  `hari` varchar(20) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `pukul` time DEFAULT NULL,
  `jenis_kelahiran` varchar(50) DEFAULT NULL,
  `kelahiran_ke` int(10) DEFAULT NULL,
  `penolong_kelahiran` varchar(50) DEFAULT NULL,
  `berat_bayi` varchar(10) DEFAULT NULL,
  `panjang_bayi` varchar(10) DEFAULT NULL,
  `nik_ibu` varchar(30) DEFAULT NULL,
  `nama_ibu` varchar(255) DEFAULT NULL,
  `tgl_lahir_ibu` date DEFAULT NULL,
  `umur_ibu` int(10) DEFAULT NULL,
  `pekerjaan_ibu` varchar(50) DEFAULT NULL,
  `alamat_ibu` varchar(100) DEFAULT NULL,
  `kewarganegaraan_ibu` varchar(30) DEFAULT NULL,
  `kebangsaan_ibu` varchar(30) DEFAULT NULL,
  `tgl_kawin` date DEFAULT NULL,
  `nik_ayah` varchar(30) DEFAULT NULL,
  `nama_ayah` varchar(255) DEFAULT NULL,
  `tgl_lahir_ayah` date DEFAULT NULL,
  `umur_ayah` int(10) DEFAULT NULL,
  `pekerjaan_ayah` varchar(50) DEFAULT NULL,
  `alamat_ayah` varchar(100) DEFAULT NULL,
  `kewarganegaraan_ayah` varchar(30) DEFAULT NULL,
  `kebangsaan_ayah` varchar(30) DEFAULT NULL,
  `nik_saksi_a` varchar(30) DEFAULT NULL,
  `nama_saksi_a` varchar(255) DEFAULT NULL,
  `umur_saksi_a` int(10) DEFAULT NULL,
  `pekerjaan_saksi_a` varchar(50) DEFAULT NULL,
  `alamat_saksi_a` varchar(100) DEFAULT NULL,
  `nik_saksi_b` varchar(30) DEFAULT NULL,
  `nama_saksi_b` varchar(255) DEFAULT NULL,
  `umur_saksi_b` int(10) DEFAULT NULL,
  `pekerjaan_saksi_b` varchar(50) DEFAULT NULL,
  `alamat_saksi_b` varchar(100) DEFAULT NULL,
  `ket_tolak` varchar(255) DEFAULT NULL,
  `penolak` varchar(30) DEFAULT NULL,
  `scan_kk` varchar(100) DEFAULT NULL,
  `scan_ktp_ayah` varchar(100) DEFAULT NULL,
  `scan_ktp_ibu` varchar(100) DEFAULT NULL,
  `scan_ktp_saksi_a` varchar(100) DEFAULT NULL,
  `scan_ktp_saksi_b` varchar(100) DEFAULT NULL,
  `scan_buku_nikah` varchar(100) DEFAULT NULL,
  `surat_keterangan_rs` varchar(100) DEFAULT NULL,
  `scan_pajak` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_surat`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `surat_kelahiran` */

insert  into `surat_kelahiran`(`id_surat`,`id_user`,`id_status`,`no_surat`,`tgl_surat`,`kepala_keluarga`,`no_kk_keluarga`,`nama_bayi`,`jenkel_bayi`,`tempat_dilahirkan`,`tempat_kelahiran`,`hari`,`tanggal`,`pukul`,`jenis_kelahiran`,`kelahiran_ke`,`penolong_kelahiran`,`berat_bayi`,`panjang_bayi`,`nik_ibu`,`nama_ibu`,`tgl_lahir_ibu`,`umur_ibu`,`pekerjaan_ibu`,`alamat_ibu`,`kewarganegaraan_ibu`,`kebangsaan_ibu`,`tgl_kawin`,`nik_ayah`,`nama_ayah`,`tgl_lahir_ayah`,`umur_ayah`,`pekerjaan_ayah`,`alamat_ayah`,`kewarganegaraan_ayah`,`kebangsaan_ayah`,`nik_saksi_a`,`nama_saksi_a`,`umur_saksi_a`,`pekerjaan_saksi_a`,`alamat_saksi_a`,`nik_saksi_b`,`nama_saksi_b`,`umur_saksi_b`,`pekerjaan_saksi_b`,`alamat_saksi_b`,`ket_tolak`,`penolak`,`scan_kk`,`scan_ktp_ayah`,`scan_ktp_ibu`,`scan_ktp_saksi_a`,`scan_ktp_saksi_b`,`scan_buku_nikah`,`surat_keterangan_rs`,`scan_pajak`) values 
('kelahiran0001','Wrg07','4','472/0001/418.69.10/2021','2021-06-24 09:56:58','Yanto','3009749875682345','Hafis','Laki-Laki','RS/RB','Kediri','Senin','2021-06-07','00:00:09','Tunggal',1,'Dokter','4.0','38','3009749875682555','Tatik','1996-09-07',25,'Wiraswasta','Jalan Kampung Baru','WNI','Indonesia','2019-03-12','3009749875682444','Joko','1996-08-11',24,'Wiraswasta','Jalan Jagalan','WNI','Indonesia','3008479798782222','Kukuh',23,'Wiraswasta','Jalan Kampung Baru','3008479798781111','Sasa',20,'Wiraswasta','Jalan Kampung Baru','',NULL,'22eb9eb85b4113b759d3024435e9de09.jpg','0b1513c16fdbd75567febc7c850cb614.jpg','f557db8b9bb7d2ffad802504f5a4e6d4.jpg','ec28f77dfef274991a43a7757a578fbb.jpg','ae85e8d777f3dee050b816791de8e0d1.jpg','965a162c5107f7e31fb9a88c18e764e3.jpg','b89d2aaedcafc6ebfeb503b4445dcbf1.jpg',''),
('kelahiran0002','Wrg04','1','472/0002/418.69.10/2021','2021-07-06 10:10:58','Suryanto','3506102404111659','Fahruq','Laki-Laki','RS/RB','Kediri','Senin','2009-08-24','00:00:10','Tunggal',1,'Dokter','3.5','38','3007879473338790','Misti','1989-09-09',24,'Wiraswasta','jagalan','WNI','Indonesia','1999-09-24','3506101005680005','Suryanto','1988-09-23',35,'Wiraswasta','Jagalan','WNI','Indonesia','3008479798783214','Warjiyo',48,'Wiraswasta','Pondok','3008479798783334','Gaga',40,'Wiraswasta','Pondok',NULL,NULL,'2efeb7ef4ac32bbee80086f9095d8637.jpg','4cd0dad7341612ff811e59af53027bbb.jpg','7cba5a252c725938234f278cee3ae5bc.jpg','29ab3ca7c4167114c799ab8f4b092cb3.jpg','fd00a760d3b3077e8c398857e828f9ad.jpg','a1e340127299eda6f4e20d2971c4d214.jpg','2f8bfe7949e99cd08a44b697fd9d768d.jpg',''),
('kelahiran0003','Wrg05','1','472/0003/418.69.10/2021','2021-07-07 10:13:07','Suryanto','3506102404111659','Arka ','Laki-Laki','RS/RB','Kediri','Senin','2021-07-07','00:00:10','Tunggal',1,'Dokter','3.5','38','3009749875682555','Munawaroh','1990-07-07',30,'Wiraswasta','Jalan Jagalan, RT.03 RW.03 Desa Gurah Kec. Gurah, Kab. Kediri','WNI','Indonesia','2020-07-15','3506101005680005','Suryanto','1990-07-07',31,'Wiraswasta','Jalan Jagalan, RT.03 RW.03 Desa Gurah Kec. Gurah, Kab. Kediri','WNI','Indonesia','3008479798783214','Warjiyo',48,'Wiraswasta','Jalan Jagalan, RT.03 RW.03 Desa Gurah Kec. Gurah, Kab. Kediri','300647979878355','Nita Sari',40,'Wiraswasta','Jalan Jagalan, RT.03 RW.03 Desa Gurah Kec. Gurah, Kab. Kediri',NULL,NULL,'2cb79e7593c8fb5f4dd343d7f9b6cbbe.jpg','599e09db55a0eedbed4e316973a4710a.jpg','3b05a0d2195e56caecdf219035373716.jpg','e2a1e1d79ac2274cd63faccef4294e5f.jpg','00174d28dd6f9be1bd14ee2e3ea491c0.jpg','efc7b035b1d9b4677d77a41a2b1ad804.jpg','2ed2c94986881ac54e3e10b83c7ab74f.jpg',NULL),
('kelahiran0004','Wrg09','1','472/0004/418.69.10/2021','2021-07-07 11:07:02','Kofif','3009749875689999','Galang','Laki-Laki','RS/RB','Kediri','Senin','2009-07-07','00:00:09','Tunggal',1,'Dokter','3.5','38','3007879473338790','Ngasinah','1978-08-09',45,'Wiraswasta','Jalan Pondok','WNI','Indonesia','2008-04-24','3009749875682555','Galih','1976-03-07',46,'Wiraswasta','Jalan Jagalan','WNI','Indonesia','3008479798782222','Sasa',23,'Wiraswasta','Jalan Pondok','3008479798787898','Koko',21,'Mahasiswa','Jalan Pondok',NULL,NULL,'867e034a70d9bff6cc138c780c17de94.jpg','35d2fd3b873c288ed4db8291e0004590.jpg','2803a0d78fe69acc4dcfb5d372f5b82c.jpg','81d162260d92fe0c82d037d82a6b7415.jpg','7337d8c0d1eb307a2bab3007c69fa7c3.jpg','6ac1923678248f396203330b3085a199.jpg','510a472732bef5522742b66937791373.jpg',NULL);

/*Table structure for table `surat_kematian` */

DROP TABLE IF EXISTS `surat_kematian`;

CREATE TABLE `surat_kematian` (
  `id_surat` varchar(30) NOT NULL,
  `id_user` varchar(20) DEFAULT NULL,
  `id_status` varchar(10) DEFAULT NULL,
  `no_surat` varchar(30) DEFAULT NULL,
  `tgl_surat` datetime DEFAULT NULL,
  `nama_kepala_keluarga` varchar(100) DEFAULT NULL,
  `no_kk_kepala_keluarga` int(30) DEFAULT NULL,
  `nik_jenazah` varchar(30) DEFAULT NULL,
  `nama_jenazah` varchar(100) DEFAULT NULL,
  `jenkel_jenazah` varchar(30) DEFAULT NULL,
  `tempat_lahir_jenazah` varchar(50) DEFAULT NULL,
  `tgl_lahir_jenazah` date DEFAULT NULL,
  `umur_jenazah` varchar(20) DEFAULT NULL,
  `agama_jenazah` varchar(20) DEFAULT NULL,
  `pekerjaan_jenazah` varchar(50) DEFAULT NULL,
  `alamat_jenazah` varchar(255) DEFAULT NULL,
  `kewarganegaraan_jenazah` varchar(20) DEFAULT NULL,
  `anak_ke` varchar(30) DEFAULT NULL,
  `tgl_kematian` date DEFAULT NULL,
  `pukul` time DEFAULT NULL,
  `sebab_kematian` varchar(100) DEFAULT NULL,
  `tempat_kematian` varchar(100) DEFAULT NULL,
  `yang_menerangkan` varchar(100) DEFAULT NULL,
  `nik_ibu` varchar(30) DEFAULT NULL,
  `nama_ibu` varchar(100) DEFAULT NULL,
  `tgl_lahir_ibu` date DEFAULT NULL,
  `umur_ibu` varchar(20) DEFAULT NULL,
  `pekerjaan_ibu` varchar(50) DEFAULT NULL,
  `alamat_ibu` varchar(255) DEFAULT NULL,
  `nik_ayah` varchar(30) DEFAULT NULL,
  `nama_ayah` varchar(100) DEFAULT NULL,
  `tgl_lahir_ayah` date DEFAULT NULL,
  `umur_ayah` varchar(20) DEFAULT NULL,
  `pekerjaan_ayah` varchar(50) DEFAULT NULL,
  `alamat_ayah` varchar(255) DEFAULT NULL,
  `nik_saksi_a` int(30) DEFAULT NULL,
  `nama_saksi_a` varchar(100) DEFAULT NULL,
  `tgl_lahir_saksi_a` date DEFAULT NULL,
  `umur_saksi_a` varchar(20) DEFAULT NULL,
  `pekerjaan_saksi_a` varchar(50) DEFAULT NULL,
  `alamat_saksi_a` varchar(255) DEFAULT NULL,
  `nik_saksi_b` varchar(30) DEFAULT NULL,
  `nama_saksi_b` varchar(100) DEFAULT NULL,
  `tgl_lahir_saksi_b` date DEFAULT NULL,
  `umur_saksi_b` varchar(20) DEFAULT NULL,
  `pekerjaan_saksi_b` varchar(100) DEFAULT NULL,
  `alamat_saksi_b` varchar(255) DEFAULT NULL,
  `ket_tolak` varchar(255) DEFAULT NULL,
  `penolak` varchar(30) DEFAULT NULL,
  `scan_kk` varchar(100) DEFAULT NULL,
  `scan_ektp_meninggal` varchar(100) DEFAULT NULL,
  `scan_ektp_saksi_a` varchar(100) DEFAULT NULL,
  `scan_ektp_saksi_b` varchar(100) DEFAULT NULL,
  `suket_meninggal` varchar(100) DEFAULT NULL,
  `scan_pajak` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_surat`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `surat_kematian` */

insert  into `surat_kematian`(`id_surat`,`id_user`,`id_status`,`no_surat`,`tgl_surat`,`nama_kepala_keluarga`,`no_kk_kepala_keluarga`,`nik_jenazah`,`nama_jenazah`,`jenkel_jenazah`,`tempat_lahir_jenazah`,`tgl_lahir_jenazah`,`umur_jenazah`,`agama_jenazah`,`pekerjaan_jenazah`,`alamat_jenazah`,`kewarganegaraan_jenazah`,`anak_ke`,`tgl_kematian`,`pukul`,`sebab_kematian`,`tempat_kematian`,`yang_menerangkan`,`nik_ibu`,`nama_ibu`,`tgl_lahir_ibu`,`umur_ibu`,`pekerjaan_ibu`,`alamat_ibu`,`nik_ayah`,`nama_ayah`,`tgl_lahir_ayah`,`umur_ayah`,`pekerjaan_ayah`,`alamat_ayah`,`nik_saksi_a`,`nama_saksi_a`,`tgl_lahir_saksi_a`,`umur_saksi_a`,`pekerjaan_saksi_a`,`alamat_saksi_a`,`nik_saksi_b`,`nama_saksi_b`,`tgl_lahir_saksi_b`,`umur_saksi_b`,`pekerjaan_saksi_b`,`alamat_saksi_b`,`ket_tolak`,`penolak`,`scan_kk`,`scan_ektp_meninggal`,`scan_ektp_saksi_a`,`scan_ektp_saksi_b`,`suket_meninggal`,`scan_pajak`) values 
('kematian0001','Wrg09','1','470/0001/418.69.10/2021','2020-06-25 08:09:08','Sunaryo',2147483647,'3506101009980998','Siti','Laki - Laki','Kediri','1965-03-22','60','Islam','Wiraswasta','Jalan Jagalan','Indonesia','1','2020-08-22','00:00:02','Sakit',NULL,'Anak','-','Munawaroh','1965-01-01',NULL,NULL,'-','-','Samsul','1965-02-02','-','-','-',2147483647,'Salsa','1999-08-13','21','pelajar','Jalan Jagalan','300847979878355','Sulis','2008-02-02','18','Pelajar','Jalan Jagalan',NULL,NULL,'ae4a1b7acaa1df59495a646ee7c8f745.jpg','081935a4ff7c69b0cc4424a6dc02d665.jpg','5747a98d81a7701b9d384f8ecc0a2e1a.jpg','207471e582e90eb306dee1b49adeee86.jpg','0d8aae566a9af708b74a3b25e5f442d6.jpg',NULL),
('kematian0002','Wrg04','1','470/0002/418.69.10/2021','2021-07-06 09:05:06','Sunaryo',2147483647,'3506101009980998','Sulis','Perempuan','Kediri','1987-09-08','34','Islam','Wiraswasta','Jalan Pondok','Indonesia','1','2021-09-23','00:00:10','Sakit',NULL,'Anak','-','Ngasinah','0000-00-00',NULL,NULL,'-','-','Marjan','0000-00-00','-','-','-',2147483647,'Siti','1987-05-23','48','Wiraswasta','Pondok','300647979878355','Joko','1983-05-12','40','Wiraswasta','Pondok',NULL,NULL,'9313bd9c6ec02e48b7796a94980cb103.jpg','577d3edbd8f1ad4fb831288f93996e7c.jpg','c5ce1623e4171cc3c98c0f27d08de6d9.jpg','e25bbad3d29e2f75757b9c0be3a81c8b.jpg','2a291ffe97cb0eaa7fe4346e831bca73.jpg',NULL),
('kematian0003','Wrg06','1','470/0003/418.69.10/2021','2021-07-06 11:04:06','Sunaryo',2147483647,'3506101009980998','Sulis','Perempuan','Kediri','1987-09-08','34','Islam','Wiraswasta','Jalan Pondok','Indonesia','1','2021-09-23','00:00:10','Sakit',NULL,'Anak','-','Ngasinah','0000-00-00',NULL,NULL,'-','-','Marjan','0000-00-00','-','-','-',2147483647,'Siti','1987-05-23','48','Wiraswasta','Pondok','300647979878355','Joko','1983-05-12','40','Wiraswasta','Pondok',NULL,NULL,'8d44ba972e361a86d59270ec484a31cb.jpg','bb930b097bc25cc1bb0d8b383fe86346.jpg','457db669b49ae50525840bf610077c12.jpg','c38c5d0cda096ad57ddc80bf1a553388.jpg','4cc2f4737fe62619c00807c1b8e28d9f.jpg',NULL);

/*Table structure for table `surat_kip` */

DROP TABLE IF EXISTS `surat_kip`;

CREATE TABLE `surat_kip` (
  `id_surat` varchar(30) NOT NULL,
  `id_user` varchar(20) DEFAULT NULL,
  `id_status` varchar(10) DEFAULT NULL,
  `no_surat` varchar(30) DEFAULT NULL,
  `tgl_surat` datetime DEFAULT NULL,
  `nama_anak` varchar(50) DEFAULT NULL,
  `jenkel` varchar(10) DEFAULT NULL,
  `tempat_lahir_anak` varchar(20) DEFAULT NULL,
  `tgl_lahir_anak` date DEFAULT NULL,
  `nik_anak` int(20) DEFAULT NULL,
  `sekolah` varchar(50) DEFAULT NULL,
  `semester` varchar(10) DEFAULT NULL,
  `nim_nisn` int(30) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `keperluan` varchar(255) DEFAULT NULL,
  `ket_tolak` varchar(255) DEFAULT NULL,
  `penolak` varchar(30) DEFAULT NULL,
  `scan_kk` varchar(100) DEFAULT NULL,
  `scan_ektp` varchar(100) DEFAULT NULL,
  `scan_sktm` varchar(100) DEFAULT NULL,
  `scan_pajak` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_surat`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `surat_kip` */

insert  into `surat_kip`(`id_surat`,`id_user`,`id_status`,`no_surat`,`tgl_surat`,`nama_anak`,`jenkel`,`tempat_lahir_anak`,`tgl_lahir_anak`,`nik_anak`,`sekolah`,`semester`,`nim_nisn`,`keterangan`,`keperluan`,`ket_tolak`,`penolak`,`scan_kk`,`scan_ektp`,`scan_sktm`,`scan_pajak`) values 
('kip0001','Wrg02','1','402/0003/418.69.10/2021','2021-06-25 08:02:01','Virginia Abrinsa','Perempuan','Kediri','1999-12-02',2147483647,'PSDKU Polinema Kediri','6',1931733109,'-','Mencari Beasiswa',NULL,NULL,'8035a9e53c07e49478fb9c460b1efde0.jpg','4a931c57963da8f488dcaa38a5b5fed0.jpg',NULL,NULL),
('kip0002','Wrg07','6','402/0001/418.69.10/2021','2021-06-24 09:04:14','Siti','Perempuan','Kediri','2008-08-23',2147483647,'SMP','-',8768979,'Pengajuan KIP','Untuk Syarat Pengajuan KIP','',NULL,'aeeeb789a617a0d19726707c115e7987.jpg','1e3b704a27c2661bfe9add9e49d12f6c.jpg',NULL,NULL),
('kip0003','Wrg07','6','402/0002/418.69.10/2021','2021-06-25 11:10:58','Virginia Abrinsa','Perempuan','Kediri','1999-12-02',2147483647,'PSDKU Polinema Kediri','6',1931733109,'-','Mencari Beasiswa','',NULL,'b2702fbdb8a54c4097467bc7e2ce4057.jpg','26b8f04e2b05dbb6119727525166ac01.jpg',NULL,NULL);

/*Table structure for table `surat_kuasa` */

DROP TABLE IF EXISTS `surat_kuasa`;

CREATE TABLE `surat_kuasa` (
  `id_surat` varchar(30) NOT NULL,
  `id_user` varchar(20) DEFAULT NULL,
  `id_status` varchar(10) DEFAULT NULL,
  `no_surat` varchar(30) DEFAULT NULL,
  `tgl_surat` datetime DEFAULT NULL,
  `nama_pihak_kedua` varchar(100) DEFAULT NULL,
  `nik_pihak_kedua` int(30) DEFAULT NULL,
  `tempat_lahir_pihak_kedua` varchar(50) DEFAULT NULL,
  `tgl_lahir_pihak_kedua` date DEFAULT NULL,
  `jenkel_pihak_kedua` varchar(30) DEFAULT NULL,
  `status_kawin_pihak_kedua` varchar(30) DEFAULT NULL,
  `pekerjaan_pihak_kedua` varchar(50) DEFAULT NULL,
  `alamat_pihak_kedua` varchar(255) DEFAULT NULL,
  `ket_tolak` varchar(255) DEFAULT NULL,
  `penolak` varchar(30) DEFAULT NULL,
  `scan_kk` varchar(100) NOT NULL,
  `scan_kk_pihak_kedua` varchar(100) DEFAULT NULL,
  `scan_ektp_pihak_kedua` varchar(100) DEFAULT NULL,
  `kuasa` varchar(255) DEFAULT NULL,
  `scan_ektp` varchar(100) NOT NULL,
  `scan_pajak` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_surat`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `surat_kuasa` */

insert  into `surat_kuasa`(`id_surat`,`id_user`,`id_status`,`no_surat`,`tgl_surat`,`nama_pihak_kedua`,`nik_pihak_kedua`,`tempat_lahir_pihak_kedua`,`tgl_lahir_pihak_kedua`,`jenkel_pihak_kedua`,`status_kawin_pihak_kedua`,`pekerjaan_pihak_kedua`,`alamat_pihak_kedua`,`ket_tolak`,`penolak`,`scan_kk`,`scan_kk_pihak_kedua`,`scan_ektp_pihak_kedua`,`kuasa`,`scan_ektp`,`scan_pajak`) values 
('Kuasa0001','Wrg02','1','148/0003/418.69.10/2021','2021-06-25 07:00:00','Virginia Abrinsa',2147483647,'Kediri','1999-12-02','Perempuan','Belum Kawin','Mahasiswa','Desa Gurah Kec Gurah Kab Kediri',NULL,NULL,'8409e839a60bdd91cfb18855c255b9a7.jpg',NULL,NULL,'mengambil uang blsm di ngadiluwih','803c244f944c75bce9ae4ba5ae2bf0a0.jpg',NULL),
('Kuasa0002','Wrg04','1','148/0002/418.69.10/2021','2021-06-24 07:30:00','Suryanto',2147483647,'Kediri','1969-07-24','Laki-Laki','Kawin','Wiraswasta','Jalan Jagalan',NULL,NULL,'de9e10a0456e02b441981f4d21ab14d5.jpg',NULL,NULL,NULL,'2c78b81e4479356332dc6ffb22fe706d.jpg',NULL),
('Kuasa0004','Wrg09','1','148/0001/418.69.10/2021','2021-06-24 08:00:00','Tutik Yuli',2147483647,'Kediri','1974-07-16','Perempuan','Kawin','Wiraswasta','Jalan Jagalan',NULL,NULL,'4196674de65470431beac32099710ee7.jpg',NULL,NULL,NULL,'e43e1bbbd7eabd1c4a0e067530affdeb.jpg',NULL),
('Kuasa0005','Wrg01','1','148/0004/418.69.10/2021','2021-07-07 08:20:00','Yahya Dwi Purnama',2147483647,'Kediri','2021-07-07','Laki-Laki','Belum Kawin','Mahasiswa','Kediri, Jawa Timur',NULL,NULL,'439c04aaeafcbd9dd37dd2164427f287.jpg','322e87af63ecd6bdcc9be5435d6d7478.jpg','2e90f39a6fd7599c7568aab566c592a9.jpg','mengambil uang blsm di ngadiluwih','be90a1281d702d541f07c3caeae41230.jpg','33633111b29b17721f109b09f9fe3035.jpg'),
('Kuasa0006','Wrg10','1','148/0005/418.69.10/2021','2021-07-08 08:30:00','Virginia Abrinsa',2147483647,'Kediri','1999-02-09','Perempuan','Belum Kawin','Mahasiswa','Kediri','',NULL,'1c586090e775451100561c11ba0891f6.jpg','64116294400caedc70eba191069abfac.jpg','352639a46247fb81da04133109b3cbec.jpg','Mengambil Bantuan Ke Balai Desa','a863730e8c07f802314dcc547e8a5bcf.jpg','6a32fb0e36b1a3516bb7db372a0359c7.jpg');

/*Table structure for table `surat_permohonan_ktp` */

DROP TABLE IF EXISTS `surat_permohonan_ktp`;

CREATE TABLE `surat_permohonan_ktp` (
  `id_surat` varchar(30) NOT NULL,
  `id_user` varchar(30) DEFAULT NULL,
  `id_status` varchar(10) DEFAULT NULL,
  `no_surat` varchar(30) DEFAULT NULL,
  `tgl_surat` datetime DEFAULT NULL,
  `tipe_permohonan` varchar(50) DEFAULT NULL,
  `ket_tolak` varchar(255) DEFAULT NULL,
  `penolak` varchar(30) DEFAULT NULL,
  `scan_kk` varchar(100) DEFAULT NULL,
  `ektp_lama` varchar(100) DEFAULT NULL,
  `ijazah_akhir` varchar(100) DEFAULT NULL,
  `akta` varchar(100) DEFAULT NULL,
  `surat_nikah_cerai` varchar(100) DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `scan_pajak` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_surat`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `surat_permohonan_ktp` */

insert  into `surat_permohonan_ktp`(`id_surat`,`id_user`,`id_status`,`no_surat`,`tgl_surat`,`tipe_permohonan`,`ket_tolak`,`penolak`,`scan_kk`,`ektp_lama`,`ijazah_akhir`,`akta`,`surat_nikah_cerai`,`foto`,`scan_pajak`) values 
('ktp0001','Wrg03','1','404/0001/418.69.10/2021','2021-06-25 07:00:00','Baru',NULL,NULL,'02dbeeef042ece0629a8dde7be430ce1.jpg','cfb97ebe8c724e93e9f9841a5af6a50b.jpg','c324f35e8f27ceaca78798aa3947b5c1.jpg','f3677f1505876b21183f715f660d8d28.jpg','51a18f91d3a4b469f8588558468a440f.jpg','b672cf974e7d3f7dc259df8559f602ba.jpg',NULL),
('ktp0002','Wrg04 ','1','404/0002/418.69.10/2021','2021-07-06 07:30:00','Baru',NULL,NULL,'cc417c953a2c7ffe2818343e5740c894.jpg','e0127f708cbd4d7ba688c45078351a47.jpg','2d7b5f79f73f3fa74e79b696b270db20.jpg','7a00e00bc3bf8ced62793c35eb15f9bc.jpg','dab93e5f71ab73f9540abfcb0fd94149.jpg','fd182cdd65fcf92150144bf5dcab670c.jpg',NULL),
('ktp0003','Wrg05 ','1','404/0003/418.69.10/2021','2021-07-07 08:00:00','Perpanjangan',NULL,NULL,'6331c374529472f4461129ffcaeb1ae1.jpg','459c80def2c2bfe4c9c1abca383380eb.jpg','34c5347b642d4553b9dee7ceadba89d2.jpg','f2c159630434e4c5f372980add09b9b5.jpg','6dff0f48f4e6d71d544fcb5a8ec5b767.jpg','9a7b8ae9142c21e5043b8f37cb3d1986.jpg',NULL),
('ktp0004','Wrg09 ','1','404/0004/418.69.10/2021','2021-07-07 08:20:00','Baru',NULL,NULL,'6bd7f14d847c7bc428d8170ea7e82405.jpg','35ec728117e17d8e4a9d4644dc5a47da.jpg','834782a8f625ec74dd428d61437064ac.jpg','009bece1c5762291d89f31aade04c158.jpg','426dbb0f5000863fcd36133ced8eff2a.jpg','7b4d53e0df77dfe54d3763d91cdeaeeb.jpg',NULL);

/*Table structure for table `surat_perubahan_kk` */

DROP TABLE IF EXISTS `surat_perubahan_kk`;

CREATE TABLE `surat_perubahan_kk` (
  `id_surat` varchar(30) NOT NULL,
  `id_user` varchar(20) DEFAULT NULL,
  `id_status` varchar(10) DEFAULT NULL,
  `no_surat` varchar(30) DEFAULT NULL,
  `tgl_surat` datetime DEFAULT NULL,
  `kepala_keluarga` varchar(100) DEFAULT NULL,
  `alasan` varchar(255) DEFAULT NULL,
  `jumlah_anggota` int(10) DEFAULT NULL,
  `nik_anggota_a` int(30) DEFAULT NULL,
  `nik_anggota_b` int(30) DEFAULT NULL,
  `nama_anggota_a` varchar(100) DEFAULT NULL,
  `nama_anggota_b` varchar(100) DEFAULT NULL,
  `ket_tolak` varchar(255) DEFAULT NULL,
  `penolak` varchar(30) DEFAULT NULL,
  `scan_kk` varchar(100) DEFAULT NULL,
  `scan_ektp` varchar(100) DEFAULT NULL,
  `ijazah_terakhir` varchar(100) DEFAULT NULL,
  `surat_nikah_cerai` varchar(100) DEFAULT NULL,
  `scan_pajak` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_surat`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `surat_perubahan_kk` */

insert  into `surat_perubahan_kk`(`id_surat`,`id_user`,`id_status`,`no_surat`,`tgl_surat`,`kepala_keluarga`,`alasan`,`jumlah_anggota`,`nik_anggota_a`,`nik_anggota_b`,`nama_anggota_a`,`nama_anggota_b`,`ket_tolak`,`penolak`,`scan_kk`,`scan_ektp`,`ijazah_terakhir`,`surat_nikah_cerai`,`scan_pajak`) values 
('kk0001','Wrg07','7','403/0001/418.69.10/2021','2021-06-25 08:00:00','Suryanto','Ganti KK',2,2147483647,2147483647,'Salsa Edi','Edi','Data Tidak Lengkap',NULL,'ac88f56621163080ceedbff3aed9f1e0.jpg','b3fce4c2efb098593d6e018cff1ea0f9.pdf','ac88f56621163080ceedbff3aed9f1e0.jpg','34d6f5e1f3656e5e83e3f5c5f73ec184.jpg',NULL),
('kk0002','Wrg04','1','403/0002/418.69.10/2021','2021-07-06 08:10:00','Suryanto','Ganti KK',2,2147483647,2147483647,'Salsa Edi','Edi',NULL,NULL,'87d900106578712f5e161632884fb9c6.jpg','73022678fa22ee13793860c0b852e022.pdf','87d900106578712f5e161632884fb9c6.jpg','725b88e7333f2be4feedf0b677598d5d.jpg',NULL),
('kk0003','Wrg05','1','403/0003/418.69.10/2021','2021-07-07 08:30:00','Suryanto','Ganti KK',2,2147483647,2147483647,'Alifia','Alisia',NULL,NULL,'0367be597e393d1fa74c95ff45abfb9f.jpg','eb80217e1ebfe5c3db60da14fefc69e9.jpg','0367be597e393d1fa74c95ff45abfb9f.jpg','24915787ce90ab8db5e3bf1e5939e473.jpg',NULL),
('kk0004','Wrg08','7','403/0004/418.69.10/2021','2021-07-07 09:00:00','Mahfud Saputra','Ganti KK',1,2147483647,0,'Kiki','','Data Tidak Lengkap',NULL,'5d45c18a6fcb7c7a67e3a566927074e9.jpg','1c9904cf4221c5ee2b5ad40276b52df0.pdf','5d45c18a6fcb7c7a67e3a566927074e9.jpg','be335d1b7ec0ca58a63b9dc8aac7834e.jpg',NULL);

/*Table structure for table `surat_skck` */

DROP TABLE IF EXISTS `surat_skck`;

CREATE TABLE `surat_skck` (
  `id_surat` varchar(30) NOT NULL,
  `id_user` varchar(20) DEFAULT NULL,
  `id_status` varchar(10) DEFAULT NULL,
  `no_surat` varchar(30) DEFAULT NULL,
  `tgl_surat` datetime DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `kegunaan` varchar(200) DEFAULT NULL,
  `catatan` varchar(100) DEFAULT NULL,
  `ket_tolak` varchar(255) DEFAULT NULL,
  `penolak` varchar(30) DEFAULT NULL,
  `scan_kk` varchar(100) DEFAULT NULL,
  `scan_ektp` varchar(100) DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `scan_pajak` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_surat`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `surat_skck` */

insert  into `surat_skck`(`id_surat`,`id_user`,`id_status`,`no_surat`,`tgl_surat`,`keterangan`,`kegunaan`,`catatan`,`ket_tolak`,`penolak`,`scan_kk`,`scan_ektp`,`foto`,`scan_pajak`) values 
('skck01','Wrg07','6','146/0001/418.69.10/2021','2021-06-24 07:00:00','Pengantar SKCK','Melamar Pekerjaan','Sangat dibutuhkan','',NULL,'da3d079a020a6ffe2e0a371618fe2769.jpg','56cb2f1275ebf2d22e1028f06841d1b9.jpg','c93256b9eb5a317662cc3cef9e57476e.jpg',NULL),
('skck02','Wrg08','7','146/0002/418.69.10/2021','2021-06-25 07:08:00','Untuk Keperluan Kerja','Melamar Pekerjaan','Sangat dibutuhkan','Persyaratan Tidak Lengkap',NULL,'d678e5bc59cbfd82643b5d0cbf77b491.jpg','316e014901955b452e2dd3f0e4a9b7d3.jpg','07aa44b4ee9fee76738931eda2b22d79.jpg',NULL),
('skck03','Wrg01','1','146/0003/418.69.10/2021','2021-07-06 08:12:00','Pengajuan KIP','Melamar Pekerjaan','Sangat dibutuhkan',NULL,NULL,'999b617a4d77f82a65a39b3807f71c27.jpg','0c051d23566e4d40957fe74c618e0bb0.jpg','a464bcc8c6ed54ba73230efd4f795bc2.jpg',NULL),
('skck04','Wrg05','1','146/0004/418.69.10/2021','2021-07-07 08:30:00','Untuk Keperluan Kerja','Melamar Pekerjaan','Sangat dibutuhkan',NULL,NULL,'10ee75cb45240274576773b45f1eeca3.jpg','f242a4a11eeaee17b112a9a5734a112d.jpg','05bca33aa2aad7884b64d5bc57d31d9b.jpg',NULL),
('skck05','Wrg09','1','146/0005/418.69.10/2021','2021-07-07 08:40:00','Pengantar SKCK','Melamar Pekerjaan','Dibutuhkan segera',NULL,NULL,'e82c59c2c4680cba0f9df7ed1ea20d50.jpg','5fe080a8611b881947ecd18cccc18044.jpg','2892a6ca4df7474887d4a6b03dfcecca.jpg',NULL),
('skck06','Wrg10','1','146/0006/418.69.10/2021','2021-07-07 09:22:00','Pengajuan KIP','Melamar Pekerjaan','Dibutuhkan segera',NULL,NULL,'e746f028ca4974d8b4a26dbc90dde8fd.jpg','68675754c17072d1db582ba11437699e.jpg','f2fd91fe9c67c1221546d03ce5769416.jpg',NULL);

/*Table structure for table `surat_sktm` */

DROP TABLE IF EXISTS `surat_sktm`;

CREATE TABLE `surat_sktm` (
  `id_surat` varchar(30) NOT NULL,
  `id_user` varchar(20) NOT NULL,
  `id_status` varchar(10) NOT NULL,
  `no_surat` varchar(30) DEFAULT NULL,
  `tgl_surat` datetime NOT NULL,
  `nama_anak` varchar(50) DEFAULT NULL,
  `jenkel` varchar(10) DEFAULT NULL,
  `tempat_lahir_anak` varchar(20) DEFAULT NULL,
  `tgl_lahir_anak` date DEFAULT NULL,
  `nik_anak` int(20) DEFAULT NULL,
  `sekolah` varchar(50) DEFAULT NULL,
  `semester` varchar(10) DEFAULT NULL,
  `nim_nisn` int(30) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `keperluan` varchar(255) DEFAULT NULL,
  `ket_tolak` varchar(255) DEFAULT NULL,
  `penolak` varchar(30) DEFAULT NULL,
  `scan_kk` varchar(100) DEFAULT NULL,
  `scan_ektp` varchar(100) DEFAULT NULL,
  `scan_pajak` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_surat`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `surat_sktm` */

insert  into `surat_sktm`(`id_surat`,`id_user`,`id_status`,`no_surat`,`tgl_surat`,`nama_anak`,`jenkel`,`tempat_lahir_anak`,`tgl_lahir_anak`,`nik_anak`,`sekolah`,`semester`,`nim_nisn`,`keterangan`,`keperluan`,`ket_tolak`,`penolak`,`scan_kk`,`scan_ektp`,`scan_pajak`) values 
('sktm0001','Wrg05','1','401/0001/418.69.10/2021','2021-06-24 07:09:00','Fahruq','Laki-Laki','Kediri','2008-08-02',2147483647,'SMP 1 Gurah','-',98997979,'Pengajuan KIP','Untuk Syarat Pengajuan Bantuan',NULL,NULL,'2721055d294aaf03fd2e9bab2b32a257.jpg','c9caa3bff5faff5004d6230c0ead71c0.jpg',NULL),
('sktm0002','Wrg08','5','401/0002/418.69.10/2021','2021-06-25 08:08:00','Lili','Perempuan','Kediri','1999-02-23',2147483647,'-','Universita',1879878987,'Untuk Beasiswa','Syarat Beasiswa','',NULL,'','',NULL),
('sktm0003','Wrg03','1','401/0003/418.69.10/2021','2021-07-06 08:10:00','Jojo','Laki-Laki','Kediri','2005-02-24',2147483647,'SMA 1 Gurah','-',1879878987,'Pengajuan KIP','Untuk Syarat Pengajuan KIP',NULL,NULL,'03771540b2006f1d5b07be22ba09a525.jpg','9aee989cee95cb6b4ec1c3f8558acd05.jpg',NULL);

/*Table structure for table `surat_sptjm` */

DROP TABLE IF EXISTS `surat_sptjm`;

CREATE TABLE `surat_sptjm` (
  `id_surat` varchar(30) NOT NULL,
  `id_user` varchar(20) DEFAULT NULL,
  `id_status` int(10) DEFAULT NULL,
  `no_surat` varchar(30) DEFAULT NULL,
  `tgl_surat` datetime DEFAULT NULL,
  `nama_a` varchar(100) DEFAULT NULL,
  `nik_a` varchar(30) DEFAULT NULL,
  `tempat_lahir_a` varchar(50) DEFAULT NULL,
  `tgl_lahir_a` date DEFAULT NULL,
  `pekerjaan_a` varchar(100) DEFAULT NULL,
  `alamat_a` varchar(255) DEFAULT NULL,
  `nama_b` varchar(100) DEFAULT NULL,
  `nik_b` varchar(30) DEFAULT NULL,
  `tempat_lahir_b` varchar(50) DEFAULT NULL,
  `tgl_lahir_b` date DEFAULT NULL,
  `pekerjaan_b` varchar(100) DEFAULT NULL,
  `alamat_b` varchar(255) DEFAULT NULL,
  `nik_saksi_a` varchar(30) DEFAULT NULL,
  `nik_saksi_b` varchar(30) DEFAULT NULL,
  `nama_saksi_a` varchar(100) DEFAULT NULL,
  `nama_saksi_b` varchar(100) DEFAULT NULL,
  `ket_tolak` varchar(255) DEFAULT NULL,
  `penolak` varchar(30) DEFAULT NULL,
  `scan_kk` varchar(100) DEFAULT NULL,
  `scan_ektp` varchar(100) DEFAULT NULL,
  `scan_pajak` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_surat`),
  KEY `id_user` (`id_user`),
  KEY `id_status` (`id_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `surat_sptjm` */

insert  into `surat_sptjm`(`id_surat`,`id_user`,`id_status`,`no_surat`,`tgl_surat`,`nama_a`,`nik_a`,`tempat_lahir_a`,`tgl_lahir_a`,`pekerjaan_a`,`alamat_a`,`nama_b`,`nik_b`,`tempat_lahir_b`,`tgl_lahir_b`,`pekerjaan_b`,`alamat_b`,`nik_saksi_a`,`nik_saksi_b`,`nama_saksi_a`,`nama_saksi_b`,`ket_tolak`,`penolak`,`scan_kk`,`scan_ektp`,`scan_pajak`) values 
('sptjm0001','Wrg07',7,'147/0001/418.69.10/2021','2021-06-24 07:00:00','Yanto','2147483647','Kediri','1986-06-05','Wiraswasta','Jalan Jagalan','Sulastri','2147483647','Kediri','1988-02-12','Wiraswasta','Jalan Jagalan','2147483647','2147483647','Sulis','Gaga','Data tidak lengkap',NULL,'bdbf8f31efdb02d6c59583a64403e77a.jpg','32fbced5178727e67766a6aebe3ebdf9.jpg',NULL),
('sptjm0002','Wrg04',1,'147/0002/418.69.10/2021','2021-07-06 08:00:00','S','3002986476566574','Kediri','2000-09-24','Wiraswasta','Jalan Pondok','Anang','23769879','Kediri','2000-09-06','Wiraswasta','Jalan pondok','1234','2345','Sasa','sisi',NULL,NULL,'de39e514cbda2ed5bcb606885d0ecb7c.jpg','99ae4740d02a04082ed3d68b872c02f1.jpg',NULL),
('sptjm0003','Wrg05',1,'147/0003/418.69.10/2021','2021-07-07 09:00:00','Marsinah','3002986476566999','Kediri','1990-01-07','Ibu Rumah Tangga','Jalan Jagalan, RT.03 RW.03 Desa Gurah Kec. Gurah, Kab. Kediri','Suryanto','3506101005680005','Kediri','1969-07-24','Wiraswasta','Jalan Jagalan, RT.03 RW.03 Desa Gurah Kec. Gurah, Kab. Kediri','3008479798783214','3008479798781111','Virginia Abrinsa','Isna Uswatun Khasanah',NULL,NULL,'3fced7a0096884e33db807ccd5d77abd.jpg','b33e59935051c76fe65dc30d1f20481c.jpg',NULL),
('sptjm0004','Wrg09',1,'147/0004/418.69.10/2021','2021-07-07 09:30:00','Siti Romlah','3506101009980009','Kediri','1960-05-09','Wiraswasta','Jalan Jagalan, RT. 03 RW. 03 Desa Gurah Kec. Gurah, Kab. Kediri','Mahfud Adi','3507121006650001','Madiun','1960-03-12','PNS','Jalan Jagalan, RT. 03 RW. 03 Desa Gurah Kec. Gurah, Kab. Kediri','3506134812990001','3506132407990003','Isna Uswatun','Abrinsa Putri Mahesa',NULL,NULL,'898656936e30e4250eb6cd6ab29cb97e.jpg','cc9f8a9599dba69afc1103ab52f65dee.jpg',NULL),
('sptjm0005','Wrg10',1,'147/0005/418.69.10/2021','2021-07-07 10:00:00','Alkhansa Putra Pratama','3002986476566999','Kediri','1995-06-07','Wiraswasta','Jalan Jagalan, RT.03 RW.03 Desa Gurah Kec. Gurah, Kab. Kediri','Kavita Salsa','3506101003420001','Kediri','1998-03-03','Wiraswasta','Jalan Jagalan, RT.03 RW.03 Desa Gurah Kec. Gurah, Kab. Kediri','3008479798783214','3008479798781111','Titan Anggara Wijaya','Rendy Fajar Utama',NULL,NULL,'05bced8c23017c42e1cd4f1261313a8b.jpg','2fed8f54fc20ef447d505d1a3819ede3.jpg',NULL);

/*Table structure for table `tb_admin` */

DROP TABLE IF EXISTS `tb_admin`;

CREATE TABLE `tb_admin` (
  `id_user` varchar(20) NOT NULL,
  `nip` int(30) DEFAULT NULL,
  `nama_admin` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_admin` */

insert  into `tb_admin`(`id_user`,`nip`,`nama_admin`) values 
('Adm01',2147483647,'Tyas Agustina');

/*Table structure for table `tb_perangkat_desa` */

DROP TABLE IF EXISTS `tb_perangkat_desa`;

CREATE TABLE `tb_perangkat_desa` (
  `id_perangkat` varchar(20) NOT NULL,
  `nip` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `jabatan` varchar(100) DEFAULT NULL,
  `ttd` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_perangkat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_perangkat_desa` */

insert  into `tb_perangkat_desa`(`id_perangkat`,`nip`,`nama`,`alamat`,`jabatan`,`ttd`) values 
('camat01','3506102404402231','a','a','camat',NULL),
('kades01','3506102404408273','kades01','Jalan Jagalan no 1 Gurah, Kediri','kades','60ddb862a72a2.png'),
('kapolsek01','3506102404409821','s','a','kapolsek',NULL);

/*Table structure for table `tb_riwayat` */

DROP TABLE IF EXISTS `tb_riwayat`;

CREATE TABLE `tb_riwayat` (
  `id_riwayat` int(30) NOT NULL AUTO_INCREMENT,
  `id_surat` varchar(30) DEFAULT NULL,
  `tgl_riwayat` datetime DEFAULT NULL,
  `riwayat_status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_riwayat`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

/*Data for the table `tb_riwayat` */

insert  into `tb_riwayat`(`id_riwayat`,`id_surat`,`tgl_riwayat`,`riwayat_status`) values 
(1,'biodata0006','2021-07-14 13:14:01','7'),
(2,'kelahiran0001','2021-07-14 14:27:23','2'),
(5,'skck01','2021-07-21 11:48:51','2'),
(6,'sktm0002','2021-07-21 11:48:57','2'),
(7,'skck02','2021-07-21 11:49:04','2'),
(8,'kk0004','2021-07-21 11:49:12','2'),
(9,'skck01','2021-07-21 00:00:00','3'),
(10,'skck02','2021-07-21 00:00:00','2'),
(11,'skck02','2021-07-21 00:00:00','3'),
(12,'skck01','2021-07-21 00:00:00','4'),
(13,'kk0004','2021-07-21 00:00:00','7'),
(14,'sktm0002','2021-07-21 00:00:00','3'),
(15,'sktm0002','2021-07-21 00:00:00','4'),
(16,'sktm0002','2021-07-21 14:02:47','5'),
(17,'skck01','2021-07-21 14:02:55','5'),
(18,'skck02','2021-07-21 00:00:00','4'),
(19,'skck02','2021-07-21 14:28:58','7'),
(20,'skck01','2021-07-21 00:00:00','6'),
(21,'kip0002','2021-07-21 14:41:12','2'),
(22,'kk0001','2021-07-21 14:41:26','7'),
(23,'kip0002','2021-07-21 00:00:00','4'),
(24,'kip0002','2021-07-21 14:42:50','5'),
(25,'kip0002','2021-07-21 00:00:00','6'),
(26,'sptjm0001','2021-07-21 14:46:39','7'),
(27,'domisili0002','2021-07-21 14:51:02','2'),
(28,'domisili0002','2021-07-21 00:00:00','3'),
(29,'domisili0002','2021-07-21 00:00:00','4'),
(30,'domisili0002','2021-07-21 14:52:03','5'),
(31,'domisili0002','2021-07-21 00:00:00','6'),
(32,'domisili0002','2021-07-22 00:00:00','6'),
(33,'biodata0001','2021-07-22 00:00:00','6'),
(34,'domisili0002','2021-07-22 00:00:00','6'),
(35,'kip0003','2021-07-22 10:33:32','2'),
(36,'kip0003','2021-07-22 00:00:00','3'),
(37,'kip0003','2021-07-22 00:00:00','4'),
(38,'kip0003','2021-07-22 10:55:13','5'),
(39,'kip0003','2021-07-22 00:00:00','6'),
(40,'keramaian0001','2021-07-22 11:08:21','7'),
(41,'kelahiran0001','2021-08-03 00:00:00','3'),
(42,'kelahiran0001','2021-08-03 00:00:00','4');

/*Table structure for table `tb_rt` */

DROP TABLE IF EXISTS `tb_rt`;

CREATE TABLE `tb_rt` (
  `id_rt` int(20) NOT NULL AUTO_INCREMENT,
  `id_user` varchar(20) DEFAULT NULL,
  `ketua_rt` varchar(10) DEFAULT NULL,
  `nik` varchar(30) DEFAULT NULL,
  `nama_rt` varchar(50) DEFAULT NULL,
  `alamat_rt` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_rt`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tb_rt` */

insert  into `tb_rt`(`id_rt`,`id_user`,`ketua_rt`,`nik`,`nama_rt`,`alamat_rt`) values 
(1,'Rt01','01','3506102404202009','Siswanto','Gurah'),
(2,'Rt02','04','3506102404302008','Suyatno','Gurah'),
(3,'Rt03','03','3506102404402007','Agus Hari','Gurah');

/*Table structure for table `tb_status` */

DROP TABLE IF EXISTS `tb_status`;

CREATE TABLE `tb_status` (
  `id_status` int(10) NOT NULL AUTO_INCREMENT,
  `status_monitoring` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `tb_status` */

insert  into `tb_status`(`id_status`,`status_monitoring`) values 
(1,'Menunggu Persetujuan'),
(2,'Menunggu Diproses'),
(3,'Surat Diproses'),
(4,'Menunggu Di TTD Kades'),
(5,'Surat Belum Diunduh'),
(6,'Surat Selesai'),
(7,'Surat Ditolak');

/*Table structure for table `tb_user` */

DROP TABLE IF EXISTS `tb_user`;

CREATE TABLE `tb_user` (
  `id_user` varchar(20) NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `level` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_user` */

insert  into `tb_user`(`id_user`,`username`,`password`,`level`) values 
('Adm01','admin','admin','admin'),
('kades01','kades','kades','kades'),
('Rt01','siswanto','siswanto','rt'),
('Rt02','suyatno','suyatno','rt'),
('Rt03','agus','agus','rt'),
('Wrg01','3506101007740001','3506101007740001','warga'),
('Wrg02','3506101007740002','3506101007740002','warga'),
('Wrg03','3506101007740003','3506101007740003','warga'),
('Wrg04','3506101005680004','3506101005680004','warga'),
('Wrg05','3506101005680005','3506101005680005','warga'),
('Wrg06','3506101007740001','3506101007740001','warga'),
('Wrg07','3506101006650007','3506101006650007','warga'),
('Wrg08','3506101006650008','3506101006650008','warga'),
('Wrg09','3506101009980009','3506101009980009','warga'),
('Wrg10','3506101003420001','3506101003420001','warga');

/*Table structure for table `tb_warga` */

DROP TABLE IF EXISTS `tb_warga`;

CREATE TABLE `tb_warga` (
  `id_user` varchar(30) NOT NULL,
  `no_kk` varchar(30) DEFAULT NULL,
  `nik` varchar(30) DEFAULT NULL,
  `nama_lengkap` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(30) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_kelamin` varchar(30) DEFAULT NULL,
  `umur` int(10) DEFAULT NULL,
  `gol_darah` varchar(10) DEFAULT NULL,
  `penyandang_cacat` varchar(50) DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `status_kawin` varchar(20) DEFAULT NULL,
  `pendidikan` varchar(50) DEFAULT NULL,
  `pekerjaan` varchar(50) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `dusun` varchar(20) DEFAULT NULL,
  `rw` varchar(10) DEFAULT NULL,
  `rt` varchar(10) DEFAULT NULL,
  `desa` varchar(30) DEFAULT NULL,
  `kecamatan` varchar(30) DEFAULT NULL,
  `kab_kota` varchar(30) DEFAULT NULL,
  `prov` varchar(30) DEFAULT NULL,
  `warga_negara` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_warga` */

insert  into `tb_warga`(`id_user`,`no_kk`,`nik`,`nama_lengkap`,`tempat_lahir`,`tanggal_lahir`,`jenis_kelamin`,`umur`,`gol_darah`,`penyandang_cacat`,`agama`,`status_kawin`,`pendidikan`,`pekerjaan`,`alamat`,`dusun`,`rw`,`rt`,`desa`,`kecamatan`,`kab_kota`,`prov`,`warga_negara`) values 
('Wrg01','3506102404111234','3506101007740001','Julianto','Kediri','1974-07-10','Laki-Laki',46,'Tidak Tahu','Tidak Ada','Islam','Kawin','SMP','Wiraswasta','Jalan Jagalan','Gurah 2','03','04','Gurah','Gurah','Kediri','Jawa Timur','Indonesia'),
('Wrg02','3506102404111234','3506101007740002','Tutik Yuli','Kediri','1974-07-16','Perempuan',46,'Tidak Tahu','Tidak Ada','Islam','Kawin','SMP','Wiraswasta','Jalan Jagalan','Gurah 2','03','04','Gurah','Gurah','Kediri','Jawa Timur','Indonesia'),
('Wrg03','3506102404111234','3506101007740003','Virginia Abrinsa','Kediri','1999-09-23','Perempuan',21,'Tidak Tahu','Tidak Ada','Islam','Belum Kawin','SMA','Mahasiswa','Jalan Jagalan','Gurah 2','03','04','Gurah','Gurah','Kediri','Jawa Timur','Indonesia'),
('Wrg04','3506102404111659','3506101005680004','Anang Mulyanto','Kediri','1980-01-26','Laki-Laki',41,'Tidak Tahu','Tidak Ada','Islam','Belum Kawin','SMP','Wiraswasta','Jalan Jagalan','Gurah 2','03','03','Gurah','Gurah','Kediri','Jawa Timur','Indonesia'),
('Wrg05','3506102404111659','3506101005680005','Suryanto','Kediri','1969-07-24','Laki-Laki',52,'Tidak Tahu','Tidak Ada','Islam','Kawin','SMP','Wiraswasta','Jalan Jagalan','Gurah 2','03','03','Gurah','Gurah','Kediri','Jawa Timur','Indonesia'),
('Wrg06','3506102404222435','3506101006650006','Sutiyah','Kediri','1970-04-24','Perempuan',51,'Tidak Tahu','Tidak Ada','Islam','Kawin','SMP','Wiraswasta','Jalan Jagalan','Gurah 2','04','01','Gurah','Gurah','Kediri','Jawa Timur','Indonesia'),
('Wrg07','3506102404222435','3506101006650007','Sulastri','Kediri','1988-02-12','Perempuan',33,'Tidak Tahu','Tidak Ada','Islam','Kawin','SMA','Wiraswasta','Jalan Jagalan','Gurah 2','04','01','Gurah','Gurah','Kediri','Jawa Timur','Indonesia'),
('Wrg08','3506102404222435','3506101006650008','Sumarno','Kediri','1990-03-01','Laki-Laki',31,'Tidak Tahu','Tidak Ada','Islam','Kawin','SMA','Wiraswasta','Jalan Jagalan','Gurah 2','04','01','Gurah','Gurah','Kediri','Jawa Timur','Indonesia'),
('Wrg09','3506102404321567','3506101009980009','Siti Romlah','Kediri','1960-05-09','Perempuan',61,'Tidak Tahu','Tidak Ada','Islam','Kawin','SMP','Wiraswasta','Jalan Jagalan','Gurah 2','03','03','Gurah','Gurah','Kediri','Jawa Timur','Indonesia'),
('Wrg10','3506102404321567','3506101003420001','Kavita Salsa','Kediri','1998-03-03','Perempuan',23,'Tidak Tahu','Tidak Ada','Islam','Belum Kawin','SMP','Pelajar','Jalan Jagalan','Gurah 2','03','03','Gurah','Gurah','Kediri','Jawa Timur','Indonesia');

/*Table structure for table `search` */

DROP TABLE IF EXISTS `search`;

/*!50001 DROP VIEW IF EXISTS `search` */;
/*!50001 DROP TABLE IF EXISTS `search` */;

/*!50001 CREATE TABLE  `search`(
 `id_surat` varchar(30) ,
 `no_surat` varchar(30) ,
 `id_user` varchar(30) ,
 `nama_lengkap` varchar(100) ,
 `tgl_surat` datetime ,
 `id_status` varchar(11) ,
 `status_monitoring` varchar(30) 
)*/;

/*View structure for view search */

/*!50001 DROP TABLE IF EXISTS `search` */;
/*!50001 DROP VIEW IF EXISTS `search` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `search` AS select `x`.`id_surat` AS `id_surat`,`x`.`no_surat` AS `no_surat`,`x`.`id_user` AS `id_user`,`x`.`nama_lengkap` AS `nama_lengkap`,`x`.`tgl_surat` AS `tgl_surat`,`x`.`id_status` AS `id_status`,`x`.`status_monitoring` AS `status_monitoring` from (select `surat_desagurah`.`surat_biodata`.`id_surat` AS `id_surat`,`surat_desagurah`.`surat_biodata`.`no_surat` AS `no_surat`,`surat_desagurah`.`surat_biodata`.`id_user` AS `id_user`,`surat_desagurah`.`tb_warga`.`nama_lengkap` AS `nama_lengkap`,`surat_desagurah`.`surat_biodata`.`tgl_surat` AS `tgl_surat`,`surat_desagurah`.`surat_biodata`.`id_status` AS `id_status`,`surat_desagurah`.`tb_status`.`status_monitoring` AS `status_monitoring` from ((`surat_desagurah`.`surat_biodata` join `surat_desagurah`.`tb_warga` on(`surat_desagurah`.`surat_biodata`.`id_user` = `surat_desagurah`.`tb_warga`.`id_user`)) join `surat_desagurah`.`tb_status` on(`surat_desagurah`.`surat_biodata`.`id_status` = `surat_desagurah`.`tb_status`.`id_status`)) union all select `surat_desagurah`.`surat_domisili`.`id_surat` AS `id_surat`,`surat_desagurah`.`surat_domisili`.`no_surat` AS `no_surat`,`surat_desagurah`.`surat_domisili`.`id_user` AS `id_user`,`surat_desagurah`.`tb_warga`.`nama_lengkap` AS `nama_lengkap`,`surat_desagurah`.`surat_domisili`.`tgl_surat` AS `tgl_surat`,`surat_desagurah`.`surat_domisili`.`id_status` AS `id_status`,`surat_desagurah`.`tb_status`.`status_monitoring` AS `status_monitoring` from ((`surat_desagurah`.`surat_domisili` join `surat_desagurah`.`tb_warga` on(`surat_desagurah`.`surat_domisili`.`id_user` = `surat_desagurah`.`tb_warga`.`id_user`)) join `surat_desagurah`.`tb_status` on(`surat_desagurah`.`surat_domisili`.`id_status` = `surat_desagurah`.`tb_status`.`id_status`)) union all select `surat_desagurah`.`surat_ijin_keramaian`.`id_surat` AS `id_surat`,`surat_desagurah`.`surat_ijin_keramaian`.`no_surat` AS `no_surat`,`surat_desagurah`.`surat_ijin_keramaian`.`id_user` AS `id_user`,`surat_desagurah`.`tb_warga`.`nama_lengkap` AS `nama_lengkap`,`surat_desagurah`.`surat_ijin_keramaian`.`tgl_surat` AS `tgl_surat`,`surat_desagurah`.`surat_ijin_keramaian`.`id_status` AS `id_status`,`surat_desagurah`.`tb_status`.`status_monitoring` AS `status_monitoring` from ((`surat_desagurah`.`surat_ijin_keramaian` join `surat_desagurah`.`tb_warga` on(`surat_desagurah`.`surat_ijin_keramaian`.`id_user` = `surat_desagurah`.`tb_warga`.`id_user`)) join `surat_desagurah`.`tb_status` on(`surat_desagurah`.`surat_ijin_keramaian`.`id_status` = `surat_desagurah`.`tb_status`.`id_status`)) union all select `surat_desagurah`.`surat_kelahiran`.`id_surat` AS `id_surat`,`surat_desagurah`.`surat_kelahiran`.`no_surat` AS `no_surat`,`surat_desagurah`.`surat_kelahiran`.`id_user` AS `id_user`,`surat_desagurah`.`tb_warga`.`nama_lengkap` AS `nama_lengkap`,`surat_desagurah`.`surat_kelahiran`.`tgl_surat` AS `tgl_surat`,`surat_desagurah`.`surat_kelahiran`.`id_status` AS `id_status`,`surat_desagurah`.`tb_status`.`status_monitoring` AS `status_monitoring` from ((`surat_desagurah`.`surat_kelahiran` join `surat_desagurah`.`tb_warga` on(`surat_desagurah`.`surat_kelahiran`.`id_user` = `surat_desagurah`.`tb_warga`.`id_user`)) join `surat_desagurah`.`tb_status` on(`surat_desagurah`.`surat_kelahiran`.`id_status` = `surat_desagurah`.`tb_status`.`id_status`)) union all select `surat_desagurah`.`surat_kematian`.`id_surat` AS `id_surat`,`surat_desagurah`.`surat_kematian`.`no_surat` AS `no_surat`,`surat_desagurah`.`surat_kematian`.`id_user` AS `id_user`,`surat_desagurah`.`tb_warga`.`nama_lengkap` AS `nama_lengkap`,`surat_desagurah`.`surat_kematian`.`tgl_surat` AS `tgl_surat`,`surat_desagurah`.`surat_kematian`.`id_status` AS `id_status`,`surat_desagurah`.`tb_status`.`status_monitoring` AS `status_monitoring` from ((`surat_desagurah`.`surat_kematian` join `surat_desagurah`.`tb_warga` on(`surat_desagurah`.`surat_kematian`.`id_user` = `surat_desagurah`.`tb_warga`.`id_user`)) join `surat_desagurah`.`tb_status` on(`surat_desagurah`.`surat_kematian`.`id_status` = `surat_desagurah`.`tb_status`.`id_status`)) union all select `surat_desagurah`.`surat_kip`.`id_surat` AS `id_surat`,`surat_desagurah`.`surat_kip`.`no_surat` AS `no_surat`,`surat_desagurah`.`surat_kip`.`id_user` AS `id_user`,`surat_desagurah`.`tb_warga`.`nama_lengkap` AS `nama_lengkap`,`surat_desagurah`.`surat_kip`.`tgl_surat` AS `tgl_surat`,`surat_desagurah`.`surat_kip`.`id_status` AS `id_status`,`surat_desagurah`.`tb_status`.`status_monitoring` AS `status_monitoring` from ((`surat_desagurah`.`surat_kip` join `surat_desagurah`.`tb_warga` on(`surat_desagurah`.`surat_kip`.`id_user` = `surat_desagurah`.`tb_warga`.`id_user`)) join `surat_desagurah`.`tb_status` on(`surat_desagurah`.`surat_kip`.`id_status` = `surat_desagurah`.`tb_status`.`id_status`)) union all select `surat_desagurah`.`surat_kuasa`.`id_surat` AS `id_surat`,`surat_desagurah`.`surat_kuasa`.`no_surat` AS `no_surat`,`surat_desagurah`.`surat_kuasa`.`id_user` AS `id_user`,`surat_desagurah`.`tb_warga`.`nama_lengkap` AS `nama_lengkap`,`surat_desagurah`.`surat_kuasa`.`tgl_surat` AS `tgl_surat`,`surat_desagurah`.`surat_kuasa`.`id_status` AS `id_status`,`surat_desagurah`.`tb_status`.`status_monitoring` AS `status_monitoring` from ((`surat_desagurah`.`surat_kuasa` join `surat_desagurah`.`tb_warga` on(`surat_desagurah`.`surat_kuasa`.`id_user` = `surat_desagurah`.`tb_warga`.`id_user`)) join `surat_desagurah`.`tb_status` on(`surat_desagurah`.`surat_kuasa`.`id_status` = `surat_desagurah`.`tb_status`.`id_status`)) union all select `surat_desagurah`.`surat_permohonan_ktp`.`id_surat` AS `id_surat`,`surat_desagurah`.`surat_permohonan_ktp`.`no_surat` AS `no_surat`,`surat_desagurah`.`surat_permohonan_ktp`.`id_user` AS `id_user`,`surat_desagurah`.`tb_warga`.`nama_lengkap` AS `nama_lengkap`,`surat_desagurah`.`surat_permohonan_ktp`.`tgl_surat` AS `tgl_surat`,`surat_desagurah`.`surat_permohonan_ktp`.`id_status` AS `id_status`,`surat_desagurah`.`tb_status`.`status_monitoring` AS `status_monitoring` from ((`surat_desagurah`.`surat_permohonan_ktp` join `surat_desagurah`.`tb_warga` on(`surat_desagurah`.`surat_permohonan_ktp`.`id_user` = `surat_desagurah`.`tb_warga`.`id_user`)) join `surat_desagurah`.`tb_status` on(`surat_desagurah`.`surat_permohonan_ktp`.`id_status` = `surat_desagurah`.`tb_status`.`id_status`)) union all select `surat_desagurah`.`surat_perubahan_kk`.`id_surat` AS `id_surat`,`surat_desagurah`.`surat_perubahan_kk`.`no_surat` AS `no_surat`,`surat_desagurah`.`surat_perubahan_kk`.`id_user` AS `id_user`,`surat_desagurah`.`tb_warga`.`nama_lengkap` AS `nama_lengkap`,`surat_desagurah`.`surat_perubahan_kk`.`tgl_surat` AS `tgl_surat`,`surat_desagurah`.`surat_perubahan_kk`.`id_status` AS `id_status`,`surat_desagurah`.`tb_status`.`status_monitoring` AS `status_monitoring` from ((`surat_desagurah`.`surat_perubahan_kk` join `surat_desagurah`.`tb_warga` on(`surat_desagurah`.`surat_perubahan_kk`.`id_user` = `surat_desagurah`.`tb_warga`.`id_user`)) join `surat_desagurah`.`tb_status` on(`surat_desagurah`.`surat_perubahan_kk`.`id_status` = `surat_desagurah`.`tb_status`.`id_status`)) union all select `surat_desagurah`.`surat_skck`.`id_surat` AS `id_surat`,`surat_desagurah`.`surat_skck`.`no_surat` AS `no_surat`,`surat_desagurah`.`surat_skck`.`id_user` AS `id_user`,`surat_desagurah`.`tb_warga`.`nama_lengkap` AS `nama_lengkap`,`surat_desagurah`.`surat_skck`.`tgl_surat` AS `tgl_surat`,`surat_desagurah`.`surat_skck`.`id_status` AS `id_status`,`surat_desagurah`.`tb_status`.`status_monitoring` AS `status_monitoring` from ((`surat_desagurah`.`surat_skck` join `surat_desagurah`.`tb_warga` on(`surat_desagurah`.`surat_skck`.`id_user` = `surat_desagurah`.`tb_warga`.`id_user`)) join `surat_desagurah`.`tb_status` on(`surat_desagurah`.`surat_skck`.`id_status` = `surat_desagurah`.`tb_status`.`id_status`)) union all select `surat_desagurah`.`surat_sktm`.`id_surat` AS `id_surat`,`surat_desagurah`.`surat_sktm`.`no_surat` AS `no_surat`,`surat_desagurah`.`surat_sktm`.`id_user` AS `id_user`,`surat_desagurah`.`tb_warga`.`nama_lengkap` AS `nama_lengkap`,`surat_desagurah`.`surat_sktm`.`tgl_surat` AS `tgl_surat`,`surat_desagurah`.`surat_sktm`.`id_status` AS `id_status`,`surat_desagurah`.`tb_status`.`status_monitoring` AS `status_monitoring` from ((`surat_desagurah`.`surat_sktm` join `surat_desagurah`.`tb_warga` on(`surat_desagurah`.`surat_sktm`.`id_user` = `surat_desagurah`.`tb_warga`.`id_user`)) join `surat_desagurah`.`tb_status` on(`surat_desagurah`.`surat_sktm`.`id_status` = `surat_desagurah`.`tb_status`.`id_status`)) union all select `surat_desagurah`.`surat_sptjm`.`id_surat` AS `id_surat`,`surat_desagurah`.`surat_sptjm`.`no_surat` AS `no_surat`,`surat_desagurah`.`surat_sptjm`.`id_user` AS `id_user`,`surat_desagurah`.`tb_warga`.`nama_lengkap` AS `nama_lengkap`,`surat_desagurah`.`surat_sptjm`.`tgl_surat` AS `tgl_surat`,`surat_desagurah`.`surat_sptjm`.`id_status` AS `id_status`,`surat_desagurah`.`tb_status`.`status_monitoring` AS `status_monitoring` from ((`surat_desagurah`.`surat_sptjm` join `surat_desagurah`.`tb_warga` on(`surat_desagurah`.`surat_sptjm`.`id_user` = `surat_desagurah`.`tb_warga`.`id_user`)) join `surat_desagurah`.`tb_status` on(`surat_desagurah`.`surat_sptjm`.`id_status` = `surat_desagurah`.`tb_status`.`id_status`))) `x` */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
