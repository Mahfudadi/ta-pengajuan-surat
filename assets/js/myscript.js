const FlashData = $('.flashdata').data('flashdata');

if (FlashData) {
  Swal.fire({
    title: 'Data Berhasil ' + FlashData,
    text: '',
    icon: 'success',
  })
}

const FlashData2 = $('.flashdata2').data('flashdata2');
if (FlashData2) {
  Swal.fire({
    title: 'Gagal ',
    text: FlashData2,
    icon: 'error',
  })
}

$('.tombol-hapus').on('click', function(e) {

  e.preventDefault();
  const href = $(this).attr('href');

  Swal.fire({
    title: 'Apakah Anda Yakin ?',
    text: "Data akan dihapus",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#17C671',
    cancelButtonColor: '#C4183C',
    confirmButtonText: 'Hapus'
  }).then((result) => {
    if (result.isConfirmed) {
      document.location.href = href;
    }
  })

});