<!doctype html>
<html class="no-js" lang="en">

<head>
    <?php $this->load->view("Warga/_Partials/Head") ?>
</head>

<body>
<div class="loader"><div class="page-loader"></div></div>

<nav class="navbar navbar-expand-lg navbar-light mb-4">
    <?php $this->load->view("Warga/_Partials/Header") ?>
</nav>

<!-- Page Content -->
<div class="page-content"> 
    <h1 class="text-center mb-5">Ubah Password</h1>
    <div class="example col-md-10 ml-auto mr-auto">
        
    <div class="flashdata" data-flashdata="<?= $this->session->flashdata('pesan') ?>"></div>
    <div class="flashdata2" data-flashdata2="<?= $this->session->flashdata('pesangagal') ?>"></div>
    </div>
    <!-- Typography -->
        <div id="typography" class="container mb-5">
            <div class="example col-md-10 ml-auto mr-auto">
                <div class="row mb-5">
                    <div class="col-md-12">
                    <form action="<?php echo base_url('Warga/Profil_Warga/ProfilWarga/update_password') ?>" method="post">
                        <div class="row">
                        <?php foreach($profil as $data) : ?>
                            <div class="col-md-2 mb-2">
                                <label for="form1-no_kk" class="col-form-label">Username</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="id_user" id="form1-no_kk" value="<?php echo $data->id_user ?>" hidden>
                                <input type="text" class="form-control" name="username" id="form1-no_kk" value="<?php echo $data->username ?>" readonly>
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Password Lama</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="password" class="form-control" name="password_lama" id="form1-nik" value="">
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Password Baru</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="password" class="form-control" name="password_baru" id="form1-nama" value="">
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Konfirmasi Password Baru</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="password" class="form-control" name="password_baru2" id="form1-jenkel" value="">
                            </div>
                            <?php endforeach;?>
                            <div class="col-md-12 mb-2">
                            <hr>
                            </div>
                            <div class="col-md-6 mb-2">
                                <button  type="submit" class="btn btn-success btn-block"><i class="fa fa-check"></i> Simpan</button>
                            </div>
                            <div class="col-md-6 mb-2">
                                <a class="btn btn-danger btn-block" href="<?php echo base_url('Warga/Profil_Warga/ProfilWarga/index/'.$this->session->userdata('ses_id')) ?>"><i class="fa fa-times"></i> Batal</a>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <!-- Footer CTA -->
    <footer class="main-footer py-5" style="background:black;">
        <?php $this->load->view("Warga/_Partials/Footer") ?>
    </footer>
</div>

<!-- JavaScript -->
<div id="fb-root"></div>
<?php $this->load->view("Warga/_Partials/Js") ?>
</body>
</html>