<!doctype html>
<html class="no-js" lang="en">

<head>
    <?php $this->load->view("Warga/_Partials/Head") ?>
</head>

<body>
<div class="loader"><div class="page-loader"></div></div>

<nav class="navbar navbar-expand-lg navbar-light mb-4">
    <?php $this->load->view("Warga/_Partials/Header") ?>
</nav>

<!-- Page Content -->
<div class="page-content"> 
    <h1 class="text-center mb-5">Profil Warga</h1>
        <div class="flashdata" data-flashdata="<?= $this->session->flashdata('pesan') ?>"> </div>
        <div id="typography" class="container mb-5">
            <div class="example col-md-10 ml-auto mr-auto">
                <div class="row mb-5">
                    <div class="col-md-12">
                    <form action="">
                        <div class="row">
                            <div class="col-md-8">
                            </div>
                            <div class="col-md-2 mb-4">
                                <a class="btn btn-primary btn-sm btn-block" href="<?php echo base_url('index.php/Warga/Profil_Warga/ProfilWarga/edit_warga/'.$this->session->userdata('ses_id')) ?>">Ubah Profil</a>
                            </div>
                            <div class="col-md-2 mb-4">
                                <a class="btn btn-secondary btn-sm btn-block" href="<?php echo base_url('index.php/Warga/Profil_Warga/ProfilWarga/edit_pw/'.$this->session->userdata('ses_id')) ?>">Ubah Password</a>
                            </div>
                        <?php foreach($profil as $data) : ?>
                            <div class="col-md-2 mb-2">
                                <label for="form1-no_kk" class="col-form-label">No. KK</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="no_kk" id="form1-no_kk" value="<?php echo $data->no_kk ?>" readonly>
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">NIK</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $data->nik ?>" readonly>
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Nama Lengkap</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $data->nama_lengkap ?>" readonly>
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Tempat, Tanggal Lahir</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $data->tempat_lahir ?>, <?php echo tgl_indo($data->tanggal_lahir) ?>" readonly>
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Jenis Kelamin</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $data->jenis_kelamin ?>" readonly>
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Umur</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $data->umur ?>" readonly>
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Golongan Darah</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $data->gol_darah ?>" readonly>
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Penyandang Cacat</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $data->penyandang_cacat ?>" readonly>
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Agama</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $data->agama ?>" readonly>
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Status Kawin</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $data->status_kawin ?>" readonly>
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Pendidikan</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $data->pendidikan ?>" readonly>
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Pekerjaan</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $data->pekerjaan ?>" readonly>
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Alamat</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $data->alamat ?>" readonly>
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Dusun</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $data->dusun ?>" readonly>
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">RW / RT</label>
                            </div>
                            <div class="col-md-2 mb-2">
                                <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $data->rw ?>" readonly>
                            </div>
                            <div class="col-md-2 mb-2">
                                <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $data->rt ?>" readonly>
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Desa</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $data->desa ?>" readonly>
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Kecamatan</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $data->kecamatan ?>" readonly>
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Kabupaten</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $data->kab_kota ?>" readonly>
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Provinsi</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $data->prov ?>" readonly>
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Warga Negara</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $data->warga_negara ?>" readonly>
                            </div>
                            <?php endforeach;?>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <!-- Footer CTA -->
    <footer class="main-footer py-5" style="background:black;">
        <?php $this->load->view("Warga/_Partials/Footer") ?>
    </footer>
</div>

<!-- JavaScript -->
<div id="fb-root"></div>
<?php $this->load->view("Warga/_Partials/Js") ?>
</body>
</html>