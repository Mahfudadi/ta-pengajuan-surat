<!doctype html>
<html class="no-js" lang="en">

<head>
    <?php $this->load->view("Warga/_Partials/Head") ?>
</head>

<body>
<div class="loader"><div class="page-loader"></div></div>

<nav class="navbar navbar-expand-lg navbar-light mb-4">
    <?php $this->load->view("Warga/_Partials/Header") ?>
</nav>

<!-- Page Content -->
<div class="page-content"> 
    <h1 class="text-center mb-5">Ubah Profil Warga</h1>
    <div class="example col-md-10 ml-auto mr-auto">
        <div class="row">
        </div>
    </div>
    <!-- Typography -->
        <div id="typography" class="container mb-5">
            <div class="example col-md-10 ml-auto mr-auto">
                <div class="row mb-5">
                    <div class="col-md-12">
                    <form action="<?php echo base_url('Warga/Profil_Warga/ProfilWarga/Update') ?>" method="post">
                        <div class="row">
                        <?php foreach($profil as $data) : ?>
                            <div class="col-md-2 mb-2">
                                <label for="form1-no_kk" class="col-form-label">No. KK</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="id_user" id="form1-no_kk" value="<?php echo $data->id_user ?>" hidden>
                                <input type="text" class="form-control" name="no_kk" id="form1-no_kk" value="<?php echo $data->no_kk ?>">
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">NIK</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $data->nik ?>">
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Nama Lengkap</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="nama" id="form1-nama" value="<?php echo $data->nama_lengkap ?>">
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Jenis Kelamin</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="jenkel" id="form1-jenkel" value="<?php echo $data->jenis_kelamin ?>">
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="form1-tempat_lahir" class="col-form-label">Tempat Lahir</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="tempat_lahir" id="form1-tempat_lahir" value="<?php echo $data->tempat_lahir ?>">
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Tanggal Lahir</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="date" class="form-control" name="tgl_lahir" id="form1-tgl_lahir" value="<?php echo $data->tanggal_lahir ?>">
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Umur</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="umur" id="form1-umur" value="<?php echo $data->umur ?>">
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Golongan Darah</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="gol_darah" id="form1-gol_darah" value="<?php echo $data->gol_darah ?>">
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Penyandang Cacat</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="cacat" id="form1-cacat" value="<?php echo $data->penyandang_cacat ?>">
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Agama</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="agama" id="form1-agama" value="<?php echo $data->agama ?>">
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Status Kawin</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="status" id="form1-status" value="<?php echo $data->status_kawin ?>">
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Pendidikan</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="pendidikan" id="form1-pendidikan" value="<?php echo $data->pendidikan ?>">
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Pekerjaan</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="pekerjaan" id="form1-pekerjaan" value="<?php echo $data->pekerjaan ?>">
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Alamat</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="alamat" id="form1-alamat" value="<?php echo $data->alamat ?>">
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Dusun</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="dusun" id="form1-dusun" value="<?php echo $data->dusun ?>">
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">RW / RT</label>
                            </div>
                            <div class="col-md-2 mb-2">
                                <input type="text" class="form-control" name="rw" id="form1-rw" value="<?php echo $data->rw ?>">
                            </div>
                            <div class="col-md-2 mb-2">
                                <input type="text" class="form-control" name="rt" id="form1-rt" value="<?php echo $data->rt ?>">
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Desa</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="desa" id="form1-desa" value="<?php echo $data->desa ?>">
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Kecamatan</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="kecamatan" id="form1-kecamatan" value="<?php echo $data->kecamatan ?>">
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Kabupaten</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="kabupaten" id="form1-kabupaten" value="<?php echo $data->kab_kota ?>">
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Provinsi</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="prov" id="form1-prov" value="<?php echo $data->prov ?>">
                            </div>
                            <div class="col-md-2 mb-2">
                                <label for="no_surat" class="col-form-label">Warga Negara</label>
                            </div>
                            <div class="col-md-4 mb-2">
                                <input type="text" class="form-control" name="warga_negara" id="form1-warga_negara" value="<?php echo $data->warga_negara ?>">
                            </div>
                            <?php endforeach;?>
                            <div class="col-md-12 mb-2">
                            <hr>
                            </div>
                            <div class="col-md-6 mb-2">
                                <button  type="submit" class="btn btn-success btn-block"><i class="fa fa-check"></i> Simpan</button>
                            </div>
                            <div class="col-md-6 mb-2">
                                <a class="btn btn-danger btn-block" href="<?php echo base_url('Warga/Profil_Warga/ProfilWarga/index/'.$this->session->userdata('ses_id')) ?>"><i class="fa fa-times"></i> Batal</a>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <!-- Footer CTA -->
    <footer class="main-footer py-5" style="background:black;">
        <?php $this->load->view("Warga/_Partials/Footer") ?>
    </footer>
</div>

<!-- JavaScript -->
<div id="fb-root"></div>
<?php $this->load->view("Warga/_Partials/Js") ?>
</body>
</html>