<!doctype html>
<html class="no-js" lang="en">

<head>
    <?php $this->load->view("Warga/_Partials/Head") ?>
</head>

<?php
    $conn = mysqli_connect('localhost','root','','surat_desagurah');
    $cari_id = mysqli_query($conn, "SELECT MAX(id_surat) AS max_id FROM surat_skck WHERE id_surat LIKE 'skck%'") or die (mysql_error());
    $data_id = mysqli_fetch_array($cari_id);
    $id_surat = $data_id['max_id'];

    $noUrut = (int) substr($id_surat, 4, 4);
    $noUrut++;
    $char = "skck";
    $newID = $char . sprintf("%02s", $noUrut);
    date_default_timezone_set('Asia/Jakarta');

?>

<?php
    $conn = mysqli_connect('localhost','root','','surat_desagurah');
    $cari_no = mysqli_query($conn, "SELECT MAX(no_surat) AS max_no FROM surat_skck WHERE no_surat LIKE '146/%'") or die (mysql_error());
    $data_no = mysqli_fetch_array($cari_no);
    $no_surat = $data_no['max_no'];

    $noUrut = (int) substr($no_surat, 4, 4);
    $noUrut++;
    $char = "146/";
    $newNO = $char . sprintf("%04s", $noUrut);
    date_default_timezone_set('Asia/Jakarta');

?>

<body>
<div class="loader"><div class="page-loader"></div></div>

<nav class="navbar navbar-expand-lg navbar-light mb-4">
    <?php $this->load->view("Warga/_Partials/Header") ?>
</nav>

<!-- Page Content -->
<div class="page-content"> 
    <h1 class="text-center mb-5">FORMULIR SURAT PENGANTAR PERMOHONAN SKCK</h1>
    <div class="example col-md-10 ml-auto mr-auto">
        <!-- Form Controls: Simple Forms -->
        <div class="row mb-5">
            <div class="col-md-12">
                <form class="form-horizontal style-form" action="<?php echo base_url().'Warga/Surat_Ajuan/SKCK/tambah' ?>" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-12">
                            <h5><b>DATA DIRI PEMOHON</b></h5>
                        </div>
                        <?php
                        foreach ($warga as $wg) : ?>

                        <div class="col-md-7">
                            
                        </div>  <div class="col-md-2">
                            <label for="no_surat" class="col-form-label text-right">Nomor Surat :</label>
                        </div>   
                        <div class="col-md-3">
                            <input type="text" class="form-control" id="no_surat" name="no_surat" value="<?php echo $newNO ?>/418.69.10/2021" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nama" class="col-form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" id="id_surat" name="id_surat" value="<?php echo $newID ?>" hidden>
                            <input type="text" class="form-control" name="nama_lengkap" id="form1-nama_lengkap" value="<?php echo $wg->nama_lengkap ?>" readonly>
                            <input type="text" class="form-control" name="id_status" value="1" hidden>
                            <input type="text" class="form-control" name="id_user" value="<?=$this->session->userdata('ses_id');?>" hidden>
                            <input type="text" class="form-control" name="tgl_surat" value="<?php echo date('Y-m-d H:i:s');?>" hidden>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-jenis_kelamin" class="col-form-label" >Jenis Kelamin</label>
                            <select class="custom-select d-block col-md-3" id="form1-jenis_kelamin" name="jenis_kelamin"  disabled >
                                <option value="<?php echo $wg->jenis_kelamin ?>"><?php echo $wg->jenis_kelamin ?></option>
                                <option value="Laki-Laki">Laki-Laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-agama" class="col-form-label">Agama</label>
                            <select class="custom-select d-block col-md-3" id="form1-agama" name="agama"  disabled>
                                <option value="<?php echo $wg->agama ?>"><?php echo $wg->agama ?></option>
                                <option value="Islam">Islam</option>
                                <option value="Kristen">Kristen</option>
                                <option value="Kristen">Katolik</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Buddha">Buddha</option>
                                <option value="Konghucu">Konghucu</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-tempat_lahir" class="col-form-label">Tempat Lahir</label>
                            <input type="text" class="form-control" id="form1-tempat_lahir" name="tempat_lahir" value="<?php echo $wg->tempat_lahir ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="datepicker-example-1" class="col-form-label">Tanggal Lahir</label>
                            <div class="input-group with-addon-icon-left">
                                <input type="text" class="form-control" name="tanggal_lahir" value="<?php echo tgl_indo($wg->tanggal_lahir) ?>" readonly >
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <label for="form1-no_ktp" class="col-form-label">No. KTP</label>
                            <input type="text" class="form-control" id="form1-no_ktp" name="nik" value="<?php echo $wg->nik ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-status" class="col-form-label">Status Perkawinan</label>
                            <input type="text" class="form-control" id="form1-status_kawin" name="status_kawin" value="<?php echo $wg->status_kawin ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-pekerjaan" class="col-form-label">Pekerjaan</label>
                            <input type="text" class="form-control" id="form1-pekerjaan" name="pekerjaan" value="<?php echo $wg->pekerjaan ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-pendidikan" class="col-form-label">Pendidikan</label>
                            <input type="text" class="form-control" id="form1-pendidikan" name="pendidikan" value="<?php echo $wg->pendidikan ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-alamat" class="col-form-label">Alamat</label>
                            <textarea class="form-control" id="form1-alamat" name="alamat"  readonly><?php echo $wg->alamat ?>, RT.<?php echo $wg->rt ?> RW.<?php echo $wg->rw ?> Desa <?php echo $wg->desa ?> Kec. <?php echo $wg->kecamatan ?>, Kab. <?php echo $wg->kab_kota ?></textarea>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-keterangan" class="col-form-label">Keterangan</label>
                            <input class="form-control" name="keterangan" id="form1-keterangan" placeholder="Keterangan">
                        </div>
                        <div class="col-md-6">
                            <label for="form1-kegunaan" class="col-form-label">Surat Dipergunakan Untuk</label>
                            <input class="form-control" name="kegunaan" id="form1-kegunaan" placeholder="Surat Dipergunakan Untuk">
                        </div>
                        <div class="col-md-6">
                            <label for="form1-catatan" class="col-form-label">Catatan</label>
                            <input class="form-control" name="catatan" id="form1-catatan" placeholder="Catatan">
                        </div>
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-3 mt-5">
                            <h5><b>BERKAS PERSYARATAN</b></h5>
                        </div>
                        <div class="col-md-9 mt-5">
                            <span><i>(Berkas Berupa File Scan dengan format jpg/jpeg/png)</i></span>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan KK (Upload Berkas Disini)</h6>
                            <div>
                                <img id="preview_gambar" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                                <input type="file" class="form-control" id="scan_kk" name="scan_kk" onchange="readURL(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan E-KTP (Upload Berkas Disini)</h6>
                            <div>
                                <img id="preview_gambar2" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                                <input type="file" class="form-control" id="scan_ektp" name="scan_ektp" onchange="readURL2(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6 mt-4">
                            <h6 class="text-muted">Foto 3x4 Terbaru (Upload Berkas Disini)</h6>
                            <div>
                                <img id="preview_gambar3" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                                <input type="file" class="form-control" id="foto" name="foto" onchange="readURL3(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan Pajak PBB Tahun <?php echo date('Y') ?> (Upload Berkas Disini)</h6>
                            <div>
                                <img id="preview_gambar4" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                                <input type="file" class="form-control" id="scan_pajak" name="scan_pajak" onchange="readURL4(this);" required>
                            </div>
                        </div>
                         <?php endforeach; ?>
                    </div>
                    <hr>
                    <button type="submit" name="submit" class="btn btn-outline-success btn-block">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <!-- Footer CTA -->
    <footer class="main-footer py-5" style="background:black;">
        <?php $this->load->view("Warga/_Partials/Footer") ?>
    </footer>
</div>

<!-- JavaScript -->
<div id="fb-root"></div>
<?php $this->load->view("Warga/_Partials/Js") ?>
<script>
function readURL(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}

function readURL2(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar2') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
function readURL3(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar3') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
function readURL4(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar4') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
</script>
</body>
</html>