<!doctype html>
<html class="no-js" lang="en">

<head>
    <?php $this->load->view("Warga/_Partials/Head") ?>
</head>

<?php
    $conn = mysqli_connect('localhost','root','','surat_desagurah');
    $cari_id = mysqli_query($conn, "SELECT MAX(id_surat) AS max_id FROM surat_kuasa WHERE id_surat LIKE 'Kuasa%'") or die (mysql_error());
    $data_id = mysqli_fetch_array($cari_id);
    $idsurat = $data_id['max_id'];

    $noUrut = (int) substr($idsurat, 5, 5);
    $noUrut++;
    $char = "Kuasa";
    $newID = $char . sprintf("%04s", $noUrut);
    date_default_timezone_set('Asia/Jakarta');
?>

<?php
    $conn = mysqli_connect('localhost','root','','surat_desagurah');
    $cari_no = mysqli_query($conn, "SELECT MAX(no_surat) AS max_no FROM surat_kuasa WHERE no_surat LIKE '148/%'") or die (mysql_error());
    $data_no = mysqli_fetch_array($cari_no);
    $no_surat = $data_no['max_no'];

    $noUrut = (int) substr($no_surat, 4, 4);
    $noUrut++;
    $char = "148/";
    $newNO = $char . sprintf("%04s", $noUrut);
    date_default_timezone_set('Asia/Jakarta');
?>

<body>
<div class="loader"><div class="page-loader"></div></div>

<nav class="navbar navbar-expand-lg navbar-light mb-4">
    <?php $this->load->view("Warga/_Partials/Header") ?>
</nav>

<!-- Page Content -->
<div class="page-content"> 
    <h1 class="text-center mb-5">FORMULIR SURAT KUASA</h1>
    <div class="example col-md-10 ml-auto mr-auto">
        <!-- Form Controls: Simple Forms -->
        <div class="row mb-5">
            <div class="col-md-12">
                <form name="form1" class="form-horizontal style-form" action="<?php echo base_url().'Warga/Surat_Ajuan/Surat_Kuasa/tambah' ?>" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-7">
                            <h5><b>PEMBERI KUASA / PIHAK KE 1</b></h5>
                        </div>
                        <?php
                        foreach ($warga as $wg) : ?>
                        <div class="col-md-2">
                            <label for="no_surat" class="col-form-label text-right">Nomor Surat :</label>
                        </div> 
                        <div class="col-md-3">
                            <input type="text" class="form-control" id="no_surat" name="no_surat" value="<?php echo $newNO ?>/418.69.10/2021" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nama_lengkap" class="col-form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" id="id_surat" name="id_surat" value="<?php echo $newID ?>" hidden>
                            <input type="text" class="form-control" name="nama_lengkap" id="form1-nama_lengkap" value="<?php echo $wg->nama_lengkap ?>" readonly>
                            <input type="text" class="form-control" name="id_status" value="1" hidden>
                            <input type="text" class="form-control" name="id_user" value="<?=$this->session->userdata('ses_id');?>" hidden>
                            <input type="text" class="form-control" name="tgl_surat" value="<?php echo date('Y-m-d H:i:s');?>" hidden>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nik" class="col-form-label">NIK </label>
                            <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $wg->nik ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-tempat">Tempat Lahir</label>
                            <input type="text" class="form-control" name="tempat_lahir" id="form1-tempat_lahir" value="<?php echo $wg->tempat_lahir ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-tanggal_lahir">Tanggal Lahir</label>
                            <input type="text" class="form-control" name="tanggal_lahir" id="form1-tanggal_lahir" value="<?php echo tgl_indo($wg->tanggal_lahir) ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-jenis_kelamin" class="col-form-label">Jenis Kelamin</label>
                             <input type="text" class="form-control" name="jenis_kelamin" id="form1-jenis_kelamin" value="<?php echo $wg->jenis_kelamin ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-status_kawin" class="col-form-label">Status Perkawinan</label>
                            <input type="text" class="form-control" name="status_kawin" id="form1-status_kawin" value="<?php echo $wg->status_kawin ?>" readonly>
                        </div>
                         <div class="col-md-6">
                            <label for="form1-pekerjaan" class="col-form-label">Pekerjaan</label>
                            <input type="text" class="form-control" name="pekerjaan" id="form1-pekerjaan" value="<?php echo $wg->pekerjaan ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-alamat" class="col-form-label">Alamat </label>
                            <textarea class="form-control" id="form1-alamat" name="alamat"  readonly><?php echo $wg->alamat ?>, RT.<?php echo $wg->rt ?> RW.<?php echo $wg->rw ?> Desa <?php echo $wg->desa ?> Kec. <?php echo $wg->kecamatan ?>, Kab. <?php echo $wg->kab_kota ?></textarea>
                        </div>
                        <br>
                        <div class="col-md-12">
                            <h5><b>PIHAK KE DUA </b></h5>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nama_pihak_kedua" class="col-form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" id="form1-nama_pihak_kedua" name="nama_pihak_kedua" placeholder="Nama Lengkap" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nik_pihak_kedua" class="col-form-label">NIK </label>
                            <input type="text" class="form-control" id="form1-nik_pihak_kedua" name="nik_pihak_kedua" placeholder="NIK" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-tempat_lahir_pihak_kedua">Tempat Lahir</label>
                            <input type="text" class="form-control" id="form1-tempat_lahir_pihak_kedua" name="tempat_lahir_pihak_kedua" placeholder="Tempat Lahir" required>
                        </div>
                        <div class="col-md-6">
                            <label for="datepicker-example-1">Tanggal Lahir</label>
                            <div class="input-group with-addon-icon-left">
                                <input type="date" class="form-control" name="tgl_lahir_pihak_kedua" placeholder="Tanggal Lahir" required>
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-jenkel_pihakk_kedua" class="col-form-label">Jenis Kelamin</label>
                            <select class="custom-select d-block col-md-3" id="form1-jenkel_pihak_kedua" name="jenkel_pihak_kedua" required>
                                <option value="Laki-Laki">Laki - Laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-status_kawin_pihak_kedua" class="col-form-label">Status Perkawinan</label>
                           <select class="custom-select d-block col-md-3" id="form1-status_kawin_pihak_kedua" name="status_kawin_pihak_kedua" required>
                                <option value="Belum Kawin">Belum Kawin</option>
                                <option value="Kawin">Kawin</option>
                                <option value="Cerai Hidup">Cerai Hidup</option>
                                <option value="Cerai Mati">Cerai Mati</option>
                            </select>
                        </div>
                         <div class="col-md-6">
                            <label for="form1-pekerjaan_pihak_kedua" class="col-form-label">Pekerjaan</label>
                            <input type="text" class="form-control" id="form1-pekerjaan_pihak_kedua" name="pekerjaan_pihak_kedua" placeholder="Pekerjaan" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-alamat_pihak_kedua" class="col-form-label">Alamat Lengkap</label>
                            <input type="text" class="form-control" id="form1-alamat_pihak_kedua" name="alamat_pihak_kedua" placeholder="Alamat Lengkap" required>
                        </div>
                        <div class="col-md-6">
                            <label for="kuasa" class="col-form-label">Pihak I Memberi Kuasa Kepada Pihak Ke II Berupa :</label>
                            <textarea type="text" class="form-control" id="kuasa" name="kuasa" placeholder="Kuasa" required></textarea>
                        </div>
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-3 mt-3">
                            <h5><b>BERKAS PERSYARATAN</b></h5>
                        </div>
                        <div class="col-md-9 mt-3">
                            <span><i>(Berkas Berupa File Scan dengan format jpg/jpeg/png)</i></span>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan KK (Upload Berkas Disini)</h6>
                            <div>
                            <img id="preview_gambar" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                            <input type="file" id="scan_kk" class="form-control" name="scan_kk" onchange="readURL(this);">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan E-KTP Pemohon (Upload Berkas Disini)</h6>
                            <div>
                            <img id="preview_gambar2" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                            <input type="file" id="scan_ektp" class="form-control" name="scan_ektp" onchange="readURL2(this);">
                            </div>
                        </div>
                        <div class="col-md-5 mt-3">
                            <h5><b>BERKAS VALIDASI PIHAK KEDUA</b></h5>
                        </div>
                        <div class="col-md-7 mt-5">
                            <span><i></i></span>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan KK Pihak Kedua(Upload Berkas Disini)</h6>
                            <div>
                            <img id="preview_gambar3" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                            <input type="file" id="scan_kk_kedua" class="form-control" name="scan_kk_kedua" onchange="readURL3(this);">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan E-KTP Pihak Kedua(Upload Berkas Disini)</h6>
                            <div>
                            <img id="preview_gambar4" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                            <input type="file" id="scan_ektp_kedua" class="form-control" name="scan_ektp_kedua" onchange="readURL4(this);">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan Pajak PBB Tahun <?php echo date('Y') ?> (Upload Berkas Disini)</h6>
                            <div>
                                <img id="preview_gambar5" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                                <input type="file" class="form-control" id="scan_pajak" name="scan_pajak" onchange="readURL5(this);" required>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <hr>
                <button type="submit" name="submit" class="btn btn-outline-success btn-block">Submit</button>
            
                </form>
                </div>
        </div>
    </div>
</div>
    <!-- Footer CTA -->
    <footer class="main-footer py-5" style="background:black;">
        <?php $this->load->view("Warga/_Partials/Footer") ?>
    </footer>
</div>

<!-- JavaScript -->
<div id="fb-root"></div>
<?php $this->load->view("Warga/_Partials/Js") ?>

<script>
function readURL(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}

function readURL2(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar2') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
function readURL3(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar3') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
function readURL4(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar4') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
function readURL5(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar5') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
</script>

</body>
</html>