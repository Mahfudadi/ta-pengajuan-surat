<!doctype html>
<html class="no-js" lang="en">

<head>
    <?php $this->load->view("Warga/_Partials/Head") ?>
</head>

<?php
    $conn = mysqli_connect('localhost','root','','surat_desagurah');
    $cari_id = mysqli_query($conn, "SELECT MAX(id_surat) AS max_id FROM surat_kip WHERE id_surat LIKE 'kip%'") or die (mysql_error());
    $data_id = mysqli_fetch_array($cari_id);
    $id_surat = $data_id['max_id'];

    $noUrut = (int) substr($id_surat, 4, 4);
    $noUrut++;
    $char = "kip";
    $newID = $char . sprintf("%04s", $noUrut);
    date_default_timezone_set('Asia/Jakarta');

?>

<?php
    $conn = mysqli_connect('localhost','root','','surat_desagurah');
    $cari_no = mysqli_query($conn, "SELECT MAX(no_surat) AS max_no FROM surat_kip WHERE no_surat LIKE '402/%'") or die (mysql_error());
    $data_no = mysqli_fetch_array($cari_no);
    $no_surat = $data_no['max_no'];

    $noUrut = (int) substr($no_surat, 4, 4);
    $noUrut++;
    $char = "402/";
    $newNO = $char . sprintf("%04s", $noUrut);
    date_default_timezone_set('Asia/Jakarta');
?>

<body>
<div class="loader"><div class="page-loader"></div></div>

<nav class="navbar navbar-expand-lg navbar-light mb-4">
    <?php $this->load->view("Warga/_Partials/Header") ?>
</nav>

<!-- Page Content -->
<div class="page-content"> 
    <h1 class="text-center mb-5">FORMULIR SURAT PENGAJUAN KARTU INDONESIA PINTAR (KIP)</h1>
    <div class="example col-md-10 ml-auto mr-auto">
        <!-- Form Controls: Simple Forms -->
        <div class="row mb-5">
            <div class="col-md-12">
                <form class="form-horizontal style-form" action="<?php echo base_url().'Warga/Surat_Ajuan/KIP/tambah' ?>" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-7">
                            <h5><b>DATA DIRI ORANG TUA</b></h5>
                        </div>  

                        <?php
                        foreach ($warga as $wg) : ?>

                        <div class="col-md-2">
                            <label for="no_surat" class="col-form-label text-right">Nomor Surat :</label>
                        </div>   
                        <div class="col-md-3">
                            <input type="text" class="form-control" id="no_surat" name="no_surat" value="<?php echo $newNO ?>/418.69.10/2021" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-no_ktp" class="col-form-label">No. KTP</label>
                            <input type="text" class="form-control" name="no_ktp_ortu" id="form1-no_ktp" value="<?php echo $wg->nik ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nama_ortu" class="col-form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" id="id_surat" name="id_surat" value="<?php echo $newID ?>" hidden>
                            <input type="text" class="form-control" name="nama_ortu" id="form1-nama_ortu" value="<?php echo $wg->nama_lengkap ?>" readonly>
                            <input type="text" class="form-control" name="id_status" value="1" hidden>
                            <input type="text" class="form-control" name="id_user" value="<?=$this->session->userdata('ses_id');?>" hidden>
                            <input type="text" class="form-control" name="tgl_surat" value="<?php echo date('Y-m-d H:i:s');?>" hidden>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-jenkel_ortu" class="col-form-label" >Jenis Kelamin</label>
                            <input type="text" class="form-control" name="jenkel_ortu" id="form1-jenkel_ortu" value="<?php echo $wg->jenis_kelamin ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-agama_ortu" class="col-form-label">Agama</label>
                            <input type="text" class="form-control" name="agama_ortu" id="form1-agama_ortu" value="<?php echo $wg->agama ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-tempat_ortu" class="col-form-label">Tempat Lahir</label>
                            <input type="text" class="form-control" id="form1-tempat_ortu" name="tempat_lahir_ortu" value="<?php echo $wg->tempat_lahir ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-tanggal_lahir" class="col-form-label">Tanggal Lahir</label>
                            <input type="text" class="form-control" name="tanggal_lahir" id="form1-tanggal_lahir" value="<?php echo tgl_indo($wg->tanggal_lahir) ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-status_ortu" class="col-form-label">Status Perkawinan</label>
                            <input type="text" class="form-control" name="status_ortu" id="form1-status_ortu" value="<?php echo $wg->status_kawin ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-alamat_ortu" class="col-form-label">Alamat </label>
                            <textarea class="form-control" id="form1-alamat_ortu" name="alamat_ortu"  readonly><?php echo $wg->alamat ?>, RT.<?php echo $wg->rt ?> RW.<?php echo $wg->rw ?> Desa <?php echo $wg->desa ?> Kec. <?php echo $wg->kecamatan ?>, Kab. <?php echo $wg->kab_kota ?></textarea>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-kwn" class="col-form-label">Kewarganegaraan </label>
                            <input type="text" class="form-control" id="form1-kwn" name="kwn_ortu" value="<?php echo $wg->warga_negara ?>"  readonly>
                        </div>
                        <br>
                        <div class="col-md-12 mt-5">
                            <h5><b>DATA DIRI ANAK</b></h5>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nama_anak" class="col-form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" id="form1-nama_anak" name="nama_anak" placeholder="Nama Lengkap" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nik_anak" class="col-form-label">NIK </label>
                            <input type="text" class="form-control" id="form1-nik_anak" name="nik_anak" placeholder="NIK" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-jenkel_anak" class="col-form-label">Jenis Kelamin</label>
                            <select class="custom-select d-block col-md-3" id="form1-jenkel_anak" name="jenkel_anak" required>
                                <option value="Laki-Laki">Laki - Laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-tempat_anak">Tempat Lahir</label>
                            <input type="text" class="form-control" id="form1-tempat_anak" name="tempat_lahir_anak" placeholder="Tempat Lahir" required>
                        </div>
                        <div class="col-md-6">
                            <label for="datepicker-example-1">Tanggal Lahir (Bulan/Tanggal/Tahun)</label>
                            <div class="input-group with-addon-icon-left">
                                <input type="date" class="form-control" name="tgl_lahir_anak" placeholder="Tanggal Lahir">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-sekolah_anak" class="col-form-label">Sekolah </label>
                            <input type="text" class="form-control" id="form1-sekolah_anak" name="sekolah_anak" placeholder="Sekolah" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-semester_anak" class="col-form-label">Semester</label>
                            <input type="text" class="form-control" id="form1-semester_anak" name="semester_anak" placeholder="Semester" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nim_anak" class="col-form-label">NIM/NISN</label>
                            <input type="text" class="form-control" id="form1-nim_anak" name="nim_anak" placeholder="NIM/NISN" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-keterangan" class="col-form-label">Keterangan</label>
                            <input class="form-control" id="form1-keterangan" name="keterangan" placeholder="Keterangan" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-keperluan" class="col-form-label">Keperluan <span><i>(surat ini digunakan untuk)</i></span></label>
                            <input class="form-control" id="form1-keperluan" name="keperluan" placeholder="Keperluan" required>
                        </div>
                        <div class="col-md-3 mt-5">
                            <h5><b>BERKAS PERSYARATAN</b></h5>
                        </div>
                        <div class="col-md-9 mt-5">
                            <span><i>(Berkas Berupa File Scan dengan format jpg/jpeg/png)</i></span>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan KK (Upload Berkas Disini)</h6>
                            <div>
                                <img id="preview_gambar" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                                <input type="file" class="form-control" id="scan_kk" name="scan_kk" onchange="readURL(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan E-KTP (Upload Berkas Disini)</h6>
                            <div>
                                <img id="preview_gambar2" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                                <input type="file" class="form-control" id="scan_ektp" name="scan_ektp" onchange="readURL2(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan Pajak PBB Tahun <?php echo date('Y') ?> (Upload Berkas Disini)</h6>
                            <div>
                                <img id="preview_gambar3" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                                <input type="file" class="form-control" id="scan_pajak" name="scan_pajak" onchange="readURL3(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan SKTM (Upload Berkas Disini)</h6>
                            <div>
                                <img id="preview_gambar4" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                                <input type="file" class="form-control" id="scan_sktm" name="scan_sktm" onchange="readURL4(this);" required>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <hr>
                    <button type="submit" name="submit" class="btn btn-outline-success btn-block">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <!-- Footer CTA -->
    <footer class="main-footer py-5" style="background:black;">
        <?php $this->load->view("Warga/_Partials/Footer") ?>
    </footer>
</div>

<!-- JavaScript -->
<div id="fb-root"></div>
<?php $this->load->view("Warga/_Partials/Js") ?>
<script>
function readURL(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}

function readURL2(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar2') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
function readURL3(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar3') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
function readURL4(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar4') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
</script>
</body>
</html>