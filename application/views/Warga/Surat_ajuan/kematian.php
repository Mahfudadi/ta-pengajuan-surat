<!doctype html>
<html class="no-js" lang="en">

<head>
    <?php $this->load->view("Warga/_Partials/Head") ?>
</head>

<?php
    $conn = mysqli_connect('localhost','root','','surat_desagurah');
    $cari_id = mysqli_query($conn, "SELECT MAX(id_surat) AS max_id FROM surat_kematian WHERE id_surat LIKE 'kematian%'") or die (mysql_error());
    $data_id = mysqli_fetch_array($cari_id);
    $id_surat = $data_id['max_id'];

    $noUrut = (int) substr($id_surat, 8, 8);
    $noUrut++;
    $char = "kematian";
    $newID = $char . sprintf("%04s", $noUrut);
    date_default_timezone_set('Asia/Jakarta');

?>

<?php
    $conn = mysqli_connect('localhost','root','','surat_desagurah');
    $cari_no = mysqli_query($conn, "SELECT MAX(no_surat) AS max_no FROM surat_kematian WHERE no_surat LIKE '470/%'") or die (mysql_error());
    $data_no = mysqli_fetch_array($cari_no);
    $no_surat = $data_no['max_no'];

    $noUrut = (int) substr($no_surat, 4, 4);
    $noUrut++;
    $char = "470/";
    $newNO = $char . sprintf("%04s", $noUrut);
    date_default_timezone_set('Asia/Jakarta');

?>

<body>
<div class="loader"><div class="page-loader"></div></div>

<nav class="navbar navbar-expand-lg navbar-light mb-4">
    <?php $this->load->view("Warga/_Partials/Header") ?>
</nav>

<!-- Page Content -->
<div class="page-content"> 
    <h1 class="text-center mb-5">FORMULIR SURAT KETERANGAN KEMATIAN</h1>
    <div class="example col-md-10 ml-auto mr-auto">
        <!-- Form Controls: Simple Forms -->
        <div class="row mb-5">
            <div class="col-md-12">
                <form action="<?php echo base_url('Warga/Surat_Ajuan/Kematian/tambah') ?>" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <?php
                        foreach ($warga as $wg) : ?>

                        <div class="col-md-7">
                        </div>  
                        <div class="col-md-2">
                            <label for="no_surat" class="col-form-label text-right">Nomor Surat :</label>
                        </div>   
                        <div class="col-md-3">
                            <input type="text" class="form-control" id="no_surat" name="no_surat" value="<?php echo $newNO ?>/418.69.10/2021" readonly>
                        </div>
                        <div class="col-md-12">
                            <h5><b>KARTU KELUARGA</b></h5>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nama_kepala" class="col-form-label">Nama Kepala Keluarga</label>
                            <input type="text" class="form-control" id="form1-nama_kepala" name="nama_kepala_keluarga" placeholder="No. KK" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nokk" class="col-form-label">No. KK</label>
                            <input type="text" class="form-control" id="form1-nokk" name="no_kk_kepala_keluarga" placeholder="No. KK" required>
                        </div>
                        <div class="col-md-12 mt-5">
                            <h5><b>DATA DIRI PELAPOR</b></h5>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nik" class="col-form-label">NIK</label>
                            <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $wg->nik ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nama_lengkap" class="col-form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" id="id_surat" name="id_surat" value="<?php echo $newID ?>" hidden>
                            <input type="text" class="form-control" name="nama_lengkap" id="form1-nama_lengkap" value="<?php echo $wg->nama_lengkap ?>" readonly>
                            <input type="text" class="form-control" name="id_status" value="1" hidden>
                            <input type="text" class="form-control" name="id_user" value="<?=$this->session->userdata('ses_id');?>" hidden>
                            <input type="text" class="form-control" name="tgl_surat" value="<?php echo date('Y-m-d H:i:s');?>" hidden>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-tanggal_lahir" class="col-form-label">Tanggal Lahir</label>
                            <input type="text" class="form-control" name="tanggal_lahir" id="form1-tanggal_lahir" value="<?php echo tgl_indo($wg->tanggal_lahir) ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-umur" class="col-form-label">Umur</label>
                            <input type="text" class="form-control" name="umur" id="form1-umur" value="<?php echo $wg->umur ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-pekerjaan" class="col-form-label">Pekerjaan</label>
                            <input type="text" class="form-control" name="pekerjaan" id="form1-pekerjaan" value="<?php echo $wg->pekerjaan ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-alamat" class="col-form-label">Alamat</label>
                            <textarea class="form-control" name="alamat" id="form1-alamat"value="<?php echo $wg->alamat ?>" readonly><?php echo $wg->alamat ?></textarea>                        
                        </div>
                        <div class="col-md-12">
                            <h5><b>DATA DIRI JENAZAH</b></h5>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nik_jenazah" class="col-form-label">NIK</label>
                            <input type="text" class="form-control" id="form1-nik_jenazah" name="nik_jenazah" placeholder="NIK" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nama_jenazah" class="col-form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" id="form1-nama_jenazah" name="nama_jenazah" placeholder="Nama Lengkap" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-jenkel_jenazah" class="col-form-label">Jenis Kelamin</label>
                            <select class="custom-select d-block col-md-3" id="form1-jenkel_jenazah" name="jenkel_jenazah" required>
                                <option value="Laki - Laki">Laki - Laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="datepicker-example-1">Tanggal Lahir</label>
                            <div class="input-group with-addon-icon-left">
                                <input type="date" class="form-control" name="tgl_lahir_jenazah" placeholder="Tanggal Lahir">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-umur_jenzah">Umur</label>
                            <input type="text" class="form-control" id="form1-umur_jenazah" name="umur_jenazah" placeholder="Umur" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-tempat_lahir_jenazah">Tempat Lahir </label>
                            <input type="text" class="form-control" id="form1-tempat_lahir_jenazah" name="tempat_lahir_jenazah" placeholder="Tempat Lahir" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-agama_jenazah" class="col-form-label">Agama</label>
                            <select class="custom-select d-block col-md-3" id="form1-agama_jenazah" name="agama_jenazah" required>
                                <option value="Islam">Islam</option>
                                <option value="Kristen">Kristen</option>
                                <option value="Kristen">Katolik</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Buddha">Buddha</option>
                                <option value="Konghucu">Konghucu</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-pekerjaan_jenazah">Pekerjaan</label>
                            <input type="text" class="form-control" id="form1-pekerjaan_jenazah" name="pekerjaan_jenazah" placeholder="Pekerjaan" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-alamat_jenazah" class="col-form-label">Alamat</label>
                            <textarea class="form-control" id="form1-alamat_jenazah" name="alamat_jenazah"placeholder="Alamat" required></textarea>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-kewarganegaraan_jenazah">Kewarganegaraan</label>
                            <input type="text" class="form-control" id="form1-kewarganegaraan_jenazah" name="kewarganegaraan_jenazah" placeholder="Kewarganegaraan" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-anak_ke" class="col-form-label">Anak Ke</label>
                            <input type="text" class="form-control" id="form1-anak_ke" name="anak_ke" placeholder="Anak Ke" required>
                        </div>
                        <div class="col-md-6">
                            <label for="datepicker-example-1">Tanggal Kematian</label>
                            <div class="input-group with-addon-icon-left">
                                <input type="date" class="form-control" name="tgl_kematian" placeholder="Tanggal Kematian">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-pukul" class="col-form-label">Pukul</label>
                            <input type="text" class="form-control" id="form1-pukul" name="pukul" placeholder="Pukul" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-sebab" class="col-form-label">Sebab Kematian</label>
                            <input type="text" class="form-control" id="form1-sebab" name="sebab_kematian" placeholder="Sebab Kematian" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-menerangkan" class="col-form-label">Yang Menerangkan</label>
                            <input type="text" class="form-control" id="form1-menerangkan" name="yang_menerangkan" placeholder="Yang Menerangkan" required>
                        </div>
                        <div class="col-md-12">
                            <h5><b>DATA DIRI IBU</b></h5>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nik_ibu" class="col-form-label">NIK</label>
                            <input type="text" class="form-control" id="form1-nik_ibu" name="nik_ibu" placeholder="NIK" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nama_ibu" class="col-form-label">Nama Lengkap </label>
                            <input type="text" class="form-control" id="form1-nama_ibu" name="nama_ibu" placeholder="Nama Lengkap" required>
                        </div>
                        <div class="col-md-6">
                            <label for="datepicker-example-1">Tanggal Lahir</label>
                            <div class="input-group with-addon-icon-left">
                                <input type="date" class="form-control" name="tgl_lahir_ibu" placeholder="Tanggal Lahir">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-umur_ibu" class="col-form-label">Umur</label>
                            <input type="text" class="form-control" id="form1-umur_ibu" name="umur_ibu" placeholder="Umur" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-pekerjaan_ibu" class="col-form-label">Pekerjaan</label>
                            <input type="text" class="form-control" id="form1-pekerjaan_ibu" name="pekerjaan_ibu" placeholder="Pekerjaan" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-alamat_ibu" class="col-form-label">Alamat</label>
                            <textarea class="form-control" id="form1-alamat_ibu" name="alamat_ibu" placeholder="Alamat" required></textarea>
                        </div>
                        <div class="col-md-12">
                            <h5><b>DATA DIRI AYAH</b></h5>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nik_ayah" class="col-form-label">NIK</label>
                            <input type="text" class="form-control" id="form1-nik_ibu" name="nik_ayah" placeholder="NIK" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nama_ayah" class="col-form-label">Nama Lengkap </label>
                            <input type="text" class="form-control" id="form1-nama_ayah" name="nama_ayah" placeholder="Nama Lengkap" required>
                        </div>
                        <div class="col-md-6">
                            <label for="datepicker-example-1">Tanggal Lahir</label>
                            <div class="input-group with-addon-icon-left">
                                <input type="date" class="form-control" name="tgl_lahir_ayah" placeholder="Tanggal Lahir">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-umur_ayah" class="col-form-label">Umur</label>
                            <input type="text" class="form-control" id="form1-umur_ayah" name="umur_ayah" placeholder="Umur" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-pekerjaan_ayah" class="col-form-label">Pekerjaan</label>
                            <input type="text" class="form-control" id="form1-pekerjaan_ayah" name="pekerjaan_ayah" placeholder="Pekerjaan" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-alamat_ayah" class="col-form-label">Alamat</label>
                            <textarea class="form-control" id="form1-alamat_ayah" name="alamat_ayah" placeholder="Alamat" required></textarea>
                        </div>
                        <div class="col-md-12">
                            <h5><b>DATA DIRI SAKSI I</b></h5>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nik_saksi_a" class="col-form-label">NIK</label>
                            <input type="text" class="form-control" id="form1-nik_saksi_a" name="nik_saksi_a" placeholder="NIK" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nama_saksi_a" class="col-form-label">Nama Lengkap </label>
                            <input type="text" class="form-control" id="form1-nama_saksi_a" name="nama_saksi_a" placeholder="Nama Lengkap" required>
                        </div>
                        <div class="col-md-6">
                            <label for="datepicker-example-1">Tanggal Lahir</label>
                            <div class="input-group with-addon-icon-left">
                                <input type="date" class="form-control" name="tgl_lahir_saksi_a" placeholder="Tanggal Lahir">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-umur_saksi_a" class="col-form-label">Umur</label>
                            <input type="text" class="form-control" id="form1-umur_saksi_a" name="umur_saksi_a" placeholder="Umur" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-pekerjaan_saksi_a" class="col-form-label">Pekerjaan</label>
                            <input type="text" class="form-control" id="form1-pekerjaan_saksi_a" name="pekerjaan_saksi_a" placeholder="Pekerjaan" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-alamat_saksi_a" class="col-form-label">Alamat</label>
                            <textarea class="form-control" id="form1-alamat_saksi_a" name="alamat_saksi_a" placeholder="Alamat" required></textarea>
                        </div>
                        <div class="col-md-12">
                            <h5><b>DATA DIRI SANKSI II</b></h5>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nik_saksi_b" class="col-form-label">NIK</label>
                            <input type="text" class="form-control" id="form1-nik_saksi_b" name="nik_saksi_b" placeholder="NIK" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nama_saksi_b" class="col-form-label">Nama Lengkap </label>
                            <input type="text" class="form-control" id="form1-nama_saksi_b" name="nama_saksi_b" placeholder="Nama Lengkap" required>
                        </div>
                        <div class="col-md-6">
                            <label for="datepicker-example-1">Tanggal Lahir</label>
                            <div class="input-group with-addon-icon-left">
                                <input type="date" class="form-control" name="tgl_lahir_saksi_b" placeholder="Tanggal Lahir">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-umur_saksi_b" class="col-form-label">Umur</label>
                            <input type="text" class="form-control" id="form1-umur_saksi_b" name="umur_saksi_b" placeholder="Umur" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-pekerjaan_saksi_b" class="col-form-label">Pekerjaan</label>
                            <input type="text" class="form-control" id="form1-pekerjaan_saksi_b" name="pekerjaan_saksi_b" placeholder="Pekerjaan" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-alamat_saksi_b" class="col-form-label">Alamat</label>
                            <textarea class="form-control" id="form1-alamat_saksi_b" name="alamat_saksi_b" placeholder="Alamat" required></textarea>
                        </div>
                        <div class="col-md-3 mt-5">
                            <h5><b>BERKAS PERSYARATAN</b></h5>
                        </div>
                        <div class="col-md-9 mt-5">
                            <span><i>(Berkas Berupa File Scan dengan format jpg/jpeg/png)</i></span>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan KK (Upload Berkas Disini)</h6>
                            <div>
                            <img id="preview_gambar" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                            <input type="file" class="form-control" id="scan_kk" name="scan_kk" onchange="readURL(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan E-KTP Orang Yang Meninggal (Upload Berkas Disini)</h6>
                            <div>
                            <img id="preview_gambar2" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                            <input type="file" class="form-control" id="scan_ektp_mati" name="scan_ektp_mati" onchange="readURL2(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan E-KTP Saksi 1 (Upload Berkas Disini)</h6>
                            <div>
                            <img id="preview_gambar3" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                            <input type="file" class="form-control" id="scan_ektp_saksi1" name="scan_ektp_saksi_a" onchange="readURL3(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan E-KTP Saksi 2 (Upload Berkas Disini)</h6>
                            <div>
                            <img id="preview_gambar4" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                            <input type="file" class="form-control" id="scan_ektp_saksi2" name="scan_ektp_saksi_b" onchange="readURL4(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan Keterangan Rumah Sakit (Upload Berkas Disini)</h6>
                            <div>
                            <img id="preview_gambar5" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                            <input type="file" class="form-control" id="scan_suket_rs" name="scan_suket_rs" onchange="readURL5(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan Pajak PBB Tahun <?php echo date('Y') ?> (Upload Berkas Disini)</h6>
                            <div>
                            <img id="preview_gambar6" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                            <input type="file" class="form-control" id="scan_pajak" name="scan_pajak" onchange="readURL6(this);" required>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <hr>
                    <button type="submit" name="submit" class="btn btn-outline-success btn-block">Submit</button>
                </form>
        </div>
    </div>
    <!-- Footer CTA -->
    <footer class="main-footer py-5" style="background:black;">
        <?php $this->load->view("Warga/_Partials/Footer") ?>
    </footer>
</div>
</div>
<!-- JavaScript -->
<div id="fb-root"></div>
<?php $this->load->view("Warga/_Partials/Js") ?>
<script>
function readURL(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}

function readURL2(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar2') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
function readURL3(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar3') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL4(input.files[0]);
    }
}

function readURL4(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar4') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
function readURL5(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar5') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
function readURL6(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar6') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
</script>
</body>
</html>