<!doctype html>
<html class="no-js" lang="en">

<head>
    <?php $this->load->view("Warga/_Partials/Head") ?>
</head>
<?php
    $conn = mysqli_connect('localhost','root','','surat_desagurah');
    $cari_id = mysqli_query($conn, "SELECT MAX(id_surat) AS max_id FROM surat_sptjm WHERE id_surat LIKE 'sptjm%'") or die (mysql_error());
    $data_id = mysqli_fetch_array($cari_id);
    $id_surat = $data_id['max_id'];

    $noUrut = (int) substr($id_surat, 5, 5);
    $noUrut++;
    $char = "sptjm";
    $newID = $char . sprintf("%04s", $noUrut);
    date_default_timezone_set('Asia/Jakarta');

?>

<?php
    $conn = mysqli_connect('localhost','root','','surat_desagurah');
    $cari_no = mysqli_query($conn, "SELECT MAX(no_surat) AS max_no FROM surat_sptjm WHERE no_surat LIKE '147/%'") or die (mysql_error());
    $data_no = mysqli_fetch_array($cari_no);
    $no_surat = $data_no['max_no'];

    $noUrut = (int) substr($no_surat, 4, 4);
    $noUrut++;
    $char = "147/";
    $newNO = $char . sprintf("%04s", $noUrut);
    date_default_timezone_set('Asia/Jakarta');

?>
<body>
<div class="loader"><div class="page-loader"></div></div>

<nav class="navbar navbar-expand-lg navbar-light mb-4">
    <?php $this->load->view("Warga/_Partials/Header") ?>
</nav>

<!-- Page Content -->
<div class="page-content"> 
    <h1 class="text-center mb-5">FORMULIR SURAT PERNYATAAN TANGGUNG JAWAB MUTLAK (SPTJM) KEBENARAN SEBAGAI PASANGAN SUAMI ISTERI</h1>
    <div class="example col-md-10 ml-auto mr-auto">
        <!-- Form Controls: Simple Forms -->
        <div class="row mb-5">
            <div class="col-md-12">
                <form action="<?php echo base_url('Warga/Surat_Ajuan/SPTJM/tambah') ?>" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <?php
                        foreach ($warga as $wg) : ?>

                        <div class="col-md-7"></div> 
                        <div class="col-md-2">
                            <label for="no_surat" class="col-form-label text-right">Nomor Surat :</label>
                        </div>   
                        <div class="col-md-3">
                            <input type="text" class="form-control" id="no_surat" name="no_surat" value="<?php echo $newNO ?>/418.69.10/2021" readonly>
                        </div>
                        <div class="col-md-12">
                            <h5><b>DATA DIRI PEMOHON</b></h5>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nama_lengkap" class="col-form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" id="id_surat" name="id_surat" value="<?php echo $newID ?>" hidden>
                            <input type="text" class="form-control" name="nama_lengkap" id="form1-nama_lengkap" value="<?php echo $wg->nama_lengkap ?>" readonly>
                            <input type="text" class="form-control" name="id_status" value="1" hidden>
                            <input type="text" class="form-control" name="id_user" value="<?=$this->session->userdata('ses_id');?>" hidden>
                            <input type="text" class="form-control" name="tgl_surat" value="<?php echo date('Y-m-d H:i:s');?>" hidden>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nik" class="col-form-label">NIK </label>
                            <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $wg->nik ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-tempat" class="col-form-label">Tempat Lahir</label>
                            <input type="text" class="form-control" name="tempat_lahir" id="form1-tempat" value="<?php echo $wg->tempat_lahir ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-tanggal"  class="col-form-label">Tanggal Lahir </label>
                            <input type="text" class="form-control" name="tanggal_lahir" id="form1-tanggal_lahir" value="<?php echo tgl_indo($wg->tanggal_lahir) ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-pekerjaan" class="col-form-label">Pekerjaan</label>
                            <input type="text" class="form-control" name="pekerjaan" id="form1-pekerjaan" value="<?php echo $wg->pekerjaan ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-alamat" class="col-form-label">Alamat </label>
                            <textarea class="form-control" id="form1-alamat" name="alamat"  readonly><?php echo $wg->alamat ?>, RT.<?php echo $wg->rt ?> RW.<?php echo $wg->rw ?> Desa <?php echo $wg->desa ?> Kec. <?php echo $wg->kecamatan ?>, Kab. <?php echo $wg->kab_kota ?></textarea>
                        </div>
                        <div class="col-md-12">
                            <h5><b>DATA DIRI SUAMI/ISTRI </b></h5>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nama_a" class="col-form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" id="form1-nama_a" name="nama_a" placeholder="Nama Lengkap" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nik_a" class="col-form-label">NIK </label>
                            <input type="text" class="form-control" id="form1-nik_a" name="nik_a" placeholder="NIK" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-tempat_a">Tempat Lahir</label>
                            <input type="text" class="form-control" id="form1-tempat_a" name="tempat_lahir_a" placeholder="Tempat Lahir" required>
                        </div>
                        <div class="col-md-6">
                            <label for="datepicker-example-1">Tanggal Lahir </label>
                            <div class="input-group with-addon-icon-left">
                                <input type="date" class="form-control" name="tgl_lahir_a" placeholder="Tanggal Lahir">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-pekerjaan_a" class="col-form-label">Pekerjaan</label>
                            <input type="text" class="form-control" id="form1-pekerjaan_a" name="pekerjaan_a" placeholder="Pekerjaan " required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-alamat" class="col-form-label">Alamat Lengkap</label>
                            <textarea class="form-control" id="form1-alamat" name="alamat_a" placeholder="Alamat" required></textarea>
                        </div>
                        <div class="col-md-12">
                            <h5><b>DATA DIRI SUAMI/ISTRI DARI</b></h5>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nama_b" class="col-form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" id="form1-nama_b" name="nama_b" placeholder="Nama Lengkap" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nik_b" class="col-form-label">NIK </label>
                            <input type="text" class="form-control" id="form1-nik_b" name="nik_b" placeholder="NIK" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-tempat_b">Tempat Lahir</label>
                            <input type="text" class="form-control" id="form1-tempat_lahir_b" name="tempat_lahir_b" placeholder="Tempat Lahir" required>
                        </div>
                        <div class="col-md-6">
                            <label for="datepicker-example-1">Tanggal Lahir </label>
                            <div class="input-group with-addon-icon-left">
                                <input type="date" class="form-control" name="tgl_lahir_b" placeholder="Tanggal Lahir">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-pekerjaan_b" class="col-form-label">Pekerjaan</label>
                            <input type="text" class="form-control" id="form1-pekerjaan_b" name="pekerjaan_b" placeholder="Pekerjaan" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-alamat_b" class="col-form-label">Alamat Lengkap</label>
                            <textarea class="form-control" id="form1-alamat_b" name="alamat_b" placeholder="Alamat" required></textarea>
                        </div>
                        <div class="col-md-12">
                            <h5><b>DATA DIRI SAKSI I & II</b></h5>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nama_b" class="col-form-label">Nama Lengkap Saksi I</label>
                            <input type="text" class="form-control" id="form1-nama_b" name="nama_saksi_a" placeholder="Nama Lengkap" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nik_b" class="col-form-label">NIK Saksi I</label>
                            <input type="text" class="form-control" id="form1-nik_b" name="nik_saksi_a" placeholder="NIK" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nama_b" class="col-form-label">Nama Lengkap Saksi II</label>
                            <input type="text" class="form-control" id="form1-nama_b" name="nama_saksi_b" placeholder="Nama Lengkap" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nik_saksi_b" class="col-form-label">NIK Saksi II</label>
                            <input type="text" class="form-control" id="form1-nik_saksi_b" name="nik_saksi_b" placeholder="NIK" required>
                        </div>
                        <div class="col-md-3 mt-5">
                            <h5><b>BERKAS PERSYARATAN</b></h5>
                        </div>
                        <div class="col-md-9 mt-5">
                            <span><i>(Berkas Berupa File Scan dengan format jpg/jpeg/png)</i></span>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan KK (Upload Berkas Disini)</h6>
                            <div>
                                <img id="preview_gambar" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                                <input type="file" class="form-control" id="scan_kk" name="scan_kk" onchange="readURL(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan E-KTP (Upload Berkas Disini)</h6>
                            <div>
                                <img id="preview_gambar2" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                                <input type="file" class="form-control" id="scan_ektp" name="scan_ektp" onchange="readURL2(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan Pajak PBB Tahun <?php echo date('Y') ?> (Upload Berkas Disini)</h6>
                            <div>
                                <img id="preview_gambar3" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                                <input type="file" class="form-control" id="scan_pajak" name="scan_pajak" onchange="readURL3(this);" required>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <hr>
                    <button type="submit" name="submit" class="btn btn-outline-success btn-block">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <!-- Footer CTA -->
    <footer class="main-footer py-5" style="background:black;">
        <?php $this->load->view("Warga/_Partials/Footer") ?>
    </footer>
</div>

<!-- JavaScript -->
<div id="fb-root"></div>
<?php $this->load->view("Warga/_Partials/Js") ?>
<script>
function readURL(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}

function readURL2(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar2') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
function readURL3(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar3') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
</script>
</body>
</html>