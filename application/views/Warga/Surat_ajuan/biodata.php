<!doctype html>
<html class="no-js" lang="en">

<head>
    <?php $this->load->view("Warga/_Partials/Head") ?>
</head>

<?php
    $conn = mysqli_connect('localhost','root','','surat_desagurah');
    $cari_id = mysqli_query($conn, "SELECT MAX(id_surat) AS max_id FROM surat_biodata WHERE id_surat LIKE 'biodata%'") or die (mysql_error());
    $data_id = mysqli_fetch_array($cari_id);
    $id_surat = $data_id['max_id'];

    $noUrut = (int) substr($id_surat, 7, 7);
    $noUrut++;
    $char = "biodata";
    $newID = $char . sprintf("%04s", $noUrut);
    date_default_timezone_set('Asia/Jakarta');

?>

<?php
    $conn = mysqli_connect('localhost','root','','surat_desagurah');
    $cari_no = mysqli_query($conn, "SELECT MAX(no_surat) AS max_no FROM surat_biodata WHERE no_surat LIKE '144/%'") or die (mysql_error());
    $data_no = mysqli_fetch_array($cari_no);
    $no_surat = $data_no['max_no'];

    $noUrut = (int) substr($no_surat, 4, 4);
    $noUrut++;
    $char = "144/";
    $newNO = $char . sprintf("%04s", $noUrut);
    date_default_timezone_set('Asia/Jakarta');
?>
<body>
<div class="loader"><div class="page-loader"></div></div>

<nav class="navbar navbar-expand-lg navbar-light mb-4">
    <?php $this->load->view("Warga/_Partials/Header") ?>
</nav>

<!-- Page Content -->
<div class="page-content"> 
    <h1 class="text-center mb-5">FORMULIR BIODATA PENDUDUK WARGA NEGARA INDONESIA</h1>
    <div class="example col-md-10 ml-auto mr-auto">
        <!-- Form Controls: Simple Forms -->
        <div class="row mb-5">
            <div class="col-md-12">
                <form action="<?php echo base_url('Warga/Surat_Ajuan/Surat_Biodata/tambah') ?>" method="post" enctype="multipart/form-data">
                    <div class="row">
                         <?php
                        foreach ($warga as $wg) : ?>
                        <div class="col-md-7"></div>
                        <div class="col-md-2">
                            <label for="no_surat" class="col-form-label text-right">Nomor Surat :</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control" id="no_surat" name="no_surat" value="<?php echo $newNO ?>/418.69.10/2021" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nama" class="col-form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" id="id_surat" name="id_surat" value="<?php echo $newID ?>" hidden>
                            <input type="text" class="form-control" name="nama_lengkap" id="form1-nama" value="<?php echo $wg->nama_lengkap ?>" readonly>
                            <input type="text" class="form-control" name="id_status" value="1" hidden>
                            <input type="text" class="form-control" name="id_user" value="<?=$this->session->userdata('ses_id');?>" hidden>
                            <input type="text" class="form-control" name="tgl_surat" value="<?php echo date('Y-m-d H:i:s');?>" hidden>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nik" class="col-form-label">No. KTP</label>
                            <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $wg->nik ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-tempat" class="col-form-label">Tempat Lahir</label>
                            <input type="text" class="form-control" name="tempat_lahir" id="form1-tempat" value="<?php echo $wg->tempat_lahir ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-tanggal" class="col-form-label">Tanggal Lahir </label>
                            <input type="text" class="form-control" name="tanggal_lahir" id="form1-tanggal" value="<?php echo tgl_indo($wg->tanggal_lahir) ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-jenkel" class="col-form-label">Jenis Kelamin</label>
                           <input type="text" class="form-control" name="jenis_kelamin" id="form1-jenkel" value="<?php echo $wg->jenis_kelamin ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-agama" class="col-form-label">Agama</label>
                            <input type="text" class="form-control" name="agama" id="form1-agama" value="<?php echo $wg->agama ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-pekerjaan" class="col-form-label">Pekerjaan</label>
                            <input type="text" class="form-control" name="agama" id="form1-agama" value="<?php echo $wg->agama ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-pendidikan" class="col-form-label">Pendidikan Terakhir</label>
                           <input type="text" class="form-control" name="pendidikan" id="form1-pendidikan" value="<?php echo $wg->pendidikan ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-golongan" class="col-form-label">Golongan Darah</label>
                            <input type="text" class="form-control" name="golongan_darah" id="form1-golongan" value="<?php echo $wg->gol_darah ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-penyadang" class="col-form-label">Penyandang Cacat</label>
                            <input type="text" class="form-control" name="penyandang_cacat" id="form1-penyadang" value="<?php echo $wg->penyandang_cacat ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-status" class="col-form-label">Status Perkawinan</label>
                            <input type="text" class="form-control" name="status_kawin" id="form1-status" value="<?php echo $wg->status_kawin ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-hub_keluarga" class="col-form-label">Status Hub. dalam Keluarga</label>
                            <input type="text" class="form-control" id="form1-hub_keluarga" name="hub_keluarga" placeholder="Status Hubungan Keluarga" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nik_ibu" class="col-form-label">NIK Ibu</label>
                            <input type="text" class="form-control" id="form1-nik_ibu" name="nik_ibu" placeholder="NIK Ibu" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nama_ibu" class="col-form-label">Nama Ibu</label>
                            <input type="text" class="form-control" id="form1-nama_ibu" name="nama_ibu" placeholder="Nama Ibu" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nik_ayah" class="col-form-label">NIK Ayah</label>
                            <input type="text" class="form-control" id="form1-nik_ayah" name="nik_ayah" placeholder="NIK Ayah" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nama_ayah" class="col-form-label">Nama Ayah</label>
                            <input type="text" class="form-control" id="form1-nama_ayah" name="nama_ayah" placeholder="Nama Ayah" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-alamatsebelum" class="col-form-label">Alamat Sebelumnya</label>
                            <textarea class="form-control" id="form1-alamat_ortu" name="alamat_ortu"  readonly><?php echo $wg->alamat ?>, RT.<?php echo $wg->rt ?> RW.<?php echo $wg->rw ?></textarea>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-alamatsekarang" class="col-form-label">Alamat Sekarang</label>
                            <textarea class="form-control" id="form1-alamatsekarang" name="alamat_sekarang" placeholder="Alamat Sekarang" required></textarea>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-rt" class="col-form-label">RT Sekarang</label>
                            <input type="text" class="form-control" id="form1-rt" name="rt_sekarang" placeholder="RT Sekarang" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-rw" class="col-form-label">RW Sekarang</label>
                            <input type="text" class="form-control" id="form1-rw" name="rw_sekarang" placeholder="RW Sekarang" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-dusun" class="col-form-label">Dusun Sekarang</label>
                            <input type="text" class="form-control" id="form1-dusun" name="dusun_sekarang" placeholder="Dusun Sekarang" required>
                        </div>
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-4 mt-5">
                            <h5><b>DATA KEPEMILIKAN DOKUMEN</b></h5>
                        </div>
                        <div class="col-md-8 mt-5">
                            <span><i>(Jika Tidak Memiliki Berkas, Silahkan dikosongi)</i></span>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-no_paspor" class="col-form-label">Nomor Paspor</label>
                            <input type="text" class="form-control" id="form1-no_paspor" name="no_paspor" placeholder="Nomor Paspor">
                        </div>
                        <div class="col-md-6">
                            <label for="datepicker-example-1" class="col-form-label">Tanggal Berakhir Paspor </label>
                            <div class="input-group with-addon-icon-left">
                                <input type="date" class="form-control" name="tanggal_paspor" placeholder="Tanggal Berakhir Paspor">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-no_kawin" class="col-form-label">Nomor Akta Perkawinan/Buku Nikah</label>
                            <input type="text" class="form-control" id="form1-no_kawin" name="no_akta_kawin" placeholder="Nomor Akta Perkawinan/Buku Nikah">
                        </div>
                        <div class="col-md-6">
                            <label for="datepicker-example-1" class="col-form-label">Tanggal Perkawinan </label>
                            <div class="input-group with-addon-icon-left">
                                <input type="date" class="form-control" name="tanggal_kawin" placeholder="Tanggal Perkawinan">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-no_akta" class="col-form-label">Nomor Akta/Surat Kenal Lahir</label>
                            <input type="text" class="form-control" id="form1-no_akta" name="no_akta_kelahiran" placeholder="Nomor Akta/Surat Kenal Lahir">
                        </div>
                        <div class="col-md-6">
                            <label for="datepicker-example-1" class="col-form-label">Tanggal Perceraian</label>
                            <div class="input-group with-addon-icon-left">
                                <input type="date" class="form-control" name="tanggal_cerai" placeholder="Tanggal Perceraian">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-3 mt-5">
                            <h5><b>BERKAS PERSYARATAN</b></h5>
                        </div>
                        <div class="col-md-9 mt-5">
                            <span><i>(Berkas Berupa File Scan dengan format jpg/jpeg/png)</i></span>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan KK (Upload Berkas Disini)</h6>
                            <div>
                                <img id="preview_gambar" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                                <input type="file" class="form-control" id="scan_kk" name="scan_kk" onchange="readURL(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan E-KTP (Upload Berkas Disini)</h6>
                            <div>
                                <img id="preview_gambar2" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                                <input type="file" class="form-control" id="scan_ektp" name="scan_ektp" onchange="readURL2(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan Pajak PBB Tahun <?php echo date('Y') ?> (Upload Berkas Disini)</h6>
                            <div>
                                <img id="preview_gambar3" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                                <input type="file" class="form-control" id="scan_pajak" name="scan_pajak" onchange="readURL3(this);" required>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <hr>
                    <button type="submit" name="submit" class="btn btn-outline-success btn-block">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <!-- Footer CTA -->
    <footer class="main-footer py-5" style="background:black;">
        <?php $this->load->view("Warga/_Partials/Footer") ?>
    </footer>
</div>

<!-- JavaScript -->
<div id="fb-root"></div>
<?php $this->load->view("Warga/_Partials/Js") ?>
<script>
function readURL(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}

function readURL2(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar2') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
function readURL3(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar3') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
</script>
</body>
</html>