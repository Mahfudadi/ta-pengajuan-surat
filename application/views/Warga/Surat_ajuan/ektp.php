<!doctype html>
<html class="no-js" lang="en">

<head>
    <?php $this->load->view("Warga/_Partials/Head") ?>
</head>

<?php
    $conn = mysqli_connect('localhost','root','','surat_desagurah');
    $cari_id = mysqli_query($conn, "SELECT MAX(id_surat) AS max_id FROM surat_permohonan_ktp WHERE id_surat LIKE 'ktp%'") or die (mysql_error());
    $data_id = mysqli_fetch_array($cari_id);
    $id_surat = $data_id['max_id'];

    $noUrut = (int) substr($id_surat, 4, 4);
    $noUrut++;
    $char = "ktp";
    $newID = $char . sprintf("%04s", $noUrut);
?>

<?php
    $conn = mysqli_connect('localhost','root','','surat_desagurah');
    $cari_no = mysqli_query($conn, "SELECT MAX(no_surat) AS max_no FROM surat_permohonan_ktp WHERE no_surat LIKE '404/%'") or die (mysql_error());
    $data_no = mysqli_fetch_array($cari_no);
    $no_surat = $data_no['max_no'];

    $noUrut = (int) substr($no_surat, 4, 4);
    $noUrut++;
    $char = "404/";
    $newNO = $char . sprintf("%04s", $noUrut);
    date_default_timezone_set('Asia/Jakarta');
?>

<body>
<div class="loader"><div class="page-loader"></div></div>

<nav class="navbar navbar-expand-lg navbar-light mb-4">
    <?php $this->load->view("Warga/_Partials/Header") ?>
</nav>

<!-- Page Content -->
<div class="page-content"> 
    <h1 class="text-center mb-5">FORMULIR PERMOHONAN KARTU TANDA PENDUDUK (KTP) </h1>
    <div class="example col-md-10 ml-auto mr-auto">
        <!-- Form Controls: Simple Forms -->
        <div class="row mb-5">
            <div class="col-md-12">
                <form class="form-horizontal style-form" action="<?php echo base_url().'Warga/Surat_Ajuan/EKTP/tambah' ?>" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <?php
                        foreach ($warga as $wg) : ?>
                        <div class="col-md-7">
                        </div> 
                        <div class="col-md-2">
                            <label for="no_surat" class="col-form-label text-right" >Nomor Surat :</label>
                        </div>   
                        <div class="col-md-3">
                            <input type="text" class="form-control" id="no_surat" name="no_surat" value="<?php echo $newNO ?>/418.69.10/2021" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-tipe" class="col-form-label">Permohonan KTP</label>
                            <select class="custom-select d-block col-md-3" id="form1-tipe" name="tipe_permohonan" required>
                                <option value="Baru">Baru</option>
                                <option value="Perpanjangan">Perpanjangan</option>
                                <option value="Penggantian">Penggantian</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nama" class="col-form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" id="id_surat" name="id_surat" value="<?php echo $newID ?>" hidden >
                            <input type="text" class="form-control" name="nama_lengkap" id="form1-nama" value="<?php echo $wg->nama_lengkap ?>" readonly>
                            <input type="text" class="form-control" name="id_status" value="1" hidden>
                            <input type="text" class="form-control" name="id_user" value="<?=$this->session->userdata('ses_id');?> " hidden>
                            <input type="text" class="form-control" name="tgl_surat" value="<?php echo date('Y-m-d H:i:s');?>" hidden>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nik" class="col-form-label">NIK </label>
                            <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $wg->nik ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-no_kk" class="col-form-label">No. KK </label>
                            <input type="text" class="form-control" name="no_kk" id="form1-no_kk" value="<?php echo $wg->no_kk ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-alamat" class="col-form-label">Alamat </label>
                            <textarea class="form-control" name="alamat" id="form1-alamat" value="<?php echo $wg->alamat ?>" readonly><?php echo $wg->alamat ?></textarea>
                        </div>
                        <div class="col-md-3">
                            <label for="form1-rt" class="col-form-label">RT</label>
                            <input type="text" class="form-control" name="rt" id="form1-rt" value="<?php echo $wg->rt ?>" readonly>
                        </div>
                        <div class="col-md-3">
                            <label for="form1-rw" class="col-form-label">RW</label>
                            <input type="text" class="form-control" name="rw" id="form1-rw" value="<?php echo $wg->rw ?>" readonly>
                        </div>
                        <div class="col-md-3 mt-5">
                            <h5><b>BERKAS PERSYARATAN</b></h5>
                        </div>
                        <div class="col-md-9 mt-5">
                            <span><i>(Berkas Berupa File Scan dengan format jpg/jpeg/png)</i></span>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan KK (Upload Berkas Disini)</h6>
                            <div>
                            <img id="preview_gambar" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                            <input type="file" class="form-control" id="scan_kk" name="scan_kk" onchange="readURL(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan E-KTP Lama (Kosongi Jika Tidak Punya)</h6>
                            <div>
                            <img id="preview_gambar2" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                            <input type="file" class="form-control" id="scan_ektp" name="ektp_lama" onchange="readURL2(this);">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan Ijazah Terakhir (Upload Berkas Disini)</h6>
                            <div>
                            <img id="preview_gambar3" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                            <input type="file" class="form-control" id="scan_ijazah" name="ijazah_akhir" onchange="readURL3(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan Akta Kelahiran (Upload Berkas Disini)</h6>
                            <div>
                            <img id="preview_gambar4" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                            <input type="file" class="form-control" id="scan_akta" name="akta" onchange="readURL4(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan Surat Nikah/Cerai (Upload Berkas Disini)</h6>
                            <div>
                            <img id="preview_gambar5" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                            <input type="file" class="form-control" id="scan_nikah" name="surat_nikah_cerai" onchange="readURL5(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Foto 3x4 Terbaru (Upload Berkas Disini)</h6>
                            <div>
                            <img id="preview_gambar6" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                            <input type="file" class="form-control" id="foto" name="foto" onchange="readURL6(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan Pajak PBB Tahun <?php echo date('Y') ?> (Upload Berkas Disini)</h6>
                            <div>
                                <img id="preview_gambar7" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                                <input type="file" class="form-control" id="scan_pajak" name="scan_pajak" onchange="readURL7(this);" required>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <hr>
                    <button type="submit" name="submit" class="btn btn-outline-success btn-block">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <!-- Footer CTA -->
    <footer class="main-footer py-5" style="background:black;">
        <?php $this->load->view("Warga/_Partials/Footer") ?>
    </footer>
</div>

<!-- JavaScript -->
<div id="fb-root"></div>
<?php $this->load->view("Warga/_Partials/Js") ?>
<script>
function readURL(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}

function readURL2(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar2') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}

function readURL3(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar3') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}

function readURL4(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar4') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}

function readURL5(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar5') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}

function readURL6(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar6') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
function readURL7(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar7') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
</script>
</body>
</html>