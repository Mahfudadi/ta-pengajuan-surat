<!doctype html>
<html class="no-js" lang="en">

<head>
    <?php $this->load->view("Warga/_Partials/Head") ?>
</head>

<?php
    $conn = mysqli_connect('localhost','root','','surat_desagurah');
    $cari_id = mysqli_query($conn, "SELECT MAX(id_surat) AS max_id FROM surat_kelahiran WHERE id_surat LIKE 'kelahiran%'") or die (mysql_error());
    $data_id = mysqli_fetch_array($cari_id);
    $id_surat = $data_id['max_id'];

    $noUrut = (int) substr($id_surat, 9, 9);
    $noUrut++;
    $char = "kelahiran";
    $newID = $char . sprintf("%04s", $noUrut);
    date_default_timezone_set('Asia/Jakarta');

?>

<?php
    $conn = mysqli_connect('localhost','root','','surat_desagurah');
    $cari_no = mysqli_query($conn, "SELECT MAX(no_surat) AS max_no FROM surat_kelahiran WHERE no_surat LIKE '472/%'") or die (mysql_error());
    $data_no = mysqli_fetch_array($cari_no);
    $no_surat = $data_no['max_no'];

    $noUrut = (int) substr($no_surat, 4, 4);
    $noUrut++;
    $char = "472/";
    $newNO = $char . sprintf("%04s", $noUrut);
    date_default_timezone_set('Asia/Jakarta');

?>

<body>
<div class="loader"><div class="page-loader"></div></div>

<nav class="navbar navbar-expand-lg navbar-light mb-4">
    <?php $this->load->view("Warga/_Partials/Header") ?>
</nav>

<!-- Page Content -->
<div class="page-content"> 
    <h1 class="text-center mb-5">FORMULIR SURAT KETERANGAN KELAHIRAN</h1>
    <div class="example col-md-10 ml-auto mr-auto">
        <!-- Form Controls: Simple Forms -->
        <div class="row mb-5">
            <div class="col-md-12">
                <form class="form-horizontal style-form" action="<?php echo base_url().'Warga/Surat_Ajuan/Kelahiran/tambah' ?>" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-7" style="margin-top:10px; margin-bottom:0px">
                            <h5><b>DATA KEPALA KELUARGA</b></h5>
                        </div>

                        <?php
                        foreach ($warga as $wg) : ?>

                        <div class="col-md-2">
                            <label for="no_surat" class="col-form-label text-right">Nomor Surat :</label>
                        </div>   
                        <div class="col-md-3">
                            <input type="text" class="form-control" id="no_surat" name="no_surat" value="<?php echo $newNO ?>/418.69.10/2021" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-kepala_keluarga" class="col-form-label">Nama Kepala Keluarga</label>
                            <input type="text" class="form-control" id="form1-kepala_keluarga" name="kepala_keluarga" placeholder="Nama Kepala Keluarga" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-no_kk_kepala" class="col-form-label">No. KK</label>
                            <input type="text" class="form-control" id="form1-no_kk_kepala" name="no_kk_kepala" placeholder="No. KK" required>
                        </div>
                        <div class="col-md-12" style="margin-top:15px; margin-bottom:0px">
                            <h5><b>DATA DIRI PELAPOR</b></h5>
                        </div>
                        <div class="col-md-3">
                            <label for="form1-nik" class="col-form-label">NIK</label>
                            <input type="text" class="form-control" name="nik" id="form1-nik" value="<?php echo $wg->nik ?>" readonly>
                        </div>
                        <div class="col-md-4">
                            <label for="form1-nama_lengkap" class="col-form-label">Nama Pelapor </label>
                            <input type="text" class="form-control" id="id_surat" name="id_surat" value="<?php echo $newID ?>" >
                            <input type="text" class="form-control" name="nama_lengkap" id="form1-nama_lengkap" value="<?php echo $wg->nama_lengkap ?>" readonly>
                            <input type="text" class="form-control" name="id_status" value="1" hidden>
                            <input type="text" class="form-control" name="id_user" value="<?=$this->session->userdata('ses_id');?>" hidden>
                            <input type="text" class="form-control" name="tgl_surat" value="<?php echo date('Y-m-d H:i:s');?>" hidden>
                        </div>
                        <div class="col-md-3">
                            <label for="form1-tanggal_lahir" class="col-form-label">Tanggal Lahir</label>
                             <input type="text" class="form-control" name="tanggal_lahir" id="form1-tanggal_lahir" value="<?php echo tgl_indo($wg->tanggal_lahir) ?>" readonly>
                        </div>
                        <div class="col-md-2">
                            <label for="form1-umur" class="col-form-label">Umur</label>
                            <input type="text" class="form-control" name="umur" id="form1-umur" value="<?php echo $wg->umur ?>" readonly>
                        </div>
                        <div class="col-md-3">
                            <label for="form1-pekerjaan" class="col-form-label">Pekerjaan</label>
                            <input type="text" class="form-control" name="pekerjaan" id="form1-pekerjaan" value="<?php echo $wg->pekerjaan ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-alamat" class="col-form-label">Alamat</label>
                            <textarea class="form-control" name="alamat" id="form1-alamat" readonly><?php echo $wg->alamat ?>, RT.<?php echo $wg->rt ?> RW.<?php echo $wg->rw ?> Desa <?php echo $wg->desa ?> Kec. <?php echo $wg->kecamatan ?>, Kab. <?php echo $wg->kab_kota ?></textarea>
                        </div>
                        <div class="col-md-12" style="margin-top:10px; margin-bottom:0px">
                            <h5><b>DATA DIRI BAYI/ANAK</b></h5>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-nama_bayi" class="col-form-label">Nama Bayi</label>
                            <input type="text" class="form-control" id="form1-nama_bayi" name="nama_bayi" placeholder="Nama Lengkap" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-jenkel_bayi" class="col-form-label">Jenis Kelamin</label>
                            <select class="custom-select d-block col-md-3" id="form1-jenkel_bayi" name="jenkel_bayi" required>
                                <option value="Laki-Laki">Laki-Laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="form1-tempat_dilahiran" class="col-form-label">Tempat Dilahirkan </label>
                            <select class="custom-select d-block col-md-3" id="form1-tempat_dilahirkan" name="tempat_dilahirkan" required>
                                <option value="RS/RB">RS/RB</option>
                                <option value="Puskesmas">Puskesmas</option>
                                <option value="Polindes">Polindes</option>
                                <option value="Rumah">Rumah</option>
                                <option value="Lainnya">Lainnya</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="form1-tempat_kelahiran" class="col-form-label">Tempat Kelahiran </label>
                            <input type="text" class="form-control" id="form1-tempat_kelahiran" name="tempat_kelahiran" placeholder="Tempat Kelahiran" required>
                        </div>
                        <div class="col-md-3">
                            <label for="form1-jenis_kelahiran" class="col-form-label">Jenis Kelahiran</label>
                            <select class="custom-select d-block col-md-3" id="form1-jenis_kelahiran" name="jenis_kelahiran" required>
                                <option value="Tunggal">Tunggal</option>
                                <option value="Kembar (2)">Kembar (2)</option>
                                <option value="Kembar (3)">Kembar (3)</option>
                                <option value="Kembar (4)">Kembar (4)</option>
                                <option value="Lainnya">Lainnya</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="form1-kelahiran_ke" class="col-form-label">Kelahiran ke</label>
                            <select class="custom-select d-block col-md-3" id="form1-kelahiran_ke" name="kelahiran_ke" required>
                                <option value="1">1 (Satu)</option>
                                <option value="2">2 (Dua)</option>
                                <option value="3">3 (Tiga)</option>
                                <option value="4">4 (Empat)</option>
                                <option value="5">5 (Lima)</option>
                                <option value="6">6 (Enam)</option>
                                <option value="7">7 (Tujuh)</option>
                                <option value="8">8 (Delapan)</option>
                                <option value="Dst">Dst.</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="form1-hari">Hari</label>
                            <select class="custom-select d-block col-md-3" id="form1-hari" name="hari" required>
                                <option value="Senin">Senin</option>
                                <option value="Selasa">Selasa</option>
                                <option value="Rabu">Rabu</option>
                                <option value="Kamis">Kamis</option>
                                <option value="Jum'at">Jum'at</option>
                                <option value="Sabtu">Sabtu</option>
                                <option value="Minggu">Minggu</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="datepicker-example-1">Tanggal Lahir (Bulan/Tanggal/Tahun)</label>
                            <div class="input-group with-addon-icon-left">
                                <input type="date" class="form-control" name="tgl_lahir_bayi" placeholder="Tanggal Lahir">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="form1-pukul">Pukul (Jam : Menit)</label>
                            <input type="text" class="form-control" id="form1-pukul" name="pukul" required>
                        </div>
                        <div class="col-md-4">
                            <label for="form1-penolong" class="col-form-label">Penolong Kelahiran</label>
                            <select class="custom-select d-block col-md-3" id="form1-penolong" name="penolong" required>
                                <option value="Dokter">Dokter</option>
                                <option value="Bidan/Perawat">Bidan/Perawat</option>
                                <option value="Dukun">Dukun</option>
                                <option value="Lainnya">Lainnya</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="form1-berat_bayi" class="col-form-label">Berat Bayi (Kg)</label>
                            <div class="input-group with-addon-icon-right">
                                <input type="text" class="form-control" id="form1-berat_bayi" name="berat_bayi" placeholder="Berat Bayi" required>
                                <span class="input-group-addon">
                                    <b>Kg</b>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="form1-panjang_bayi" class="col-form-label">Panjang Bayi (Cm)</label>
                            <div class="input-group with-addon-icon-right">
                                <input type="text" class="form-control" id="form1-panjang_bayi" name="panjang_bayi" placeholder="Panjang Bayi" required>
                                <span class="input-group-addon">
                                    <b>Cm</b>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top:10px; margin-bottom:0px">
                            <h5><b>DATA DIRI IBU</b></h5>
                        </div>
                        <div class="col-md-3">
                            <label for="form1-nik_ibu" class="col-form-label">NIK</label>
                            <input type="text" class="form-control" id="form1-nik_ibu" name="nik_ibu" placeholder="NIK" required>
                        </div>
                        <div class="col-md-4">
                            <label for="form1-nama_ibu" class="col-form-label">Nama Ibu </label>
                            <input type="text" class="form-control" id="form1-nama_ibu" name="nama_ibu" placeholder="Nama Lengkap" required>
                        </div>
                        <div class="col-md-3">
                            <label for="datepicker-example-2" class="col-form-label">Tanggal Lahir</label>
                            <div class="input-group with-addon-icon-left">
                                <input type="date" class="form-control"  name="tgl_lahir_ibu" placeholder="Tanggal Pencatatan Perkawinan">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label for="form1-umur_ibu" class="col-form-label">Umur</label>
                            <input type="text" class="form-control" id="form1-umur_ibu" name="umur_ibu" placeholder="Umur" required>
                        </div>
                        <div class="col-md-3">
                            <label for="form1-pekerjaan_ibu" class="col-form-label">Pekerjaan</label>
                            <input type="text" class="form-control" id="form1-pekerjaan_ibu" name="pekerjaan_ibu" placeholder="Pekerjaan" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-alamat_ibu" class="col-form-label">Alamat</label>
                            <textarea class="form-control" id="form1-alamat_ibu" name="alamat_ibu" placeholder="Alamat" required></textarea>
                        </div>
                        <div class="col-md-3">
                            <label for="form1-kewarganegaraan_ibu">Kewarganegaraan</label>
                            <select class="custom-select d-block col-md-3" id="form1-kewarganegaraan_ibu" name="kewarganegaraan_ibu" required>
                                <option value="WNI">1. WNI</option>
                                <option value="WNA">2. WNA</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="form1-kebangsaan_ibu" class="col-form-label">Kebangsaan</label>
                            <input type="text" class="form-control" id="form1-kebangsaan_ibu" name="kebangsaan_ibu" placeholder="Kebangsaan" required>
                        </div>
                        <div class="col-sm-12" style="margin-top:0px; margin-bottom:0px"><hr></div>
                        <div class="col-md-4" >
                            <label for="datepicker-example-3">Tanggal Pencatatan Perkawinan</label>
                            <div class="input-group with-addon-icon-left">
                                <input type="date" class="form-control"  name="tgl_kawin" placeholder="Tanggal Pencatatan Perkawinan">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-12"  style="margin-top:0px; margin-bottom:0px"><hr></div>
                        <div class="col-md-12" style="margin-top:10px; margin-bottom:0px">
                            <h5><b>DATA DIRI AYAH</b></h5>
                        </div>
                        <div class="col-md-3">
                            <label for="form1-nik_ayah" class="col-form-label">NIK</label>
                            <input type="text" class="form-control" id="form1-nik_ayah" name="nik_ayah" placeholder="NIK" required>
                        </div>
                        <div class="col-md-4">
                            <label for="form1-nama_ayah" class="col-form-label">Nama Ayah </label>
                            <input type="text" class="form-control" id="form1-nama_ayah" name="nama_ayah" placeholder="Nama Lengkap" required>
                        </div>
                        <div class="col-md-3">
                            <label for="datepicker-example-2" class="col-form-label">Tanggal Lahir</label>
                            <div class="input-group with-addon-icon-left">
                                <input type="date" class="form-control"  name="tgl_lahir_ayah" placeholder="Tanggal Pencatatan Perkawinan">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label for="form1-umur_ayah" class="col-form-label">Umur</label>
                            <input type="text" class="form-control" id="form1-umur_ayah" name="umur_ayah" placeholder="Umur" required>
                        </div>
                        <div class="col-md-3">
                            <label for="form1-pekerjaan_ayah" class="col-form-label">Pekerjaan</label>
                            <input type="text" class="form-control" id="form1-pekerjaan_ayah" name="pekerjaan_ayah" placeholder="Pekerjaan" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-alamat_ayah" class="col-form-label">Alamat</label>
                            <textarea class="form-control" id="form1-alamat_ayah" name="alamat_ayah" placeholder="Alamat" required></textarea>
                        </div>
                        <div class="col-md-3">
                            <label for="form1-kewarganegaraan_ayah">Kewarganegaraan</label>
                            <select class="custom-select d-block col-md-3" id="form1-kewarganegaraan_ayah" name="kewarganegaraan_ayah" required>
                                <option value="WNI">1. WNI</option>
                                <option value="WNA">2. WNA</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="form1-kebangsaan_ayah" class="col-form-label">Kebangsaan</label>
                            <input type="text" class="form-control" id="form1-kebangsaan_ibu" name="kebangsaan_ayah" placeholder="Kebangsaan" required>
                        </div>
                        
                        <div class="col-md-12 mt-3">
                            <h5><b>DATA DIRI SAKSI I</b></h5>
                        </div>
                        <div class="col-md-3">
                            <label for="form1-nik_saksi_a" class="col-form-label">NIK</label>
                            <input type="text" class="form-control" id="form1-nik_saksi_a" name="nik_saksi_a" placeholder="NIK" required>
                        </div>
                        <div class="col-md-4">
                            <label for="form1-nama_saksi_a" class="col-form-label">Nama Saksi I </label>
                            <input type="text" class="form-control" id="form1-nama_saksi_a" name="nama_saksi_a" placeholder="Nama Lengkap" required>
                        </div>
                        <div class="col-md-2">
                            <label for="form1-umur_saksi_a" class="col-form-label">Umur</label>
                            <input type="text" class="form-control" id="form1-umur_saksi_a" name="umur_saksi_a" placeholder="Umur" required>
                        </div>
                        <div class="col-md-3">
                            <label for="form1-pekerjaan_saksi_a" class="col-form-label">Pekerjaan</label>
                            <input type="text" class="form-control" id="form1-pekerjaan_saksi_a" name="pekerjaan_saksi_a" placeholder="Pekerjaan" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-alamat_saksi_a" class="col-form-label">Alamat</label>
                            <textarea class="form-control" id="form1-alamat_saksi_ar" name="alamat_saksi_a" placeholder="Alamat" required></textarea>
                        </div>
                        <div class="col-md-12 mt-3">
                            <h5><b>DATA DIRI SAKSI II</b></h5>
                        </div>
                        <div class="col-md-3">
                            <label for="form1-nik_saksi_b" class="col-form-label">NIK</label>
                            <input type="text" class="form-control" id="form1-nik_saksi_b" name="nik_saksi_b" placeholder="NIK" required>
                        </div>
                        <div class="col-md-4">
                            <label for="form1-nama_saksi_b" class="col-form-label">Nama Saksi II </label>
                            <input type="text" class="form-control" id="form1-nama_saksi_b" name="nama_saksi_b" placeholder="Nama Lengkap" required>
                        </div>
                        <div class="col-md-2">
                            <label for="form1-umur_saksi_b" class="col-form-label">Umur</label>
                            <input type="text" class="form-control" id="form1-umur_saksi_b" name="umur_saksi_b" placeholder="Umur" required>
                        </div>
                        <div class="col-md-3">
                            <label for="form1-pekerjaan_saksi_b" class="col-form-label">Pekerjaan</label>
                            <input type="text" class="form-control" id="form1-pekerjaan_saksi_b" name="pekerjaan_saksi_b" placeholder="Pekerjaan" required>
                        </div>
                        <div class="col-md-6">
                            <label for="form1-alamat_saksi_b" class="col-form-label">Alamat</label>
                            <textarea class="form-control" id="form1-alamat_saksi_b" name="alamat_saksi_b" placeholder="Alamat" required></textarea>
                        </div>
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-3 mt-5">
                            <h5><b>BERKAS PERSYARATAN</b></h5>
                        </div>
                        <div class="col-md-9 mt-5">
                            <span><i>(Berkas Berupa File Scan dengan format jpg/jpeg/png)</i></span>
                        </div>
                        <div class="col-md-6 mt-2">
                            <h6 class="text-muted">Scan KK (Upload Berkas Disini)</h6>
                            <div>
                            <img id="preview_gambar" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                            <input type="file" class="form-control" id="scan_kk" name="scan_kk" onchange="readURL(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6 mt-2">
                            <h6 class="text-muted">Scan E-KTP Ayah (Upload Berkas Disini)</h6>
                            <div>
                            <img id="preview_gambar2" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                            <input type="file" class="form-control" id="scan_ektp_ayah" name="scan_ektp_ayah" onchange="readURL2(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6 mt-2">
                            <h6 class="text-muted">Scan E-KTP Ibu (Upload Berkas Disini)</h6>
                            <div>
                            <img id="preview_gambar3" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                            <input type="file" class="form-control" id="scan_ektp_ibu" name="scan_ektp_ibu" onchange="readURL3(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6 mt-2">
                            <h6 class="text-muted">Scan E-KTP Saksi I (Upload Berkas Disini)</h6>
                            <div>
                            <img id="preview_gambar4" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                            <input type="file" class="form-control" id="scan_ektp_saksi_a" name="scan_ektp_saksi_a" onchange="readURL4(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6 mt-2">
                            <h6 class="text-muted">Scan E-KTP Saksi II (Upload Berkas Disini)</h6>
                            <div>
                            <img id="preview_gambar5" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                            <input type="file" class="form-control" id="scan_ektp_saksi_b" name="scan_ektp_saksi_b" onchange="readURL5(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6 mt-2">
                            <h6 class="text-muted">Scan Buku Nikah (Upload Berkas Disini)</h6>
                            <div>
                            <img id="preview_gambar6" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                            <input type="file" class="form-control" id="scan_buku_nikah" name="scan_buku_nikah" onchange="readURL6(this);" required>
                            </div>
                        </div>
                        <div class="col-md-6 mt-2">
                            <h6 class="text-muted">Scan Keterangan Surat Rumah Sakit (Upload Berkas Disini)</h6>
                            <div>
                            <img id="preview_gambar7" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                            <input type="file" class="form-control" id="scan_surat_rs" name="scan_surat_rs" onchange="readURL7(this);"required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-muted">Scan Pajak PBB Tahun <?php echo date('Y') ?> (Upload Berkas Disini)</h6>
                            <div>
                                <img id="preview_gambar8" src="#" alt="Gambar Anda" />
                            </div>
                            <br>
                            <div>
                                <input type="file" class="form-control" id="scan_pajak" name="scan_pajak" onchange="readURL8(this);" required>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <hr>
                    <button type="submit" name="submit" class="btn btn-outline-success btn-block">Submit</button>
                </form>
        </div>
    </div>
    <!-- Footer CTA -->
    <footer class="main-footer py-5" style="background:black;">
        <?php $this->load->view("Warga/_Partials/Footer") ?>
    </footer>
</div>
</div>
<!-- JavaScript -->
<div id="fb-root"></div>
<?php $this->load->view("Warga/_Partials/Js") ?>
<script>
function readURL(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}

function readURL2(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar2') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
function readURL3(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar3') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}

function readURL4(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar4') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
function readURL5(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar5') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}

function readURL6(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar6') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}

function readURL7(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar7') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
function readURL8(input) { // Mulai membaca inputan gambar
    if (input.files && input.files[0]) {
    var reader = new FileReader(); // Membuat variabel reader untuk API FileReader
    
    reader.onload = function (e) { // Mulai pembacaan file
    $('#preview_gambar8') // Tampilkan gambar yang dibaca ke area id #preview_gambar
    .attr('src', e.target.result)
    .width(150); // Menentukan lebar gambar preview (dalam pixel)
    //.height(200); // Jika ingin menentukan lebar gambar silahkan aktifkan perintah pada baris ini
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
</script>
</body>
</html>