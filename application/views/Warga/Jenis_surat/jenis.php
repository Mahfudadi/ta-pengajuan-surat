<!doctype html>
<html class="no-js" lang="en">

<head>
    <?php $this->load->view("Warga/_Partials/Head") ?>
</head>

<body>
<div class="loader"><div class="page-loader"></div></div>

<nav class="navbar navbar-expand-lg navbar-light mb-2">
    <?php $this->load->view("Warga/_Partials/Header") ?>
</nav>

<!-- Page Content -->
<div class="page-content"> 
    <h1 class="text-center mb-2">Jenis Surat</h1>
    <div class="flashdata" data-flashdata="<?= $this->session->flashdata('pesan') ?>"> </div>
    <div class="example col-md-10 ml-auto mr-auto">
        <div class="row">
            <div class="color-wrapper col-lg-12 col-md-6 col-sm-6">
                <label for="search" class="col-form-label">Cari Jenis Surat Disini </label>
                <input type="text" class="form-control col-md-6 mb-5" name="search" id="search" value="" placeholder="Contoh : Kuasa">
            </div>
            <div class="color-wrapper col-lg-3 col-md-6 col-sm-6" id="suratkuasa">
                <div class="color">
                    <div class="fa fa-envelope-o fa-5x"></div>
                    <span class="title"><b>Surat Kuasa</b></span>
                    <hr>
                    <span class="title"><span class="badge badge-secondary">Persyaratan :</span></span>
                    <ol class="text-left">
                        <li>Scan Kartu Keluarga Pemohon Atau Pihak I</li>
                        <li>Scan E-KTP Pemohon Atau Pihak I</li>
                        <li>Scan Kartu Keluarga Pemohon Atau Pihak II</li>
                        <li>Scan E-KTP Pemohon Atau Pihak II</li>
                        <li>Scan Bukti Pelunasan PBB Tahun <?php echo date('Y') ?></li>
                    </ol>
                    <?php 
                    foreach($kuasa as $kuasa):?>
                    <?php 
                        if($kuasa->aktif=='1'){?>
                        <span class="badge badge-danger">Surat Ajuan Anda Belum Selesai</span>
                    <?php }elseif($kuasa->aktif=='0'){?>
                        <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/Surat_Kuasa') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a> 
                        <?php }else{ ?>
                        <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/Surat_Kuasa') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a> 
                    <?php } ?>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="color-wrapper col-lg-3 col-md-6 col-sm-6" id="suratdomisili">
                <div class="color">
                    <div class="fa fa-envelope-o fa-5x"></div>
                    <span class="title"><b>Surat Keterangan Domisili</b></span>
                    <hr>
                    <span class="title"><span class="badge badge-secondary">Persyaratan :</span></span>
                    <ol class="text-left">
                         
                        <li>Scan Kartu Keluarga</li>
                        <li>Scan E-KTP</li>
                        <li>Scan Bukti Pelunasan PBB Tahun <?php echo date('Y') ?></li>
                    </ol>
                    <?php 
                    foreach($domisili as $domisili):?>
                    <?php 
                        if($domisili->aktif=='1'){?>
                        <span class="badge badge-danger">Surat Ajuan Anda Belum Selesai</span>
                    <?php }elseif($domisili->aktif=='0'){?>
                        <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/Surat_Domisili') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a> 
                        <?php }else{ ?>
                        <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/Surat_Domisili') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a> 
                    <?php } ?>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="color-wrapper col-lg-3 col-md-6 col-sm-6" id="suratkip">
                <div class="color">
                    <div class="fa fa-envelope-o fa-5x"></div>
                    <span class="title"><b>Surat Pengajuan Kartu Indonesia Pintar</b></span>
                    <hr>
                    <span class="title"><span class="badge badge-secondary">Persyaratan :</span></span>
                    <ol class="text-left">
                         
                        <li>Scan Kartu Keluarga</li>
                        <li>Scan E-KTP</li>
                        <li>Scan SKTM</li>
                        <li>Scan Bukti Pelunasan PBB Tahun <?php echo date('Y') ?></li>
                    </ol>
                    <?php 
                    foreach($kip as $kip):?>
                    <?php 
                        if($kip->aktif=='1'){?>
                        <span class="badge badge-danger">Surat Ajuan Anda Belum Selesai</span>
                    <?php }elseif($kip->aktif=='0'){?>
                        <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/KIP') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a> 
                        <?php }else{ ?>
                        <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/KIP') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a> 
                    <?php } ?>
                    <?php endforeach; ?>
                    </div>
            </div>
            <div class="color-wrapper col-lg-3 col-md-6 col-sm-6" id="suratsktm">
                <div class="color">
                    <div class="fa fa-envelope-o fa-5x"></div>
                    <span class="title"><b>Surat Keterangan Tidak Mampu</b></span>
                    <hr>
                    <span class="title"><span class="badge badge-secondary">Persyaratan :</span></span>
                    <ol class="text-left">
                         
                        <li>Scan Kartu Keluarga</li>
                        <li>Scan E-KTP</li>
                        <li>Scan Bukti Pelunasan PBB Tahun <?php echo date('Y') ?></li>
                    </ol>
                    
                    <?php 
                    foreach($sktm as $sktm):?>
                    <?php 
                        if($sktm->aktif=='1'){?>
                        <span class="badge badge-danger">Surat Ajuan Anda Belum Selesai</span>
                    <?php }elseif($sktm->aktif=='0'){?>
                        <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/SKTM') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a> 
                        <?php }else{ ?>
                        <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/SKTM') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a> 
                    <?php } ?>
                    <?php endforeach; ?></div>
            </div>
            <div class="color-wrapper col-lg-3 col-md-6 col-sm-6" id="suratkeramaian">
                <div class="color">
                    <div class="fa fa-envelope-o fa-5x"></div>
                    <span class="title"><b>Surat Ijin Keramaian</b></span>
                    <hr>
                    <span class="title"><span class="badge badge-secondary">Persyaratan :</span></span>
                    <ol class="text-left">
                         
                        <li>Scan Kartu Keluarga</li>
                        <li>Scan E-KTP</li>
                        <li>Scan Bukti Pelunasan PBB Tahun <?php echo date('Y') ?></li>
                    </ol>
                    
                    <?php 
                    foreach($keramaian as $keramaian):?>
                    <?php 
                        if($keramaian->aktif=='1'){?>
                        <span class="badge badge-danger">Surat Ajuan Anda Belum Selesai</span>
                    <?php }elseif($keramaian->aktif=='0'){?>
                        <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/Surat_IjinKeramaian') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a> 
                        <?php }else{ ?>
                        <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/Surat_IjinKeramaian') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a> 
                    <?php } ?>
                    <?php endforeach; ?>
                    </div>
            </div>
            <div class="color-wrapper col-lg-3 col-md-6 col-sm-6" id="suratbiodata">
                <div class="color">
                    <div class="fa fa-envelope-o fa-5x"></div>
                    <span class="title"><b>Surat Keterangan Biodata Penduduk</b></span>
                    <hr>
                    <span class="title"><span class="badge badge-secondary">Persyaratan :</span></span>
                    <ol class="text-left">
                         
                        <li>Scan Kartu Keluarga</li>
                        <li>Scan E-KTP</li>
                        <li>Scan Bukti Pelunasan PBB Tahun <?php echo date('Y') ?></li>
                    </ol>
                    <?php 
                    foreach($biodata as $biodata):?>
                    <?php 
                        if($biodata->aktif=='1'){?>
                        <span class="badge badge-danger">Surat Ajuan Anda Belum Selesai</span>
                    <?php }elseif($biodata->aktif=='0'){?>
                        <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/Surat_Biodata') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a> 
                        <?php }else{ ?>
                        <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/Surat_Biodata') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a> 
                    <?php } ?>
                    <?php endforeach; ?>
                    </div>
            </div>
            <div class="color-wrapper col-lg-3 col-md-6 col-sm-6" id="suratsptjm">
                <div class="color">
                    <div class="fa fa-envelope-o fa-5x"></div>
                    <span class="title"><b>Surat Pernyataan Tanggung Jawab Mutlak (SPTJM)</b></span>
                    <hr>
                    <span class="title"><span class="badge badge-secondary">Persyaratan :</span></span>
                    <ol class="text-left">
                         
                        <li>Scan Kartu Keluarga</li>
                        <li>Scan E-KTP</li>
                        <li>Scan Bukti Pelunasan PBB Tahun <?php echo date('Y') ?></li>
                    </ol>
                    <?php 
                    foreach($sptjm as $sptjm):?>
                    <?php 
                        if($sptjm->aktif=='1'){?>
                        <span class="badge badge-danger">Surat Ajuan Anda Belum Selesai</span>
                    <?php }elseif($sptjm->aktif=='0'){?>
                    <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/SPTJM') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a>
                <?php }else{ ?>
                        <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/SPTJM') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a> 
                    <?php } ?>
                    <?php endforeach; ?>
                    </div>
            </div>
            <div class="color-wrapper col-lg-3 col-md-6 col-sm-6" id="suratskck">
                <div class="color">
                    <div class="fa fa-envelope-o fa-5x"></div>
                    <span class="title"><b>Surat Permohonan Pengantar SKCK</b></span>
                    <hr>
                    <span class="title"><span class="badge badge-secondary">Persyaratan :</span></span>
                    <ol class="text-left">
                        <li>Scan Kartu Keluarga</li>
                        <li>Scan E-KTP</li>
                        <li>Foto 3x4 Terbaru</li>
                        <li>Scan Bukti Pelunasan PBB Tahun <?php echo date('Y') ?></li>
                    </ol>
                    <?php 
                    foreach($skck as $skck):?>
                    <?php 
                        if($skck->aktif=='1'){?>
                        <span class="badge badge-danger">Surat Ajuan Anda Belum Selesai</span>
                    <?php }elseif($skck->aktif=='0'){?>
                    <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/SKCK') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a>
                <?php }else{ ?>
                        <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/SKCK') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a> 
                    <?php } ?>
                    <?php endforeach; ?>
                    </div>
            </div>
            <div class="color-wrapper col-lg-3 col-md-6 col-sm-6" id="suratkk">
                <div class="color">
                    <div class="fa fa-envelope-o fa-5x"></div>
                    <span class="title"><b>Surat Permohonan Perubahan Kartu Keluarga</b></span>
                    <hr>
                    <span class="title"><span class="badge badge-secondary">Persyaratan :</span></span>
                    <ol class="text-left">
                         
                        <li>Scan Fotocopy Kartu Keluarga & Kartu Keluarga <b><u>Asli</u></b></li>
                        <li>Scan E-KTP <b>Seluruh Anggota Keluarga</b></li>
                        <li>Scan Ijazah Terakhir</li>
                        <li>Scan Akta Kelahiran</li>
                        <li>Scan Surat Nikah</li>
                        <li>Scan Surat Cerai</li>
                        <li>Scan Bukti Pelunasan PBB Tahun <?php echo date('Y') ?></li>
                        <li>Materai 10 ribu <i>(Untuk Pembetulan Kartu Keluarga yang salah/keliru)</i><b>(Bila Sudah Dicetak)</b></li>

                    </ol>
                    
                    <?php 
                    foreach($kk as $kk):?>
                    <?php 
                        if($kk->aktif=='1'){?>
                        <span class="badge badge-danger">Surat Ajuan Anda Belum Selesai</span>
                    <?php }elseif($kk->aktif=='0'){?>
                    <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/Surat_PerubahanKK') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a>
                <?php }else{ ?>
                        <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/Surat_PerubahanKK') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a> 
                    <?php } ?>
                    <?php endforeach; ?>
                    </div>
            </div>
            <div class="color-wrapper col-lg-3 col-md-6 col-sm-6" id="suratkelahiran">
                <div class="color">
                    <div class="fa fa-envelope-o fa-5x"></div>
                    <span class="title"><b>Surat Kelahiran</b></span>
                    <hr>
                    <span class="title"><span class="badge badge-secondary">Persyaratan :</span></span>
                    <ol class="text-left">
                         
                        <li>Scan Kartu Keluarga <b><u>Asli</u></b></li>
                        <li>Scan E-KTP Ibu & Ayah</li>
                        <li>Scan E-KTP SAKSI I & II</li>
                        <li>Scan Buku Nikah</li>
                        <li>Scan Keterangan Surat Rumah Sakit</li>
                        <li>Scan Bukti Pelunasan PBB Tahun <?php echo date('Y') ?></li>
                    </ol>
                    <?php 
                    foreach($kelahiran as $kelahiran):?>
                    <?php 
                        if($kelahiran->aktif=='1'){?>
                        <span class="badge badge-danger"><span class="badge badge-danger">Surat Ajuan Anda Belum Selesai</span></span>
                    <?php }elseif($kelahiran->aktif=='0'){?>
                    <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/Kelahiran') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a>
                <?php }else{ ?>
                        <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/Kelahiran') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a> 
                    <?php } ?>
                    <?php endforeach; ?>
                    </div>
            </div>
            <div class="color-wrapper col-lg-3 col-md-6 col-sm-6" id="suratkematian">
                <div class="color">
                    <div class="fa fa-envelope-o fa-5x"></div>
                    <span class="title"><b>Surat Keterangan Kematian</b></span>
                    <hr>
                    <span class="title"><span class="badge badge-secondary">Persyaratan :</span></span>
                    <ol class="text-left">
                         
                        <li>Scan Kartu Keluarga <b>Asli</b></li>
                        <li>Scan E-KTP Orang yang Meninggal</li>
                        <li>Scan E-KTP Saksi I & II</li>
                        <li>Surat Keterangan Rumah Sakit</li>
                        <li>Scan Bukti Pelunasan PBB Tahun <?php echo date('Y') ?></li>
                    </ol>
                    <?php 
                    foreach($kematian as $kematian):?>
                    <?php 
                        if($kematian->aktif=='1'){?>
                        <span class="badge badge-danger"><span class="badge badge-danger">Surat Ajuan Anda Belum Selesai</span></span>
                    <?php }elseif($kematian->aktif=='0'){?>
                    <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/Kematian') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a>
                <?php }else{ ?>
                        <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/Kematian') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a> 
                    <?php } ?>
                    <?php endforeach; ?>
                    </div>
            </div>
            <div class="color-wrapper col-lg-3 col-md-6 col-sm-6" id="suratektp">
                <div class="color">
                    <div class="fa fa-envelope-o fa-5x"></div>
                    <span class="title"><b>Surat Permohonan E-KTP WNI</b></span>
                    <hr>
                    <span class="title"><span class="badge badge-secondary">Persyaratan :</span></span>
                    <ol class="text-left">
                         
                        <li>Scan Kartu Keluarga</li>
                        <li>Scan E-KTP Asli</li>
                        <li>Scan Ijazah Terakhir</li>
                        <li>Scan Akta Kelahiran</li>
                        <li>Scan Surat Nikah/ Surat Cerai</li>
                        <li>Foto 3x4 Terbaru (<i style="color:red">Background Merah untuk tahun lahir ganjil</i> & <i style="color:blue">Background Biru untuk tahun lahir genap</i>)</li>
                        <li>Scan Bukti Pelunasan PBB Tahun <?php echo date('Y') ?></li>
                    </ol>
                    <?php 
                    foreach($ektp as $ektp):?>
                    <?php 
                        if($ektp->aktif=='1'){?>
                        <span class="badge badge-danger"><span class="badge badge-danger">Surat Ajuan Anda Belum Selesai</span></span>
                    <?php }elseif($ektp->aktif=='0'){?>
                        <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/EKTP') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a>
                    <?php }else{ ?>
                        <a class="btn btn-outline-primary btn-block btn-sm" href="<?php echo base_url('Warga/Surat_Ajuan/EKTP') ?>">AJUKAN <i class="fa fa-arrow-right"></i></a>
                    <?php } ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer CTA -->
    <footer class="main-footer py-5" style="background:black;">
        <?php $this->load->view("Warga/_Partials/Footer") ?>
    </footer>
</div>

<!-- JavaScript -->
<div id="fb-root"></div>
<?php $this->load->view("Warga/_Partials/Js") ?>
<script>
    $('#search').keyup(function(){
        if ($('#search').val().toLowerCase().indexOf('kuasa') >= 0) {
            $('#suratkuasa').show();
        } else if ($('#search').val().toLowerCase().indexOf('domisili') >= 0) {
            $('#suratdomisili').show();
        } else if ($('#search').val().toLowerCase().indexOf('kip') >= 0) {
        $('#suratkip').show();
        } else if ($('#search').val().toLowerCase().indexOf('sktm') >= 0) {
            $('#suratsktm').show();
        } else if ($('#search').val().toLowerCase().indexOf('keramaian') >= 0) {
            $('#suratkeramaian').show();
        } else if ($('#search').val().toLowerCase().indexOf('biodata') >= 0) {
            $('#suratbiodata').show();
        } else if ($('#search').val().toLowerCase().indexOf('sptjm') >= 0) {
            $('#suratsptjm').show();
        } else if ($('#search').val().toLowerCase().indexOf('skck') >= 0) {
            $('#suratskck').show();
        } else if ($('#search').val().toLowerCase().indexOf('kk') >= 0) {
            $('#suratkk').show();
        } else if ($('#search').val().toLowerCase().indexOf('kelahiran') >= 0) {
            $('#suratkelahiran').show();
        } else if ($('#search').val().toLowerCase().indexOf('kematian') >= 0) {
            $('#suratkematian').show();
        } else if ($('#search').val().toLowerCase().indexOf('ektp') >= 0) {
            $('#suratektp').show();
        } else if ($('#search').val() == '') {
            $('#suratkuasa').show();
            $('#suratdomisili').show();
            $('#suratkip').show();
            $('#suratsktm').show();
            $('#suratkeramaian').show();
            $('#suratbiodata').show();
            $('#suratsptjm').show();
            $('#suratskck').show();
            $('#suratkk').show();
            $('#suratkelahiran').show();
            $('#suratkematian').show();
            $('#suratektp').show();
        } else {
            $('#suratkuasa').hide();
            $('#suratdomisili').hide();
            $('#suratkip').hide();
            $('#suratsktm').hide();
            $('#suratkeramaian').hide();
            $('#suratbiodata').hide();
            $('#suratsptjm').hide();
            $('#suratskck').hide();
            $('#suratkk').hide();
            $('#suratkelahiran').hide();
            $('#suratkematian').hide();
            $('#suratektp').hide();
        }
    });
</script>
</body>
</html>