<!doctype html>
<html class="no-js" lang="en">

<head>
    <?php $this->load->view("Warga/_Partials/Head") ?>
</head>

<body>
<div class="loader"><div class="page-loader"></div></div>

<nav class="navbar navbar-expand-lg navbar-light mb-4">
    <?php $this->load->view("Warga/_Partials/Header") ?>
</nav>

<!-- Page Content -->
<div class="page-content"> 
    <h1 class="text-center mb-5">Riwayat Ajuan Surat</h1>
    <div class="example col-md-10 ml-auto mr-auto">
        <div class="row">
        </div>
    </div>
    <!-- Typography -->
        <div id="typography" class="container mb-5">
            <div class="example col-md-10 ml-auto mr-auto">
                <table class="table table-striped table-responsive">
                    <thead class="text-center">
                    <tr>
                        <th>No.</th>
                        <th>Nomor Surat</th>
                        <th>Jenis Surat</th>
                        <th>Tanggal Diajukan</th>
                        <th>Tanggal Selesai</th>
                        <th>Status</th>
                        <th>Alasan Ditolak</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php 
                        $no = 1;
                        foreach($surat as $data):?>
                        <tr>
                            <td class="text-center"><?= $no++ ?></td>
                            <td><?= $data->no_surat ?></td>
                            <?php if(substr($data->id_surat,0,7) == 'biodata'){ ?>
                                <td>Surat Keterangan Biodata</td>
                            <?php }elseif(substr($data->id_surat,0,8) == 'domisili'){ ?>
                                <td>Surat Keterangan Domisili</td>
                            <?php }elseif(substr($data->id_surat,0,9) == 'keramaian') { ?>
                                <td>Surat Ijin Keramaian</td>
                            <?php }elseif(substr($data->id_surat,0,9) == 'kelahiran'){ ?>
                                <td>Surat Kelahiran</td>
                            <?php }elseif(substr($data->id_surat,0,8) == 'kematian'){ ?>
                                <td>Surat Keterangan Kematian</td>
                            <?php }elseif(substr($data->id_surat,0,3) == 'kip'){ ?>
                                <td>Surat Pengajuan KIP</td>
                            <?php }elseif(substr($data->id_surat,0,5) == 'Kuasa'){ ?>
                                <td>Surat Kuasa</td>
                            <?php }elseif(substr($data->id_surat,0,3) == 'ktp'){ ?>
                                <td>Surat Permohonan EKTP</td>
                            <?php }elseif(substr($data->id_surat,0,2) == 'kk'){ ?>
                                <td>Surat Permohonan Perubahan KK</td>
                            <?php }elseif(substr($data->id_surat,0,4) == 'skck'){ ?>
                                <td>Surat Pengantar SKCK</td>
                            <?php }elseif(substr($data->id_surat,0,4) == 'sktm'){ ?>
                                <td>Surat Pengajuan SKTM</td>
                            <?php }elseif(substr($data->id_surat,0,5) == 'sptjm'){ ?>
                                <td>Surat Pengajuan SPTJM</td>
                            <?php }else{echo "data gagal";} ?>
                            <td><?= format_indo($data->tgl_surat) ?></td>
                            <td><?= format_indo($data->tgl_riwayat) ?></td>
                            <?php if($data->riwayat_status=='7'){?>
                            <td><span class="badge badge-danger">Surat Ditolak</span></td>
                            <?php }elseif($data->riwayat_status=='6'){?>
                                <td><span class="badge badge-success">Surat Selesai</span></td>
                            <?php } ?>
                            <td><?= $data->ket_tolak ?></td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    <!-- Footer CTA -->
    <footer class="main-footer py-5" style="background:black;">
        <?php $this->load->view("Warga/_Partials/Footer") ?>
    </footer>
</div>

<!-- JavaScript -->
<?php $this->load->view("Warga/_Partials/Js") ?>
</body>
</html>