<!doctype html>
<html class="no-js" lang="en">

<head>
    <?php $this->load->view("Warga/_Partials/Head") ?>
</head>

<body>
<div class="loader"><div class="page-loader"></div></div>

<nav class="navbar navbar-expand-lg navbar-light mb-4">
    <?php $this->load->view("Warga/_Partials/Header") ?>
</nav>

<!-- Page Content -->
<div class="page-content"> 
    <h1 class="text-center mb-5">Surat Ajuan Anda</h1>
    <div class="example col-md-10 ml-auto mr-auto">
        <div class="row">
        </div>
    </div>
    <!-- Typography -->
        <div id="typography" class="container mb-5">
            <div class="example col-md-10 ml-auto mr-auto">
                <table class="table table-striped table-responsive">
                    <thead class="text-center">
                    <tr>
                        <th>No.</th>
                        <th>Nomor Surat</th>
                        <th>Jenis Surat</th>
                        <th>Tanggal Diajukan</th>
                        <th>Status Surat</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php 
                        $no = 1;
                        foreach($surat as $data):?>
                        <tr>
                            <td class="text-center"><?= $no++ ?></td>
                            <td><?= $data->no_surat ?></td>
                            <?php if(substr($data->id_surat,0,7) == 'biodata'){ ?>
                                <td>Surat Keterangan Biodata</td>
                            <?php }elseif(substr($data->id_surat,0,8) == 'domisili'){ ?>
                                <td>Surat Keterangan Domisili</td>
                            <?php }elseif(substr($data->id_surat,0,9) == 'keramaian') { ?>
                                <td>Surat Ijin Keramaian</td>
                            <?php }elseif(substr($data->id_surat,0,9) == 'kelahiran'){ ?>
                                <td>Surat Kelahiran</td>
                            <?php }elseif(substr($data->id_surat,0,8) == 'kematian'){ ?>
                                <td>Surat Keterangan Kematian</td>
                            <?php }elseif(substr($data->id_surat,0,3) == 'kip'){ ?>
                                <td>Surat Pengajuan KIP</td>
                            <?php }elseif(substr($data->id_surat,0,5) == 'Kuasa'){ ?>
                                <td>Surat Kuasa</td>
                            <?php }elseif(substr($data->id_surat,0,3) == 'ktp'){ ?>
                                <td>Surat Permohonan EKTP</td>
                            <?php }elseif(substr($data->id_surat,0,2) == 'kk'){ ?>
                                <td>Surat Permohonan Perubahan KK</td>
                            <?php }elseif(substr($data->id_surat,0,4) == 'skck'){ ?>
                                <td>Surat Pengantar SKCK</td>
                            <?php }elseif(substr($data->id_surat,0,4) == 'sktm'){ ?>
                                <td>Surat Pengajuan SKTM</td>
                            <?php }elseif(substr($data->id_surat,0,5) == 'sptjm'){ ?>
                                <td>Surat Pengajuan SPTJM</td>
                            <?php }else{echo "data gagal";} ?>
                            <td><?= format_indo($data->tgl_surat) ?></td>
                            <?php if($data->status_monitoring=='Menunggu Persetujuan'){?>
                            <td><span class="badge badge-secondary"><?= $data->status_monitoring ?></span></td>
                            <?php }elseif($data->status_monitoring=='Menunggu Diproses'){?>
                                <td><span class="badge badge-warning"><?= $data->status_monitoring ?></span></td>
                            <?php }elseif($data->status_monitoring=='Surat Diproses'){?>
                                <td><span class="badge badge-info"><?= $data->status_monitoring ?></span></td>
                            <?php }elseif($data->status_monitoring=='Menunggu Di TTD Kades'){?>
                                <td><span class="badge badge-primary"><?= $data->status_monitoring ?></span></td>
                            <?php }elseif($data->status_monitoring=='Surat Belum Diunduh'){?>
                                <td><span class="badge badge-light"><?= $data->status_monitoring ?></span></td>
                            <?php }else{echo "";} ?>
                            <td class="text-center">
                            <?php if(substr($data->id_surat,0,7) == 'biodata'&&$data->id_status=='5'){ ?>
                                <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Unduh Surat" 
                                href="<?php echo base_url('Monitoring/Surat/Ajuan_biodata/cetak/'.$data->id_surat) ?>">
                                    <i class="fa fa-download"></i>
                                </a>   
                            <?php }elseif(substr($data->id_surat,0,8) == 'domisili'&&$data->id_status=='5'){ ?>
                                <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Unduh Surat" 
                                href="<?php echo base_url('Monitoring/Surat/Ajuan_domisili/cetak/'.$data->id_surat) ?>">
                                    <i class="fa fa-download"></i>
                                </a>   
                            <?php }elseif(substr($data->id_surat,0,9) == 'keramaian'&&$data->id_status=='5') { ?>
                                <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Unduh Surat" 
                                href="<?php echo base_url('Monitoring/Surat/Ajuan_keramaian/cetak/'.$data->id_surat) ?>">
                                    <i class="fa fa-download"></i>
                                </a>   
                            <?php }elseif(substr($data->id_surat,0,9) == 'kelahiran'&&$data->id_status=='5'){ ?>
                                <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Unduh Surat" 
                                href="<?php echo base_url('Monitoring/Surat/Ajuan_kelahiran/cetak/'.$data->id_surat) ?>">
                                    <i class="fa fa-download"></i>
                                </a>   
                            <?php }elseif(substr($data->id_surat,0,8) == 'kematian'&&$data->id_status=='5'){ ?>
                                <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Unduh Surat" 
                                href="<?php echo base_url('Monitoring/Surat/Ajuan_kematian/cetak/'.$data->id_surat) ?>">
                                    <i class="fa fa-download"></i>
                                </a>   
                            <?php }elseif(substr($data->id_surat,0,3) == 'kip'&&$data->id_status=='5'){ ?>
                                <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Unduh Surat" 
                                href="<?php echo base_url('Monitoring/Surat/Ajuan_kip/cetak/'.$data->id_surat) ?>">
                                    <i class="fa fa-download"></i>
                                </a>   
                            <?php }elseif(substr($data->id_surat,0,5) == 'Kuasa'&&$data->id_status=='5'){ ?>
                                <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Unduh Surat" 
                                href="<?php echo base_url('Monitoring/Surat/Ajuan_kuasa/cetak/'.$data->id_surat) ?>">
                                    <i class="fa fa-download"></i>
                                </a>   
                            <?php }elseif(substr($data->id_surat,0,3) == 'ktp'&&$data->id_status=='5'){ ?>
                                <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Unduh Surat" 
                                href="<?php echo base_url('Monitoring/Surat/Ajuan_ktp/cetak/'.$data->id_surat) ?>">
                                    <i class="fa fa-download"></i>
                                </a>    
                            <?php }elseif(substr($data->id_surat,0,2) == 'kk'&&$data->id_status=='5'){ ?>
                                <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Unduh Surat" 
                                href="<?php echo base_url('Monitoring/Surat/Ajuan_kk/cetak/'.$data->id_surat) ?>">
                                    <i class="fa fa-download"></i>
                                </a>   
                            <?php }elseif(substr($data->id_surat,0,4) == 'skck'&&$data->id_status=='5'){ ?>
                                <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Unduh Surat" 
                                href="<?php echo base_url('Monitoring/Surat/Ajuan_skck/cetak/'.$data->id_surat) ?>">
                                    <i class="fa fa-download"></i>
                                </a>   
                            <?php }elseif(substr($data->id_surat,0,4) == 'sktm'&&$data->id_status=='5'){ ?>
                                <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Unduh Surat" 
                                href="<?php echo base_url('Monitoring/Surat/Ajuan_sktm/cetak/'.$data->id_surat) ?>">
                                    <i class="fa fa-download"></i>
                                </a>   
                            <?php }elseif(substr($data->id_surat,0,5) == 'sptjm'&&$data->id_status=='5'){ ?>
                                <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Unduh Surat" 
                                href="<?php echo base_url('Monitoring/Surat/Ajuan_sptjm/cetak/'.$data->id_surat) ?>">
                                    <i class="fa fa-download"></i>
                                </a>   
                            <?php }else{echo "";} ?>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    <!-- Footer CTA -->
    <footer class="main-footer py-5" style="background:black;">
        <?php $this->load->view("Warga/_Partials/Footer") ?>
    </footer>
</div>

<!-- JavaScript -->
<div id="fb-root"></div>
<?php $this->load->view("Warga/_Partials/Js") ?>
</body>
</html>