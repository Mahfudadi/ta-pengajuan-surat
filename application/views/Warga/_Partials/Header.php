<img src="<?php echo base_url('assets/images/demo/logo_pemkabkediri.png')?>"  class="mr-2" height="70px">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown-1"
            aria-controls="navbarNavDropdown-1"
            aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown-1">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('index.php/Validasi/Page') ?>">Beranda
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('index.php/Warga/Jenis_Surat/Jenis') ?>">Jenis Surat</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('index.php/Warga/Monitor/Monitoring') ?>">Monitoring</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('index.php/Warga/Riwayat/RiwayatSurat') ?>">Riwayat</a>
            </li>
            
        </ul>
        <ul class="navbar-nav">
            <li>
                <a href="#" class="nav-link"> <?php date_default_timezone_set('Asia/Jakarta'); echo format_indo(date('Y-m-d H:i:s'));?>
                </a>
            </li>
        </ul>
        ||
        <ul class="navbar-nav">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle fa fa-lg fa-user" href="https://designrevision.com" id="navbarDropdownMenuLink"
                    data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false"> <?=$this->session->userdata('username');?>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item fa fa-user" href="<?php echo base_url('index.php/Warga/Profil_Warga/ProfilWarga/index/'.$this->session->userdata('ses_id')) ?>"> Profil</a>
                    <a class="dropdown-item fa fa-power-off" href="<?php echo base_url('index.php/validasi/login/logout'); ?>"> Logout</a>
                </div>
            </li>
        </ul>
    </div>