<!doctype html>
<html class="no-js" lang="en">

<head>
    <?php $this->load->view("Warga/_Partials/Head") ?>
</head>

<body>
<div class="loader">
    <div class="page-loader"></div>
</div>

<!-- Welcome Section -->
<div class="welcome d-flex justify-content-center flex-column">
    
    <nav class="navbar navbar-expand-lg navbar-light bg-white mb-4">
        <?php $this->load->view("Warga/_Partials/Header") ?>
    </nav>
    <div class="inner-wrapper mt-auto mb-auto">
        <h1 class="slide-in">Pengajuan Surat</h1>
        <p class="slide-in">Online</p>
        <div class="action-links slide-in">
          <a href="<?php echo base_url('index.php/Warga/Jenis_Surat/Jenis') ?>" class="btn btn-primary btn-pill align-self-center mr-2"><i class="fa fa-envelope mr-1"></i> Ajukan Surat</a>
        </div>
    </div>
</div>

<!-- Page Content -->
<div class="page-content"> 
    <!-- Footer CTA -->
    <footer class="main-footer py-5" style="background:black;">
        <?php $this->load->view("Warga/_Partials/Footer") ?>
    </footer>
</div>

<!-- JavaScript -->
<div id="fb-root"></div>
<?php $this->load->view("Warga/_Partials/Js") ?>
</body>
</html>
