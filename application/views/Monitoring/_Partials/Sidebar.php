<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image" style="margin-left: 15px">
                        <img src="<?php echo base_url('assets/images/icon/user.png') ?>" class="img-circle elevation-2" alt="User Image" style="width: 50px">
                    </div>
                    <div class="">
                        <div class="info" style="margin-left: 12px">
                            <a href="<?php echo base_url('Monitoring/Admin/Data_admin')?>" class="d-block text-light" style="margin-bottom:0px"><span class="badge badge-info"><?php echo $user['id_user']; ?></a>
                            <p class="d-block text-light"><span class="badge" style="background-color:indigo;"><?php echo $user['nip']; ?></p>
                        </div>
                    </div>
                </div>
                <a class="nav-link" href="<?php echo base_url ('Monitoring/Beranda')?>">
                    <div class="sb-nav-link-icon"><i class="fas fa-home"></i></div>
                    Beranda
                </a>
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-folder-open"></i></div>
                    Master
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Master/Data_warga')?>">Warga</a>
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Master/Data_perangkat')?>"></i>Perangkat Desa</a> 
                    </nav>
                </div>
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#surat" aria-expanded="false" aria-controls="surat">
                    <div class="sb-nav-link-icon"><i class="fas fa-folder-open"></i></div>
                    Surat
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="surat" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Surat/Semua_surat')?>" style="padding-bottom:2px;padding-top:2px">Semua Surat</a>
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Surat/Ajuan_biodata')?>" style="padding-bottom:2px;padding-top:2px">Biodata</a>
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Surat/Ajuan_domisili')?>" style="padding-bottom:2px;padding-top:2px">Domisili</a> 
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Surat/Ajuan_keramaian')?>" style="padding-bottom:2px;padding-top:2px">Izin Keramaian</a>
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Surat/Ajuan_kelahiran')?>" style="padding-bottom:2px;padding-top:2px">Kelahiran</a> 
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Surat/Ajuan_kematian')?>" style="padding-bottom:2px;padding-top:2px">Kematian</a>
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Surat/Ajuan_kip')?>" style="padding-bottom:2px;padding-top:2px">KIP</a> 
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Surat/Ajuan_kuasa')?>" style="padding-bottom:2px;padding-top:2px">Kuasa</a>
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Surat/Ajuan_ktp')?>" style="padding-bottom:2px;padding-top:2px">Permohonan KTP</a> 
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Surat/Ajuan_kk')?>" style="padding-bottom:2px;padding-top:2px">Perubahan KK</a>
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Surat/Ajuan_skck')?>" style="padding-bottom:2px;padding-top:2px">SKCK</a> 
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Surat/Ajuan_sktm')?>" style="padding-bottom:2px;padding-top:2px">SKTM</a>
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Surat/Ajuan_sptjm')?>" style="padding-bottom:2px;padding-top:2px">SPTJM</a> 
                    </nav>
                </div>
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#laporan" aria-expanded="false" aria-controls="surat">
                    <div class="sb-nav-link-icon"><i class="fas fa-folder-open"></i></div>
                    Laporan
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="laporan" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Laporan/Laporan_semua')?>" style="padding-bottom:2px;padding-top:2px">Semua Surat</a>
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Laporan/Laporan_biodata')?>" style="padding-bottom:2px;padding-top:2px">Biodata</a>
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Laporan/Laporan_domisili')?>" style="padding-bottom:2px;padding-top:2px">Domisili</a> 
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Laporan/Laporan_keramaian')?>" style="padding-bottom:2px;padding-top:2px">Izin Keramaian</a>
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Laporan/Laporan_kelahiran')?>" style="padding-bottom:2px;padding-top:2px">Kelahiran</a> 
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Laporan/Laporan_kematian')?>" style="padding-bottom:2px;padding-top:2px">Kematian</a>
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Laporan/Laporan_kip')?>" style="padding-bottom:2px;padding-top:2px">KIP</a> 
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Laporan/Laporan_kuasa')?>" style="padding-bottom:2px;padding-top:2px">Kuasa</a>
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Laporan/Laporan_ktp')?>" style="padding-bottom:2px;padding-top:2px">Permohonan KTP</a> 
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Laporan/Laporan_kk')?>" style="padding-bottom:2px;padding-top:2px">Perubahan KK</a>
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Laporan/Laporan_skck')?>" style="padding-bottom:2px;padding-top:2px">SKCK</a> 
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Laporan/Laporan_sktm')?>" style="padding-bottom:2px;padding-top:2px">SKTM</a>
                        <a class="nav-link" href="<?php echo base_url('Monitoring/Laporan/Laporan_sptjm')?>" style="padding-bottom:2px;padding-top:2px">SPTJM</a> 
                    </nav>
                </div>
            </div>
        </div>
        <div class="sb-sidenav-footer">
            <div class="small">Pemerintah</div>
            Desa Gurah
        </div>
    </nav>
</div>