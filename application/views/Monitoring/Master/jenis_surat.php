<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("Monitoring/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("Monitoring/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("Monitoring/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Jenis Surat</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Master</li>
                            <li class="breadcrumb-item active">Jenis Surat</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                Jenis Surat
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead class="text-center">
                                            <tr>
                                                <th style="width: 5%">No</th>
                                                <th>Jenis Surat</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php
                                                $no=1;
                                                foreach ($jenissurat as $js) : ?>
                                                <tr>
                                                    <td><?php echo $no++ ?></td>
                                                    <td><?php echo $js->jenis_surat ?></td>
                                                </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("Monitoring/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("Monitoring/_Partials/Js.php") ?>
    </body>
</html>