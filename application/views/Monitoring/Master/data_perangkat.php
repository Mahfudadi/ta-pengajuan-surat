<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("Monitoring/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("Monitoring/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("Monitoring/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 align="center" class="mt-4">Data Perangkat Desa</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Master</li>
                            <li class="breadcrumb-item active">Perangkat Desa</li>
                        </ol>
                        <!-- <a class="btn btn-primary"><i class="fas fa-plus"> </i>Tambah Ketua RT</a> -->
                        <hr>
                        <!-- Alert -->
                        <div class="flashdata" data-flashdata="<?= $this->session->flashdata('pesan') ?>"> </div>
                        <!-- Akhir Alert -->
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                Data Ketua RT
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead class="text-center">
                                            <tr>
                                                <th>Ketua RT</th>
                                                <th>Nama Ketua RT</th>
                                                <th >Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php
                                                foreach ($rt as $r) : ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $r->ketua_rt ?></td>
                                                    <td><?php echo $r->nama_rt ?></td>
                                                    <td class="text-center">
                                                        <a class="btn btn-sm btn-warning " data-toggle="tooltip" data-placement="top" title="Detail & Edit" href="<?php echo base_url('Monitoring/Master/data_perangkat/edit_rt/'.$r->id_rt) ?>"><i class="fas fa-edit"></i></a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                Data Kepala Desa Gurah
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" width="100%" cellspacing="0">
                                        <thead class="text-center">
                                            <tr>
                                                <th>NIP Kepala Desa</th>
                                                <th>Nama Kepala Desa</th>
                                                <th>Alamat</th>
                                                <th >Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php
                                                foreach ($kades as $kd) : ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $kd->nip ?></td>
                                                    <td><?php echo $kd->nama ?></td>
                                                    <td><?php echo $kd->alamat ?></td>
                                                    <td class="text-center">
                                                        <a class="btn btn-sm btn-warning " data-toggle="tooltip" data-placement="top" title="Detail & Edit" href="<?php echo base_url('Monitoring/Master/data_perangkat/edit_kades/'.$kd->id_perangkat) ?>"><i class="fas fa-edit"></i></a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                Data Camat Gurah
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" width="100%" cellspacing="0">
                                        <thead class="text-center">
                                            <tr>
                                                <th>NIP Camat</th>
                                                <th>Nama Camat</th>
                                                <th>Alamat</th>
                                                <th >Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php
                                                foreach ($camat as $cm) : ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $cm->nip ?></td>
                                                    <td><?php echo $cm->nama ?></td>
                                                    <td><?php echo $cm->alamat ?></td>
                                                    <td class="text-center">
                                                        <a class="btn btn-sm btn-warning " data-toggle="tooltip" data-placement="top" title="Detail & Edit" href="<?php echo base_url('Monitoring/Master/data_perangkat/edit_camat/'.$cm->id_perangkat) ?>"><i class="fas fa-edit"></i></a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                Data Kapolsek Gurah
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" width="100%" cellspacing="0">
                                        <thead class="text-center">
                                            <tr>
                                                <th>NIP Kapolsek</th>
                                                <th>Nama Kapolsek</th>
                                                <th>Alamat</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php
                                                foreach ($kapolsek as $kp) : ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $kp->nip ?></td>
                                                    <td><?php echo $kp->nama ?></td>
                                                    <td><?php echo $kp->alamat ?></td>
                                                    <td class="text-center">
                                                        <a class="btn btn-sm btn-warning " data-toggle="tooltip" data-placement="top" title="Detail & Edit" href="<?php echo base_url('Monitoring/Master/data_perangkat/edit_kapolsek/'.$kp->id_perangkat) ?>"><i class="fas fa-edit"></i></a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("Monitoring/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("Monitoring/_Partials/Js.php") ?>
    </body>
</html>