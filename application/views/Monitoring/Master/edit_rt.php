<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("Monitoring/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("Monitoring/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("Monitoring/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Edit Ketua RT</h1>
                        <hr>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                Form Edit Data Ketua RT
                            </div>
                            <div class="card-body">
                                <form class="form-horizontal style-form" action="<?php echo base_url().'Monitoring/Master/data_perangkat/update_rt' ?>" method="post">
                                    <?php foreach ($editrt as $rt) : ?>  
                                    <input name="id_rt" class="form-control" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')" value="<?php echo $rt->id_rt ?>" hidden>
                                    <div class="form-group row">
                                        <div class="col-12 col-lg-1"></div>
                                        <label class="col-12 col-lg-2 col-form-label text-sm-left">Ketua RT</label>
                                        <div class="col-12 col-lg-8">
                                                <input name="ketua_rt" id="ketua" class="form-control" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')" value="<?php echo $rt->ketua_rt ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12 col-lg-1"></div>
                                        <label class="col-12 col-lg-2 col-form-label text-sm-left">NIK</label>
                                        <div class="col-12 col-lg-8">
                                                <input name="nik" id="nik" class="form-control" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')" value="<?php echo $rt->nik ?>">
                                        </div>
                                    </div>                                 
                                    <div class="form-group row">
                                        <div class="col-12 col-lg-1"></div>
                                        <label class="col-12 col-lg-2 col-form-label text-sm-left">Nama Ketua RT</label>
                                        <div class="col-12 col-lg-8">
                                            <input name="nama_rt" id="nama" class="form-control" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')" value="<?php echo $rt->nama_rt ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12 col-lg-1"></div>
                                        <label class="col-12 col-lg-2 col-form-label text-sm-left">Alamat</label>
                                        <div class="col-12 col-lg-8">
                                                <textarea name="alamat_rt" id="alamat" class="form-control" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')"><?php echo $rt->alamat_rt ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col col-lg-1"></div>
                                        <div class="col col-lg-5">
                                            <button type="submit" class="btn btn-outline-success btn-block"><i class="fas fa-edit"></i> Ubah</button>
                                        </div>
                                        <div class="col col-lg-5">
                                            <a href="<?php echo base_url('Monitoring/master/data_perangkat') ?>" class="btn btn-outline-danger btn-block"><i class="fas fa-times"></i> Batal</a>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                </form>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("Monitoring/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("Monitoring/_Partials/Js.php") ?>
    </body>
</html>