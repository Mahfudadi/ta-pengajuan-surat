<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("Monitoring/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("Monitoring/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("Monitoring/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Data Warga</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Master</li>
                            <li class="breadcrumb-item active">Warga</li>
                            <li class="breadcrumb-item active">Detail Warga</li>
                        </ol>
                        <hr>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-user mr-1"></i>
                                <?php foreach ($detail as $dt) : ?>
                                <?php echo $dt->nama_lengkap ?>
                                <?php endforeach;?>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                <div class="col-7">
                                    <table class="table-form" border="0" width="70%" cellpadding="2" cellspacing="0" >
                                        <?php
                                        $no=1;
                                        foreach ($detail as $dt) : ?>
                                        <thead>
                                            <tr>
                                                <th>No. KK</th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $dt->no_kk ?></td>
                                                </tr>
                                            </tbody>
                                        <thead>
                                            <tr>
                                                <th> NIK </th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $dt->nik ?></td>
                                                </tr>
                                            </tbody>
                                        <thead>
                                            <tr>
                                                <th>Nama Lengkap</th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $dt->nama_lengkap ?></td>
                                                </tr>
                                            </tbody>
                                        <thead>
                                            <tr>
                                                <th>Tempat, Tanggal Lahir </th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $dt->tempat_lahir ?>, <?php echo tgl_indo($dt->tanggal_lahir) ?></td>
                                                </tr>
                                            </tbody>
                                        <thead>    
                                            <tr>
                                                <th > Jenis Kelamin </th>
                                                
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <td><?php echo $dt->jenis_kelamin ?></td>
                                            </tbody>
                                        <thead>
                                            <tr>
                                                <th>Umur</th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <tr>
                                                     <td><?php echo $dt->umur ?></td>
                                            </tr>
                                            </tbody>
                                        <thead>
                                            <tr>
                                                <th>Golongan Darah</th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $dt->gol_darah ?></td>
                                                </tr>
                                            </tbody>
                                        <thead>
                                            <tr>
                                                <th>Penyandang Cacat</th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $dt->penyandang_cacat ?></td>
                                                </tr>
                                            </tbody>
                                        <thead>
                                            <tr>
                                                <th>Agama</th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $dt->agama ?></td>
                                                </tr>
                                            </tbody>
                                        <thead>
                                            <tr>
                                                <th>Status Kawin</th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $dt->status_kawin ?></td>
                                                </tr>
                                            </tbody>
                                        <thead>
                                            <tr>
                                                <th> Pendidikan </th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $dt->pendidikan ?></td>
                                                </tr>
                                            </tbody>
                                        <thead>
                                            <tr>
                                                <th> Pekerjaan </th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $dt->pekerjaan ?></td>
                                                </tr>
                                            </tbody>
                                        <thead>
                                            <tr>
                                                <th> Pekerjaan </th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $dt->pekerjaan ?></td>
                                                </tr>
                                            </tbody>
                                        <thead>
                                            <tr>
                                                <th> Alamat </th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $dt->alamat ?></td>
                                                </tr>
                                            </tbody>
                                        <thead>
                                            <tr>
                                                <th> Dusun </th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $dt->dusun ?></td>
                                                </tr>
                                            </tbody>
                                        <thead>
                                            <tr>
                                                <th> RW</th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $dt->rw ?></td>
                                                </tr>
                                            </tbody>
                                        <thead>
                                            <tr>
                                                <th> RT </th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $dt->rt ?></td>
                                                </tr>
                                            </tbody>
                                        <thead>
                                            <tr>
                                                <th> Desa</th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $dt->desa ?></td>
                                                </tr>
                                            </tbody>
                                        <thead>
                                            <tr>
                                                <th> Kecamatan </th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $dt->kecamatan ?></td>
                                                </tr>
                                            </tbody>
                                        <thead>
                                            <tr>
                                                <th> Kabupaten / Kota </th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $dt->kab_kota ?></td>
                                                </tr>
                                            </tbody>
                                        <thead>
                                            <tr>
                                                <th> Provinsi </th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $dt->prov ?></td>
                                                </tr>
                                            </tbody>
                                        <thead>
                                            <tr>
                                                <th> Warga Negara</th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $dt->warga_negara ?></td>
                                                </tr>
                                            </tbody>
                                            <?php endforeach;?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("Monitoring/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("Monitoring/_Partials/Js.php") ?>
    </body>
</html>