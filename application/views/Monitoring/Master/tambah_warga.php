<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("Monitoring/_Partials/Head.php") ?>
    </head>
    <?php
        $conn = mysqli_connect('localhost','root','','surat_desagurah');
        $cari_id = mysqli_query($conn, "SELECT MAX(id_user) AS max_id FROM tb_warga WHERE id_user LIKE 'Wrg%'") or die (mysql_error());
        $data_id = mysqli_fetch_array($cari_id);
        $id_warga = $data_id['max_id'];

        $noUrut = (int) substr($id_warga, 4, 4);
        $noUrut++;
        $char = "Wrg";
        $newID = $char . sprintf("%02s", $noUrut);
        date_default_timezone_set('Asia/Jakarta');

    ?>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("Monitoring/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("Monitoring/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Tambah Data Warga</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Master</li>
                            <li class="breadcrumb-item active">Data Warga</li>
                            <li class="breadcrumb-item active">Tambah Data Warga</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                Form Tambah Data Warga
                            </div>
                            <div class="card-body">
                                    <form class="form-horizontal style-form" action="<?php echo base_url().'Monitoring/Master/Data_warga/tambah_datawarga' ?>" method="post">
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-left">ID User</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                    <input name="id_user" id="id_user" class="form-control" value="<?php echo $newID ?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-left">No. KK</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                    <input name="no_kk" id="no_kk" class="form-control" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-left">NIK</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                    <input name="nik" id="nik" class="form-control" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-left">Nama</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                    <input name="nama" id="nama" class="form-control" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-left">Tempat Lahir</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                    <input name="tempat" id="tempat" class="form-control" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-left">Tanggal Lahir</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                    <input type="date" name="tanggal" id="tanggal" class="form-control" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-left">Jenis Kelamin</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                            <select class="custom-select d-block col-md-3" id="jenis_kelamin" name="jenis_kelamin" required>
                                                <option value="Laki-Laki">Laki-Laki</option>
                                                <option value="Perempuan">Perempuan</option>
                                            </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-left">Umur</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                    <input name="umur" id="umur" class="form-control" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-left">Golongan Darah</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                    <input name="golongan" id="golongan" class="form-control"  required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-left">Penyandang Cacat</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                    <input name="penyandang" id="penyandang" class="form-control"  required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-left">Agama</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                            <select class="custom-select d-block col-md-12" id="agama" name="agama" required>
                                                <option value="Islam">Islam</option>
                                                <option value="Kristen">Kristen</option>
                                                <option value="Kristen">Katolik</option>
                                                <option value="Hindu">Hindu</option>
                                                <option value="Buddha">Buddha</option>
                                                <option value="Konghucu">Konghucu</option>
                                            </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-left">Status</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                            <select class="custom-select d-block col-md-12" id="status" name="status" required>
                                                <option value="Belum Kawin">Belum Kawin</option>
                                                <option value="Kawin">Kawin</option>
                                                <option value="Cerai Hidup">Cerai Hidup</option>
                                                <option value="Cerai Mati">Cerai Mati</option>
                                            </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-left">Pendidikan</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                    <input name="pendidikan" id="pendidikan" class="form-control" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-left">Pekerjaan</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                    <input name="pekerjaan" id="pekerjaan" class="form-control" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-left">Alamat</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                <textarea name="alamat" id="alamat" class="form-control" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-left">Dusun</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                    <input name="dusun" id="dusun" class="form-control" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-left">RW</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                    <input name="rw" id="rw" class="form-control" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-left">RT</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                    <input name="rt" id="rt" class="form-control" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-left">Desa</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                    <input name="desa" id="desa" class="form-control" value="Gurah" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-left">Kecamatan</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                    <input name="kecamatan" id="kecamatan" class="form-control" value="Gurah"required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-left">Kota/Kabupaten</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                    <input name="kab_kota" id="kab_kota" class="form-control" value="Kediri"required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-left">Provinsi</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                    <input name="prov" id="prov" class="form-control" value="Jawa Timur" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-form-label text-sm-left">Warga Negara</label>
                                            <div class="col-12 col-sm-8 col-lg-6">
                                                <input name="warga_negara" id="warga_negara" class="form-control" value="Indonesia" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-12 col-lg-6">
                                                <button type="submit" class="btn btn-space btn-success btn-block">Simpan</button>
                                            </div>
                                            <div class="col-12 col-lg-6">
                                                <a class="btn btn-space btn-danger btn-block" href="<?= base_url('monitoring/master/data_warga') ?>">Batal</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("Monitoring/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("Monitoring/_Partials/Js.php") ?>
    </body>
</html>