<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("Monitoring/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("Monitoring/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("Monitoring/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Edit Kapolsek Gurah</h1>
                        <hr>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                Form Edit Data Kapolsek Gurah
                            </div>
                            <div class="card-body">
                                <form class="form-horizontal style-form" action="<?php echo base_url().'Monitoring/Master/data_perangkat/update' ?>" method="post">
                                    <?php foreach ($kapolsek as $kp) : ?>                                    
                                    <div class="form-group row">
                                        <div class="col col-lg-1"></div>
                                        <label class="col-12 col-lg-2 col-form-label text-sm-left">NIP</label>
                                        <div class="col-12 col-lg-8">
                                                <input name="nip" id="nip" class="form-control" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')" value="<?php echo $kp->nip ?>">
                                                <input name="id_perangkat" id="id_perangkat" class="form-control" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')" value="<?php echo $kp->id_perangkat ?>" hidden>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col col-lg-1"></div>
                                        <label class="col-12 col-lg-2 col-form-label text-sm-left">Nama Kapolsek Gurah</label>
                                        <div class="col-12 col-lg-8">
                                                <input name="nama" id="nama" class="form-control" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')" value="<?php echo $kp->nama ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col col-lg-1"></div>
                                        <label class="col-12 col-lg-2 col-form-label text-sm-left">Alamat</label>
                                        <div class="col-12 col-lg-8">
                                                <input name="alamat" id="alamat" class="form-control" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')" value="<?php echo $kp->alamat ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col col-lg-1"></div>
                                        <div class="col col-lg-5">
                                            <button type="submit" class="btn btn-outline-success btn-block"><i class="fas fa-edit"></i> Ubah</button>
                                        </div>
                                        <div class="col col-lg-5">
                                            <a href="<?php echo base_url('Monitoring/master/data_perangkat') ?>" class="btn btn-outline-danger btn-block"><i class="fas fa-times"></i> Batal</a>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                </form>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("Monitoring/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("Monitoring/_Partials/Js.php") ?>
    </body>
</html>