<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("Monitoring/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("Monitoring/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("Monitoring/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Data Warga</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Master</li>
                            <li class="breadcrumb-item active">Warga</li>
                        </ol>
                        <a class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Tambah" href="<?php echo base_url().'Monitoring/Master/Data_warga/tambahwarga'?>">
                            <i class="fa fa-plus"></i> Tambah Warga
                        </a>
                        <div class="flashdata" data-flashdata="<?= $this->session->flashdata('pesan') ?>"> </div>
                        <hr>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                Data Warga
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead class="text-center">
                                            <tr>
                                                <th>No</th>
                                                <th>No KK</th>
                                                <th>NIK</th>
                                                <th>Nama Lengkap</th>
                                                <th>RW</th>
                                                <th>RT</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php
                                                $no=1;
                                                foreach ($warga as $wg) : ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $no++ ?></td>
                                                    <td><?php echo $wg->no_kk ?></td>
                                                    <td><?php echo $wg->nik ?></td>
                                                    <td><?php echo $wg->nama_lengkap ?></td>
                                                    <td><?php echo $wg->rw ?></td>
                                                    <td><?php echo $wg->rt ?></td>
                                                    <td class="text-center">
                                                        <a class="btn btn-sm btn-success " data-toggle="tooltip" data-placement="top" title="Detail" href="<?php echo base_url('Monitoring/Master/Data_warga/detail/'.$wg->id_user) ?>"><i class="fas fa-file"></i></a>
                                                        <a class="btn btn-sm btn-warning " data-toggle="tooltip" data-placement="top" title="Edit" href="<?php echo base_url('Monitoring/Master/Data_warga/edit/'.$wg->id_user) ?>"><i class="fas fa-edit"></i></a>
                                                        <a class="btn btn-sm btn-danger tombol-hapus" data-toggle="tooltip" data-placement="top" title="Hapus" href="<?php echo base_url('Monitoring/Master/Data_warga/hapus/'.$wg->id_user) ?>">
                                                            <i class="fa fa-trash"></i>
                                                        </a> 
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("Monitoring/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("Monitoring/_Partials/Js.php") ?>
    </body>
</html>