<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("Monitoring/_Partials/Head.php") ?>
</head>

<body class="sb-nav-fixed">
	<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
		<?php $this->load->view("Monitoring/_Partials/Header.php") ?>
	</nav>
	<div id="layoutSidenav">
		<?php $this->load->view("Monitoring/_Partials/Sidebar.php") ?>
		<div id="layoutSidenav_content">
			<main>
				<div class="container-fluid">
					<h1 class="mt-2">Beranda</h1>
					<div class="alert alert-secondary" role="alert">
						<div class="row">
							<div class="col-md-8">
								<h5>Selamat Datang, <strong><?php echo $user['nama_admin']; ?></strong></h5>
							</div>
							<div class="col-md-4">
								<button type="button" class="btn btn-secondary btn-sm float-right ml-1" data-toggle="modal" data-target="#ubah-pass">Ubah Password</button>
								<button type="button" class="btn btn-secondary btn-sm float-right" data-toggle="modal" data-target="#profile">Ubah Profil</button>
							</div>
						</div>
					</div>
					<div class="flashdata" data-flashdata="<?= $this->session->flashdata('pesan') ?>"> </div>
					<div class="flashdata2" data-flashdata2="<?= $this->session->flashdata('pesangagal') ?>"></div>

					<div class="alert alert-success alert-dismissible" role="alert" auto-close="5000">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<strong><?php foreach ($menunggu as $data) : ?><?php echo $data->total ?><?php endforeach; ?></strong> Surat Menunggu Diproses <i class="fas fa-info-circle"></i>
					</div>
					<ol class="breadcrumb mb-4">
						<li class="breadcrumb-item active"><a href="beranda.php">Beranda</li></a>
					</ol>
					
					<div class="row">
						<div class="col-xl-12">
							<h5 class="text-center">Surat Ajuan</h5>
						</div>
					</div>

					<div class="row my-3">
						<div class="col-md-4">
							<div class="form-group row">
								<label class="col-sm-4 col-form-label">Jenis Surat</label>
								<div class="col-sm-8">
									<select class="custom-select" id="pilihSurat">
										<option value="">Semua</option>
										<option value="surat_biodata">Biodata</option>
										<option value="surat_domisili">Domisili</option>
										<option value="surat_ijin_keramaian">Izin Keramaian</option>
										<option value="surat_kelahiran">Kelahiran</option>
										<option value="surat_kematian">Kematian</option>
										<option value="surat_kip">KIP</option>
										<option value="surat_kuasa">Kuasa</option>
										<option value="surat_permohonan_ktp">Permohonan KTP</option>
										<option value="surat_perubahan_kk">Perubahan KK</option>
										<option value="surat_skck">SKCK</option>
										<option value="surat_sktm">SKTM</option>
										<option value="surat_sptjm">SPTJM</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Bulan</label>
								<div class="col-sm-10">
									<select class="custom-select" id="pilihBulan">
										<option value="">Semua</option>
										<option value="01">Januari</option>
										<option value="02">Februari</option>
										<option value="03">Maret</option>
										<option value="04">April</option>
										<option value="05">Mei</option>
										<option value="06">Juni</option>
										<option value="07">Juli</option>
										<option value="08">Agustus</option>
										<option value="09">September</option>
										<option value="10">Oktober</option>
										<option value="11">November</option>
										<option value="12">Desember</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group row">
								<label class="col-sm-2 col-form-label">Tahun</label>
								<div class="col-sm-10">
									<select class="custom-select" id="tahun">
										<option value="">Semua</option>
										<option value="2020">2020</option>
										<option value="2021">2021</option>
										<option value="2022">2022</option>
										<option value="2023">2023</option>
										<option value="2024">2024</option>
										<option value="2025">2025</option>
										<option value="2026">2026</option>
										<option value="2027">2027</option>
										<option value="2028">2028</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xl-3 col-md-6 mb-4 mx-auto">
							<div class="card bg-info shadow py-2">
								<div class="card-body">
									<div class="row align-items-center">
										<div class="col mr-2">
											<div class="text-xs font-weight-bold text-white mb-1">
												Surat Masuk
											</div>
											<div class="h5 mb-0 font-weight-bold text-white" id="allLetter">
												<?= $monthly['semua']; ?>
											</div>
										</div>
										<div class="col-auto">
											<i class="fas fa-envelope fa-2x text-white"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-md-6 mb-4 mx-auto">
							<div class="card bg-warning shadow py-2">
								<div class="card-body">
									<div class="row align-items-center">
										<div class="col mr-2">
											<div class="text-xs font-weight-bold text-white mb-1">
												Surat Diproses
											</div>
											<div class="h5 mb-0 font-weight-bold text-white" id="letterInProcess">
												<?= $monthly['diproses']; ?>
											</div>
										</div>
										<div class="col-auto">
											<i class="fas fa-envelope fa-2x text-white"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-md-6 mb-4 mx-auto">
							<div class="card bg-success shadow py-2">
								<div class="card-body">
									<div class="row align-items-center">
										<div class="col mr-2">
											<div class="text-xs font-weight-bold text-white mb-1">
												Surat Selesai
											</div>
											<div class="h5 mb-0 font-weight-bold text-white" id="letterFinished">
												<?= $monthly['selesai']; ?>
											</div>
										</div>
										<div class="col-auto">
											<i class="fas fa-envelope fa-2x text-white"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-md-6 mb-4 mx-auto">
							<div class="card bg-danger shadow py-2">
								<div class="card-body">
									<div class="row align-items-center">
										<div class="col mr-2">
											<div class="text-xs font-weight-bold text-white mb-1">
												Surat Ditolak
											</div>
											<div class="h5 mb-0 font-weight-bold text-white" id="letterRejected">
												<?= $monthly['ditolak']; ?>
											</div>
										</div>
										<div class="col-auto">
											<i class="fas fa-envelope fa-2x text-white"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xl-6">
							<div class="card mb-3">
								<div class="card-header">
									<div class="row">
										<div class="col-md-6">
											<i class="fas fa-chart-area"></i>
											Surat Ajuan Perbulan
										</div>
										<div class="col-md-6">
											<!-- <select class="form-control" id="pilihBulan">
												<option value="" selected disabled>Pilih Bulan</option>
												<option value="1">Januari</option>
												<option value="2">Februari</option>
												<option value="3">Maret</option>
												<option value="4">April</option>
												<option value="5">Mei</option>
												<option value="6">Juni</option>
												<option value="7">Juli</option>
												<option value="8">Agustus</option>
												<option value="9">September</option>
												<option value="10">Oktober</option>
												<option value="11">November</option>
												<option value="12">Desember</option>
											</select> -->
										</div>
									</div>
								</div>
								<div class="card-body">
									<div class="chart-area">
										<canvas id="pieMonthly"></canvas>
									</div>
								</div>
							</div>
						</div>
						<!-- <div class="col-xl-6">
							<div class="card mb-3">
								<div class="card-header">
									<div class="row">
										<div class="col-md-6">
											<i class="fas fa-chart-area mr-1"></i>
											Surat Ajuan Tahunan
										</div>
										<div class="col-md-6">
											<div class="row float-right my-auto">
												<div class="col-sm-1">
													<button type="button" class="border-0 bg-transparent" id="btnDecrement"><i class="fas fa-caret-left"></i></button>
												</div>
												<div class="col-sm-7 p-0">
													<p class="text-center" id="tahun"><//?= date("Y"); ?></p>
												</div>
												<div class="col-sm-1 ml-0 pl-0">
													<button type="button" class="border-0 bg-transparent" disabled id="btnIncrement"><i class="fas fa-caret-right"></i></button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="card-body">
									<div class="chart-area">
										<canvas id="pieYearly"></canvas>
									</div>
								</div>
							</div>
						</div> -->
						<div class="col-xl-6">
							<div class="card mb-3">
								<div class="card-header">
									<i class="fas fa-chart-bar mr-1"></i>
									Surat Ajuan Pertahun
								</div>
								<div class="card-body"><canvas id="ChartTahun"></canvas></div>
							</div>
						</div>
						
					</div>
					
					<div class="row">
						<div class="col-xl-6">
							<div class="card mb-3">
								<div class="card-header">
									<i class="fas fa-chart-area mr-1"></i>
									Surat Ajuan Bulanan
								</div>
								<div class="card-body"><canvas id="ChartBulan"></canvas></div>
							</div>
						</div>
						<div class="col-xl-6">
							<h3 class="text-center mb-3">Data Tahun <?php echo date('Y') ?></h3>
							<hr>
							<?php $rata_semua = $average['semua']/12;
								  $rata_diproses = $average['diproses']/12;
								  $rata_selesai = $average['selesai']/12;
								  $rata_ditolak = $average['ditolak']/12; ?>
							<?php $total = $rata_semua+$rata_diproses+$rata_selesai+$rata_ditolak ?>
							<h5 class="text-center">Rata Rata Per Bulan</h5>
							<h4>Masuk = <span class="badge badge-primary mb-0"><?= number_format($rata_semua, 2, '.', '') ?></span> <i style="font-size:15px"> rumus : (Jumlah surat masuk 1 tahun)/12</i></h4>
							<div class="progress mb-1">
								<div class="progress-bar progress-bar-striped bg-primary" role="progressbar" style="width: <?= round(($rata_semua/$total)*100) ?>%" aria-valuenow="<?= round(($rata_semua/$total)*100) ?>" aria-valuemin="0" aria-valuemax="100"><strong><?= round(($rata_semua/$total)*100) ?>%</strong></div>
							</div>
							<i style="font-size:13px"> rumus presentase : Jumlah surat masuk 1 tahun/(Total rata - rata 1 tahun) x100</i>
							<h4>Diproses = <span class="badge badge-warning mb-0"><?= number_format($rata_diproses, 2, '.', '') ?></span> <i style="font-size:15px"> rumus : (Jumlah surat diproses 1 tahun)/12</i></h4>
							<div class="progress mb-1">
								<div class="progress-bar progress-bar-striped bg-warning" role="progressbar" style="width: <?= round(($rata_diproses/$total)*100) ?>%" aria-valuenow="<?= round(($rata_diproses/$total)*100) ?>" aria-valuemin="0" aria-valuemax="100"><strong><?= round(($rata_diproses/$total)*100) ?>%</strong></div>
							</div>
							<i style="font-size:13px"> rumus presentase : Jumlah surat diproses 1 tahun/(Total rata - rata 1 tahun) x100</i>
							<h4>Selesai = <span class="badge badge-success mb-0"><?= number_format($rata_selesai, 2, '.', '') ?></span> <i style="font-size:15px"> rumus : (Jumlah surat selesai 1 tahun)/12</i></h4>
							<div class="progress mb-1">
								<div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: <?= round(($rata_selesai/$total)*100) ?>%" aria-valuenow="<?= round(($rata_selesai/$total)*100) ?>" aria-valuemin="0" aria-valuemax="100"><strong><?= round(($rata_selesai/$total)*100) ?>%</strong></div>
							</div>
							<i style="font-size:13px"> rumus presentase : Jumlah surat selesai 1 tahun/(Total rata - rata 1 tahun) x100</i>
							<h4>Ditolak = <span class="badge badge-danger mb-0"><?= number_format($rata_ditolak, 2, '.', '') ?></span> <i style="font-size:15px"> rumus : (Jumlah surat ditolak 1 tahun)/12</i></h4>
							<div class="progress mb-1">
								<div class="progress-bar progress-bar-striped bg-danger" role="progressbar" style="width: <?= round(($rata_ditolak/$total)*100) ?>%" aria-valuenow="<?= round(($rata_ditolak/$total)*100) ?>" aria-valuemin="0" aria-valuemax="100"><strong><?= round(($rata_ditolak/$total)*100) ?>%</strong></div>
							</div>
							<i style="font-size:13px"> rumus presentase : Jumlah surat ditolak 1 tahun/(Total rata - rata 1 tahun) x100</i>
						</div>
					</div>
				</div>
			</main>

			<div class="modal fade" id="profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Ubah Profil</h5>
						</div>
						<div class="modal-body">
							<form action="<?php echo base_url('Monitoring/Beranda/update_profil'); ?>" method="post">
								<div class="form-group row">
									<label class="col-sm-2 col-form-label">NIP</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="nip" value="<?php echo $user['nip']; ?>">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-2 col-form-label">Nama</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="nama_admin" value="<?php echo $user['nama_admin']; ?>">
									</div>
								</div>
								<div class="form-group row">
									<div class="col-sm-6">
										<button type="submit" class="btn btn-success btn-block"><i class="fas fa-check"> </i> Simpan Perubahan </button>
									</div>
									<div class="col-sm-6">
										<button type="button" class="btn btn-danger btn-block" data-dismiss="modal"><i class="fas fa-times"> </i> Batal</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>

			<div class="modal fade" id="ubah-pass">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Ubah Password</h4>
						</div>
						<div class="modal-body">
							<div class="box-body">
								<form action="<?php echo base_url('Monitoring/Beranda/update_password'); ?>" method="post">
									<div class="form-group">
										<label for="current_password">Password Lama</label>
										<input type="password" class="form-control" id="current_password" name="current_password" placeholder="Password lama" required>
									</div>
									<div class="form-group">
										<label for="new_password1">Password Baru</label>
										<div class="input-group">
											<input type="password" class="form-control" id="new_password1" name="new_password1" placeholder="Password baru" required>
											<div class="input-group-append">

												<!-- kita pasang onclick untuk merubah icon buka/tutup mata setiap diklik  -->
												<span id="mybutton" onclick="change()" class="input-group-text">

													<!-- icon mata bawaan bootstrap  -->
													<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
														<path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z" />
														<path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z" />
													</svg>
												</span>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label for="new_password2">Ulang Password Baru</label>
										<div class="input-group">
											<input type="password" class="form-control" id="new_password2" name="new_password2" placeholder="Password baru" required>
											<div class="input-group-append">

												<!-- kita pasang onclick untuk merubah icon buka/tutup mata setiap diklik  -->
												<span id="mybutton2" onclick="change2()" class="input-group-text">

													<!-- icon mata bawaan bootstrap  -->
													<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
														<path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z" />
														<path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z" />
													</svg>
												</span>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<div class="col">
												<button type="submit" class="btn btn-outline-success btn-block">Simpan </button>
											</div>
											<div class="col">
												<button type="button" class="btn btn-outline-danger btn-block btn-outline" data-dismiss="modal">Batal</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>

			<footer class="py-4 bg-light mt-auto">
				<?php $this->load->view("Monitoring/_Partials/Footer.php") ?>
			</footer>
		</div>
	</div>
	<?php $this->load->view("Monitoring/_Partials/Js.php") ?>

	<script>
		// membuat fungsi change
		function change() {

			// membuat variabel berisi tipe input dari id='pass', id='pass' adalah form input password 
			var x = document.getElementById('new_password1').type;

			//membuat if kondisi, jika tipe x adalah password maka jalankan perintah di bawahnya
			if (x == 'password') {

				//ubah form input password menjadi text
				document.getElementById('new_password1').type = 'text';

				//ubah icon mata terbuka menjadi tertutup
				document.getElementById('mybutton').innerHTML = `<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-slash-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M10.79 12.912l-1.614-1.615a3.5 3.5 0 0 1-4.474-4.474l-2.06-2.06C.938 6.278 0 8 0 8s3 5.5 8 5.5a7.029 7.029 0 0 0 2.79-.588zM5.21 3.088A7.028 7.028 0 0 1 8 2.5c5 0 8 5.5 8 5.5s-.939 1.721-2.641 3.238l-2.062-2.062a3.5 3.5 0 0 0-4.474-4.474L5.21 3.089z"/>
                                                                    <path d="M5.525 7.646a2.5 2.5 0 0 0 2.829 2.829l-2.83-2.829zm4.95.708l-2.829-2.83a2.5 2.5 0 0 1 2.829 2.829z"/>
                                                                    <path fill-rule="evenodd" d="M13.646 14.354l-12-12 .708-.708 12 12-.708.708z"/>
                                                                    </svg>`;
			} else {

				//ubah form input password menjadi text
				document.getElementById('new_password1').type = 'password';

				//ubah icon mata terbuka menjadi tertutup
				document.getElementById('mybutton').innerHTML = `<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                                                                    <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
                                                                    </svg>`;
			}
		}
	</script>

	<script>
		// membuat fungsi change
		function change2() {

			// membuat variabel berisi tipe input dari id='pass', id='pass' adalah form input password 
			var x = document.getElementById('new_password2').type;

			//membuat if kondisi, jika tipe x adalah password maka jalankan perintah di bawahnya
			if (x == 'password') {

				//ubah form input password menjadi text
				document.getElementById('new_password2').type = 'text';

				//ubah icon mata terbuka menjadi tertutup
				document.getElementById('mybutton2').innerHTML = `<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-slash-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M10.79 12.912l-1.614-1.615a3.5 3.5 0 0 1-4.474-4.474l-2.06-2.06C.938 6.278 0 8 0 8s3 5.5 8 5.5a7.029 7.029 0 0 0 2.79-.588zM5.21 3.088A7.028 7.028 0 0 1 8 2.5c5 0 8 5.5 8 5.5s-.939 1.721-2.641 3.238l-2.062-2.062a3.5 3.5 0 0 0-4.474-4.474L5.21 3.089z"/>
                                                                    <path d="M5.525 7.646a2.5 2.5 0 0 0 2.829 2.829l-2.83-2.829zm4.95.708l-2.829-2.83a2.5 2.5 0 0 1 2.829 2.829z"/>
                                                                    <path fill-rule="evenodd" d="M13.646 14.354l-12-12 .708-.708 12 12-.708.708z"/>
                                                                    </svg>`;
			} else {

				//ubah form input password menjadi text
				document.getElementById('new_password2').type = 'password';

				//ubah icon mata terbuka menjadi tertutup
				document.getElementById('mybutton2').innerHTML = `<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                                                                    <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
                                                                    </svg>`;
			}
		}
	</script>

</body>

<script type="text/javascript">
	$(function() {
		var alert = $('div.alert[auto-close]');
		alert.each(function() {
			var that = $(this);
			var time_period = that.attr('auto-close');
			setTimeout(function() {
				that.alert('close');
			}, time_period);
		});
	});
</script>
<script>
	$(document).ready(function() {
		$("#btnIncrement").prop('disabled', true);
		var chart1 = document.getElementById("ChartTahun").getContext('2d');
		var chart2 = document.getElementById("ChartBulan").getContext('2d');
		var chartPie1 = document.getElementById("pieMonthly").getContext('2d');
		// var chartPie2 = document.getElementById("pieYearly").getContext('2d');

		var myChart1 = new Chart(chart1, {
			type: 'bar',
			data: {
				labels: [
					<?php
					foreach ($tahunan as $year) {
						echo "'" . $year['tahun'] . "',";
					}
					?>
				],
				datasets: [{
						label: 'Masuk',
						data: [
							<?php
							foreach ($tahunan as $year) {
								echo "'" . $year['semua'] . "',";
							}
							?>
						],
						backgroundColor: [
							'rgb(2, 117, 216)',
							'rgb(2, 117, 216)',
							'rgb(2, 117, 216)',
							'rgb(2, 117, 216)',
							'rgb(2, 117, 216)',
							'rgb(2, 117, 216)'
						],
						borderColor: [
							'rgb(0, 123, 255)',
							'rgb(0, 123, 255)',
							'rgb(0, 123, 255)',
							'rgb(0, 123, 255)',
							'rgb(0, 123, 255)',
							'rgb(0, 123, 255)'
						],
						borderWidth: 1
					},
					{
						label: 'Diproses',
						data: [
							<?php
							foreach ($tahunan as $year) {
								echo "'" . $year['diproses'] . "',";
							}
							?>
						],
						backgroundColor: [
							'rgb(240, 173, 78)',
							'rgb(240, 173, 78)',
							'rgb(240, 173, 78)',
							'rgb(240, 173, 78)',
							'rgb(240, 173, 78)',
							'rgb(240, 173, 78)'
						],
						borderColor: [
							'rgb(255, 193, 7)',
							'rgb(255, 193, 7)',
							'rgb(255, 193, 7)',
							'rgb(255, 193, 7)',
							'rgb(255, 193, 7)',
							'rgb(255, 193, 7)',
						],
						borderWidth: 1
					},
					{
						label: 'Selesai',
						data: [
							<?php
							foreach ($tahunan as $year) {
								echo "'" . $year['selesai'] . "',";
							}
							?>
						],
						backgroundColor: [
							'rgb(92, 184, 92)',
							'rgb(92, 184, 92)',
							'rgb(92, 184, 92)',
							'rgb(92, 184, 92)',
							'rgb(92, 184, 92)',
							'rgb(92, 184, 92)',
						],
						borderColor: [
							'rgb(40, 167, 69)',
							'rgb(40, 167, 69)',
							'rgb(40, 167, 69)',
							'rgb(40, 167, 69)',
							'rgb(40, 167, 69)',
							'rgb(40, 167, 69)',
						],
						borderWidth: 1
					},
					{
						label: 'Ditolak',
						data: [
							<?php
							foreach ($tahunan as $year) {
								echo "'" . $year['ditolak'] . "',";
							}
							?>
						],
						backgroundColor: [
							'rgb(217, 83, 79)',
							'rgb(217, 83, 79)',
							'rgb(217, 83, 79)',
							'rgb(217, 83, 79)',
							'rgb(217, 83, 79)',
							'rgb(217, 83, 79)',
						],
						borderColor: [
							'rgb(220, 53, 69)',
							'rgb(220, 53, 69)',
							'rgb(220, 53, 69)',
							'rgb(220, 53, 69)',
							'rgb(220, 53, 69)',
							'rgb(220, 53, 69)',
						],
						borderWidth: 1
					}
				]
			},
			options: {
				scales: {
					y: {
						beginAtZero: true
					}
				}
			}
		});
		var myChart2 = new Chart(chart2, {
			type: 'line',
			data: {
				labels: [
					<?php
					foreach ($bulanan as $month) {
						echo "'" . $month['bulan'] . "',";
					}
					?>
				],
				datasets: [{
						label: 'Masuk',
						data: [
							<?php
							foreach ($bulanan as $month) {
								echo "'" . $month['semua'] . "',";
							}
							?>
						],
						borderColor: [
							'rgb(0, 123, 255)',
							'rgb(0, 123, 255)',
							'rgb(0, 123, 255)',
							'rgb(0, 123, 255)',
							'rgb(0, 123, 255)',
							'rgb(0, 123, 255)'
						],
						borderWidth: 1
					},
					{
						label: 'Diproses',
						data: [
							<?php
							foreach ($bulanan as $month) {
								echo "'" . $month['diproses'] . "',";
							}
							?>
						],
						borderColor: [
							'rgb(255, 193, 7)',
							'rgb(255, 193, 7)',
							'rgb(255, 193, 7)',
							'rgb(255, 193, 7)',
							'rgb(255, 193, 7)',
							'rgb(255, 193, 7)',
						],
						borderWidth: 1
					},
					{
						label: 'Selesai',
						data: [
							<?php
							foreach ($bulanan as $month) {
								echo "'" . $month['selesai'] . "',";
							}
							?>
						],
						borderColor: [
							'rgb(40, 167, 69)',
							'rgb(40, 167, 69)',
							'rgb(40, 167, 69)',
							'rgb(40, 167, 69)',
							'rgb(40, 167, 69)',
							'rgb(40, 167, 69)',
						],
						borderWidth: 1
					},
					{
						label: 'Ditolak',
						data: [
							<?php
							foreach ($bulanan as $month) {
								echo "'" . $month['ditolak'] . "',";
							}
							?>
						],
						borderColor: [
							'rgb(220, 53, 69)',
							'rgb(220, 53, 69)',
							'rgb(220, 53, 69)',
							'rgb(220, 53, 69)',
							'rgb(220, 53, 69)',
							'rgb(220, 53, 69)',
						],
						borderWidth: 1
					}
				]
			},
			options: {
				scales: {
					y: {
						beginAtZero: true
					}
				}
			}
		});

		var total1 = <?= $chartPie['semua'] ?> + <?= $chartPie['diproses'] ?> + <?= $chartPie['selesai']; ?> + <?= $chartPie['ditolak']; ?>;
		var total2 = <?= $chartPieYearly['semua'] ?> + <?= $chartPieYearly['diproses'] ?> + <?= $chartPieYearly['selesai']; ?> + <?= $chartPieYearly['ditolak']; ?>;
		var dataCtxP1 = [{
			data: [
				(<?= $chartPie['semua'] ?> / total1 * 100).toFixed(1), (<?= $chartPie['diproses'] ?> / total1 * 100).toFixed(1), (<?= $chartPie['selesai']; ?> / total1 * 100).toFixed(1), (<?= $chartPie['ditolak']; ?> / total1 * 100).toFixed(1)
			],
			backgroundColor: ["#4285F4", "#FFBB33", "#00C851", "#FF4444"],
			hoverBackgroundColor: ["#4285F4", "#FFBB33", "#00C851", "#FF4444"],
		}];

		var dataCtxP2 = [{
			data: [
				(<?= $chartPieYearly['semua'] ?> / total2 * 100).toFixed(1), (<?= $chartPieYearly['diproses'] ?> / total2 * 100).toFixed(1), (<?= $chartPieYearly['selesai']; ?> / total2 * 100).toFixed(1), (<?= $chartPieYearly['ditolak']; ?> / total2 * 100).toFixed(1)
			],
			backgroundColor: ["#4285F4", "#FFBB33", "#00C851", "#FF4444"],
			hoverBackgroundColor: ["#4285F4", "#FFBB33", "#00C851", "#FF4444"],
		}];

		var options = {
			responsive: true,
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						var dataset = data.datasets[tooltipItem.datasetIndex];
						var labels = data.labels[tooltipItem.index];
						var currentValue = dataset.data[tooltipItem.index];
						return labels + ": " + currentValue + " %";
					}
				}
			}
		}

		var myPieChart = new Chart(chartPie1, {
			type: 'pie',
			data: {
				labels: ["Masuk", "Diproses", "Selesai", "Ditolak"],
				datasets: dataCtxP1,
			},
			options: options,
		});

		// var myPieChart2 = new Chart(chartPie2, {
		// 	type: 'pie',
		// 	data: {
		// 		labels: ["Masuk", "Diproses", "Selesai", "Ditolak"],
		// 		datasets: dataCtxP2,
		// 	},
		// 	options: options
		// });

		$("#pilihSurat").on('change', function() {
			var letter = this.value;
			var month = document.getElementById("pilihBulan").value;
			var year = $("#tahun").html();
			var curMonth = new Date().getMonth() + 1;

			const xmlHttp = new XMLHttpRequest();
			const xmlHttpMonth = new XMLHttpRequest();
			const xmlHttpYear = new XMLHttpRequest();

			if (letter != "" && month != "") {
				xmlHttp.open("GET", "<?= base_url('Monitoring/Beranda/fetchDataByFilter/'); ?>" + letter + "/" + month, true);
				xmlHttpYear.open("GET", "<?= base_url('Monitoring/Beranda/fetchDataByYear/'); ?>" + year + "/" + letter, true);
				xmlHttpMonth.open("GET", "<?= base_url('Monitoring/Beranda/fetchDataByFilter/') ?>" + letter + "/" + curMonth, true);
			} else if (letter == "" && month != "") {
				xmlHttp.open("GET", "<?= base_url('Monitoring/Beranda/fetchDataByMonth/'); ?>" + month, true);
				xmlHttpYear.open("GET", "<?= base_url('Monitoring/Beranda/fetchDataByYear/'); ?>" + year, true);
				xmlHttpMonth.open("GET", "<?= base_url('Monitoring/Beranda/fetchDataByMonth/'); ?>" + curMonth, true);
			} else if (letter != "" && month == "") {
				xmlHttp.open("GET", "<?= base_url('Monitoring/Beranda/fetchDataByFilter/') ?>" + letter, true);
				xmlHttpYear.open("GET", "<?= base_url('Monitoring/Beranda/fetchDataByYear/'); ?>" + year + "/" + letter, true);
				xmlHttpMonth.open("GET", "<?= base_url('Monitoring/Beranda/fetchDataByFilter/') ?>" + letter + "/" + curMonth, true);
			} else {
				xmlHttp.open("GET", "<?= base_url('Monitoring/Beranda/fetchDataByFilter/') ?>" + letter, true);
				xmlHttpYear.open("GET", "<?= base_url('Monitoring/Beranda/fetchDataByYear/'); ?>" + year, true);
				xmlHttpMonth.open("GET", "<?= base_url('Monitoring/Beranda/fetchDataByMonth/'); ?>" + curMonth, true);
			}

			xmlHttpYear.onload = function() {
				if (this.status == 200) {
					const result = JSON.parse(xmlHttpYear.response);
					var total = parseInt(result['semua']) + parseInt(result['diproses']) + parseInt(result['selesai']) + parseInt(result['ditolak']);
					myPieChart2.data.datasets[0].data = [
						(result['semua'] / total * 100).toFixed(1), (result['diproses'] / total * 100).toFixed(1), (result['selesai'] / total * 100).toFixed(1), (result['ditolak'] / total * 100).toFixed(1)
					];
					myPieChart2.update();
				}
			}

			xmlHttp.onload = function() {
				if (this.status == 200) {
					const result = JSON.parse(xmlHttp.response);
					var total = parseInt(result['semua']) + parseInt(result['diproses']) + parseInt(result['selesai']) + parseInt(result['ditolak']);
					myPieChart.data.datasets[0].data = [
						(result['semua'] / total * 100).toFixed(1), (result['diproses'] / total * 100).toFixed(1), (result['selesai'] / total * 100).toFixed(1), (result['ditolak'] / total * 100).toFixed(1)
					];
					myPieChart.update();
				}
			}

			xmlHttpMonth.onload = function() {
				if (this.status == 200) {
					const result = JSON.parse(xmlHttpMonth.response);
					document.getElementById("allLetter").innerHTML = result['semua'];
					document.getElementById("letterInProcess").innerHTML = result['diproses'];
					document.getElementById("letterFinished").innerHTML = result['selesai'];
					document.getElementById("letterRejected").innerHTML = result['ditolak'];
				}
			}

			xmlHttp.send();
			xmlHttpYear.send();
			xmlHttpMonth.send();
		});

		$("#pilihBulan").on('change', function() {
			var month = this.value;
			var letter = document.getElementById("pilihSurat").value;

			const xmlHttp = new XMLHttpRequest();
			if (letter != "") {
				xmlHttp.open("GET", "<?= base_url('Monitoring/Beranda/fetchDataByFilter/'); ?>" + letter + "/" + month, true);
			} else {
				xmlHttp.open("GET", "<?= base_url('Monitoring/Beranda/fetchDataByMonth/'); ?>" + month, true);
			}
			xmlHttp.onload = function() {
				if (this.status == 200) {
					const result = JSON.parse(xmlHttp.response);
					var total = parseInt(result['semua']) + parseInt(result['diproses']) + parseInt(result['selesai']) + parseInt(result['ditolak']);
					myPieChart.data.datasets[0].data = [
						(result['semua'] / total * 100).toFixed(1), (result['diproses'] / total * 100).toFixed(1), (result['selesai'] / total * 100).toFixed(1), (result['ditolak'] / total * 100).toFixed(1)
					];
					myPieChart.update();
				}
			}
			xmlHttp.send();
		});

		$("#btnDecrement").on('click', function() {
			var desc = subtraction();
			var letter = document.getElementById("pilihSurat").value;
			$("#tahun").text(desc);
			$("#btnIncrement").prop('disabled', false);

			const xmlHttp = new XMLHttpRequest();
			if (letter != "") {
				xmlHttp.open("GET", "<?= base_url('Monitoring/Beranda/fetchDataByYear/'); ?>" + desc + "/" + letter, true);
			} else {
				xmlHttp.open("GET", "<?= base_url('Monitoring/Beranda/fetchDataByYear/'); ?>" + desc, true);
			}
			xmlHttp.onload = function() {
				if (this.status == 200) {
					const result = JSON.parse(xmlHttp.response);
					var total = parseInt(result['semua']) + parseInt(result['diproses']) + parseInt(result['selesai']) + parseInt(result['ditolak']);
					myPieChart2.data.datasets[0].data = [
						(result['semua'] / total * 100).toFixed(1), (result['diproses'] / total * 100).toFixed(1), (result['selesai'] / total * 100).toFixed(1), (result['ditolak'] / total * 100).toFixed(1)
					];
					myPieChart2.update();
				}
			}
			xmlHttp.send();
		});

		$("#btnIncrement").on('click', function() {
			var adding = addition();
			var letter = document.getElementById("pilihSurat").value;
			$("#tahun").text(adding);
			var date = new Date().getFullYear();

			const xmlHttp = new XMLHttpRequest();
			if (letter != "") {
				xmlHttp.open("GET", "<?= base_url('Monitoring/Beranda/fetchDataByYear/'); ?>" + adding + "/" + letter, true);
			} else {
				xmlHttp.open("GET", "<?= base_url('Monitoring/Beranda/fetchDataByYear/'); ?>" + adding, true);
			}
			xmlHttp.onload = function() {
				if (this.status == 200) {
					const result = JSON.parse(xmlHttp.response);
					var total = parseInt(result['semua']) + parseInt(result['diproses']) + parseInt(result['selesai']) + parseInt(result['ditolak']);
					myPieChart2.data.datasets[0].data = [
						(result['semua'] / total * 100).toFixed(1), (result['diproses'] / total * 100).toFixed(1), (result['selesai'] / total * 100).toFixed(1), (result['ditolak'] / total * 100).toFixed(1)
					];
					myPieChart2.update();
				}
			}
			xmlHttp.send();

			if (date == adding) {
				$("#btnIncrement").prop('disabled', true);
			}
		});

		function subtraction() {
			var year = $("#tahun").html();
			year--;
			return year;
		}

		function addition() {
			var year = $("#tahun").html();
			year++;
			return year;
		}
	});
</script>

</html>
