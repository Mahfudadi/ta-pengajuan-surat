<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("Monitoring/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("Monitoring/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("Monitoring/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 align="center" class="mt-4">Profil </h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Profil</li>
                        </ol>
                        <hr>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-user mr-1"></i>
                                <?php foreach ($profil as $pr) : ?>
                                <?php echo $pr->nama_admin ?>
                                <?php endforeach;?>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                <div class="col-7">
                                    <table class="table-form" border="0" width="70%" cellpadding="2" cellspacing="0" >
                                        <?php
                                        $no=1;
                                        foreach ($profil as $pr) : ?>
                                        <thead>
                                            <tr>
                                                <th>NIP</th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $pr->nip ?></td>
                                                </tr>
                                            </tbody>
                                        <thead>
                                            <tr>
                                                <th> Nama Lengkap </th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $pr->nama_admin ?></td>
                                                </tr>
                                            </tbody>
                                            <?php endforeach;?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("Monitoring/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("Monitoring/_Partials/Js.php") ?>
    </body>
</html>