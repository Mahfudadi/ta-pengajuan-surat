<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("Monitoring/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("Monitoring/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("Monitoring/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4 text-center ">Detail Data Ajuan Biodata Penduduk</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Surat Ajuan</li>
                            <li class="breadcrumb-item active" >Biodata Penduduk</li>
                            <li class="breadcrumb-item active">Detail Data Ajuan Biodata Penduduk</li>
                        </ol>
                        <a class="btn btn-danger rounded" href="<?php echo base_url('Monitoring/Surat/Ajuan_biodata') ?>"><i class="fas fa-reply"></i> Kembali</a>
                        <hr>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-user mr-1"></i>
                                <?php foreach ($detailbiodata as $bio) : ?>
                                <?php echo $bio->nama_lengkap ?>
                                <?php endforeach;?>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table-form" border="0" width="50%" cellpadding="5" cellspacing="0">
                                        <?php
                                        $no=1;
                                        foreach ($detailbiodata as $bio) : ?>
                                        <tr>
                                            <td>Nomor Surat</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $bio->no_surat ?></td>
                                        </tr>
                                        <tr>
                                            <td >Tanggal Surat</td>
                                            <td width="1%">:</td>
                                            <td><?php echo format_indo($bio->tgl_surat) ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>DATA PEMOHON</b></td>
                                        </tr>
                                        <tr>
                                            <td>Nama Lengkap </td>
                                            <td width="1%">:</td>
                                            <td><?php echo $bio->nama_lengkap ?></td>
                                        </tr>
                                        <tr>
                                            <td>Nomor KTP</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $bio->nik ?></td>
                                        </tr>
                                        <tr>
                                            <td>Tempat & Tanggal Lahir</td>
                                            <td width="1%">:</td>
                                            <?php if($bio->tanggal_lahir=="0000-00-00"||$bio->tanggal_lahir==""){ ?>
                                                <td>-</td>
                                            <?php }else{?>
                                                <td><?php echo $bio->tempat_lahir ?>, <?php echo tgl_indo($bio->tanggal_lahir) ?></td>
                                            <?php } ?>
                                        </tr>
                                        <tr>
                                            <td>Jenis Kelamin</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $bio->jenis_kelamin ?></td>
                                        </tr>
                                        <tr>
                                            <td>Agama</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $bio->agama ?></td>
                                        </tr>
                                        <tr>
                                            <td>Pekerjaan</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $bio->pekerjaan ?></td>
                                        </tr>
                                        <tr>
                                            <td>Pendidikan Terakhir</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $bio->pendidikan ?></td>
                                        </tr>
                                        <tr>
                                            <td>Golongan Darah</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $bio->gol_darah ?></td>
                                        </tr>
                                        <tr>
                                            <td>Penyandang Cacat</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $bio->penyandang_cacat ?></td>
                                        </tr>
                                        <tr>
                                            <td>Status Perkawinan</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $bio->status_kawin ?></td>
                                        </tr>
                                        <tr>
                                            <td>Status Hubungan dalam Keluarga</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $bio->hub_keluarga ?></td>
                                        </tr>
                                        <tr>
                                            <td>NIK Ibu</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $bio->nik_ibu ?></td>
                                        </tr>
                                        <tr>
                                            <td>Nama Ibu</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $bio->nama_ibu ?></td>
                                        </tr>
                                        <tr>
                                            <td>NIK Ayah</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $bio->nik_ayah ?></td>
                                        </tr>
                                        <tr>
                                            <td>Nama Ayah</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $bio->nama_ayah ?></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;" >Alamat Sebelumnya</td>
                                            <td style="vertical-align: top;"  width="1%">:</td>
                                            <td ><?php echo $bio->alamat ?> RT <?php echo $bio->rt ?> RW <?php echo $bio->rw ?> Dusun <?php echo $bio->dusun ?> <br>Kelurahan/Desa : <?php echo $bio->desa ?> <br> Kecamatan : <?php echo $bio->kecamatan ?></td>
                                        </tr>
                                        <tr>
                                            <td>Alamat Sekarang</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $bio->alamat_sekarang ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>DATA KEPEMILIKAN DOKUMEN</b></td>
                                        </tr>
                                        <tr>
                                            <td>Nomor Paspor</td>
                                            <td width="1%">:</td>
                                            <?php if($bio->no_paspor=="0000-00-00"||$bio->no_paspor==""){ ?>
                                                <td>-</td>
                                            <?php }else{?>
                                                <td><?php echo $bio->no_paspor ?></td>
                                            <?php } ?>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Berakhir Paspor</td>
                                            <td width="1%">:</td>
                                            <?php if($bio->tenggang_paspor=="0000-00-00"||$bio->tenggang_paspor==""){ ?>
                                                <td>-</td>
                                            <?php }else{?>
                                                <td><?php echo tgl_indo($bio->tenggang_paspor) ?></td>
                                            <?php } ?>
                                        </tr>
                                        <tr>
                                            <td> Nomor Akta/Surat Kenal Lahir</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $bio->no_akta_kelahiran ?></td>
                                        </tr>
                                        <tr>
                                            <td>Nomor Akta Perkawinan/Buku Nikah</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $bio->no_akta_kawin ?></td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Perkawinan</td>
                                            <td width="1%">:</td>
                                            <?php if($bio->tgl_kawin=="0000-00-00"||$bio->tgl_kawin==""){ ?>
                                                <td>-</td>
                                            <?php }else{?>
                                                <td><?php echo tgl_indo($bio->tgl_kawin) ?></td>
                                            <?php } ?>
                                        </tr>
                                        <tr>
                                            <td>Nomor Akta Perceraian/Surat Cerai</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $bio->no_akta_cerai ?></td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Perceraian</td>
                                            <td width="1%">:</td>
                                            <?php if($bio->tgl_kawin=="0000-00-00"||$bio->tgl_kawin==""){ ?>
                                                <td>-</td>
                                            <?php }else{?>
                                                <td><?php echo tgl_indo($bio->tgl_cerai) ?></td>
                                            <?php } ?>
                                        </tr>
                                        <tr>
                                            <td><b>BERKAS PERSYARATAN</b></td>
                                        </tr>
                                        <tr>
                                            <td>Scan KK</td>
                                            <td width="1%">:</td>
                                            <td>
                                                <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_Biodata/'.$bio->scan_kk ?>">
                                                    <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_Biodata/'.$bio->scan_kk ?>" alt="Scan KK">
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Scan E-KTP</td>
                                            <td width="1%">:</td>
                                            <td>
                                                <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_Biodata/'.$bio->scan_ektp ?>">
                                                    <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_Biodata/'.$bio->scan_ektp ?>" alt="Scan E-KTP">
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Scan Pajak</td>
                                            <td width="1%">:</td>
                                            <td>
                                                <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_Biodata/'.$bio->scan_pajak ?>">
                                                    <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_Biodata/'.$bio->scan_pajak ?>" alt="Scan pajak">
                                                </a>
                                            </td>
                                        </tr>
                                        <?php endforeach;?>
                                    </table>
                                </div>
                            </div>
                        </div> 
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("Monitoring/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("Monitoring/_Partials/Js.php") ?>
    </body>
</html>