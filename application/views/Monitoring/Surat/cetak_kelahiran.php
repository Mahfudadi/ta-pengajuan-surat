<!DOCTYPE html>
<html lang="en">
    <head>
    </head>
    <body class="sb-nav-fixed" >
        <section class="content">
            <?php foreach ($cetak as $ct) : ?> 
                <table width="100%">
                    <tr>
                        <td align="left"><font style="font-size: 8pt;">Pemerintahan Desa/Kelurahan : GURAH<br> Kecamatan : GURAH <br> Kabupaten/Kota : KEDIRI</font></td>
                        <td align="right"><font style="font-size: 8pt;">
                        Lembar 1    : UPTD/Instansi Pelaksana <br> Lembar 2    : Untuk yang bersangkutan <br> Lembar 3   : Desa/Kelurahan <br> Lembar 4    : Kecamatan</font></td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td align="center"><b><font style="font-size: 8pt;"><u> SURAT KETERANGAN KELAHIRAN </u> </font>
                        </b></td>
                    </tr>
                    <tr>
                        <td align="center"><font style="font-size: 8pt;">Nomor: </font><font style="font-size: 8pt;"><?php echo $ct->no_surat ?> </font></td>
                    </tr>
                </table>
                <table align="center" width="70%" style="margin-top:8;">
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">Nama Kepala Keluarga</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->kepala_keluarga ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">Nomor Kartu Keluarga</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->no_kk_keluarga ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;"><b>BAYI/ANAK</b></font></td>
                                
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">1. Nama</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nama_bayi ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">2. Jenis Kelamin</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->jenkel_bayi ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">3. Tempat di lahirkan</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->tempat_dilahirkan ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">4. Tempat Kelahiran</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->tempat_kelahiran ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">5. Hari dan Tanggal Lahir</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->hari ?> dan <?php echo tgl_indo($ct->tanggal) ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">6. Pukul</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->pukul ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">7. Jenis Kelahiran</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->jenis_kelahiran ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">8. Kelahiran ke</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->kelahiran_ke ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">9. Penolong Kelahiran</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->penolong_kelahiran ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">10. Berat Bayi</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->berat_bayi ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">11. Panjang Bayi</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->panjang_bayi ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;"><b>IBU</b></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">1. NIK</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nik_ibu ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">2. Nama Lengkap </font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nama_ibu ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">3. Tanggal Lahir </font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo tgl_indo($ct->tgl_lahir_ibu) ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">4. Umur</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->umur_ibu ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">5. Pekerjaan</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->pekerjaan_ibu ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%" style="vertical-align: top;"><font style="font-size: 8pt;">6. Alamat</font></td>
                                <td width="1%" style="vertical-align: top;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->alamat_ibu ?><br>Desa/Kelurahan : GURAH<br>Kecamatan : GURAH<br> Kapupaten/Kota : KEDIRI<br> Propinsi : JAWA TIMUR</font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">7. Kewarganegaraan</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->kewarganegaraan_ibu ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">8. Kebangsaan</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->kebangsaan_ibu ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">9. Tanggal Pencatatan Perkawinan</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo tgl_indo($ct->tgl_kawin) ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;"><b>AYAH</b></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">1. NIK</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nik_ayah ?></font></td>
                            </tr>
                            <tr>
                                 <td width="20%"><font style="font-size: 8pt;">2. Nama Lengkap </font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nama_ayah ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">3. Tanggal Lahir</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo tgl_indo($ct->tgl_lahir_ayah) ?></font></td>
                            </tr>
                            <tr>
                                 <td width="20%"><font style="font-size: 8pt;">4. Umur</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->umur_ayah ?></font></td>
                            </tr>
                            <tr>
                                 <td width="20%"><font style="font-size: 8pt;">5. Pekerjaan</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->pekerjaan_ayah ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%" style="vertical-align: top;"><font style="font-size: 8pt;">6. Alamat</font></td>
                                <td width="1%" style="vertical-align: top;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->alamat_ayah ?><br>Desa/Kelurahan : GURAH<br>Kecamatan : GURAH<br> Kapupaten/Kota : KEDIRI<br> Propinsi : JAWA TIMUR</font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">7. Kewarganegaraan</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->kewarganegaraan_ayah ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">8. Kebangsaan</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->kebangsaan_ayah ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;"><b>PELAPOR</b></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">1. NIK</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nik ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">2. Nama Lengkap </font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nama_lengkap ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">3. Umur</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->umur ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">4. Jenis Kelamin</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->jenis_kelamin ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">5. Pekerjaan</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->pekerjaan ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%" style="vertical-align: top;"><font style="font-size: 8pt;">6. Alamat</font></td>
                                <td width="1%" style="vertical-align: top;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->alamat ?><br>Desa/Kelurahan : GURAH<br>Kecamatan : GURAH<br> Kapupaten/Kota : KEDIRI<br> Propinsi : JAWA TIMUR</font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;"><b>SAKSI I</b></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">1. NIK</font></td>
                                 <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nik_saksi_a ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">2. Nama Lengkap </font></td>
                                 <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nama_saksi_a ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">3. Umur</font></td>
                                 <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->umur_saksi_a ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">4. Pekerjaan</font></td>
                                 <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->pekerjaan_saksi_a ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%" style="vertical-align: top;"><font style="font-size: 8pt;">5. Alamat</font></td>
                                <td width="1%" style="vertical-align: top;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->alamat_saksi_a ?><br>Desa/Kelurahan : GURAH<br>Kecamatan : GURAH<br> Kapupaten/Kota : KEDIRI<br> Propinsi : JAWA TIMUR</font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;"><b>SAKSI II</b></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">1. NIK</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nik_saksi_b ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">2. Nama Lengkap </font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nama_saksi_b ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">3. Umur</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->umur_saksi_b ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 8pt;">4. Pekerjaan</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->pekerjaan_saksi_b ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%" style="vertical-align: top;"><font style="font-size: 8pt;">5. Alamat</font></td>
                                <td width="1%" style="vertical-align: top;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->alamat_saksi_b ?><br>Desa/Kelurahan : GURAH<br>Kecamatan : GURAH<br> Kapupaten/Kota : KEDIRI<br> Propinsi : JAWA TIMUR</font></td>
                            </tr>
                        </table>
                        <table align="center" width="70%" style="margin-top:8;">
                            <tr>
                                <td width="33%"><font style="font-size: 8pt;"></font></td>
                                <td width="33%"></td>
                                <td align="center" width="33%"><font style="font-size: 8pt;">Gurah, <?php echo tgl_indo(date('Y-m-d'));?></font></td>
                            </tr>
                            <tr>
                                <td align="center" width="33%" valign="top"><font style="font-size: 8pt;">Mengetahui, <br> Kepala Desa Gurah</font></td>
                                <td width="33%" valign="top"><font style="font-size: 8pt;"></td>
                                <td align="center" width="33%"><font style="font-size: 8pt;">Pelapor</font></td>
                            </tr>
                            <tr> 
                            <?php
                            foreach($kades as $data): ?>
                                <td align="center" width="33%" height="10%"><img src="<?php echo base_url('assets/images/ttd_kades/').$data->ttd ?>" style="width:220px"></td>
                            <?php endforeach; ?>
                                <td width="33%"><font style="font-size: 10pt;"></font></td>
                           
                            <td width="33%"><font style="font-size: 10pt;"></font></td>
                            </tr>
                            <tr>
                                <td align="center" width="33%"><font style="font-size: 8pt;">SUYONO</font></td>
                                <td width="33%"><font style="font-size: 8pt;"></td>
                                <td align="center" width="33%"><font style="font-size: 8pt;"><?php echo $ct->nama_lengkap ?></font></td>
                            </tr>
                
                        </table>
            <?php endforeach;?>
        </section>
    </body>
</html>