<!DOCTYPE html>
<html lang="en">
    <head>
    </head>
    <body class="sb-nav-fixed" >
        <section class="content">
            <!-- KOP SURAT -->
            <table width="100%" style="margin-top:0;">
                <tr>
                    <td align="center"><img src="<?php echo base_url('assets/images/kop1.png'); ?>" style="width:100%;"></td>
                </tr>
            </table>
        <!-- AKHIR KOP SURAT -->
            <?php foreach ($cetak as $ct) : ?> 
            <table width="100%">
                <tr>
                    <td align="center"><b><font style="font-size: 10pt;"><u>FORMULIR PERMOHONAN PERUBAHAN KARTU KELUARGA (KK) WARGA NEGARA INDONESIA</u> </font></b></td>
                </tr>
                <tr>
                    <td align="center"><font style="font-size: 10pt;">Nomor : </font><font style="font-size: 10pt;"><?php echo $ct->no_surat ?></font></td>
                </tr>
            </table>
            <table width="100%" style="margin-top:8;">
                <td align="left"><font style="font-size: 10pt;">1. Harap diisi dengan huruf cetak dan menggunakan tinta hitam<br>2. Setelah formulir ini diisi dan ditandatangani, harap diserahkan kembali ke Kantor Desa/Kelurahan</font></td>
            </table>
            <table width="100%" style="margin-top:8;">
                            <tr>
                                <td width="31%"><font style="font-size: 10pt;"><b>PEMERINTAH PROVINSI</b></font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"><b> : </b></font></td>
                                <td width="31%"><font style="font-size: 10pt;"><b>35 JAWA TIMUR</b></font></td>
                            </tr>
                            <tr>
                                <td width="31%"><font style="font-size: 10pt;"><b>PEMERINTAH KABUPATEN/KOTA</b></font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"><b> : </b></font></td>
                                <td width="31%"><font style="font-size: 10pt;"><b>06 KEDIRI</b></font></td>
                            </tr>
                            <tr>
                                <td width="31%"><font style="font-size: 10pt;"><b>KECAMATAN</b></font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"><b> : </b></font></td>
                                <td width="31%"><font style="font-size: 10pt;"><b>10 GURAH</b></font></td>
                            </tr>
                            <tr>
                                <td width="31%"><font style="font-size: 10pt;"><b>KELURAHAN/DESA</b></font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"><b> : </b></font></td>
                                <td width="31%"><font style="font-size: 10pt;"><b>2010 GURAH</b></font></td>
                            </tr>
            </table>
                    <table align="center" width="100%" style="margin-top:8;">
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">1. Nama Lengkap Pemohon</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nama_lengkap ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 10pt;">2. NIK Pemohon</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nik ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 10pt;">3. Nama Kepala Keluarga</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->kepala_keluarga ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 10pt;">4. No. KK</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->no_kk ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%" style="vertical-align: top;"><font style="font-size: 10pt;">5. Alamat</font></td>
                                <td width="1%" style="vertical-align: top;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;">Dusun : <?php echo $ct->dusun ?> RT :<?php echo $ct->rt ?> RW : <?php echo $ct->rw ?> <br>Desa/Kelurahan : GURAH <br> Kecamatan : GURAH <br>Kabupaten/Kota : KEDIRI<br> Propinsi : JAWA TIMUR <br> Kode Pos : 64181</font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 10pt;">6. Nama Kepala Keluarga Lama</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"></font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 10pt;">7. No. KK Lama</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"></font></td>
                            </tr>
                            <tr>
                                <td width="20%" style="vertical-align: top;"><font style="font-size: 10pt;">8. Alamat Keluarga Lama</font></td>
                                <td width="1%" style="vertical-align: top;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;">Dusun :  RT : RW :  <br>Desa/Kelurahan : GURAH <br> Kecamatan : GURAH <br>Kabupaten/Kota : KEDIRI<br> Propinsi : JAWA TIMUR <br> Kode Pos : 64181</font></td>
                            </tr>
                            <tr>
                                <td width="20%" style="vertical-align: top;"><font style="font-size: 10pt;">9. Alasan Permohonan</font></td>
                                <td width="1%" style="vertical-align: top;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->alasan ?><br>1. Karena Penambahan Anggota Keluarga (Kelahiran, Kedatangan)<br>2. Karena Pengurangan Anggota Keluarga (Kematian, Kepindahan)</font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font style="font-size: 10pt;">10. Jumlah Anggota Keluarga</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->jumlah_anggota ?></font></td>
                            </tr>
                        </table>
                        <table align="center" width="100%" style="margin-top:8;">
                            <tr>
                                <td width="100%"><font style="font-size: 10pt;"><b>11. DAFTAR ANGGOTA KELUARGA PEMOHON (hanya diisi Anggota keluarga saja)</b></font></td>
                            </tr>
                        </table>
                        <table border ="1" align="center" width="100%" style="margin-top:8;">
                            <tr>
                                <td align="center" width="5%"><font style="font-size: 10pt;"><b>No.</b></font></td>
                                <td align="center" width="20%"><font style="font-size: 10pt;"><b>NIK</b></font></td>
                                <td align="center" width="50%"><font style="font-size: 10pt;"><b>Nama</b></font></td>
                                <td align="center" width="15%"><font style="font-size: 10pt;"><b>SHDK</b></font></td>
                            </tr>
                            <tr>
                                <td width="5%"><font style="font-size: 10pt;">1</font></td>
                                <td width="20%"><font style="font-size: 10pt;"><?php echo $ct->nik_anggota_a ?></font></td>
                                <td width="50%"><font style="font-size: 10pt;"><?php echo $ct->nama_anggota_a ?></font></td>
                                <td width="15%"><font style="font-size: 10pt;"></font></td>
                            </tr>
                            <tr>
                                <td width="5%"><font style="font-size: 10pt;">2</font></td>
                                <td width="20%"><font style="font-size: 10pt;"><?php echo $ct->nik_anggota_b ?></font></td>
                                <td width="50%"><font style="font-size: 10pt;">a</font></td>
                                <td width="15%"><font style="font-size: 10pt;"><?php echo $ct->nama_anggota_b ?></font></td>
                            </tr>
                        </table>
                    <table align="center" width="100%" style="margin-top:8;">
                        <tr>
                            <td width="33%"><font style="font-size: 10pt;"></font></td>
                            <td width="33%"></td>
                            <td width="33%"><font style="font-size: 10pt;">Gurah, <?php echo format_indo(date('Y-m-d'));?></font></td>
                        </tr>
                        <tr>
                            <td align="center" width="33%" valign="top"><font style="font-size: 10pt;"></font></td>
                            <td align="center" width="33%" valign="top"><font style="font-size: 10pt;">Mengetahui,</font></td>
                            <td align="center" width="33%"><font style="font-size: 10pt;"></font></td>
                        </tr>
                        <tr>
                            <td align="center" width="33%" valign="top"><font style="font-size: 10pt;">Camat</font></td>
                            <td align="center" width="33%" valign="top"><font style="font-size: 10pt;">Kepala Desa</font></td>
                            <td align="center" width="33%"><font style="font-size: 10pt;">Pemohon</font></td>
                        </tr>
                        <tr>
                            <td width="33%"><font style="font-size: 10pt;"></font></td>
                        <?php
                        foreach($kades as $data): ?>
                            <td align="center" width="33%" height="10%"><img src="<?php echo base_url('assets/images/ttd_kades/').$data->ttd ?>" style="width:220px"></td>
                        <?php endforeach; ?>
                        <td width="33%"><font style="font-size: 10pt;"></font></td>
                        </tr>
                        <tr>
                            <td align="center" width="33%"><font style="font-size: 10pt;">Camat Gurah</font></td>
                            <td align="center" width="33%"><font style="font-size: 10pt;">SUYONO</font></td>
                            <td align="center" width="33%"><font style="font-size: 10pt;"><?php echo $ct->nama_lengkap ?></font></td>
                        </tr>
                    </table>
            <?php endforeach;?>
        </section>
    </body>
</html>