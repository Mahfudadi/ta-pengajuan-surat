<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("Monitoring/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("Monitoring/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("Monitoring/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4 text-center" >Monitoring Surat Pengajuan Kartu Indonesia Pintar</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Surat Ajuan</li>
                            <li class="breadcrumb-item active" >Pengajuan KIP</li>
                            <li class="breadcrumb-item active">Monitoring Surat Pengajuan KIP</li>
                        </ol>
                        <hr>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                Form Monitoring Surat Pengajuan Kartu Indonesia Pintar
                            </div>
                            <div class="card-body">
                                <form class="form-horizontal style-form" action="<?php echo base_url().'Monitoring/Surat/Ajuan_kip/update_kip' ?>" method="post">
                                    <?php foreach ($editkip as $kip) : ?>                                    
                                    <div class="form-group row">
                                        
                                        <label class="col-12  col-sm-2 col-form-label text-sm-left">Nomor Surat</label>
                                        <div class="col-12 col-sm-8 ">
                                                <input name="no_surat" id="no_surat" class="form-control" required value="<?php echo $kip->no_surat ?>" readonly>
                                                <input name="id_surat" id="id_surat" class="form-control" value="<?php echo $kip->id_surat ?>" hidden>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-12  col-sm-2 col-form-label text-sm-left">Tanggal Surat</label>
                                        <div class="col-12 col-sm-8 ">
                                                <input name="tgl_surat" id="tgl_surat" class="form-control" required value="<?php echo format_indo($kip->tgl_surat) ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-12  col-sm-2 col-form-label text-sm-left">NIK</label>
                                        <div class="col-12 col-sm-8 ">
                                                <input name="nik" id="nik" class="form-control"  value="<?php echo $kip->nik ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-12  col-sm-2 col-form-label text-sm-left">Nama Lengkap</label>
                                        <div class="col-12 col-sm-8 ">
                                                <input name="nama_lengkap" id="nama_lengkap" class="form-control" required oninvalid="this.setCustomValidity('Data tidak boleh kosong')" oninput="setCustomValidity('')" value="<?php echo $kip->nama_lengkap ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-12  col-sm-2 col-form-label text-sm-left"><b>STATUS MONITORING</b></label>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-12 col-sm-3 col-form-label text-sm-left">Status Monitoring</label>
                                        <div class="col-12 col-sm-8 col-lg-6">
                                            <select class="custom-select d-block mr-4" id="id_status" name="id_status" onchange="if (this.selectedIndex==4){ document.getElementById('tampil_alasan').style.display = 'inline' }else { document.getElementById('tampil_tanggal').style.display = 'none' };">
                                                <option value="<?php echo $kip->id_status ?>"><?php echo $kip->status_monitoring ?></option>
                                                <option value="2">Menunggu Diproses</option>
                                                <option value="3">Surat Diproses</option>
                                                <option value="4">Menunggu Di TTD Kades</option>
                                                <option value="7">Ditolak</option>
                                            </select>
                                            <span id="tampil_alasan" style="display:none;">
                                            <label>Alasan Ditolak</label>
                                            <input type="text"  name="ditolak" class="form-control">
                                            </span>
                                        </div>
                                        <input name="tgl_riwayat" id="tgl_riwayat" class="form-control" value="<?php echo date('Y-m-d');?>" hidden>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col col-sm-5">
                                            <button type="submit" class="btn btn-success btn-block tombol-ubah"><i class="fas fa-edit"></i> Ubah</button>
                                        </div>
                                        <div class="col col-sm-5">
                                            <a class="btn btn-danger btn-block" href="<?php echo base_url('Monitoring/Surat/Ajuan_kip') ?>"><i class="fas fa-times"></i> Batal</a>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                </form>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("Monitoring/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("Monitoring/_Partials/Js.php") ?>
    </body>
</html>