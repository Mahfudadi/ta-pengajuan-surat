<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("Monitoring/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("Monitoring/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("Monitoring/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4 text-center ">Detail Data Ajuan Surat Keterangan Domisili</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Surat Ajuan</li>
                            <li class="breadcrumb-item active" >Keterangan Domisili</li>
                            <li class="breadcrumb-item active">Detail Data Ajuan Surat Keterangan Domisili</li>
                        </ol>
                        <a class="btn btn-danger rounded" href="<?php echo base_url('Monitoring/Surat/Ajuan_domisili') ?>"><i class="fas fa-reply"></i> Kembali</a>
                        <hr>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-user mr-1"></i>
                                <?php foreach ($detaildomisili as $domisili) : ?>
                                <?php echo $domisili->nama_lengkap ?>
                                <?php endforeach;?>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table-form" border="0" width="50%" cellpadding="5" cellspacing="0">
                                        <?php
                                        $no=1;
                                        foreach ($detaildomisili as $domisili) : ?>
                                            <tr>
                                                <td>Nomor Surat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $domisili->no_surat ?></td>
                                            </tr>
                                            <tr>
                                                <td >Tanggal Surat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo format_indo($domisili->tgl_surat) ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA PEMOHON</b></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap </td>
                                                <td width="1%">:</td>
                                                <td><?php echo $domisili->nama_lengkap ?></td>
                                            </tr>
                                            <tr>
                                                <td>Jenis Kelamin</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $domisili->jenis_kelamin ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tempat, Tanggal Lahir</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $domisili->tempat_lahir ?>, <?php echo tgl_indo($domisili->tanggal_lahir) ?></td>
                                            </tr>
                                            <tr>
                                                <td>Kewarganegaraan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $domisili->warga_negara ?></td>
                                            </tr>
                                            <tr>
                                                <td>Agama</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $domisili->agama ?></td>
                                            </tr>
                                            <tr>
                                                <td>Status Perkawinan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $domisili->status_kawin ?></td>
                                            </tr>
                                            <tr>
                                                <td>Pekerjaan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $domisili->pekerjaan ?></td>
                                            </tr>
                                            <tr>
                                                <td>Pendidikan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $domisili->pendidikan ?></td>
                                            </tr>
                                            <tr>
                                                <td>Nomor KTP</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $domisili->nik ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $domisili->alamat ?></td>
                                            </tr>
                                            <tr>
                                                <td>Keterangan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $domisili->keterangan ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>BERKAS PERSYARATAN</b></td>
                                            </tr>
                                            <tr>
                                                <td>Scan KK</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_Domisili/'.$domisili->scan_kk ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_Domisili/'.$domisili->scan_kk ?>" alt="Scan KK">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan E-KTP</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_Domisili/'.$domisili->scan_ektp ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_Domisili/'.$domisili->scan_ektp ?>" alt="Scan E-KTP">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan Pajak</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_Domisili/'.$domisili->scan_pajak ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_Domisili/'.$domisili->scan_pajak ?>" alt="Scan Pajak">
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endforeach;?>
                                        </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("Monitoring/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("Monitoring/_Partials/Js.php") ?>
    </body>
</html>