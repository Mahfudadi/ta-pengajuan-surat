<!DOCTYPE html>
<html lang="en">
    <body class="sb-nav-fixed" >
        <section class="content">
        <?php foreach ($cetak as $ct) : ?> 
            <table width="100%">
                <tr>
                    <td align="center"><b><font style="font-size: 10pt;"><u>SURAT KUASA</u></font></b></td>
                </tr>
                <tr>
                    <td align="center"><font style="font-size: 10pt;">Nomor : </font><font style="font-size: 10pt;"><?php echo $ct->no_surat ?></font></td>
                </tr>
            </table>
            <table align="center" width="90%" style="margin-top:8pt;">
                <tr>
                    <td align="justify"><font style="font-size: 10pt;">Yang bertanda tangan dibawah ini :</font></td>
                </tr>
            </table>
                    <table align="center" width="90%" style="margin-top:8;">
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Nama Lengkap</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nama_lengkap ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">NIK</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nik ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Tempat, tanggal lahir</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->tempat_lahir ?>, <?php echo tgl_indo($ct->tanggal_lahir) ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Jenis Kelamin</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->jenis_kelamin ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Status Perkawinan</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->status_kawin ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Pekerjaan</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->pekerjaan ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Alamat Lengkap</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->alamat ?> RT <?php echo $ct->rt ?> RW <?php echo $ct->rw ?> Dusun <?php echo $ct->dusun ?> Desa <?php echo $ct->desa ?> Kecamatan <?php echo $ct->kecamatan ?> Kabupaten/Kota <?php echo $ct->kab_kota ?></font></td>
                            </tr>
                    </table>
                    
                    <table align="center" width="90%" style="margin-top:8pt;">
                        <tr>
                            <td align="justify"><font style="font-size: 10pt;">Di sebut dengan PIHAK KE SATU</font></td>
                        </tr>
                    </table>
                    <table align="center" width="90%" style="margin-top:8;">
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Nama Lengkap</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nama_pihak_kedua ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">NIK</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nik_pihak_kedua ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Tempat, tanggal lahir</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->tempat_lahir_pihak_kedua ?>, <?php echo tgl_indo($ct->tgl_lahir_pihak_kedua) ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Jenis Kelamin</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->jenkel_pihak_kedua?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Status Perkawinan</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->status_kawin_pihak_kedua ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Pekerjaan</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->pekerjaan_pihak_kedua ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Alamat Lengkap</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->alamat_pihak_kedua ?></font></td>
                            </tr>
                        </table>
                    <table align="center" width="90%" style="margin-top:8pt;">
                        <tr>
                            <td align="justify"><font style="font-size: 10pt;">Di sebut dengan PIHAK KE DUA</font></td>
                        </tr>
                        <tr>
                            <td align="justify"><font style="font-size: 10pt;">Selanjutnya PIHAK KE SATU memberi Kuasa kepada PIHAK KE DUA untuk :</font></td>
                        </tr>
                    </table>
                    <table align="center" width="90%">
                        <tr>
                            <td align="justify"><font style="font-size: 10pt;">1. <?php echo $ct->kuasa ?></font></td>
                        </tr>
                    </table>
                    <table align="center" width="90%" style="margin-top:4pt;">
                        <tr>
                            <td align="justify"><font style="font-size: 10pt;">Demikian Surat Kuasa ini untuk dipergunakan semestinya.</font></td>
                        </tr>
                    </table>
                    <table align="center" width="70%" style="margin-top:10pt;">
                        <tr>
                            <td width="33%"><font style="font-size: 10pt;"></font></td>
                            <td width="33%"></td>
                            <td align="center" width="33%"><font style="font-size: 10pt;">Gurah, <?php echo tgl_indo(date('Y-m-d'));?></font></td>
                        </tr>
                        <tr>
                            <td align="center" width="33%" valign="top"><font style="font-size: 10pt;">Yang Menerima Kuasa</font></td>
                            <td width="33%" valign="top"><font style="font-size: 10pt;"></td>
                            <td align="center" width="33%"><font style="font-size: 10pt;">Yang Memberi Kuasa</font></td>
                        </tr>
                        <tr>
                            <td width="33%"><font style="font-size: 10pt;"></font></td>
                            <td width="33%"><font style="font-size: 10pt;"></td>
                            <td width="33%" height="10%" ><font style="font-size: 10pt;"></font></td>
                        </tr>
                        <tr>
                            <td align="center" width="33%"><font style="font-size: 10pt;"><?php echo $ct->nama_pihak_kedua ?></font></td>
                            <td width="33%"><font style="font-size: 10pt;"></td>
                            <td align="center" width="33%"><font style="font-size: 10pt;"><?php echo $ct->nama_lengkap ?></font></td>
                        </tr>
                    </table>
                    <table align="center" width="70%" style="margin-top:8;">
                        <tr>
                            <td align="center" width="33%" valign="top"><font style="font-size: 10pt;"></font></td>
                            <td align="center" width="33%" valign="top"><font style="font-size: 10pt;">Mengetahui, <br> Kepala Desa Gurah</td>
                            <td align="center" width="33%"><font style="font-size: 10pt;"></font></td>
                        </tr>
                        <tr>
                            <td width="33%"><font style="font-size: 10pt;"></font></td>
                        <?php
                        foreach($kades as $data): ?>
                            <td align="center" width="33%" height="10%"><img src="<?php echo base_url('assets/images/ttd_kades/').$data->ttd ?>" style="width:220px"></td>
                        <?php endforeach; ?>
                        <td width="33%"><font style="font-size: 10pt;"></font></td>
                        </tr>
                        <tr>
                            <td align="center" width="33%"><font style="font-size: 10pt;"></font></td>
                            <td align="center" width="33%"><font style="font-size: 10pt;">SUYONO</font></td>
                            <td align="center" width="33%"><font style="font-size: 10pt;"></font></td>
                        </tr>
                    </table>
            <?php endforeach;?>
        </section>
    </body>
</html>