<!DOCTYPE html>
<html lang="en">
    <head>
    </head>
    <body class="sb-nav-fixed" >
        <section class="content">
            <?php foreach ($cetak as $ct) : ?> 
                <table width="100%">
                    <tr>
                        <td align="left"><font style="font-size: 8pt;">Provinsi : JAWA TIMUR <br> Kabupaten/Kota : KEDIRI <br> Kecamatan : GURAH <br>Desa/Kelurahan : GURAH</font></td>
                        <td align="right"><font style="font-size: 8pt;">
                        Lembar 1    : Untuk Yang Bersangkutan <br> Lembar 2    : Untuk UPTD/Instansi Pelaksana <br> Lembar 3   : Untuk Desa/Kelurahan <br> Lembar 4    : Untuk Kecamatan</font></td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td align="center"><b><font style="font-size: 8pt;"><u> SURAT KETERANGAN KEMATIAN </u> </font>
                        </b></td>
                    </tr>
                    <tr>
                        <td align="center"><font style="font-size: 8pt;">Nomor: </font><font style="font-size: 8pt;"><?php echo $ct->no_surat ?> </font></td>
                    </tr>
                </table>
                
                        <table align="center" width="90%" style="margin-top:8;">
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">Nama Kepala Keluarga</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nama_kepala_keluarga ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">Nomor Kartu Keluarga</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->no_kk_kepala_keluarga ?></font></td>
                            </tr>
                        </table>
                        <table align="center" width="85%" style="margin-top:8;">
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;"><b>JENAZAH</b></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">1. NIK</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nik_jenazah ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">2. Nama Lengkap</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nama_jenazah ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">3. Jenis Kelamin</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->jenkel_jenazah ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">4. Tanggal Lahir </font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo tgl_indo($ct->tgl_lahir_jenazah) ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">5. Tempat Kelahiran</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->tempat_lahir_jenazah ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">6. Agama</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->agama_jenazah ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">7. Pekerjaan</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->pekerjaan_jenazah ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%" style="vertical-align: top;"><font style="font-size: 8pt;">8. Alamat</font></td>
                                <td width="1%" style="vertical-align: top;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->alamat_jenazah ?><br>Desa/Kelurahan : GURAH <br>Kecamatan : GURAH <br> Kapupaten/Kota : KEDIIR<br> Provinsi : JAWA TIMUR</font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">9. Kwarganegaraan</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->kewarganegaraan_jenazah ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">10. Anak Ke</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->anak_ke ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">11. Tanggal Kematian</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo tgl_indo($ct->tgl_kematian) ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">12. Pukul</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->pukul ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">13. Sebab Kematian</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->sebab_kematian ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">14. Tempat Kematian</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->tempat_kematian ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">15. Yang Menerangkan</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->yang_menerangkan ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;"><b>IBU</b></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">1. NIK</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nik_ibu ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">2. Nama Lengkap </font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nama_ibu ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">3. Tanggal Lahir / Umur</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo tgl_indo($ct->tgl_lahir_ibu) ?> / <?php echo $ct->umur_ibu ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">4. Pekerjaan</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->pekerjaan_ibu ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%" style="vertical-align: top;"><font style="font-size: 8pt;">5. Alamat</font></td>
                                <td width="1%" style="vertical-align: top;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->alamat_ibu ?><br>Desa/Kelurahan : GURAH <br>Kecamatan : GURAH <br> Kapupaten/Kota : KEDIIR<br> Provinsi : JAWA TIMUR</font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;"><b>AYAH</b></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">1. NIK</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nik_ayah ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">2. Nama Lengkap </font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nama_ayah ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">3. Tanggal Lahir / Umur</font></td>
                                <<td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo tgl_indo($ct->tgl_lahir_ayah) ?> / <?php echo $ct->umur_ayah ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">4. Pekerjaan</font></td>
                                td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->pekerjaan_ayah ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%" style="vertical-align: top;"><font style="font-size: 8pt;">5. Alamat</font></td>
                                <td width="1%" style="vertical-align: top;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->alamat_ayah ?><br>Desa/Kelurahan : GURAH <br>Kecamatan : GURAH <br> Kapupaten/Kota : KEDIIR<br> Provinsi : JAWA TIMUR</font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;"><b>PELAPOR</b></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">1. NIK</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nik ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">2. Nama Lengkap </font></td>
                                td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nama_lengkap ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">3. Tanggal Lahir / Umur</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo tgl_indo($ct->tanggal_lahir) ?> / <?php echo $ct->umur ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">4. Pekerjaan</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->pekerjaan ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%" style="vertical-align: top;"><font style="font-size: 8pt;">5. Alamat</font></td>
                                <td width="1%" style="vertical-align: top;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->alamat ?> RT <?php echo $ct->rt ?> RW <?php echo $ct->rw ?> Dusun <?php echo $ct->dusun ?> <br>Desa/Kelurahan : GURAH <br>Kecamatan : GURAH <br> Kapupaten/Kota : KEDIIR<br> Provinsi : JAWA TIMUR</font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;"><b>SAKSI I</b></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">1. NIK</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nik_saksi_a ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">2. Nama Lengkap </font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nama_saksi_a ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">3. Tanggal Lahir / Umur</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo tgl_indo($ct->tanggal_lahir_saksi_a) ?> / <?php echo $ct->umur_lahir_saksi_a ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">4. Pekerjaan</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->pekerjaan_saksi_a ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%" style="vertical-align: top;"><font style="font-size: 8pt;">5. Alamat</font></td>
                                <td width="1%" style="vertical-align: top;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->alamat_saksi_a ?><br>Desa/Kelurahan : GURAH <br>Kecamatan : GURAH <br> Kapupaten/Kota : KEDIIR<br> Provinsi : JAWA TIMUR</font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;"><b>SAKSI II</b></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">1. NIK</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nik_saksi_b ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">2. Nama Lengkap </font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->nama_saksi_b ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">3. Tanggal Lahir / Umur</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo tgl_indo($ct->tanggal_lahir_saksi_b) ?> / <?php echo $ct->umur_lahir_saksi_b ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 8pt;">4. Pekerjaan</font></td>
                                <td width="1%" style="font-size: 8pt;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->pekerjaan_saksi_b ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%" style="vertical-align: top;"><font style="font-size: 8pt;">5. Alamat</font></td>
                                <td width="1%" style="vertical-align: top;"><font style="font-size: 8pt;"> : </font></td>
                                <td><font style="font-size: 8pt;"><?php echo $ct->alamat_saksi_b ?><br>Desa/Kelurahan : GURAH <br>Kecamatan : GURAH <br> Kapupaten/Kota : KEDIIR<br> Provinsi : JAWA TIMUR</font></td>
                            </tr>
                        </table>
                    <table align="center" width="70%" style="margin-top:8;">
                            <tr>
                                <td width="33%"><font style="font-size: 8pt;"></font></td>
                                <td width="33%"></td>
                                <td align="center" width="33%"><font style="font-size: 8pt;">Gurah, <?php echo tgl_indo(date('Y-m-d'));?></font></td>
                            </tr>
                            <tr>
                                <td align="center" width="33%" valign="top"><font style="font-size: 8pt;"></font></td>
                                <td width="33%" valign="top"><font style="font-size: 8pt;"></td>
                                <td align="center" width="33%"><font style="font-size: 8pt;">;Kepala Desa Gurah</font></td>
                            </tr>
                            <tr>
                                <td width="33%"><font style="font-size: 10pt;"></font></td>
                            <td width="33%"><font style="font-size: 10pt;"></font></td>
                            <?php
                            foreach($kades as $data): ?>
                                <td align="center" width="33%" height="10%"><img src="<?php echo base_url('assets/images/ttd_kades/').$data->ttd ?>" style="width:220px"></td>
                            <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td align="center" width="33%"><font style="font-size: 8pt;"></font></td>
                                <td width="33%"><font style="font-size: 8pt;"></td>
                                <td align="center" width="33%"><font style="font-size: 8pt;">SUYONO</font></td>
                            </tr>
                
                        </table>
            <?php endforeach;?>
        </section>
    </body>
</html>