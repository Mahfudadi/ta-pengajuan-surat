<!DOCTYPE html>
<html lang="en">
    <head>
    </head>
    <body class="sb-nav-fixed" >
        <section class="content">
            <!-- KOP SURAT -->
            <table width="100%" style="margin-top:0;">
                <tr>
                    <td align="center"><img src="<?php echo base_url('assets/images/kop1.png'); ?>" style="width:100%;"></td>
                </tr>
            </table>
        <!-- AKHIR KOP SURAT -->
        <?php foreach ($cetak as $ct) : ?>
            <table width="100%">
                <tr>
                    <td align="center"><b><font style="font-size: 10pt;"><u>FORMULIR PERMOHONAN KARTU TANDA PENDUDUK (KTP) WARGA NEGARA INDONESIA</u> </font></b></td>
                </tr>
                <tr>
                    <td align="center"><font style="font-size: 10pt;">Nomor : </font><font style="font-size: 10pt;"><?php echo $ct->no_surat ?></font></td>
                </tr>
            </table>
            <table width="100%" style="margin-top:8;">
                <td align="left"><font style="font-size: 10pt;">1. Harap diisi dengan huruf cetak dan menggunakan tinta hitam<br>2. Untuk kolom pilihan, harap memberi tanda silang (X) pada kotak pilihan. <br> 3. Setelah formulir ini diisi dan ditandatangani, harap diserahkan kembali ke Kantor Desa/Kelurahan</font></td>
            </table>
                    <table width="100%" style="margin-top:8;">
                            <tr>
                                <td width="31%"><font style="font-size: 10pt;"><b>PEMERINTAH PROVINSI</b></font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"><b> : </b></font></td>
                                <td width="31%"><font style="font-size: 10pt;"><b>35 JAWA TIMUR</b></font></td>
                            </tr>
                            <tr>
                                <td width="31%"><font style="font-size: 10pt;"><b>PEMERINTAH KABUPATEN/KOTA</b></font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"><b> : </b></font></td>
                                <td width="31%"><font style="font-size: 10pt;"><b>06 KEDIRI</b></font></td>
                            </tr>
                            <tr>
                                <td width="31%"><font style="font-size: 10pt;"><b>KECAMATAN</b></font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"><b> : </b></font></td>
                                <td width="31%"><font style="font-size: 10pt;"><b>10 GURAH</b></font></td>
                            </tr>
                            <tr>
                                <td width="31%"><font style="font-size: 10pt;"><b>KELURAHAN/DESA</b></font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"><b> : </b></font></td>
                                <td width="31%"><font style="font-size: 10pt;"><b>2010 GURAH</b></font></td>
                            </tr>
                            <tr>
                                <td width="31%" style="vertical-align: top;"><font style="font-size: 10pt;"><b><u>PERMOHONAN KTP</u></b></font></td>
                                <td width="1%" style="vertical-align: top;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->tipe_permohonan ?> A. Baru B. Perpanjangan C. Penggantian</font></td>
                            </tr>
                    </table>
                       <table align="center" width="100%" style="margin-top:8;">
                            <tr>
                                <td width="20%"><font style="font-size: 10pt;">1. Nama Lengkap</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nama_lengkap ?></font></td>
                            </tr>
                            <tr>
                                 <td width="20%"><font style="font-size: 10pt;">2. No. KK</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->no_kk ?></font></td>
                            </tr>
                            <tr>
                                 <td width="20%"><font style="font-size: 10pt;">3. NIK</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nik ?></font></td>
                            </tr>
                            <tr>
                                <td width="31%" style="vertical-align: top;"><font style="font-size: 10pt;">4. Alamat</font></td>
                                <td width="1%" style="vertical-align: top;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->alamat ?> <br> RT : <?php echo $ct->rt ?>  RW : <?php echo $ct->rw ?></font></td>
                            </tr>
                        </table>
                    <table border ="1" align="center" width="100%" style="margin-top:8;">
                            <tr>
                                <td align="center" width="20%"><font style="font-size: 10pt;">Pas Foto 2x3</font></td>
                                <td align="center" width="20%"><font style="font-size: 10pt;">Cap Jempol</font></td>
                                <td align="center" width="20%"><font style="font-size: 10pt;">Specimen tanda Tangan</font></td>
                            </tr>
                            <tr>
                                <td align="center" width="20%"><font style="font-size: 10pt;"><?php echo $ct->foto ?></font></td>
                                <td width="20%"><font style="font-size: 10pt;">a</font></td>
                                <td width="20%"><font style="font-size: 10pt;">a</font></td>
                            </tr>
                        </table>
                    <table align="center" width="100%" style="margin-top:8;">
                        <tr>
                            <td width="33%"><font style="font-size: 10pt;"></font></td>
                            <td width="33%"></td>
                            <td width="33%"><font style="font-size: 10pt;">Gurah, <?php echo tgl_indo(date('Y-m-d'));?></font></td>
                        </tr>
                        <tr>
                            <td align="center" width="33%" valign="top"><font style="font-size: 10pt;"></font></td>
                            <td align="center" width="33%" valign="top"><font style="font-size: 10pt;">Mengetahui,</font></td>
                            <td align="center" width="33%"><font style="font-size: 10pt;"></font></td>
                        </tr>
                        <tr>
                            <td align="center" width="33%" valign="top"><font style="font-size: 10pt;">Camat</font></td>
                            <td align="center" width="33%" valign="top"><font style="font-size: 10pt;">Kepala Desa</font></td>
                            <td align="center" width="33%"><font style="font-size: 10pt;">Pemohon</font></td>
                        </tr>
                        <tr>
                            <td width="33%"><font style="font-size: 10pt;"></font></td>
                        <?php
                        foreach($kades as $data): ?>
                            <td align="center" width="33%" height="10%"><img src="<?php echo base_url('assets/images/ttd_kades/').$data->ttd ?>" style="width:220px"></td>
                        <?php endforeach; ?>
                        <td width="33%"><font style="font-size: 10pt;"></font></td>
                        </tr>
                        <tr>
                            <td align="center" width="33%"><font style="font-size: 10pt;">Camat Gurah</font></td>
                            <td align="center" width="33%"><font style="font-size: 10pt;">SUYONO</font></td>
                            <td align="center" width="33%"><font style="font-size: 10pt;"><?php echo $ct->nama_lengkap ?></font></td>
                        </tr>
                    </table>
                <?php endforeach;?>
        </section>
    </body>
</html>