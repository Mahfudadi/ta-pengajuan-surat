<!DOCTYPE html>
<html lang="en">
    <head>
    </head>
    <body class="sb-nav-fixed" >
        <section class="content">
        <!-- KOP SURAT -->
        <table width="100%" style="margin-top:0;">
            <tr>
                <td align="center"><img src="<?php echo base_url('assets/images/kop1.png'); ?>" style="width:100%;"></td>
            </tr>
        </table>
        <!-- AKHIR KOP SURAT -->
        <?php foreach ($cetak as $ct) : ?>   
                <table width="100%">
                    <tr>
                        <td align="center"><b><font style="font-size: 10pt;"><u> SURAT KETERANGAN DOMISILI </u> </font></b></td>
                    </tr>
                    <tr>
                        <td align="center"><font style="font-size: 10pt;">Nomor : </font><font style="font-size: 10pt;"><?php echo $ct->no_surat ?></font></td>
                    </tr>
                    
                </table>
                <table align="center" width="90%" style="margin-top:8pt;">
                    <tr>
                        <td align="justify"><font style="font-size: 10pt;">Yang bertanda tangan di bawah ini kami Kepala Desa Gurah Kecamatan Gurah Kabupaten Kediri dengan Verifikasi RT & RW menerangkan bahwa :</font></td>
                    </tr>
                </table>
                <table align="center" width="70%" style="margin-top:8;">
                    <tr>
                        <td width="20%"><font style="font-size: 10pt;">Nama</font></td>
                        <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                        <td><font style="font-size: 10pt;"><?php echo $ct->nama_lengkap ?></font></td>
                    </tr>
                    <tr>
                        <td width="20%"><font style="font-size: 10pt;">Jenis Kelamin</font></td>
                        <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                        <td><font style="font-size: 10pt;"><?php echo $ct->jenis_kelamin ?></font></td>
                    </tr>
                    <tr>
                        <td width="20%"><font style="font-size: 10pt;">Tempat, tanggal lahir</font></td>
                        <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                        <td><font style="font-size: 10pt;"><?php echo $ct->tempat_lahir ?>, <?php echo tgl_indo($ct->tanggal_lahir) ?></font></td>
                    </tr>
                    <tr>
                        <td width="20%"><font style="font-size: 10pt;">Kewarganegaraan</font></td>
                        <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                        <td><font style="font-size: 10pt;"><?php echo $ct->warga_negara ?></font></td>
                    </tr>
                    <tr>
                        <td width="20%"><font style="font-size: 10pt;">Agama</font></td>
                        <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                        <td><font style="font-size: 10pt;"><?php echo $ct->agama ?></font></td>
                    </tr>
                    <tr>
                        <td width="20%"><font style="font-size: 10pt;">Status Perkawinan</font></td>
                        <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                        <td><font style="font-size: 10pt;"><?php echo $ct->status_kawin ?></font></td>
                    </tr>
                    <tr>
                        <td width="20%"><font style="font-size: 10pt;">Pekerjaan</font></td>
                        <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                        <td><font style="font-size: 10pt;"><?php echo $ct->pekerjaan ?></font></td>
                    </tr>
                    <tr>
                        <td width="20%"><font style="font-size: 10pt;">Pendidikan</font></td>
                        <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                        <td><font style="font-size: 10pt;"><?php echo $ct->pendidikan ?></font></td>
                    </tr>
                    <tr>
                        <td width="20%"><font style="font-size: 10pt;">Nomor KTP</font></td>
                        <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                        <td><font style="font-size: 10pt;"><?php echo $ct->nik ?></font></td>
                    </tr>
                    <tr>
                        <td width="20%"><font style="font-size: 10pt;">Alamat</font></td>
                        <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                        <td><font style="font-size: 10pt;"><?php echo $ct->alamat ?></font></td>
                    </tr>
                    <tr>
                        <td width="20%" style="vertical-align: top;"><font style="font-size: 10pt;">Keterangan</font></td>
                        <td width="1%" style="vertical-align: top;"><font style="font-size: 10pt;"> : </font></td>
                        <td><font style="font-size: 10pt;">- Orang tersebut adalah benar–benar penduduk Desa Gurah Kec. Gurah Kab. Kediri. Dan yang bersangkutan berdomisili di RT. <?php echo $ct->rt ?> RW. <?php echo $ct->rw ?> DSN. <?php echo $ct->dusun ?> DS.GURAH KEC.GURAH KAB. KEDIRI  sejak 1, dan sekarang beralamat di : 1 <br>- Surat Keterangan ini diberikan untuk 1</font></td>
                    </tr>
                </table>
                <table align="center" width="90%" style="margin-top:8;">
                    <tr>
                        <td align="justify"><font style="font-size: 10pt;">Demikian keterangan ini kami buat dengan sebenarnya untuk dipergunakan sebagimana perlunya.</font></td>
                    </tr>
                </table>
                <table align="center" width="70%" style="margin-top:8;">
                    <tr>
                        <td width="33%"><font style="font-size: 10pt;"></font></td>
                        <td width="33%"></td>
                        <td width="33%"><font style="font-size: 10pt;">Gurah, <?php echo tgl_indo(date('Y-m-d'));?></font></td>
                    </tr>
                    <tr>
                        <td align="center" width="33%" valign="top"><font style="font-size: 10pt;">Yang Bersangkutan</font></td>
                        <td width="33%" valign="top"><font style="font-size: 10pt;"></td>
                        <td align="center" width="33%"><font style="font-size: 10pt;">Kepala Desa</font></td>
                    </tr>
                    <tr>
                        <td width="33%"><font style="font-size: 10pt;"></font></td>
                    
                    <td width="33%"><font style="font-size: 10pt;"></font></td>
                    <?php
                    foreach($kades as $data): ?>
                        <td align="center" width="33%" height="10%"><img src="<?php echo base_url('assets/images/ttd_kades/').$data->ttd ?>" style="width:220px"></td>
                    <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td align="center" width="33%"><font style="font-size: 10pt;"><?php echo $ct->nama_lengkap ?></font></td>
                        <td width="33%"><font style="font-size: 10pt;"></td>
                        <td align="center" width="33%"><font style="font-size: 10pt;">SUYONO</font></td>
                    </tr>
                </table>
            <?php endforeach;?>
        </section>
    </body>
</html>