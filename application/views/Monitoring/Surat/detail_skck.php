<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("Monitoring/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("Monitoring/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("Monitoring/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4 text-center ">Detail Data Ajuan Surat Pengantar SKCK</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Surat Ajuan</li>
                            <li class="breadcrumb-item active" >Pengantar SKCK</li>
                            <li class="breadcrumb-item active">Detail Data Ajuan Surat Pengantar SKCK</li>
                        </ol>
                        <a class="btn btn-danger rounded" href="<?php echo base_url('Monitoring/Surat/Ajuan_skck') ?>"><i class="fas fa-reply"></i> Kembali</a>
                        <hr>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-user mr-1"></i>
                                <?php foreach ($detailskck as $skck) : ?>
                                <?php echo $skck->nama_lengkap ?>
                                <?php endforeach;?>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table-form" border="0" width="100%" cellpadding="5" cellspacing="0">
                                        <?php
                                        $no=1;
                                        foreach ($detailskck as $skck) : ?>
                                            <tr>
                                                <td>Nomor Surat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $skck->no_surat ?></td>
                                            </tr>
                                            <tr>
                                                <td >Tanggal Surat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo format_indo($skck->tgl_surat) ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA PEMOHON</b></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap </td>
                                                <td width="1%">:</td>
                                                <td><?php echo $skck->nama_lengkap ?></td>
                                            </tr>
                                            <tr>
                                                <td>Jenis Kelamin</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $skck->jenis_kelamin ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tempat, Tanggal Lahir</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $skck->tempat_lahir ?>, <?php echo tgl_indo($skck->tanggal_lahir) ?></td>
                                            </tr>
                                            <tr>
                                                <td>Agama</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $skck->agama ?></td>
                                            </tr>
                                            <tr>
                                                <td>Status Perkawinan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $skck->status_kawin ?></td>
                                            </tr>
                                            <tr>
                                                <td>Pekerjaan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $skck->pekerjaan ?></td>
                                            </tr>
                                            <tr>
                                                <td>Pendidikan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $skck->pendidikan ?></td>
                                            </tr>
                                            <tr>
                                                <td>Nomor KTP</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $skck->nik ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $skck->alamat ?> RT <?php echo $skck->rt ?> RW <?php echo $skck->rw ?> Dusun <?php echo $skck->dusun ?> Desa <?php echo $skck->desa ?> Kecamatan <?php echo $skck->kecamatan ?> Kabupaten/Kota <?php echo $skck->kab_kota ?></td>
                                            </tr>
                                            <tr>
                                                <td>Keterangan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $skck->keterangan ?></td>
                                            </tr>
                                            <tr>
                                                <td>Surat Ini Dipergunakan Untuk</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $skck->kegunaan ?></td>
                                            </tr>
                                            <tr>
                                                <td>Catatan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $skck->catatan ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>BERKAS PERSYARATAN</b></td>
                                            </tr>
                                            <tr>
                                                <td>Scan KK</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_SKCK/'.$skck->scan_kk ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_SKCK/'.$skck->scan_kk ?>" alt="Scan KK">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan E-KTP</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_SKCK/'.$skck->scan_ektp ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_SKCK/'.$skck->scan_ektp ?>" alt="Scan EKTP">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Foto 3x4 Terbaru</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_SKCK/'.$skck->foto ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_SKCK/'.$skck->foto ?>" alt="Foto 3x4">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan Pajak</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_SKCK/'.$skck->scan_pajak ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_SKCK/'.$skck->scan_pajak ?>" alt="Scan Pajak">
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endforeach;?>
                                        </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("Monitoring/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("Monitoring/_Partials/Js.php") ?>
    </body>
</html>