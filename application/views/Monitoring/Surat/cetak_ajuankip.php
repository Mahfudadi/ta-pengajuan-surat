<!DOCTYPE html>
<html lang="en">
    <head>
    </head>
    <body class="sb-nav-fixed" >
        <section class="content">
            <!-- KOP SURAT -->
            <table width="100%" style="margin-top:0;">
                <tr>
                    <td align="center"><img src="<?php echo base_url('assets/images/kop1.png'); ?>" style="width:100%;"></td>
                </tr>
            </table>
        <!-- AKHIR KOP SURAT -->
            <?php foreach ($cetak as $ct) : ?> 
            <table width="100%">
                <tr>
                    <td align="center"><b><font style="font-size: 10pt;"><u> SURAT PENGAJUAN KARTU INDONESIA PINTAR (KIP) </u> </font></b></td>
                </tr>
                <tr>
                    <td align="center"><font style="font-size: 10pt;">Nomor: </font><font style="font-size: 10pt;"><?php echo $ct->no_surat ?></font></td>
                </tr> 
            </table>
            <table align="center" width="90%" style="margin-top:8pt;">
                <tr>
                     <td align="justify"><font style="font-size: 10pt;">Yang bertanda tangan di bawah ini kami Kepala Desa Gurah Kecamatan Gurah Kabupaten Kediri dengan Verifikasi RT & RW menerangkan bahwa :</font></td>
                </tr>
            </table>
            <table align="center" width="85%" style="margin-top:8;">
                <tr>
                    <td width="30%"><font style="font-size: 10pt;">Nama</font></td>
                    <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                    <td><font style="font-size: 10pt;"><?php echo $ct->nama_lengkap ?></font></td>
                </tr>
                <tr>
                    <td width="20%"><font style="font-size: 10pt;">Jenis Kelamin</font></td>
                    <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                    <td><font style="font-size: 10pt;"><?php echo $ct->jenis_kelamin ?></font></td>
                </tr>
                <tr>
                    <td width="20%"><font style="font-size: 10pt;">Tempat, Tanggal Lahir</font></td>
                    <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                    <td><font style="font-size: 10pt;"><?php echo $ct->tempat_lahir ?>, <?php echo tgl_indo($ct->tanggal_lahir) ?></font></td>
                </tr>
                <tr>
                    <td width="30%"><font style="font-size: 10pt;">Kewarganegaraan</font></td>
                    <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                    <td><font style="font-size: 10pt;"><?php echo $ct->warga_negara ?></font></td>
                </tr>
                <tr>
                    <td width="30%"><font style="font-size: 10pt;">Agama</font></td>
                    <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                    <td><font style="font-size: 10pt;"><?php echo $ct->agama ?></font></td>
                </tr>
                <tr>
                    <td width="30%"><font style="font-size: 10pt;">Status Perkawinan</font></td>
                    <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                    <td><font style="font-size: 10pt;"><?php echo $ct->status_kawin ?></font></td>
                </tr>
                <tr>
                    <td width="30%"><font style="font-size: 10pt;">Pekerjaan</font></td>
                    <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                    <td><font style="font-size: 10pt;"><?php echo $ct->pekerjaan ?></font></td>
                </tr>
                <tr>
                    <td width="30%"><font style="font-size: 10pt;">Nomor KTP</font></td>
                    <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                    <td><font style="font-size: 10pt;"><?php echo $ct->nik ?></font></td>
                </tr>
                <tr>
                    <td width="30%"><font style="font-size: 10pt;">Alamat</font></td>
                    <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                    <td><font style="font-size: 10pt;"><?php echo $ct->alamat ?>RT <?php echo $ct->rt ?> RW <?php echo $ct->rw ?> Dusun <?php echo $ct->dusun ?> Desa <?php echo $ct->desa ?> Kecamatan <?php echo $ct->kecamatan ?> Kabupaten/Kota <?php echo $ct->kab_kota ?></font></td>
                </tr>
            </table>
            <table align="center" width="90%" style="margin-top:10;">
                <tr>
                    <td align="justify"><font style="font-size: 10pt;">Adalah benar-benar Orang Tua/Wali dari :</font></td>
                </tr>
            </table>
            <table align="center" width="85%" style="table-layout: fixed;" style="margin-top:8;">
                <tr>
                    <td width="30%"><font style="font-size: 10pt;">Nama</font></td>
                    <td width="1%"><font style="font-size: 10pt;"> : </font></td>
                    <td><font style="font-size: 10pt;"><?php echo $ct->nama_anak ?></font></td>
                </tr>
                <tr>
                    <td width="20%"><font style="font-size: 10pt;">Jenis Kelamin</font></td>
                    <td width="1%"><font style="font-size: 10pt;"> : </font></td>
                    <td><font style="font-size: 10pt;"><?php echo $ct->jenkel ?></font></td>
                </tr>
                <tr>
                    <td width="20%"><font style="font-size: 10pt;">Tempat, tanggal lahir</font></td>
                    <td width="1%"><font style="font-size: 10pt;"> : </font></td>
                    <td><font style="font-size: 10pt;"><?php echo $ct->tempat_lahir_anak ?>, <?php echo tgl_indo($ct->tgl_lahir_anak) ?></font></td>
                </tr>
                <tr>
                    <td width="30%"><font style="font-size: 10pt;">NIK</font></td>
                    <td width="1%"><font style="font-size: 10pt;"> : </font></td>
                    <td><font style="font-size: 10pt;"><?php echo $ct->nik_anak ?></font></td>
                </tr>
                <tr>
                    <td width="30%"><font style="font-size: 10pt;">Sekolah</font></td>
                    <td width="1%"><font style="font-size: 10pt;"> : </font></td>
                    <td><font style="font-size: 10pt;"><?php echo $ct->sekolah ?></font></td>
                </tr>
                <tr>
                    <td width="30%"><font style="font-size: 10pt;">Semester</font></td>
                    <td width="1%"><font style="font-size: 10pt;"> : </font></td>
                    <td><font style="font-size: 10pt;"><?php echo $ct->semester ?></font></td>
                </tr>
                <tr>
                    <td width="30%"><font style="font-size: 10pt;">NIM</font></td>
                    <td width="1%"><font style="font-size: 10pt;"> : </font></td>
                    <td><font style="font-size: 10pt;"><?php echo $ct->nim_nisn ?></font></td>
                </tr>
                <tr>
                    <td width="30%" valign="top"><font style="font-size: 10pt;">Keterangan</font></td>
                    <td width="1%" valign="top"><font style="font-size: 10pt;">:</font></td>
                    <td><font style="font-size: 10pt;">Orang tersebut di atas benar benar penduduk Desa Gurah Kecamatan Gurah Kabupaten Kediri, dan termasuk dalam Keluarga tidak mampu.</font></td>
                </tr>
                <tr>
                    <td width="30%" valign="top"><font style="font-size: 10pt;">Keperluan</font></td>
                    <td width="1%" valign="top"><font style="font-size: 10pt;"> : </font></td>
                    <td ><font style="font-size: 10pt;">Surat Keterangan ini dibuat untuk Persyaratan Pengajuan BEASISWA</font></td>
                </tr>
            </table>
            <br>
            <table align="center" width="90%" style="margin-top:8;">
                <tr>
                    <td align="justify"><font style="font-size: 10pt;">Demikian Surat Keterangan ini kami buat dengan sebenarnya untuk dipergunakan sebagaimana perlunya.</font></td>
                </tr>
            </table>
            <table align="center" width="70%" style="margin-top:8;">
                <tr>
                    <td width="33%"><font style="font-size: 10pt;"></font></td>
                    <td width="33%"></td>
                    <td align="center" width="33%"><font style="font-size: 10pt;">Gurah, <?php echo tgl_indo(date('Y-m-d'));?></font></td>
                </tr>
                <tr>
                    <td align="center" width="33%" valign="top"><font style="font-size: 10pt;">Yang Bersangkutan</font></td>
                    <td width="33%" valign="top"><font style="font-size: 10pt;"></td>
                    <td align="center" width="33%"><font style="font-size: 10pt;">Kepala Desa</font></td>
                </tr>
                <tr>
                    <td width="33%"><font style="font-size: 10pt;"></font></td>
               
                <td width="33%"><font style="font-size: 10pt;"></font></td> <?php
                foreach($kades as $data): ?>
                    <td align="center" width="33%" height="10%"><img src="<?php echo base_url('assets/images/ttd_kades/').$data->ttd ?>" style="width:220px"></td>
                <?php endforeach; ?>
                </tr>
                <tr>
                    <td align="center" width="33%"><font style="font-size: 10pt;"><?php echo $ct->nama_lengkap ?></font></td>
                    <td width="33%"><font style="font-size: 10pt;"></td>
                    <td align="center" width="33%"><font style="font-size: 10pt;">SUYONO</font></td>
                </tr>
                <!-- <tr>
                    <td width="33%"><font style="font-size: 10pt;"></font></td>
                    <td width="33%"><font style="font-size: 10pt;"></td>
                    <td width="33%"><font style="font-size: 10pt;">NIP</font></td>
                </tr> -->
            </table>
            <?php endforeach;?>
        </section>
    </body>
</html>