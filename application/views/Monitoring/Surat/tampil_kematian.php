<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("Monitoring/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("Monitoring/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("Monitoring/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4 text-left" >Surat Ajuan Keterangan Kematian</h1>
                        <ol class="breadcrumb" style="margin-bottom:4px">
                            <li class="breadcrumb-item active">Surat</li>
                            <li class="breadcrumb-item active">Keterangan Kelahiran</li>
                        </ol>
                        <div class="row">
                            <div class="col-md-10">
                                <form class="form-horizontal style-form" action="<?php echo base_url().'Monitoring/Surat/Ajuan_kematian/filter_kematian' ?>" method="post">
                                    <div class="form-group">
                                        <label for="jenis_surat" class="col-form-label"><b>Filter Surat</b></label>
                                        <div class="input-group mb-3">
                                            <label for="status" class="col-form-label mr-1"><b>Status</b></label>
                                            <select class="custom-select d-block col-md-4 mr-4" id="status" name="status" required>
                                                <option value="0">Semua Data</option>
                                                <option value="2">Menunggu Diproses</option>
                                                <option value="3">Surat Diproses</option>
                                                <option value="4">Menunggu Di TTD Kades</option>
                                            </select>
                                            <label for="bulan" class="col-form-label mr-1"><b>Bulan</b></label>
                                            <select class="custom-select d-block col-md-4 mr-4" id="bulan" name="bulan" required>
                                                <option value="0">Semua Data</option>
                                                <option value="01">Januari</option>
                                                <option value="02">Februari</option>
                                                <option value="03">Maret</option>
                                                <option value="04">April</option>
                                                <option value="05">Mei</option>
                                                <option value="06">Juni</option>
                                                <option value="07">Juli</option>
                                                <option value="08">Agustus</option>
                                                <option value="09">September</option>
                                                <option value="10">Oktober</option>
                                                <option value="11">November</option>
                                                <option value="12">Desember</option>
                                            </select>
                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-md btn-outline-secondary" >
                                                    <i class="fas fa-search-plus"></i> Tampilkan
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="flashdata" data-flashdata="<?= $this->session->flashdata('pesan') ?>"> </div>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                <span class="badge badge-secondary">Data Surat Keterangan Kematian</span>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th col='1'>No</th>
                                                <th>No. Surat</th>
                                                <th>Nama Lengkap</th>
                                                <th>Tanggal Surat</th>
                                                <th>Status Surat</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php
                                                $no = 1;
                                                foreach ($data_surat as $ds):?>
                                                <tr>
                                                    <td class="text-center"><?php echo $no++ ?></td>
                                                    <td><?php echo $ds->no_surat ?></td>
                                                    <td><?php echo $ds->nama_lengkap ?></td>
                                                    <td><?php echo format_indo($ds->tgl_surat) ?></td>
                                                    <td class="text-center"><?php echo $ds->status_monitoring?></td>
                                                    <td class="text-center">
                                                        <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Detail" 
                                                        href="<?php echo base_url('Monitoring/Surat/Ajuan_kematian/detail/'.$ds->id_surat) ?>">
                                                            <i class="fas fa-file"></i>
                                                        </a>                                                   
                                                        <a class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Monitoring" 
                                                        href="<?php echo base_url('Monitoring/Surat/Ajuan_kematian/edit/'.$ds->id_surat) ?>">
                                                            <i class="fas fa-desktop"></i>
                                                        </a>
                                                        
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("Monitoring/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("Monitoring/_Partials/Js.php") ?>
        <script>
        var chart1 = document.getElementById('ChartTahun').getContext('2d');
        var chart2 = document.getElementById('ChartBulan').getContext('2d');

        var myChart1 = new Chart(chart1, {
            type: 'bar',
            data: {
                labels: ['2021', '2022', '2023', '2024', '2025', '2026'],
                datasets: [{
                    label: '',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: [
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(153, 102, 255, 0.2)'
                    ],
                    borderColor: [
                        'rgba(153, 102, 255, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(153, 102, 255, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
        var myChart2 = new Chart(chart2, {
            type: 'bar',
            data: {
                labels: ['Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November'],
                datasets: [{
                    label: '#Masuk',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: [
                        'rgba(23, 162, 184, 0.2)',
                        'rgba(23, 162, 184, 0.2)',
                        'rgba(23, 162, 184, 0.2)',
                        'rgba(23, 162, 184, 0.2)',
                        'rgba(23, 162, 184, 0.2)',
                        'rgba(23, 162, 184, 0.2)'
                    ],
                    borderColor: [
                        'rgba(23, 162, 184, 1)',
                        'rgba(23, 162, 184, 1)',
                        'rgba(23, 162, 184, 1)',
                        'rgba(23, 162, 184, 1)',
                        'rgba(23, 162, 184, 1)',
                        'rgba(23, 162, 184, 1)'
                    ],
                    borderWidth: 1
                },
                {
                    label: '#Diproses',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: [
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(255, 206, 86, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 206, 86, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(255, 206, 86, 1)'
                    ],
                    borderWidth: 1
                },
                {
                    label: '#Selesai',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: [
                        'rgba(40, 167, 69, 0.2)',
                        'rgba(40, 167, 69, 0.2)',
                        'rgba(40, 167, 69, 0.2)',
                        'rgba(40, 167, 69, 0.2)',
                        'rgba(40, 167, 69, 0.2)',
                        'rgba(40, 167, 69, 0.2)'
                    ],
                    borderColor: [
                        'rgba(40, 167, 69, 1)',
                        'rgba(40, 167, 69, 1)',
                        'rgba(40, 167, 69, 1)',
                        'rgba(40, 167, 69, 1)',
                        'rgba(40, 167, 69, 1)',
                        'rgba(40, 167, 69, 1)'
                    ],
                    borderWidth: 1
                },
                {
                    label: '#Ditolak',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: [
                        'rgba(220, 53, 69, 0.2)',
                        'rgba(220, 53, 69, 0.2)',
                        'rgba(220, 53, 69, 0.2)',
                        'rgba(220, 53, 69, 0.2)',
                        'rgba(220, 53, 69, 0.2)',
                        'rgba(220, 53, 69, 0.2)'
                    ],
                    borderColor: [
                        'rgba(220, 53, 69, 1)',
                        'rgba(220, 53, 69, 1)',
                        'rgba(220, 53, 69, 1)',
                        'rgba(220, 53, 69, 1)',
                        'rgba(220, 53, 69, 1)',
                        'rgba(220, 53, 69, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
        </script>
    </body>
</html>