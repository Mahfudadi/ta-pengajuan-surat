<!DOCTYPE html>
<html lang="en">
    <head>
    </head>
    <body class="sb-nav-fixed" >
        <section class="content">
            <!-- KOP SURAT -->
            <table width="100%" style="margin-top:0;">
                <tr>
                    <td align="center"><img src="<?php echo base_url('assets/images/kop1.png'); ?>" style="width:100%;"></td>
                </tr>
            </table>
        <!-- AKHIR KOP SURAT -->
         <?php foreach ($cetak as $ct) : ?>  
            <table width="100%">
                <tr>
                    <td align="center"><b><font style="font-size: 10pt;"><u>SURAT PERNYATAAN TANGGUNG JAWAB MUTLAK (SPTJM) <br> KEBENARAN SEBAGAI PASANGAN SUAMI ISTRI</u></font></b></td>
                </tr>
                <tr>
                    <td align="center"><font style="font-size: 10pt;">Nomor : </font><font style="font-size: 10pt;"><?php echo $ct->no_surat ?></font></td>
                </tr>
            </table>
            <table align="center" width="100%" style="margin-top:8pt;">
                <tr>
                    <td align="justify"><font style="font-size: 10pt;">Saya yang bertandatangan dibawah ini :</font></td>
                </tr>
            </table>
                    <table align="center" width="100%" style="margin-top:8;">
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Nama</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nama_lengkap ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">NIK</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nik ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Tempat, tanggal lahir</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->tempat_lahir ?>, <?php echo tgl_indo($ct->tanggal_lahir) ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Pekerjaan</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->pekerjaan ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Alamat</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->alamat ?></font></td>
                            </tr>
                    </table>
            <table align="center" width="100%" style="margin-top:8pt;">
                <tr>
                    <td align="justify"><font style="font-size: 10pt;">Menyatakan Bahwa :</font></td>
                </tr>
            </table>
                    <table align="center" width="100%" style="margin-top:8;">
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Nama</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nama_a ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">NIK</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nik_a ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Tempat, tanggal lahir</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->tempat_lahir_a ?>, <?php echo tgl_indo($ct->tgl_lahir_a) ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Pekerjaan</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->pekerjaan_a ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Alamat</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->alamat_a ?></font></td>
                            </tr>
                    </table>
            <table align="center" width="100%" style="margin-top:8pt;">
                <tr>
                    <td align="justify"><font style="font-size: 10pt;">Adalah Suami/Istri *) dari :</font></td>
                </tr>
            </table>
                     <table align="center" width="100%" style="margin-top:8;">
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Nama</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nama_b ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">NIK</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nik_b ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Tempat, tanggal lahir</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->tempat_lahir_b ?>, <?php echo tgl_indo($ct->tgl_lahir_b) ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Pekerjaan</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->pekerjaan_b ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Alamat</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->alamat_b ?></font></td>
                            </tr>
                    </table>
                   <table align="center" width="100%" style="margin-top:8;">
                            <tr>
                                <td align="justify"><font style="font-size: 10pt;">Sebagaimana tercantum dalam Kartu Keluarga (KK) Nomor :</font></td>
                            </tr>
                            
                    </table>
                    <table align="center" width="100%" style="margin-top:8;">
                            <tr>
                                <td align="justify"><font style="font-size: 10pt;">Demikian surat pernyataan ini saya buat dengan sebenar-benarnya dan apabila dikemudian hari ternyata pernyataan saya ini tidak benar, maka saya bersedia diproses secara hukum sesuai dengan peraturan perundang-undangan dan dokumen yang diterbitkan akibat dari pernyataan ini menjadi tidak sah.</font></td>
                            </tr>
                            
                    </table>
                    <table align="center" width="70%" style="margin-top:8;">
                        <tr>
                            <td width="33%"><font style="font-size: 10pt;"></font></td>
                            <td width="33%"></td>
                            <td align="center" width="33%"><font style="font-size: 10pt;">Gurah, <?php echo tgl_indo(date('Y-m-d'));?></font></td>
                        </tr>
                        <tr>
                            <td align="center" width="33%" valign="top"><font style="font-size: 10pt;">Saksi I</font></td>
                            <td width="33%" valign="top"><font style="font-size: 10pt;"></td>
                            <td align="center" width="33%"><font style="font-size: 10pt;">Saya yang menyatakan,</font></td>
                        </tr>
                        <tr>
                            <td width="33%"><font style="font-size: 10pt;"></font></td>
                            <td width="33%"><font style="font-size: 10pt;"></td>
                            <td width="33%" height="10%" ><font style="font-size: 10pt;"></font></td>
                        </tr>
                        <tr>
                            <td align="center" width="33%"><font style="font-size: 10pt;"><br></font></td>
                            <td width="33%"><font style="font-size: 10pt;"></td>
                            <td align="center" width="33%"><font style="font-size: 10pt;"></font></td>
                        </tr>
                    </table>
                    <table align="center" width="70%" style="margin-top:8;">
                        <tr>
                            <td align="center" width="33%" valign="top"><font style="font-size: 10pt;">Saksi II</font></td>
                            <td width="33%" valign="top"><font style="font-size: 10pt;"></td>
                            <td align="center" width="33%"><font style="font-size: 10pt;"></font></td>
                        </tr>
                        <tr>
                            <td width="33%"><font style="font-size: 10pt;"></font></td>
                            <td width="33%"><font style="font-size: 10pt;"></td>
                            <td width="33%" height="10%" ><font style="font-size: 10pt;"></font></td>
                        </tr>
                        <tr>
                            <td align="center" width="33%"><font style="font-size: 10pt;"><br></font></td>
                            <td width="33%"><font style="font-size: 10pt;"></td>
                            <td align="center" width="33%"><font style="font-size: 10pt;"></font></td>
                        </tr>
                    </table>
                    <table align="center" width="100%" style="margin-top:8;">
                            <tr>
                                <td align="justify"><font style="font-size: 10pt;">Keterangan: <br> Lampiran ini digunakan dalam hal perkawinan tidak dapat dibuktikan dengan akta perkawinan atau akta nikah <br> *) coret yang tidak perlu.<br>**)Ditulis nama Ibu kota Kabupaten/Kota, Tanggal-Bulan-Tahun</font></td>
                            </tr>
                            
                    </table>
            <?php endforeach;?>
        </section>
    </body>
</html>