<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("Monitoring/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("Monitoring/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("Monitoring/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4 text-center ">Detail Data Ajuan Surat Keterangan Perubahan Kartu Keluarga</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Surat Ajuan</li>
                            <li class="breadcrumb-item active" >Perubahan KK</li>
                            <li class="breadcrumb-item active">Detail Data Ajuan Surat Perubahan KK</li>
                        </ol>
                        <a class="btn btn-danger rounded" href="<?php echo base_url('Monitoring/Surat/Ajuan_kk') ?>"><i class="fas fa-reply"></i> Kembali</a>
                        <hr>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-user mr-1"></i>
                                <?php foreach ($detailkk as $kk) : ?>
                                <?php echo $kk->nama_lengkap ?>
                                <?php endforeach;?>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table-form" border="0" width="50%" cellpadding="5" cellspacing="0">
                                        <?php
                                        $no=1;
                                        foreach ($detailkk as $kk) : ?>
                                            <tr>
                                                <td>Nomor Surat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kk->no_surat ?></td>
                                            </tr>
                                            <tr>
                                                <td >Tanggal Surat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo format_indo($kk->tgl_surat) ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA PEMOHON</b></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap </td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kk->nama_lengkap ?></td>
                                            </tr>
                                            <tr>
                                                <td>NIK</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kk->nik ?></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Kepala Keluarga</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kk->kepala_keluarga ?></td>
                                            </tr>
                                            <tr>
                                                <td>Nomor KK</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kk->no_kk ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kk->alamat ?></td>
                                            </tr>
                                            <tr>
                                                <td>RT </td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kk->rt ?></td>
                                            </tr>
                                            <tr>
                                                <td>RW </td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kk->rw ?></td>
                                            </tr>
                                            <tr>
                                                <td>Dusun </td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kk->dusun ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alasan Permohonan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kk->alasan ?></td>
                                            </tr>
                                            <tr>
                                                <td>Jumlah Anggota Keluarga</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kk->jumlah_anggota ?></td>
                                            </tr>
                                            <tr>
                                                <td>Daftar NIK Anggota Keluarga I</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kk->nik_anggota_a ?></td>
                                            </tr>
                                            <tr>
                                                <td>Daftar Anggota Keluarga I</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kk->nama_anggota_a ?></td>
                                            </tr>
                                            <tr>
                                                <td>Daftar NIK Anggota Keluarga II</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kk->nik_anggota_b ?></td>
                                            </tr>
                                            <tr>
                                                <td>Daftar Anggota Keluarga II</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kk->nama_anggota_b ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>BERKAS PERSYARATAN</b></td>
                                            </tr>
                                            <tr>
                                                <td>Scan KK</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_KK/'.$kk->scan_kk ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_KK/'.$kk->scan_kk ?>" alt="Scan KK">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan E-KTP Seluruh Anggota Keluarga</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_KK/'.$kk->scan_ektp ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_KK/'.$kk->scan_ektp ?>" alt="Scan EKTP Berupa file pdf">
                                                    </a>(Klik Untuk Mengunduh)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan Ijazah Terakhir</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_KK/'.$kk->ijazah_terakhir ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_KK/'.$kk->ijazah_terakhir ?>" alt="Scan Ijazah">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan Surat Nikah / Surat Cerai</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_KK/'.$kk->surat_nikah_cerai ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_KK/'.$kk->surat_nikah_cerai ?>" alt="Scan Nikah / Cerai">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan Pajak</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_KK/'.$kk->scan_pajak ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_KK/'.$kk->scan_pajak ?>" alt="Scan Pajak">
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endforeach;?>
                                        </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("Monitoring/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("Monitoring/_Partials/Js.php") ?>
    </body>
</html>