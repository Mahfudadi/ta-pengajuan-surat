<!DOCTYPE html>
<html lang="en">
    <head>
    </head>
    <body class="sb-nav-fixed" >
        <section class="content">
            <!-- KOP SURAT -->
            <table width="100%" style="margin-top:0;">
                <tr>
                    <td align="center"><img src="<?php echo base_url('assets/images/kop1.png'); ?>" style="width:100%;"></td>
                </tr>
            </table>
        <!-- AKHIR KOP SURAT -->
        <?php foreach ($cetak as $ct) : ?>
            <table width="100%">
                <tr>
                    <td align="center"><b><font style="font-size: 10pt;"><u> SURAT PENGANTAR PERMOHONAN SKCK</u></font></b></td>
                </tr>
                <tr>
                    <td align="center"><font style="font-size: 10pt;">Nomor : </font><font style="font-size: 10pt;"><?php echo $ct->no_surat ?></font></td>
                </tr>
            </table>
            <table align="center" width="90%" style="margin-top:8pt;">
                <tr>
                    <td align="justify"><font style="font-size: 10pt;">Yang bertanda tangan di bawah ini kami Kepala Desa Gurah Kecamatan Gurah Kabupaten Kediri Menerangkan Bahwa:</font></td>
                </tr>
            </table>
                    <table align="center" width="80%" style="margin-top:8;">
                            <tr>
                                <td width="35%"><font style="font-size: 10pt;">Nama Lengkap</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nama_lengkap ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Jenis Kelamin</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->jenis_kelamin ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Tempat, tanggal lahir</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->tempat_lahir ?>, <?php echo tgl_indo($ct->tanggal_lahir) ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Agama</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->agama ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Status Perkawinan</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->status_kawin ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Pekerjaan</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->pekerjaan ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Pendidikan</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->pendidikan ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Nomor KTP</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nik ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Alamat</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->alamat ?> RT <?php echo $ct->rt ?> RW <?php echo $ct->rw ?> Dusun <?php echo $ct->dusun ?> Desa <?php echo $ct->desa ?> Kecamatan <?php echo $ct->kecamatan ?> Kabupaten/Kota <?php echo $ct->kab_kota ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%" style="vertical-align: top;"><font style="font-size: 10pt;">Keterangan</font></td>
                                <td width="1%" style="vertical-align: top;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;">- Bahwa orang tersebut diatas adalah betul berdomisili di Desa Gurah Kecamatan Gurah Kabupaten Kediri. <br> - Pada saat surat keterangan ini diberikan yang bersangkutan sedang tidak menjalani tindak pidana.</font></td>
                            </tr>
                            <tr>
                                <td width="30%" style="vertical-align: top;"><font style="font-size: 10pt;">Surat ini dipergunakan untuk</font></td>
                                <td width="1%" style="vertical-align: top;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;">1</font></td>
                            </tr>
                            <tr>
                                <td width="30%" style="vertical-align: top;"><font style="font-size: 10pt;">Catatan</font></td>
                                <td width="1%" style="vertical-align: top;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;">Surat Keterangan ini berlaku selama mulai tanggal : <br> 20 Oktober 2020 s/d 20 Januari 2021 xxxx</font></td>
                            </tr>
                        </table>
            <table align="center" width="90%" style="margin-top:8pt;">
                <tr>
                    <td align="justify"><font style="font-size: 10pt;">Demikian Surat Keterangan ini diberikan kepada yang bersangkutan, untuk dapat dipergunakan sebagaimana mestinya.</font></td>
                </tr>
            </table>
            <table align="center" width="70%" style="margin-top:8;">
                <tr>
                    <td width="33%"><font style="font-size: 10pt;"></font></td>
                    <td width="33%"></td>
                    <td align="center" width="33%"><font style="font-size: 10pt;">Gurah, <?php echo tgl_indo(date('Y-m-d'));?></font></td>
                </tr>
                <tr>
                    <td align="center" width="33%" valign="top"><font style="font-size: 10pt;">Yang Bersangkutan</font></td>
                    <td width="33%" valign="top"><font style="font-size: 10pt;"></td>
                    <td align="center" width="33%"><font style="font-size: 10pt;">Kepala Desa</font></td>
                </tr>
                <tr>
                    <td width="33%"><font style="font-size: 10pt;"></font></td>
                <td width="33%"><font style="font-size: 10pt;"></font></td>
                <?php
                foreach($kades as $data): ?>
                    <td align="center" width="33%" height="10%"><img src="<?php echo base_url('assets/images/ttd_kades/').$data->ttd ?>" style="width:220px"></td>
                <?php endforeach; ?>
                </tr>
                <tr>
                    <td align="center" width="33%"><font style="font-size: 10pt;"><?php echo $ct->nama_lengkap ?></font></td>
                    <td width="33%"><font style="font-size: 10pt;"></td>
                    <td align="center" width="33%"><font style="font-size: 10pt;">SUYONO</font></td>
                </tr>
            </table>
            <table align="center" width="70%" style="margin-top:8;">
                <tr>
                    <td align="center" width="33%" valign="top"><font style="font-size: 10pt;"></font></td>
                    <td align="center" width="33%" valign="top"><font style="font-size: 10pt;">Camat Gurah</td>
                    <td align="center" width="33%"><font style="font-size: 10pt;"></font></td>
                </tr>
                <tr>
                    <td width="33%"><font style="font-size: 10pt;"></font></td>
                    <td width="33%"><font style="font-size: 10pt;"></td>
                    <td width="33%" height="10%" ><font style="font-size: 10pt;"></font></td>
                </tr>
                <tr>
                    <td align="center" width="33%"><font style="font-size: 10pt;"></font></td>
                    <td align="center" width="33%"><font style="font-size: 10pt;"><u>Camat Gurah</u></td>
                    <td align="center" width="33%"><font style="font-size: 10pt;"></font></td>
                </tr>
            </table>
            <?php endforeach;?>
        </section>
    </body>
</html>