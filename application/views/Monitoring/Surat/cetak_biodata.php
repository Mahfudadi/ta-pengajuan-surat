<!DOCTYPE html>
<html lang="en">
    <head>
    </head>
    <body class="sb-nav-fixed" >
        <section class="content">
            
        <!-- KOP SURAT -->
                <table width="100%" style="margin-top:0px;">
                    <tr>
                        <td align="center"><img src="<?php echo base_url('assets/images/kop1.png'); ?>" style="width:95%;"></td>
                    </tr>
                </table>
        <!-- AKHIR KOP SURAT -->
        <?php foreach ($cetak as $ct) : ?>
                <table width="100%">
                    <tr>
                        <td align="center"><b><font style="font-size: 10pt;"><u>BIODATA PENDUDUK WARGA NEGARA INDONESIA </u></font></b></td>
                    </tr>
                    <tr>
                        <td align="center"><font style="font-size: 10pt;">Nomor : </font><font style="font-size: 10pt;"><?php echo $ct->no_surat ?></font></td>
                    </tr>
                </table>

                <table align="center" width="90%" style="margin-top:8pt;">
                    <tr>
                        <td align="justify"><font style="font-size: 10pt;"><b>NIK : <?php echo $ct->nik ?><br> NO.KK : <?php echo $ct->no_kk ?></b></font></td>
                    </tr>
                </table>
                        <table align="center" width="90%" style="margin-top:8pt;">
                            <tr>
                                <td ><font style="font-size: 10pt;"><b><u>DATA PERSONAL</u></b></font></td>
                            </tr>
                        </table>
                        <table align="center" width="90%" style="margin-top:8;">
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">1. Nama Lengkap</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nama_lengkap ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">2. Nomor KTP</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nik ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">3. Tempat & Tanggal Lahir</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->tempat_lahir ?>, <?php echo tgl_indo($ct->tanggal_lahir) ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">4. Jenis Kelamin</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->jenis_kelamin ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">5. Agama</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->agama ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">6. Pekerjaan</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->pekerjaan ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">7. Pendidikan Terakhir</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->pendidikan ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">8. Golongan Darah</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->gol_darah ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">9. Penyandang Cacat</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->penyandang_cacat ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">10. Status Perkawinan</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->status_kawin ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">11. Status Hub. dalam Keluarga</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->hub_keluarga ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">12. NIK Ibu</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nik_ibu ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">13. Nama Ibu</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nama_ibu ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">14. NIK Ayah</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nik_ayah ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">15. Nama Ayah</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nama_ayah ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%" style="vertical-align: top;"><font style="font-size: 10pt;">16. Alamat sebelumnya</font></td>
                                <td width="1%" style="vertical-align: top;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->alamat ?> RT <?php echo $ct->rt ?> RW <?php echo $ct->rw ?> Dusun <?php echo $ct->dusun ?> <br>Kelurahan/Desa : <?php echo $ct->desa ?> <br> Kecamatan : <?php echo $ct->kecamatan ?></font></td>
                            </tr>
                            <tr>
                                <td width="20%" style="vertical-align: top;"><font style="font-size: 10pt;">17. Alamat sekarang</font></td>
                                <td width="1%" style="vertical-align: top;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->alamat ?> <br>Kelurahan/Desa : Gurah <br> Kecamatan : Gurah <br> Kabupaten/Kota : Kediri <br> Provinsi : Jawa Timur</font></td>
                            </tr>
                        </table>
                    <table align="center" width="90%" style="margin-top:8pt;">
                        <tr>
                            <td ><font style="font-size: 10pt;"><b><u>DATA KEPEMILIKAN DOKUMEN</u></b></font></td>
                        </tr>
                    </table>
                        <table align="center" width="90%" style="margin-top:8;">
                            <tr>
                                <td width="40%"><font style="font-size: 10pt;">1. Nomor Paspor</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->no_paspor ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">2. Tanggal Berakhir Paspor</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->tenggang_paspor ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">3. Nomor Akta/Surat Kenal Lahir</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->no_akta_kelahiran ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">4. Nomor Akta Perkawinan/Buku Nikah</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->no_akta_kawin ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">5. Tanggal Perkawinan</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->tgl_kawin ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">6. Nomor Akta Perceraian/Surat Cerai</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->no_akta_cerai ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">7. Tanggal Perceraian</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->tgl_cerai ?></font></td>
                            </tr>
                        </table>
                    <table align="center" width="70%" style="margin-top:8;">
                <tr>
                    <td width="33%"><font style="font-size: 10pt;"></font></td>
                    <td width="33%"></td>
                    <td align="center" width="33%"><font style="font-size: 10pt;">Gurah, <?php echo tgl_indo(date('Y-m-d'));?></font></td>
                </tr>
                <tr>
                    <td align="center" width="33%" valign="top"><font style="font-size: 10pt;">Yang Bersangkutan</font></td>
                    <td width="33%" valign="top"><font style="font-size: 10pt;"></td>
                    <td align="center" width="33%"><font style="font-size: 10pt;">Kepala Desa</font></td>
                </tr>
                <tr>
                    <td width="33%"><font style="font-size: 10pt;"></font></td>
                <td width="33%"><font style="font-size: 10pt;"></font></td>
                <?php
                foreach($kades as $data): ?>
                    <td align="center" width="33%" height="10%"><img src="<?php echo base_url('assets/images/ttd_kades/').$data->ttd ?>" style="width:220px"></td>
                <?php endforeach; ?>
                </tr>
                <tr>
                    <td align="center" width="33%"><font style="font-size: 10pt;"><?php echo $ct->nama_lengkap ?></font></td>
                    <td width="33%"><font style="font-size: 10pt;"></td>
                    <td align="center" width="33%"><font style="font-size: 10pt;">SUYONO</font></td>
                </tr>
            </table>
            <?php endforeach;?>
        </section>
    </body>
</html>