<!DOCTYPE html>
<html lang="en">
    <head>
    </head>
    <body class="sb-nav-fixed" >
        <section class="content">
            <!-- KOP SURAT -->
            <table width="100%" style="margin-top:0;">
                <tr>
                    <td align="center"><img src="<?php echo base_url('assets/images/kop1.png'); ?>" style="width:100%;"></td>
                </tr>
            </table>
        <!-- AKHIR KOP SURAT -->
        <table align="center" width="90%" style="margin-top:8;">
                <tr>
                    <td width="33%"><font style="font-size: 10pt;"></font></td>
                    <td width="33%"></td>
                    <td align="right" width="33%"><font style="font-size: 10pt;">Gurah, <?php echo tgl_indo(date('Y-m-d'));?></font></td>
                </tr>

        </table>
        <table align="center" width="70%" style="margin-top:8;">
                <tr>
                    <td width="33%"><font style="font-size: 10pt;"></font></td>
                    <td width="33%"></td>
                    <td align="left"><font style="font-size: 10pt;">Kepada,</font></td>
                </tr>
                <tr>
                    <td width="33%"><font style="font-size: 10pt;"></font></td>
                    <td width="33%"></td>
                    <td align="left"><font style="font-size: 10pt;">Yth. Kapolsek Gurah</font></td>
                </tr>
                <tr>
                    <td width="33%"><font style="font-size: 10pt;"></font></td>
                    <td width="33%"></td>
                    <td align="left"><font style="font-size: 10pt;">Di</font></td>
                </tr>
                <tr>
                    <td width="33%"><font style="font-size: 10pt;"></font></td>
                    <td width="33%"></td>
                    <td align="center"><font style="font-size: 10pt;"></font></td>
                </tr>
                <tr>
                    <td width="33%"><font style="font-size: 10pt;"></font></td>
                    <td width="33%"></td>
                    <td align="left"><font style="font-size: 10pt;"><u>TEMPAT</u></font></td>
                </tr>

        </table>
        <br>
         <?php foreach ($cetak as $ct) : ?>  
            <table width="100%">
                <tr>
                    <td align="center"><b><font style="font-size: 10pt;"><u>PERMOHONAN IJIN KERAMAIAN</u></font></b></td>
                </tr>
                <tr>
                    <td align="center"><font style="font-size: 10pt;">Nomor Registrasi : </font><font style="font-size: 10pt;"><?php echo $ct->no_surat ?></font></td>
                </tr>
            </table>
            <table align="center" width="90%" style="margin-top:8;">
                <tr>
                    <td align="justify"><font style="font-size: 10pt;">Yang bertanda tangan dibawah ini kami :</u></font></td>
                </tr>
            </table>
                        <table align="center" width="90%" style="margin-top:8;" >
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Nama</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;">SUYONO</font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Jabatan</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;">Kepala Desa</font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Alamat</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;">Jalan Seruji</font></td>
                            </tr>
                        </table>
            <table align="center" width="90%" style="margin-top:8;">
                <tr>
                    <td align="justify"><font style="font-size: 10pt;">Dengan ini mengajukan permohonan ijin keramaian terkait kegiatan pada hari tanggal xxx  atas nama warga masyarakat :</u></font></td>
                </tr>
            </table>
                    
                       <table align="center" width="90%" style="margin-top:8;" >
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Nama</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nama_lengkap ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Tempat, tanggal lahir</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->tempat_lahir ?>, <?php echo tgl_indo($ct->tanggal_lahir) ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Umur</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->umur ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Pekerjaan</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->pekerjaan ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">NIK</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->nik ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Alamat</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->alamat ?> RT <?php echo $ct->rt ?> RW <?php echo $ct->rw ?> Dusun <?php echo $ct->dusun ?> Desa <?php echo $ct->desa ?> Kecamatan <?php echo $ct->kecamatan ?> Kabupaten/Kota <?php echo $ct->kab_kota ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Jenis Kegiatan</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->jenis_kegiatan ?></font></td>
                            </tr>
                            <tr>
                                <td width="30%"><font style="font-size: 10pt;">Penanggung Jawab</font></td>
                                <td width="1%" style="font-size: 10pt;"><font style="font-size: 10pt;"> : </font></td>
                                <td><font style="font-size: 10pt;"><?php echo $ct->penanggungjawab ?></font></td>
                            </tr>
                        </table>
            <table align="center" width="90%" style="margin-top:8;">
                <tr>
                    <td align="justify"><font style="font-size: 10pt;">Demikian Permohonan ijin ini dibuat untuk menjadikan periksa.</u></font></td>
                </tr>
            </table>
            <table align="center" width="70%" style="margin-top:8;">
                <tr>
                    <td align="center" width="33%"><font style="font-size: 10pt;">Mengetahui,</font></td>
                    <td width="33%"></td>
                    <td  width="33%"></td>
                </tr>
                <tr>
                    <td align="center" width="33%" valign="top"><font style="font-size: 10pt;">Camat Gurah</font></td>
                    <td width="33%" valign="top"><font style="font-size: 10pt;"></td>
                    <td align="center" width="33%"><font style="font-size: 10pt;">Kepala Desa</font></td>
                </tr>
                <tr>
                    <td width="33%"><font style="font-size: 10pt;"></font></td>
                <td width="33%"><font style="font-size: 10pt;"></font></td>
                <?php
                foreach($kades as $data): ?>
                    <td align="center" width="33%" height="10%"><img src="<?php echo base_url('assets/images/ttd_kades/').$data->ttd ?>" style="width:220px"></td>
                <?php endforeach; ?>
                </tr>
                <tr>
                    <td align="center" width="33%"><font style="font-size: 10pt;"><u>aaa</u></font></td>
                    <td width="33%"><font style="font-size: 10pt;"></td>
                    <td align="center" width="33%"><font style="font-size: 10pt;">SUYONO</font></td>
                </tr>
            </table>
            <table align="center" width="70%" style="margin-top:8;">
                <tr>
                    <td align="center" width="33%" valign="top"><font style="font-size: 10pt;"></font></td>
                    <td align="center" width="33%" valign="top"><font style="font-size: 10pt;">Kapolsek Gurah</td>
                    <td align="center" width="33%"><font style="font-size: 10pt;"></font></td>
                </tr>
                <tr>
                    <td width="33%"><font style="font-size: 10pt;"></font></td>
                    <td width="33%"><font style="font-size: 10pt;"></td>
                    <td width="33%" height="10%" ><font style="font-size: 10pt;"></font></td>
                </tr>
                <tr>
                    <td align="center" width="33%"><font style="font-size: 10pt;"></font></td>
                    <td align="center" width="33%"><font style="font-size: 10pt;"><u>aaa</u></td>
                    <td align="center" width="33%"><font style="font-size: 10pt;"></font></td>
                </tr>
            </table>
            <?php endforeach;?>
        </section>
    </body>
</html>