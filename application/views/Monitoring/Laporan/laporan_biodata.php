<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("Monitoring/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("Monitoring/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("Monitoring/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 align="center" class="mt-4">Laporan Surat Ajuan Biodata</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Laporan</li>
                            <li class="breadcrumb-item active">Laporan Surat Ajuan Biodata</li>
                        </ol>
                        <hr>
                        <div class="row">
                        <div class="col-md-10">
                            <form class="form-horizontal style-form" action="<?php echo base_url('Monitoring/Laporan/Laporan_biodata/filter') ?>" method="post">
                                <div class="form-group">
                                    <label for="bulan" class="col-form-label"><b>Filter</b></label>
                                    <div class="input-group mb-3">
                                        <label for="status" class="col-form-label mr-1"><b>Status</b></label>
                                        <select class="custom-select d-block col-md-4 mr-4" id="status" name="status" required>
                                            <option value="0">Semua Data</option>
                                            <option value="6">Surat Selesai</option>
                                            <option value="7">Surat Ditolak</option>
                                        </select>
                                        
                                        <label for="bulan" class="col-form-label mr-1"><b>Bulan & Tahun</b></label>
                                        <select class="custom-select d-block col-md-4" id="bulan" name="bulan" required>
                                            <option value="0">--Pilih Bulan--</option>
                                            <option value="01">Januari</option>
                                            <option value="02">Februari</option>
                                            <option value="03">Maret</option>
                                            <option value="04">April</option>
                                            <option value="05">Mei</option>
                                            <option value="06">Juni</option>
                                            <option value="07">Juli</option>
                                            <option value="08">Agustus</option>
                                            <option value="09">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                        <select class="custom-select d-block col-md-4" id="tahun" name="tahun" required>
                                            <option value="0">--Pilih Tahun--</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                            <option value="2023">2023</option>
                                            <option value="2024">2024</option>
                                            <option value="2025">2025</option>
                                            <option value="2026">2026</option>
                                            <option value="2023">2027</option>
                                            <option value="2024">2028</option>
                                            <option value="2025">2029</option>
                                            <option value="2026">2030</option>
                                        </select>
                                        <button type="submit" class="btn btn-md btn-outline-secondary ml-2" >
                                            <i class="fas fa-search-plus"></i> Tampil
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        </div>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                Laporan Surat Ajuan
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" style="font-size:11px">
                                        <thead class="text-center">
                                            <tr>
                                                <th>No. Surat</th>
                                                <th>NIK</th>
                                                <th>Nama</th>
                                                <th>Tanggal Diajukan</th>
                                                <th>Tanggal Selesai</th>
                                                <th>Status</th>
                                                <th>Alasan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php
                                                foreach ($laporan as $lp) : ?>
                                                <tr>
                                                    <td><?php echo $lp->no_surat ?></td>
                                                    <td><?php echo $lp->nik ?></td>
                                                    <td><?php echo $lp->nama_lengkap ?></td>
                                                    <td><?php echo format_indo($lp->tgl_surat) ?></td>
                                                    <td><?php echo format_indo($lp->tgl_riwayat) ?></td>
                                                    <?php if($lp->riwayat_status == '7'){ ?>
                                                        <td>Surat Ditolak</td>
                                                    <?php }else{?>
                                                        <td>Surat Selesai</td>
                                                    <?php } ?>
                                                    <td><?php echo $lp->ket_tolak ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("Monitoring/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("Monitoring/_Partials/Js.php") ?>
    </body>
</html>