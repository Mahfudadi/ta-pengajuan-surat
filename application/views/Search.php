<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("RT/_Partials/Head") ?>
    </head>
    <body class="bg-info" style="background-image:url('<?php echo base_url().'assets/images/background.png'?>');background-attachment: fixed;background-repeat: no-repeat;background-size:cover;">>
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container6">
                        <div class="row justify-content-center">
                            <div class="col-lg-6">
								<a class="btn btn-danger mt-5" href="<?php echo base_url('') ?>"><i class="fas fa-reply"></i> Kembali</a>
                                <div class="card shadow-lg border-0 rounded-lg mt-1">
                                    <div class="card-header">
                                      <h2 class="text-center">Cari Surat Anda Disini</h2>
                                      <h5 class="text-center">Masukkan Nomor Surat Anda Untuk Mengetahui Detail Surat</h5>
                                    </div>
                                    <div class="card-body">
                                      <form action="<?php echo base_url('Welcome/search'); ?>" method="post">
                                            <div class="form-group row">
												<div class="col-sm-9">
                                                    <input class="form-control" name="no_surat" type="text" placeholder="Masukkan Nomor Surat" />
                                                </div>
												<div class="col-sm-3">
													<button type="submit" class="btn btn-outline-success btn-block"><i class="fas fa-search"></i> Cari</button>
												</div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer text-center">
									<?php foreach($hasil as $data) : ?>
										<div class="form-group row">
											<div class="col-sm-2">
                                            	<p class="text-left"><b>No. Surat</b></p>
											</div>
											<div class="col-sm-5">
												<p class="text-left"><?php echo $data->no_surat ?></p>
											</div>
											<div class="col-sm-2 mb-0">
                                            	<p class="text-left"><b>Nama</b></p>
											</div>
											<div class="col-sm-3 mb-0">
												<p class="text-left"><?php echo $data->nama_lengkap ?></p>
											</div>
											<div class="col-sm-2">
                                            	<p class="text-left"><b>Tanggal Diajukan</b></p>
											</div>
											<div class="col-sm-5">
												<p class="text-left" ><?php echo format_indo($data->tgl_surat) ?></p>
											</div>
											<div class="col-sm-2">
                                            	<p class="text-left"><b>Status saat ini</b></p>
											</div>
											<div class="col-sm-3">
												<p class="text-left"><?php echo $data->status_monitoring ?></p>
											</div>
									    <?php endforeach; ?>
                                        <?php foreach($hasil2 as $data) : ?>
											<div class="col-sm-12">
												<table class="table">
                                                    <tbody>
                                                        <td width=40%><?php echo format_indo($data->tgl_riwayat) ?></td>
                                                        <td width=10%>:</td>
                                                        <td class="text-right"><?php echo $data->status_monitoring ?></td>
                                                    </tbody>
                                                </table>
											</div>
									    <?php endforeach; ?>
										</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div class="modal fade" id="akun" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Belum Punya Akun ?</h5>
                            </div>
                            <div class="modal-body">
                                <h4 class="mb-4">Silahkan Menghubungi Admin Desa Gurah</h4>
                                <div class="form-group row">
                                    <h5 class="col-sm-3">No. Telp </h5>
                                    <div class="col-sm-9">
                                        <h5>: 081519115100</h5>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <h5 class="col-sm-3">Email </h5>
                                    <div class="col-sm-9">
                                        <h5>: DesaGurah@gmail.com</h5>
                                    </div>
                                </div>
                                <hr>
                                <button type="button" class="btn btn-success btn-block"data-dismiss="modal"><i class="fas fa-check"> </i> OK</button>
                            </div>
                        </div>
                    </div>
                </div>
            <div id="layoutAuthentication_footer">
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2020</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <?php $this->load->view("RT/_Partials/js") ?>
    </body>
</html>
