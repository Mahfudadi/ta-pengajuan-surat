<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view("RT/_Partials/Head") ?>
    </head>
    <body class="bg-info" style="background-image:url('<?php echo base_url().'assets/images/background.png'?>');background-attachment: fixed;background-repeat: no-repeat;background-size:cover;">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header">
                                      <h1 class="text-center my-3" style="font-family:Arial;">SISTEM INFORMASI</h1>
                                      <h5 class="text-center font-weight-light" style="font-family:Arial;">PELAYANAN PENGAJUAN SURAT</h5>
                                      
                                    <?= $this->session->flashdata('msg') ?>
                                    </div>
                                    <div class="card-body">
                                      <form action="<?php echo base_url().'/Validasi/login/auth'?>" method="post">
                                            <div class="form-group">
                                                <label class="small mb-0" for="username">Username</label>
                                                <input class="form-control py-4" name="username" type="text" placeholder="Masukkan Username" />
                                            </div>
                                            <div class="form-group">
                                                <label class="small mb-0" for="password">Password</label>
                                                <input class="form-control py-4" type="password" name="password" placeholder="Masukkan Password" />
                                            </div>
                                            <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                                                <button type="submit" class="btn btn-success btn-block"><i class="fas fa-sign-in-alt"></i> Masuk</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="small"><a class="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#akun"><i class="fas fa-question-circle"></i> Belum punya akun ?</a> 
                                        <a class="btn btn-sm btn-outline-secondary" href="<?php echo base_url('Welcome/search'); ?>"><i class="fas fa-search"></i> Cari Surat Anda</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div class="modal fade" id="akun" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Belum Punya Akun ?</h5>
                            </div>
                            <div class="modal-body">
                                <h4 class="mb-4">Silahkan Menghubungi Admin Desa Gurah</h4>
                                <div class="form-group row">
                                    <h5 class="col-sm-3">No. Telp </h5>
                                    <div class="col-sm-9">
                                        <h5>: 081519115100</h5>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <h5 class="col-sm-3">Email </h5>
                                    <div class="col-sm-9">
                                        <h5>: DesaGurah@gmail.com</h5>
                                    </div>
                                </div>
                                <hr>
                                <button type="button" class="btn btn-success btn-block"data-dismiss="modal"><i class="fas fa-check"> </i> OK</button>
                            </div>
                        </div>
                    </div>
                </div>
            <div id="layoutAuthentication_footer">
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2020</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <?php $this->load->view("RT/_Partials/js") ?>
    </body>
</html>
