<!DOCTYPE html>
<html lang="en">

    <head>
        <?php $this->load->view("RT/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
    
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("RT/_Partials/Header.php") ?>
        </nav>
        
        <div id="layoutSidenav">
            <?php $this->load->view("RT/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4 text-center ">Detail Data Ajuan Surat Keterangan Tidak Mampu</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Surat Ajuan</li>
                            <li class="breadcrumb-item active" >SKTM</li>
                            <li class="breadcrumb-item active">Detail Data Ajuan SKTM</li>
                        </ol>
                        <a class="btn btn-danger rounded" href="<?php echo base_url('RT/Surat/Surat_Ajuan') ?>"><i class="fas fa-reply"></i> Kembali</a>
                        <hr>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-user mr-1"></i>
                                <?php foreach ($detailsktm as $sktm) : ?>
                                <?php echo $sktm->nama_lengkap ?>
                                <?php endforeach;?>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table-form" border="0" width="100%" cellpadding="5" cellspacing="0">
                                        <?php
                                        $no=1;
                                        foreach ($detailsktm as $sktm) : ?>
                                            <tr>
                                                <td>Nomor Surat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sktm->no_surat ?></td>
                                            </tr>
                                            <tr>
                                                <td >Tanggal Surat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo format_indo($sktm->tgl_surat) ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA PEMOHON</b></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap </td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sktm->nama_lengkap ?></td>
                                            </tr>
                                            <tr>
                                                <td>Jenis Kelamin</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sktm->jenis_kelamin ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tempat, Tanggal Lahir</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sktm->tempat_lahir ?>, <?php echo tgl_indo($sktm->tanggal_lahir) ?></td>
                                            </tr>
                                            <tr>
                                                <td>Kewarganegaraan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sktm->warga_negara ?></td>
                                            </tr>
                                            <tr>
                                                <td>Agama</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sktm->agama ?></td>
                                            </tr>
                                            <tr>
                                                <td>Status Perkawinan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sktm->status_kawin ?></td>
                                            </tr>
                                            <tr>
                                                <td>Pekerjaan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sktm->pekerjaan ?></td>
                                            </tr>
                                            <tr>
                                                <td>Nomor KTP</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sktm->nik ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sktm->alamat ?> RT.<?php echo $sktm->rt ?> RW.<?php echo $sktm->rw ?> Dusun <?php echo $sktm->dusun ?> Desa <?php echo $sktm->desa ?> Kecamatan <?php echo $sktm->kecamatan ?> Kabupaten <?php echo $sktm->kab_kota ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA ANAK</b></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sktm->nama_anak?></td>
                                            </tr>
                                            <tr>
                                                <td>Jenis Kelamin</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sktm->jenkel ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tempat, Tanggal Lahir</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sktm->tempat_lahir_anak ?>, <?php echo tgl_indo($sktm->tgl_lahir_anak) ?></td>
                                            </tr>
                                            <tr>
                                                <td>NIK</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sktm->nik_anak ?></td>
                                            </tr>
                                            <tr>
                                                <td>Sekolah</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sktm->sekolah ?></td>
                                            </tr>
                                            <tr>
                                                <td>NIM </td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sktm->nim_nisn ?></td>
                                            </tr>
                                            <tr>
                                                <td>Semester </td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sktm->semester ?></td>
                                            </tr>
                                            <tr>
                                                <td>Keterangan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sktm->keterangan ?></td>
                                            </tr>
                                            <tr>
                                                <td>Keperluan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sktm->keperluan ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>BERKAS PERSYARATAN</b></td>
                                            </tr>
                                            <tr>
                                                <td>Scan KK</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_SKTM/'.$sktm->scan_kk ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_SKTM/'.$sktm->scan_kk ?>" alt="Scan KK">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan E-KTP</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_SKTM/'.$sktm->scan_ektp ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_SKTM/'.$sktm->scan_ektp ?>" alt="Scan E-KTP">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan Pajak</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_SKTM/'.$sktm->scan_pajak ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_SKTM/'.$sktm->scan_pajak ?>" alt="Scan E-KTP">
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endforeach;?>
                                        </table>
                                        <hr>
                                        <?php echo form_open('RT/Surat/Surat_ajuan/update_sktm')?>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-sm-left">Status Monitoring</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                <select class="custom-select d-block mr-4" id="id_status" name="id_status" onchange="if (this.selectedIndex==1){ document.getElementById('tampil_alasan').style.display = 'inline' }else { document.getElementById('tampil_tanggal').style.display = 'none' };">
                                                    <option value="2">Setujui</option>
                                                    <option value="7">Ditolak</option>
                                                    </select>
                                                    <span id="tampil_alasan" style="display:none;">
                                                    <label>Alasan Ditolak</label>
                                                    <input type="text"  name="ditolak" class="form-control">
                                                    </span>
                                                </div>
                                                <input name="tgl_riwayat" id="tgl_riwayat" class="form-control" value="<?php echo date('Y-m-d H:i:s');?>" hidden>
                                            </div>
                                            <input name="id_surat" class="form-control" value="<?php echo $sktm->id_surat ?>" hidden>
                                            <button class="btn btn-success btn-block" type="submit" data-toggle="tooltip" data-placement="top" title="Terima" >
                                                <i class="fa fa-check-square"></i> Konfirmasi
                                            </button> 
                                        <?php echo form_close()?>
                                        <br>
                                </div>
                            </div>
                        </div>
                    </div>

                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("RT/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        
        <?php $this->load->view("RT/_Partials/Js.php") ?>
        
    </body>
    <div id="myModal" class="modal">

            <!-- The Close Button -->
            <span class="close">&times;</span>

            <!-- Modal Content (The Image) -->
            <img class="modal-content" id="img01">

            <!-- Modal Caption (Image Text) -->
            <div id="caption"></div>
        </div>
    </div>
</html>