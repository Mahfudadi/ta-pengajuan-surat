<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("RT/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("RT/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("RT/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4 text-center ">Detail Data Ajuan Surat Keterangan Kelahiran</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Surat Ajuan</li>
                            <li class="breadcrumb-item active" >Keterangan Kelahiran</li>
                            <li class="breadcrumb-item active">Detail Data Ajuan Surat Keterangan Kelahiran</li>
                        </ol>
                        <a class="btn btn-danger rounded" href="<?php echo base_url('RT/Surat/Surat_Ajuan') ?>"><i class="fas fa-reply"></i> Kembali</a>
                        <hr>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-user mr-1"></i>
                                <?php foreach ($detailkelahiran as $lahir) : ?>
                                <?php echo $lahir->nama_lengkap ?>
                                <?php endforeach;?>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table-form" border="0" width="50%" cellpadding="5" cellspacing="0">
                                        <?php
                                        $no=1;
                                        foreach ($detailkelahiran as $lahir) : ?>
                                            <tr>
                                                <td>Nomor Surat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->no_surat ?></td>
                                            </tr>
                                            <tr>
                                                <td >Tanggal Surat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo format_indo($lahir->tgl_surat) ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA KK</b></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Kepala Keluarga</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->kepala_keluarga ?></td>
                                            </tr>
                                            <tr>
                                                <td>Nomor Kartu Keluarga</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->no_kk_keluarga ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA PELAPOR</b></td>
                                            </tr>
                                            <tr>
                                                <td>NIK</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->nik ?></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->nama_lengkap ?></td>
                                            </tr>
                                            <tr>
                                                <td>Umur</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->umur ?></td>
                                            </tr>
                                            <tr>
                                                <td>Jenis Kelamin</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->jenis_kelamin ?></td>
                                            </tr>
                                            <tr>
                                                <td>Pekerjaan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->pekerjaan ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->alamat ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA BAYI/ANAK</b></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->nama_bayi ?></td>
                                            </tr>
                                            <tr>
                                                <td>Jenis Kelamin</td>
                                                <td width="1%">:</td>
                                                <td>jenkel_bayi</td>
                                            </tr>
                                            <tr>
                                                <td>Tempat Dilahirkan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->tempat_dilahirkan ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tempat Kelahiran</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->tempat_kelahiran ?></td>
                                            </tr>
                                            <tr>
                                                <td>Hari dan Tanggal Lahir</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->hari ?>, <?php echo tgl_indo($lahir->tanggal) ?></td>
                                            </tr>
                                            <tr>
                                                <td>Pukul</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->pukul ?></td>
                                            </tr>
                                            <tr>
                                                <td>Jenis Kelahiran</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->jenis_kelahiran ?></td>
                                            </tr>
                                            <tr>
                                                <td>Kelahiran Ke</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->kelahiran_ke ?></td>
                                            </tr>
                                            <tr>
                                                <td>Penolong Kelahiran</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->penolong_kelahiran ?></td>
                                            </tr>
                                            <tr>
                                                <td>Berat Bayi</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->berat_bayi ?></td>
                                            </tr>
                                            <tr>
                                                <td>Panjang Bayi</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->panjang_bayi ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA IBU BAYI</b></td>
                                            </tr>
                                            <tr>
                                                <td>NIK</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->nik_ibu ?></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->nama_ibu ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Lahir / Umur</td>
                                                <td width="1%">:</td>
                                                <td><?php echo tgl_indo($lahir->tgl_lahir_ibu) ?> / <?php echo $lahir->umur_ibu ?></td>
                                            </tr>
                                            <tr>
                                                <td>Pekerjaan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->pekerjaan_ibu ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->alamat_ibu ?></td>
                                            </tr>
                                            <tr>
                                                <td>Kewarganegaraan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->kewarganegaraan_ibu ?></td>
                                            </tr>
                                            <tr>
                                                <td>Kebangsaan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->kebangsaan_ibu ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Pencatatan Perkawinan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo tgl_indo($lahir->tgl_kawin) ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA AYAH BAYI</b></td>
                                            </tr>
                                            <tr>
                                                <td>NIK</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->nik_ayah ?></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->nama_ayah ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Lahir / Umur</td>
                                                <td width="1%">:</td>
                                                <td><?php echo tgl_indo($lahir->tgl_lahir_ayah) ?> / <?php echo $lahir->umur_ayah ?></td>
                                            </tr>
                                            <tr>
                                                <td>Pekerjaan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->pekerjaan_ayah ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->alamat_ayah ?></td>
                                            </tr>
                                            <tr>
                                                <td>Kewarganegaraan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->kewarganegaraan_ayah ?></td>
                                            </tr>
                                            <tr>
                                                <td>Kebangsaan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->kebangsaan_ayah ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA SAKSI I</b></td>
                                            </tr>
                                            <tr>
                                                <td>NIK</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->nik_saksi_a ?></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->nama_saksi_a ?></td>
                                            </tr>
                                            <tr>
                                                <td>Umur</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->umur_saksi_a ?></td>
                                            </tr>
                                            <tr>
                                                <td>Pekerjaan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->pekerjaan_saksi_a ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->alamat_saksi_a ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA SAKSI II</b></td>
                                            </tr>
                                            <tr>
                                                <td>NIK</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->nik_saksi_b ?></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->nama_saksi_b ?></td>
                                            </tr>
                                            <tr>
                                                <td>Umur</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->umur_saksi_b ?></td>
                                            </tr>
                                            <tr>
                                                <td>Pekerjaan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->pekerjaan_saksi_a ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $lahir->alamat_saksi_b ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>BERKAS PERSYARATAN</b></td>
                                            </tr>
                                            <tr>
                                                <td>Scan KK</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_Kelahiran/'.$lahir->scan_kk ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_Kelahiran/'.$lahir->scan_kk ?>" alt="Scan KK">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan E-KTP Ibu</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_Kelahiran/'.$lahir->scan_ktp_ibu ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_Kelahiran/'.$lahir->scan_ktp_ibu ?>" alt="Scan E-KTP Ibu">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan E-KTP Ayah</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_Kelahiran/'.$lahir->scan_ktp_ayah ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_Kelahiran/'.$lahir->scan_ktp_ayah ?>" alt="Scan E-KTP Ayah">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan E-KTP Saksi I</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_Kelahiran/'.$lahir->scan_ktp_saksi_a ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_Kelahiran/'.$lahir->scan_ktp_saksi_a ?>" alt="Scan E-KTP Saksi I">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan E-KTP Saksi II</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_Kelahiran/'.$lahir->scan_ktp_saksi_b ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_Kelahiran/'.$lahir->scan_ktp_saksi_b ?>" alt="Scan E-KTP Saksi II">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan Buku Nikah</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_Kelahiran/'.$lahir->scan_buku_nikah ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_Kelahiran/'.$lahir->scan_buku_nikah ?>" alt="Scan Buku Nikah">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Surat Keterangan Rumah Sakit</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_Kelahiran/'.$lahir->surat_keterangan_rs ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_Kelahiran/'.$lahir->surat_keterangan_rs ?>" alt="Surat Keterangan RS">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Surat Pajak</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_Kelahiran/'.$lahir->scan_pajak ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_Kelahiran/'.$lahir->scan_pajak ?>" alt="Scan Pajak">
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endforeach;?>
                                        </table>
                                        <hr>
                                        <?php echo form_open('RT/Surat/Surat_ajuan/update_kelahiran')?>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-sm-left">Status Monitoring</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <select class="custom-select d-block mr-4" id="id_status" name="id_status" onchange="if (this.selectedIndex==2){ document.getElementById('tampil_alasan').style.display = 'inline' }else { document.getElementById('tampil_tanggal').style.display = 'none' };">
                                                        <option value="<?php echo $lahir->id_status ?>"><?php echo $lahir->status_monitoring ?></option>
                                                        <option value="2">Setujui</option>
                                                        <option value="7">Ditolak</option>
                                                    </select>
                                                    <span id="tampil_alasan" style="display:none;">
                                                    <label>Alasan Ditolak</label>
                                                    <input type="text"  name="ditolak" class="form-control">
                                                    </span>
                                                </div>
                                                <input name="tgl_riwayat" id="tgl_riwayat" class="form-control" value="<?php echo date('Y-m-d H:i:s');?>" hidden>
                                            </div>
                                            <input name="id_surat" class="form-control" value="<?php echo $lahir->id_surat ?>" hidden>
                                            <button class="btn btn-success btn-block" type="submit" data-toggle="tooltip" data-placement="top" title="Terima" >
                                                <i class="fa fa-check-square"></i> Konfirmasi
                                            </button> 
                                        <?php echo form_close()?>
                                        <br>
                                </div>
                            </div>
                        </div>
                    </div>

                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("RT/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("RT/_Partials/Js.php") ?>
    </body>
</html>