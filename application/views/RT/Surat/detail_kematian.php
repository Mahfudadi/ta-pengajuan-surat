<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("RT/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("RT/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("RT/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4 text-center ">Detail Data Ajuan Surat Keterangan Kematian</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Surat Ajuan</li>
                            <li class="breadcrumb-item active" >Keterangan Kematian</li>
                            <li class="breadcrumb-item active">Detail Data Ajuan Surat Keterangaan Kematian</li>
                        </ol>
                        <a class="btn btn-danger rounded" href="<?php echo base_url('RT/Surat/Surat_Ajuan') ?>"><i class="fas fa-reply"></i> Kembali</a>
                        <hr>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-user mr-1"></i>
                                <?php foreach ($detailkematian as $kematian) : ?>
                                <?php echo $kematian->nama_lengkap ?>
                                <?php endforeach;?>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table-form" border="0" width="50%" cellpadding="5" cellspacing="0">
                                        <?php
                                        $no=1;
                                        foreach ($detailkematian as $kematian) : ?>
                                            <tr>
                                                <td>Nomor Surat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->no_surat ?></td>
                                            </tr>
                                            <tr>
                                                <td >Tanggal Surat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo format_indo($kematian->tgl_surat) ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA KK</b></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Kepala Keluarga</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->nama_kepala_keluarga ?></td>
                                            </tr>
                                            <tr>
                                                <td>Nomor Kartu Keluarga</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->no_kk_kepala_keluarga ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA PELAPOR</b></td>
                                            </tr>
                                            <tr>
                                                <td>NIK</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->nik ?></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap </td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->nama_lengkap ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Lahir / Umur</td>
                                                <td width="1%">:</td>
                                                <td><?php echo tgl_indo($kematian->tanggal_lahir) ?> / <?php echo $kematian->umur ?></td>
                                            </tr>
                                            <tr>
                                                <td>Pekerjaan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->pekerjaan ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->alamat ?>RT <?php echo $kematian->rt ?> RW <?php echo $kematian->rw ?> Dusun <?php echo $kematian->dusun ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA JENAZAH</b></td>
                                            </tr>
                                            <tr>
                                                <td>NIK</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->nik_jenazah ?></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->nama_jenazah ?></td>
                                            </tr>
                                            <tr>
                                                <td>Jenis Kelamin</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->jenkel_jenazah ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Lahir / Umur</td>
                                                <td width="1%">:</td>
                                                <td><?php echo tgl_indo($kematian->tgl_lahir_jenazah) ?>/ <?php echo $kematian->umur ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tempat Lahir</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->tempat_lahir_jenazah ?></td>
                                            </tr>
                                            <tr>
                                                <td>Agama</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->agama_jenazah ?></td>
                                            </tr>
                                            <tr>
                                                <td>Pekerjaan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->pekerjaan_jenazah ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->alamat_jenazah ?></td>
                                            </tr>
                                            <tr>
                                                <td>Kewarganegaraan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->kewarganegaraan_jenazah ?></td>
                                            </tr>
                                            <tr>
                                                <td>Anak Ke</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->anak_ke ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Kematian</td>
                                                <td width="1%">:</td>
                                                <td><?php echo tgl_indo($kematian->tgl_kematian) ?></td>
                                            </tr>
                                            <tr>
                                                <td>Pukul</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->pukul ?></td>
                                            </tr>
                                            <tr>
                                                <td>Sebab Kematian</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->sebab_kematian ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tempat Kematian</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->tempat_kematian ?></td>
                                            </tr>
                                            <tr>
                                                <td>Yang Menerangkan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->yang_menerangkan ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA IBU</b></td>
                                            </tr>
                                            <tr>
                                                <td>NIK</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->nik_ibu ?></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap </td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->nama_ibu ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Lahir / Umur</td>
                                                <td width="1%">:</td>
                                                <?php if($kematian->tgl_lahir_ibu=="0000-00-00"||$kematian->tgl_lahir_ibu==""){?>
                                                <td>-</td>
                                                <?php }else{ ?>
                                                    <td><?php echo tgl_indo($kematian->tgl_lahir_ibu) ?> / <?php echo $kematian->umur_ibu?></td>
                                                <?php } ?>
                                            </tr>
                                            <tr>
                                                <td>Pekerjaan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->pekerjaan_ibu ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->alamat_ibu ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA AYAH</b></td>
                                            </tr>
                                            <tr>
                                                <td>NIK</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->nik_ayah ?></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap </td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->nama_ayah ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Lahir / Umur</td>
                                                <td width="1%">:</td>
                                                <?php if($kematian->tgl_lahir_ayah=="0000-00-00"||$kematian->tgl_lahir_ayah==""){?>
                                                <td>-</td>
                                                <?php }else{ ?>
                                                    <td><?php echo tgl_indo($kematian->tgl_lahir_ayah) ?> / <?php echo $kematian->umur_ayah?></td>
                                                <?php } ?>
                                            </tr>
                                            <tr>
                                                <td>Pekerjaan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->pekerjaan_ayah ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->alamat_ayah ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA SAKSI I</b></td>
                                            </tr>
                                            <tr>
                                                <td>NIK</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->nik_saksi_a ?></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap </td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->nama_saksi_a ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Lahir / Umur</td>
                                                <td width="1%">:</td>
                                                <?php if($kematian->tgl_lahir_saksi_a=="0000-00-00"||$kematian->tgl_lahir_saksi_a==""){?>
                                                <td>-</td>
                                                <?php }else{ ?>
                                                    <td><?php echo tgl_indo($kematian->tgl_lahir_saksi_a) ?> / <?php echo $kematian->umur_saksi_a ?></td>
                                                <?php } ?>
                                            </tr>
                                            <tr>
                                                <td>Pekerjaan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->pekerjaan_saksi_a ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->alamat_saksi_a ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA SAKSI II</b></td>
                                            </tr>
                                            <tr>
                                                <td>NIK</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->nik_saksi_b ?></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap </td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->nama_saksi_b ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Lahir / Umur</td>
                                                <td width="1%">:</td>
                                                <?php if($kematian->tgl_lahir_saksi_b=="0000-00-00"||$kematian->tgl_lahir_saksi_b==""){?>
                                                <td>-</td>
                                                <?php }else{ ?>
                                                    <td><?php echo tgl_indo($kematian->tgl_lahir_saksi_b) ?> / <?php echo $kematian->umur_saksi_b ?></td>
                                                <?php } ?>
                                            </tr>
                                            <tr>
                                                <td>Pekerjaan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->pekerjaan_saksi_b ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $kematian->alamat_saksi_b ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>BERKAS PERSYARATAN</b></td>
                                            </tr>
                                            <tr>
                                                <td>Scan KK</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_Kematian/'.$kematian->scan_kk ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_Kematian/'.$kematian->scan_kk ?>" alt="Scan KK">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan E-KTP Orang yang Meninggal</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_Kematian/'.$kematian->scan_ektp_meninggal ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_Kematian/'.$kematian->scan_ektp_meninggal ?>" alt="Scan EKTP Meninggal">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan E-KTP Saksi I</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_Kematian/'.$kematian->scan_ektp_saksi_a ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_Kematian/'.$kematian->scan_ektp_saksi_a ?>" alt="Scan EKTP Saksi I">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan E-KTP Saksi II</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_Kematian/'.$kematian->scan_ektp_saksi_b ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_Kematian/'.$kematian->scan_ektp_saksi_b ?>" alt="Scan EKTP Saksi II">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Surat Keterangan Rumah Sakit</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_Kematian/'.$kematian->suket_meninggal ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_Kematian/'.$kematian->suket_meninggal ?>" alt="Surat Keterangan Rumah Sakit">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Surat Pajak</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_Kematian/'.$kematian->scan_pajak ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_Kematian/'.$kematian->scan_pajak ?>" alt="Scan Pajak">
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endforeach;?>
                                        </table>
                                        <hr>
                                        <?php echo form_open('RT/Surat/Surat_ajuan/update_kematian')?>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-sm-left">Status Monitoring</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                <select class="custom-select d-block mr-4" id="id_status" name="id_status" onchange="if (this.selectedIndex==1){ document.getElementById('tampil_alasan').style.display = 'inline' }else { document.getElementById('tampil_tanggal').style.display = 'none' };">
                                                    <option value="2">Setujui</option>
                                                    <option value="7">Ditolak</option>
                                                    </select>
                                                    <span id="tampil_alasan" style="display:none;">
                                                    <label>Alasan Ditolak</label>
                                                    <input type="text"  name="ditolak" class="form-control">
                                                    </span>
                                                </div>
                                                <input name="tgl_riwayat" id="tgl_riwayat" class="form-control" value="<?php echo date('Y-m-d H:i:s');?>" hidden>
                                            </div>
                                            <input name="id_surat" class="form-control" value="<?php echo $kematian->id_surat ?>" hidden>
                                            <button class="btn btn-success btn-block" type="submit" data-toggle="tooltip" data-placement="top" title="Terima" >
                                                <i class="fa fa-check-square"></i> Konfirmasi
                                            </button> 
                                        <?php echo form_close()?>
                                        <br>
                                </div>
                            </div>
                        </div>
                    </div>

                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("RT/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("RT/_Partials/Js.php") ?>
    </body>
</html>