<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("RT/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("RT/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("RT/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4 text-center ">Detail Data Ajuan Biodata Penduduk</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Surat Ajuan</li>
                            <li class="breadcrumb-item active" >Ijin Keramaian</li>
                            <li class="breadcrumb-item active">Detail Data Ajuan Surat Ijin Keramaian</li>
                        </ol>
                        <a class="btn btn-danger rounded" href="<?php echo base_url('RT/Surat/Surat_Ajuan') ?>"><i class="fas fa-reply"></i> Kembali</a>
                        <hr>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-user mr-1"></i>
                                <?php foreach ($detailkeramaian as $ramai) : ?>
                                <?php echo $ramai->nama_lengkap ?>
                                <?php endforeach;?>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table width="100%" class="table-form" border="0" cellpadding="5" cellspacing="0">
                                        <?php
                                        $no=1;
                                        foreach ($detailkeramaian as $ramai) : ?>
                                        <tr>
                                            <td>Nomor Surat</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $ramai->no_surat ?></td>
                                        </tr>
                                        <tr>
                                            <td >Tanggal Surat</td>
                                            <td width="1%">:</td>
                                            <td><?php echo format_indo($ramai->tgl_surat) ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>DATA PEMOHON</b></td>
                                        </tr>
                                        <tr>
                                            <td>Nama Lengkap </td>
                                            <td width="1%">:</td>
                                            <td><?php echo $ramai->nama_lengkap ?></td>
                                        </tr>
                                        <tr>
                                            <td>Tempat, Tanggal Lahir</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $ramai->tempat_lahir ?>, <?php echo tgl_indo($ramai->tanggal_lahir) ?></td>
                                        </tr>
                                        <tr>
                                            <td>Umur</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $ramai->umur ?></td>
                                        </tr>
                                        <tr>
                                            <td>Pekerjaan</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $ramai->pekerjaan ?></td>
                                        </tr>
                                        <tr>
                                            <td>NIK</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $ramai->nik ?></td>
                                        </tr>
                                        <tr>
                                            <td>Alamat</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $ramai->alamat ?>RT <?php echo $ramai->rt ?> RW <?php echo $ramai->rw ?> Dusun <?php echo $ramai->dusun ?> Desa <?php echo $ramai->desa ?> Kecamatan <?php echo $ramai->kecamatan ?> Kabupaten/Kota <?php echo $ramai->kab_kota ?></td>
                                        </tr>
                                        <tr>
                                            <td>Jenis Kegiatan</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $ramai->jenis_kegiatan ?></td>
                                        </tr>
                                            <td>Penanggung Jawab</td>
                                            <td width="1%">:</td>
                                            <td><?php echo $ramai->penanggungjawab ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>BERKAS PERSYARATAN</b></td>
                                        </tr>
                                        <tr>
                                            <td>Scan KK</td>
                                            <td width="1%">:</td>
                                            <td>
                                                <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_Keramaian/'.$ramai->scan_kk ?>">
                                                    <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_Keramaian/'.$ramai->scan_kk ?>" alt="Scan KK">
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Scan E-KTP</td>
                                            <td width="1%">:</td>
                                            <td>
                                                <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_Keramaian/'.$ramai->scan_ektp ?>">
                                                    <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_Keramaian/'.$ramai->scan_ektp ?>" alt="Scan EKTP">
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Scan Pajak</td>
                                            <td width="1%">:</td>
                                            <td>
                                                <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_Keramaian/'.$ramai->scan_pajak ?>">
                                                    <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_Keramaian/'.$ramai->scan_pajak ?>" alt="Scan Pajak">
                                                </a>
                                            </td>
                                        </tr>
                                        <?php endforeach;?>
                                    </table>
                                        <hr>
                                        <?php echo form_open('RT/Surat/Surat_ajuan/update_keramaian')?>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-sm-left">Status Monitoring</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                <select class="custom-select d-block mr-4" id="id_status" name="id_status" onchange="if (this.selectedIndex==1){ document.getElementById('tampil_alasan').style.display = 'inline' }else { document.getElementById('tampil_tanggal').style.display = 'none' };">
                                                    <option value="2">Setujui</option>
                                                    <option value="7">Ditolak</option>
                                                    </select>
                                                    <span id="tampil_alasan" style="display:none;">
                                                    <label>Alasan Ditolak</label>
                                                    <input type="text"  name="ditolak" class="form-control">
                                                    </span>
                                                </div>
                                                <input name="tgl_riwayat" id="tgl_riwayat" class="form-control" value="<?php echo date('Y-m-d H:i:s');?>" hidden>
                                            </div>
                                            <input name="id_surat" class="form-control" value="<?php echo $ramai->id_surat ?>" hidden>
                                            <button class="btn btn-success btn-block" type="submit" data-toggle="tooltip" data-placement="top" title="Terima" >
                                                <i class="fa fa-check-square"></i> Konfirmasi
                                            </button> 
                                        <?php echo form_close()?>
                                        <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("RT/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("RT/_Partials/Js.php") ?>
    </body>
</html>