<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("RT/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("RT/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("RT/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4 text-center ">Detail Data Ajuan Surat Pengajuan E-KTP WNI</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Surat Ajuan</li>
                            <li class="breadcrumb-item active" >Pengajuan E-KTP</li>
                            <li class="breadcrumb-item active">Detail Data Ajuan Surat Pengajuan E-KTP WNI</li>
                        </ol>
                        <a class="btn btn-danger rounded" href="<?php echo base_url('RT/Surat/Surat_Ajuan') ?>"><i class="fas fa-reply"></i> Kembali</a>
                        <hr>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-user mr-1"></i>
                                <?php foreach ($detailktp as $ktp) : ?>
                                <?php echo $ktp->nama_lengkap ?>
                                <?php endforeach;?>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table-form" border="0" width="30%" cellpadding="5" cellspacing="0">
                                        <?php
                                        $no=1;
                                        foreach ($detailktp as $ktp) : ?>
                                            <tr>
                                                <td>Nomor Surat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $ktp->no_surat ?></td>
                                            </tr>
                                            <tr>
                                                <td >Tanggal Surat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo format_indo($ktp->tgl_surat) ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA PEMOHON</b></td>
                                            </tr>
                                            <tr>
                                                <td>Permohonan KTP</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $ktp->tipe_permohonan ?></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $ktp->nama_lengkap ?></td>
                                            </tr>
                                            <tr>
                                                <td>Nomor KK</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $ktp->no_kk ?></td>
                                            </tr>
                                            <tr>
                                                <td>NIK</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $ktp->nik ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $ktp->alamat ?></td>
                                            </tr>
                                            <tr>
                                                <td>RT</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $ktp->rt ?></td>
                                            </tr>
                                            <tr>
                                                <td>RW</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $ktp->rw ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>BERKAS PERSYARATAN</b></td>
                                            </tr>
                                            <tr>
                                                <td>Scan KK</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_KTP/'.$ktp->scan_kk ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_KTP/'.$ktp->scan_kk ?>" alt="Scan KK">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan E-KTP Lama</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_KTP/'.$ktp->ektp_lama ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_KTP/'.$ktp->ektp_lama ?>" alt="EKTP Lama">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan Akta Kelahiran</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_KTP/'.$ktp->akta ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_KTP/'.$ktp->akta ?>" alt="Akta Kelahiran">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan Ijazah Terakhir</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_KTP/'.$ktp->ijazah_akhir ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_KTP/'.$ktp->ijazah_akhir ?>" alt="Ijazah Akhir">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan Surat Nikah / Surat Cerai</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_KTP/'.$ktp->surat_nikah_cerai ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_KTP/'.$ktp->surat_nikah_cerai ?>" alt="Surat Nikah Cerai">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Foto 3x4 Terbaru</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_KTP/'.$ktp->foto ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_KTP/'.$ktp->foto ?>" alt="Foto 3x4">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan Pajak</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_KTP/'.$ktp->scan_pajak ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_KTP/'.$ktp->scan_pajak ?>" alt="Scan Pajak">
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endforeach;?>
                                        </table>
                                        <hr>
                                        <?php echo form_open('RT/Surat/Surat_ajuan/update_ktp')?>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-sm-left">Status Monitoring</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                <select class="custom-select d-block mr-4" id="id_status" name="id_status" onchange="if (this.selectedIndex==1){ document.getElementById('tampil_alasan').style.display = 'inline' }else { document.getElementById('tampil_tanggal').style.display = 'none' };">
                                                    <option value="2">Setujui</option>
                                                    <option value="7">Ditolak</option>
                                                    </select>
                                                    <span id="tampil_alasan" style="display:none;">
                                                    <label>Alasan Ditolak</label>
                                                    <input type="text"  name="ditolak" class="form-control">
                                                    </span>
                                                </div>
                                                <input name="tgl_riwayat" id="tgl_riwayat" class="form-control" value="<?php echo date('Y-m-d H:i:s');?>" hidden>
                                            </div>
                                            <input name="id_surat" class="form-control" value="<?php echo $ktp->id_surat ?>" hidden>
                                            <button class="btn btn-success btn-block" type="submit" data-toggle="tooltip" data-placement="top" title="Terima" >
                                                <i class="fa fa-check-square"></i> Konfirmasi
                                            </button> 
                                        <?php echo form_close()?>
                                        <br>
                                </div>
                            </div>
                        </div>
                    </div>

                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("RT/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("RT/_Partials/Js.php") ?>
    </body>
</html>