<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu" style="background-color: grey">
            <div class="nav">
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image" style="margin-left: 15px">
                        <img src="<?php echo base_url('assets/images/icon/user.png') ?>" class="img-circle elevation-2" alt="User Image" style="width: 50px">
                    </div>
                    <div class="">
                        <div class="info" style="margin-left: 12px">
                            <strong><a href="<?php echo base_url('RT/Profil/Data_rt')?>" class="d-block text-light"></a></strong>
                        </div>
                        <div class="info" style="margin-left: 12px">
                            <a href="<?php echo base_url('RT/Profil/Data_rt/index/'.$this->session->userdata('ses_id')); ?>" class="d-block text-light" style="margin-bottom:0px">RT <?php echo $user['ketua_rt']; ?></a>
                            <p class="d-block text-light"><?php echo $user['nik']; ?></p>
                        </div>
                    </div>
                </div>
                <a class="nav-link" href="<?php echo base_url ('RT/BerandaRT')?>">
                    <div class="sb-nav-link-icon"><i class="fas fas fa-home"></i></div>
                    Beranda
                </a>
                <a class="nav-link" href="<?php echo base_url ('RT/Surat/Surat_Ajuan')?>">
                    <div class="sb-nav-link-icon"><i class="fas fa-envelope"></i></div>
                    Surat Ajuan
                </a>
                <?php 
                    $koneksi = mysqli_connect("localhost","root","","surat_desagurah");
                    // Check connection
                    if (mysqli_connect_errno()){
                        echo "Koneksi database gagal : " . mysqli_connect_error();
                    }
                 ?>
                <?php  
                    $where = $this->session->userdata('ses_id');
                    
                    $hasil = mysqli_query($koneksi," SELECT *
                        FROM tb_rt
                        WHERE id_user='$where'");
                        while($sd = mysqli_fetch_array($hasil)){ 
                            $id = $sd['ketua_rt'];?>

                <a class="nav-link" href="<?php echo base_url('RT/Riwayat/RiwayatRT/index/'.$id)?>">
                    <div class="sb-nav-link-icon"><i class="fas fa-list-alt"></i></div>
                    Riwayat
                </a>
                <?php } ?>
            </div>
        </div>
        <div class="sb-sidenav-footer">
            <div class="small">Pemerintah</div>
            Desa Gurah
        </div>
    </nav>
</div>