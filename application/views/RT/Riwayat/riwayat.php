<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("RT/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("RT/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("RT/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 align="center" class="mt-4">Riwayat Ajuan Surat </h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Riwayat</li>
                        </ol>
                        <hr>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                Riwayat Surat Ajuan
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" style="font-size:12px">
                                        <thead class="text-center">
                                            <tr>
                                                <th>Jenis Surat</th>
                                                <th>No. Surat</th>
                                                <th width="20%">Nama Lengkap</th>
                                                <th>Tanggal Surat</th>
                                                <th>Tanggal Diubah</th>
                                                <th>Status</th>
                                                <th>Alasan Ditolak</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php
                                                $no=1;
                                                foreach ($riwayat as $data) : ?>
                                                <tr>
                                                    <?php if(substr($data->id_surat,0,7) == 'biodata'){ ?>
                                                        <td>Surat Keterangan Biodata</td>
                                                    <?php }elseif(substr($data->id_surat,0,8) == 'domisili'){ ?>
                                                        <td>Surat Keterangan Domisili</td>
                                                    <?php }elseif(substr($data->id_surat,0,9) == 'keramaian') { ?>
                                                        <td>Surat Ijin Keramaian</td>
                                                    <?php }elseif(substr($data->id_surat,0,9) == 'kelahiran'){ ?>
                                                        <td>Surat Kelahiran</td>
                                                    <?php }elseif(substr($data->id_surat,0,8) == 'kematian'){ ?>
                                                        <td>Surat Keterangan Kematian</td>
                                                    <?php }elseif(substr($data->id_surat,0,3) == 'kip'){ ?>
                                                        <td>Surat Pengajuan KIP</td>
                                                    <?php }elseif(substr($data->id_surat,0,5) == 'Kuasa'){ ?>
                                                        <td>Surat Kuasa</td>
                                                    <?php }elseif(substr($data->id_surat,0,3) == 'ktp'){ ?>
                                                        <td>Surat Permohonan EKTP</td>
                                                    <?php }elseif(substr($data->id_surat,0,2) == 'kk'){ ?>
                                                        <td>Surat Permohonan Perubahan KK</td>
                                                    <?php }elseif(substr($data->id_surat,0,4) == 'skck'){ ?>
                                                        <td>Surat Pengantar SKCK</td>
                                                    <?php }elseif(substr($data->id_surat,0,4) == 'sktm'){ ?>
                                                        <td>Surat Pengajuan SKTM</td>
                                                    <?php }elseif(substr($data->id_surat,0,5) == 'sptjm'){ ?>
                                                        <td>Surat Pengajuan SPTJM</td>
                                                    <?php }else{echo "data gagal";} ?>
                                                    <td><?php echo $data->no_surat ?></td>
                                                    <td><?php echo $data->nama_lengkap ?></td>
                                                    <td><?php echo format_indo($data->tgl_surat) ?></td>
                                                    <td><?php echo format_indo($data->tgl_riwayat) ?></td>
                                                    <?php if($data->riwayat_status==2){?>
                                                        <td>Menunggu Diproses</td>
                                                    <?php }else{?>
                                                        <td>Surat Ditolak</td>
                                                    <?php }?>
                                                    <td><?php echo $data->ket_tolak ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("RT/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("RT/_Partials/Js.php") ?>
    </body>
</html>