<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("Kades/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("Kades/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("Kades/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 align="center" class="mt-4">Laporan TTD Surat </h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Laporan</li>
                        </ol>
                        <hr>
                        <form class="form-horizontal style-form" action="<?php echo base_url().'Kades/Laporan/LaporanKades/index' ?>" method="post">
                            <div class="form-group">
                                <label for="filter" class="col-form-label"><b>Filter</b></label>
                                <div class="input-group mb-3">
                                    <label for="jenis_surat" class="col-form-label mr-2"><b>Jenis Surat</b></label>
                                    <select class="custom-select d-block col-md-3 mr-2" id="jenis_surat" name="jenis_surat" required>
                                        <option value="0">Semua Data</option>
                                        <option value="1">Surat Kuasa</option>
                                        <option value="2">Surat Keterangan Domisili</option>
                                        <option value="3">Surat Pengajuan Kartu Indonesia Pintar</option>
                                        <option value="4">Surat Keterangan Tidak Mampu</option>
                                        <option value="5">Surat Ijin Keramaian</option>
                                        <option value="6">Surat Keterangan Biodata Penduduk</option>
                                        <option value="7">Surat Pernyataan Tanggung Jawab Mutlak (SPTJM)</option>
                                        <option value="8">Surat Permohonan Pengantar SKCK</option>
                                        <option value="9">Surat Kelahiran</option>
                                        <option value="10">Surat Permohonan Perubahan KK</option>
                                        <option value="11">Surat Keterangan Kematian</option>
                                        <option value="12">Surat Permohonan E-KTP WNI</option>
                                    </select>
                                    <label for="bulan" class="col-form-label mr-2"><b>Bulan & Tahun</b></label>
                                    <select class="custom-select d-block col-md-3" id="bulan" name="bulan" required>
                                        <option value="0">--Pilih Bulan--</option>
                                        <option value="01">Januari</option>
                                        <option value="02">Februari</option>
                                        <option value="03">Maret</option>
                                        <option value="04">April</option>
                                        <option value="05">Mei</option>
                                        <option value="06">Juni</option>
                                        <option value="07">Juli</option>
                                        <option value="08">Agustus</option>
                                        <option value="09">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                    <select class="custom-select d-block col-md-3" id="tahun" name="tahun" required>
                                        <option value="0">--Pilih Tahun--</option>
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                        <option value="2025">2025</option>
                                    </select>
                                    <button type="submit" class="btn btn-md btn-outline-secondary ml-4" >
                                        <i class="fas fa-search-plus"></i> Tampilkan
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                               Laporan TTD Surat Ajuan
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" style="font-size:11px">
                                        <thead class="text-center">
                                            <tr>
                                                <th>Jenis Surat</th>
                                                <th>No. Surat</th>
                                                <th width="18%">Nama Lengkap</th>
                                                <th>Tanggal Diajukan</th>
                                                <th>Tanggal Selesai</th>
                                                <th width="13%">Status</th>
                                                <th>Alasan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php
                                                $no=1;
                                                foreach ($data as $lp) : ?>
                                                <tr>
                                                    <?php if(substr($lp->id_surat,0,7) == 'biodata'){ ?>
                                                        <td>Surat Keterangan Biodata</td>
                                                    <?php }elseif(substr($lp->id_surat,0,8) == 'domisili'){ ?>
                                                        <td>Surat Keterangan Domisili</td>
                                                    <?php }elseif(substr($lp->id_surat,0,9) == 'keramaian') { ?>
                                                        <td>Surat Ijin Keramaian</td>
                                                    <?php }elseif(substr($lp->id_surat,0,9) == 'kelahiran'){ ?>
                                                        <td>Surat Kelahiran</td>
                                                    <?php }elseif(substr($lp->id_surat,0,8) == 'kematian'){ ?>
                                                        <td>Surat Keterangan Kematian</td>
                                                    <?php }elseif(substr($lp->id_surat,0,3) == 'kip'){ ?>
                                                        <td>Surat Pengajuan KIP</td>
                                                    <?php }elseif(substr($lp->id_surat,0,5) == 'Kuasa'){ ?>
                                                        <td>Surat Kuasa</td>
                                                    <?php }elseif(substr($lp->id_surat,0,3) == 'ktp'){ ?>
                                                        <td>Surat Permohonan EKTP</td>
                                                    <?php }elseif(substr($lp->id_surat,0,2) == 'kk'){ ?>
                                                        <td>Surat Permohonan Perubahan KK</td>
                                                    <?php }elseif(substr($lp->id_surat,0,4) == 'skck'){ ?>
                                                        <td>Surat Pengantar SKCK</td>
                                                    <?php }elseif(substr($lp->id_surat,0,4) == 'sktm'){ ?>
                                                        <td>Surat Pengajuan SKTM</td>
                                                    <?php }elseif(substr($lp->id_surat,0,5) == 'sptjm'){ ?>
                                                        <td>Surat Pengajuan SPTJM</td>
                                                    <?php }else{echo "data gagal";} ?>
                                                    <td><?php echo $lp->no_surat ?></td>
                                                    <td><?php echo $lp->nama_lengkap ?></td>
                                                    <td><?php echo format_indo($lp->tgl_surat) ?></td>
                                                    <td><?php echo format_indo($lp->tgl_riwayat) ?></td>
                                                    <?php if($lp->riwayat_status==5){?>
                                                        <td>Surat Belum Diunduh</td>
                                                    <?php }else{?>
                                                        <td>Surat Ditolak</td>
                                                    <?php }?>
                                                    <td><?php echo $lp->ket_tolak ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("Kades/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("Kades/_Partials/Js.php") ?>
    </body>
</html>