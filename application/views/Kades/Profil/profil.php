<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("Kades/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("Kades/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("Kades/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 align="center" class="mt-4">Profil Kepala Desa</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Profil</li>
                        </ol>
                        <hr>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-user mr-1"></i>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="table-responsive">
                                            <table class="table-form" border="0" width="100%" cellpadding="5" cellspacing="0">
                                                <?php
                                                foreach ($profil as $pr) : ?>
                                                    <tr>
                                                        <td width="20%">NIP</td>
                                                        <td width="1%">:</td>
                                                        <td><?php echo $pr->nip ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%">Nama Lengkap</td>
                                                        <td width="1%">:</td>
                                                        <td><?php echo $pr->nama ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%">Alamat</td>
                                                        <td width="1%">:</td>
                                                        <td><?php echo $pr->alamat ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tanda Tangan</td>
                                                        <td width="1%">:</td>
                                                        <td>
                                                            <a data-fancybox="gallery" href="<?php echo base_url().'assets/images/ttd_kades/'.$pr->ttd ?>">
                                                                <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/images/ttd_kades/'.$pr->ttd ?>" alt="TTD">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach?>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("Kades/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("Kades/_Partials/Js.php") ?>
    </body>
</html>