<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("Kades/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("Kades/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("Kades/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 align="Left" class="mt-4">Ubah Tanda Tangan Kepala Desa</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Ubah Tanda Tangan</li>
                        </ol>
                        <hr>
                        <div class="row">
                            <div class="col-12">
                                <div class="signature-pad" id="signature-pad">
                                    <div class="m-signature-pad">
                                        <div class="m-signature-pad-body">
                                        <canvas width="625" height="318"></canvas>
                                        </div>
                                    </div>
                                    <div class="m-signature-pad-footer">
                                        <button type="button"  id="save2" data-action="save" class="btn btn-success">
                                          <i class="fa fa-check"></i> Simpan
                                        </button>
                                        <button type="button" data-action="clear"  class="btn btn-danger">
                                          <i class="fa fa-eraser"></i> Hapus
                                        </button>
                                    </div>
                                    <input type="hidden" value="<?php echo rand();?>" id="rowno">
                                    <div id="previewsign1" class="previewsign">
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Warning!</h4>
                      </div>
                      <div class="modal-body">
                        <div class="alert alert-danger">
                          Sign before you submit!
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("Kades/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("Kades/_Partials/Js.php") ?>
        <script>

    var wrapper = document.getElementById("signature-pad"),
    clearButton = wrapper.querySelector("[data-action=clear]"),
    saveButton = document.getElementById("save2"),
    canvas = wrapper.querySelector("canvas"),
    signaturePad;


    function resizeCanvas() {

      var ratio =  window.devicePixelRatio || 1;
      canvas.width = canvas.offsetWidth * ratio;
      canvas.height = canvas.offsetHeight * ratio;
      canvas.getContext("2d").scale(ratio, ratio);
    }
    signaturePad = new SignaturePad(canvas);

    clearButton.addEventListener("click", function (event) {
      signaturePad.clear();
    });
    saveButton.addEventListener("click", function (event) {
      
      if (signaturePad.isEmpty()) {
        $('#myModal').modal('show');
      }

      else {
        $.ajax({
          type: "POST",
          url: "<?php echo base_url('Kades/Profil/Profil/insert_single_signature');?>",
          data: {'image': signaturePad.toDataURL(),'rowno':$('#rowno').val()},
          success: function(datas1)
          {            
            signaturePad.clear();
            $('.previewsign').html(datas1);
          }
        });
      }
    }); 

  </script>
    <style type="text/css">
    
    .previewsign
    {   
    border: 3px dashed #ccc;
    border-radius: 5px;
    color: #bbbabb;
    height: 250px;
    width: 38%;
    text-align: center;
    float: right;
    vertical-align: middle;
    top: 220px;
    position: fixed;
    right: 25px;
  }
  .m-signature-pad-body
  {
    border: 3px dashed #ccc;
    border-radius: 5px;
    color: #bbbabb;
    height: 250px;
    width: 38%;
    text-align: center;
    float: right;
    vertical-align: middle;
    top: 220px;
    position: fixed;
    left: 250px;
  }
  .m-signature-pad-footer
  {
    bottom: 120px;
    left: 250px;
    position: fixed;
  }
    .img
  {
        right: 0;
    position: absolute;
  }
</style>
    </body>
</html>