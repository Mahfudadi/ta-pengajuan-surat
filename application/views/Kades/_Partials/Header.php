<a class="navbar-brand" href="<?php echo base_url('Monitoring/Beranda')?>" ><span class="brand-text font-weight-light font-weight-bolder ml-2" ><img src="<?php echo base_url().'assets/images/demo/logo_pemkabkediri.png'?>" style="width:50px;height:50px" >DESA GURAH</a>
<button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
<strong><a class="nav-link text-white" disabled> <?php date_default_timezone_set('Asia/Jakarta');
echo format_indo(date('Y-m-d H:i:s')); ?></a></strong>
<!-- Navbar Search-->
<form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
    <div class="input-group">
    </div>
</form>
<?php 
$koneksi = mysqli_connect("localhost","root","","surat_desagurah");

// Check connection
if (mysqli_connect_errno()){
	echo "Koneksi database gagal : " . mysqli_connect_error();
}?>
<!-- Navbar-->
<ul class="navbar-nav ml-auto ml-md-0">

    <li class="dropdown mr-5">
    <?php 
        $data = mysqli_query($koneksi,"SELECT SUM(total) AS total FROM (
            SELECT COUNT('id_status') AS total FROM surat_biodata WHERE id_status = '4' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_domisili WHERE id_status = '4' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_ijin_keramaian WHERE id_status = '4' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kelahiran WHERE id_status = '4' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kematian WHERE id_status = '4' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kip WHERE id_status = '4' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kuasa WHERE id_status = '4' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_permohonan_ktp WHERE id_status = '4' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_perubahan_kk WHERE id_status = '4' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_skck WHERE id_status = '4' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_sktm WHERE id_status = '4' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_sptjm WHERE id_status = '4'
        )X");
        while($d = mysqli_fetch_array($data)){ ?>
        <a href="javascript:void(0);" class="dropdown-toggle btn btn-info" data-toggle="dropdown" role="button">
            <i class="fas fa-bell"></i>
            <span class="badge badge-light"><?php echo $d['total']; ?></span>
        </a>
        <?php } ?>
        <ul class="dropdown-menu" style="overflow:auto;width:230px;height:500px">
            <li class="header text-center">Surat Menunggu Disetujui</li>
            <hr>
            <li class="body">
                <div class="list-group">
                <?php 
                    $data = mysqli_query($koneksi,"SELECT id_surat, no_surat, id_user, nama_lengkap, tgl_surat, id_status, status_monitoring FROM (
                        SELECT id_surat, no_surat, surat_biodata.id_user, nama_lengkap, tgl_surat, surat_biodata.id_status, status_monitoring FROM surat_biodata JOIN tb_warga ON surat_biodata.id_user = tb_warga.id_user JOIN tb_status ON surat_biodata.id_status = tb_status.id_status WHERE surat_biodata.id_status='4' UNION ALL
                        SELECT id_surat, no_surat, surat_domisili.id_user, nama_lengkap, tgl_surat, surat_domisili.id_status, status_monitoring FROM surat_domisili JOIN tb_warga ON surat_domisili.id_user = tb_warga.id_user JOIN tb_status ON surat_domisili.id_status = tb_status.id_status WHERE surat_domisili.id_status='4' UNION ALL
                        SELECT id_surat, no_surat, surat_ijin_keramaian.id_user, nama_lengkap, tgl_surat, surat_ijin_keramaian.id_status, status_monitoring FROM surat_ijin_keramaian JOIN tb_warga ON surat_ijin_keramaian.id_user = tb_warga.id_user JOIN tb_status ON surat_ijin_keramaian.id_status = tb_status.id_status WHERE surat_ijin_keramaian.id_status='4' UNION ALL
                        SELECT id_surat, no_surat, surat_kelahiran.id_user, nama_lengkap, tgl_surat, surat_kelahiran.id_status, status_monitoring FROM surat_kelahiran JOIN tb_warga ON surat_kelahiran.id_user = tb_warga.id_user JOIN tb_status ON surat_kelahiran.id_status = tb_status.id_status WHERE surat_kelahiran.id_status='4' UNION ALL
                        SELECT id_surat, no_surat, surat_kematian.id_user, nama_lengkap, tgl_surat, surat_kematian.id_status, status_monitoring FROM surat_kematian JOIN tb_warga ON surat_kematian.id_user = tb_warga.id_user JOIN tb_status ON surat_kematian.id_status = tb_status.id_status WHERE surat_kematian.id_status='4' UNION ALL
                        SELECT id_surat, no_surat, surat_kip.id_user, nama_lengkap, tgl_surat, surat_kip.id_status, status_monitoring FROM surat_kip JOIN tb_warga ON surat_kip.id_user = tb_warga.id_user JOIN tb_status ON surat_kip.id_status = tb_status.id_status WHERE surat_kip.id_status='4' UNION ALL
                        SELECT id_surat, no_surat, surat_kuasa.id_user, nama_lengkap, tgl_surat, surat_kuasa.id_status, status_monitoring FROM surat_kuasa JOIN tb_warga ON surat_kuasa.id_user = tb_warga.id_user JOIN tb_status ON surat_kuasa.id_status = tb_status.id_status WHERE surat_kuasa.id_status='4' UNION ALL
                        SELECT id_surat, no_surat, surat_permohonan_ktp.id_user, nama_lengkap, tgl_surat, surat_permohonan_ktp.id_status, status_monitoring FROM surat_permohonan_ktp JOIN tb_warga ON surat_permohonan_ktp.id_user = tb_warga.id_user JOIN tb_status ON surat_permohonan_ktp.id_status = tb_status.id_status WHERE surat_permohonan_ktp.id_status='4' UNION ALL
                        SELECT id_surat, no_surat, surat_perubahan_kk.id_user, nama_lengkap, tgl_surat, surat_perubahan_kk.id_status, status_monitoring FROM surat_perubahan_kk JOIN tb_warga ON surat_perubahan_kk.id_user = tb_warga.id_user JOIN tb_status ON surat_perubahan_kk.id_status = tb_status.id_status WHERE surat_perubahan_kk.id_status='4' UNION ALL
                        SELECT id_surat, no_surat, surat_skck.id_user, nama_lengkap, tgl_surat, surat_skck.id_status, status_monitoring FROM surat_skck JOIN tb_warga ON surat_skck.id_user = tb_warga.id_user JOIN tb_status ON surat_skck.id_status = tb_status.id_status WHERE surat_skck.id_status='4' UNION ALL
                        SELECT id_surat, no_surat, surat_sktm.id_user, nama_lengkap, tgl_surat, surat_sktm.id_status, status_monitoring FROM surat_sktm JOIN tb_warga ON surat_sktm.id_user = tb_warga.id_user JOIN tb_status ON surat_sktm.id_status = tb_status.id_status WHERE surat_sktm.id_status='4' UNION ALL
                        SELECT id_surat, no_surat, surat_sptjm.id_user, nama_lengkap, tgl_surat, surat_sptjm.id_status, status_monitoring FROM surat_sptjm JOIN tb_warga ON surat_sptjm.id_user = tb_warga.id_user JOIN tb_status ON surat_sptjm.id_status = tb_status.id_status WHERE surat_sptjm.id_status='4'
                    )X  ORDER BY tgl_surat");
                    while($da = mysqli_fetch_array($data)){ ?>
                    <?php if(substr($da['id_surat'],0,7) == 'biodata'){ ?>
                        <a href="<?php echo base_url('Monitoring/Surat/Ajuan_biodata/detail/'.$da['id_surat']) ?>" class="list-group-item list-group-item-action flex-column align-items-start active">
                            <div class="d-flex w-100 justify-content-between">
                            <h6>Surat Biodata</h6>
                            <small><?php echo tgl_indo($da['tgl_surat']); ?></small>
                            </div>
                            <p class="mb-1" style="font-size:14px;"><?php echo $da['no_surat']; ?></p>
                            <small><?php echo $da['nama_lengkap']; ?></small>
                        </a>
                    <?php }elseif(substr($da['id_surat'],0,8) == 'domisili'){ ?>
                        <a href="<?php echo base_url('Monitoring/Surat/Ajuan_domisili/detail/'.$da['id_surat']) ?>" class="list-group-item list-group-item-action flex-column align-items-start active">
                            <div class="d-flex w-100 justify-content-between">
                            <h6>Surat Domisili</h6>
                            <small><?php echo tgl_indo($da['tgl_surat']); ?></small>
                            </div>
                            <p class="mb-1" style="font-size:14px;"><?php echo $da['no_surat']; ?></p>
                            <small><?php echo $da['nama_lengkap']; ?></small>
                        </a>
                    <?php }elseif(substr($da['id_surat'],0,9) == 'keramaian') { ?>
                        <a href="<?php echo base_url('Monitoring/Surat/Ajuan_keramaian/detail/'.$da['id_surat']) ?>" class="list-group-item list-group-item-action flex-column align-items-start active">
                            <div class="d-flex w-100 justify-content-between">
                            <h6>Surat Keramaian</h6>
                            <small><?php echo tgl_indo($da['tgl_surat']); ?></small>
                            </div>
                            <p class="mb-1" style="font-size:14px;"><?php echo $da['no_surat']; ?></p>
                            <small><?php echo $da['nama_lengkap']; ?></small>
                        </a>
                    <?php }elseif(substr($da['id_surat'],0,9) == 'kelahiran'){ ?>
                        <a href="<?php echo base_url('Monitoring/Surat/Ajuan_kelahiran/detail/'.$da['id_surat']) ?>" class="list-group-item list-group-item-action flex-column align-items-start active">
                            <div class="d-flex w-100 justify-content-between">
                            <h6>Surat Kelahiran</h6>
                            <small><?php echo tgl_indo($da['tgl_surat']); ?></small>
                            </div>
                            <p class="mb-1" style="font-size:14px;"><?php echo $da['no_surat']; ?></p>
                            <small><?php echo $da['nama_lengkap']; ?></small>
                        </a>
                    <?php }elseif(substr($da['id_surat'],0,8) == 'kematian'){ ?>
                        <a href="<?php echo base_url('Monitoring/Surat/Ajuan_kematian/detail/'.$da['id_surat']) ?>" class="list-group-item list-group-item-action flex-column align-items-start active">
                            <div class="d-flex w-100 justify-content-between">
                            <h6>Surat Kematian</h6>
                            <small><?php echo tgl_indo($da['tgl_surat']); ?></small>
                            </div>
                            <p class="mb-1" style="font-size:14px;"><?php echo $da['no_surat']; ?></p>
                            <small><?php echo $da['nama_lengkap']; ?></small>
                        </a>
                    <?php }elseif(substr($da['id_surat'],0,3) == 'kip'){ ?>
                        <a href="<?php echo base_url('Monitoring/Surat/Ajuan_kip/detail/'.$da['id_surat']) ?>" class="list-group-item list-group-item-action flex-column align-items-start active">
                            <div class="d-flex w-100 justify-content-between">
                            <h6>Surat KIP</h6>
                            <small><?php echo tgl_indo($da['tgl_surat']); ?></small>
                            </div>
                            <p class="mb-1" style="font-size:14px;"><?php echo $da['no_surat']; ?></p>
                            <small><?php echo $da['nama_lengkap']; ?></small>
                        </a>
                    <?php }elseif(substr($da['id_surat'],0,5) == 'Kuasa'){ ?>
                        <a href="<?php echo base_url('Monitoring/Surat/Ajuan_kuasa/detail/'.$da['id_surat']) ?>" class="list-group-item list-group-item-action flex-column align-items-start active">
                            <div class="d-flex w-100 justify-content-between">
                            <h6>Surat Kuasa</h6>
                            <small><?php echo tgl_indo($da['tgl_surat']); ?></small>
                            </div>
                            <p class="mb-1" style="font-size:14px;"><?php echo $da['no_surat']; ?></p>
                            <small><?php echo $da['nama_lengkap']; ?></small>
                        </a>
                    <?php }elseif(substr($da['id_surat'],0,3) == 'ktp'){ ?>
                        <a href="<?php echo base_url('Monitoring/Surat/Ajuan_ktp/detail/'.$da['id_surat']) ?>" class="list-group-item list-group-item-action flex-column align-items-start active">
                            <div class="d-flex w-100 justify-content-between">
                            <h6>Permohonan KTP</h6>
                            <small><?php echo tgl_indo($da['tgl_surat']); ?></small>
                            </div>
                            <p class="mb-1" style="font-size:14px;"><?php echo $da['no_surat']; ?></p>
                            <small><?php echo $da['nama_lengkap']; ?></small>
                        </a>
                    <?php }elseif(substr($da['id_surat'],0,2) == 'kk'){ ?>
                        <a href="<?php echo base_url('Monitoring/Surat/Ajuan_kk/detail/'.$da['id_surat']) ?>" class="list-group-item list-group-item-action flex-column align-items-start active">
                            <div class="d-flex w-100 justify-content-between">
                            <h6>Perubahan KK</h6>
                            <small><?php echo tgl_indo($da['tgl_surat']); ?></small>
                            </div>
                            <p class="mb-1" style="font-size:14px;"><?php echo $da['no_surat']; ?></p>
                            <small><?php echo $da['nama_lengkap']; ?></small>
                        </a>
                    <?php }elseif(substr($da['id_surat'],0,4) == 'skck'){ ?>
                        <a href="<?php echo base_url('Monitoring/Surat/Ajuan_skck/detail/'.$da['id_surat']) ?>" class="list-group-item list-group-item-action flex-column align-items-start active">
                            <div class="d-flex w-100 justify-content-between">
                            <h6>Pengantar SKCK</h6>
                            <small><?php echo tgl_indo($da['tgl_surat']); ?></small>
                            </div>
                            <p class="mb-1" style="font-size:14px;"><?php echo $da['no_surat']; ?></p>
                            <small><?php echo $da['nama_lengkap']; ?></small>
                        </a>
                    <?php }elseif(substr($da['id_surat'],0,4) == 'sktm'){ ?>
                        <a href="<?php echo base_url('Monitoring/Surat/Ajuan_sktm/detail/'.$da['id_surat']) ?>" class="list-group-item list-group-item-action flex-column align-items-start active">
                            <div class="d-flex w-100 justify-content-between">
                            <h6>SKTM</h6>
                            <small><?php echo tgl_indo($da['tgl_surat']); ?></small>
                            </div>
                            <p class="mb-1" style="font-size:14px;"><?php echo $da['no_surat']; ?></p>
                            <small><?php echo $da['nama_lengkap']; ?></small>
                        </a>
                    <?php }elseif(substr($da['id_surat'],0,5) == 'sptjm'){ ?>
                        <a href="<?php echo base_url('Monitoring/Surat/Ajuan_sptjm/detail/'.$da['id_surat']) ?>" class="list-group-item list-group-item-action flex-column align-items-start active">
                            <div class="d-flex w-100 justify-content-between">
                            <h6>SPTJM</h6>
                            <small><?php echo tgl_indo($da['tgl_surat']); ?></small>
                            </div>
                            <p class="mb-1" style="font-size:14px;"><?php echo $da['no_surat']; ?></p>
                            <small><?php echo $da['nama_lengkap']; ?></small>
                        </a>
                    <?php }else{echo "data gagal";} ?>
                    <?php } ?>
                </div>
            </li>
            <li class="header text-center">Notifikasi</li>
        </ul>
    </li>
    
    <li class="dropdown">                        
        <a class="btn btn-danger" href="<?php echo base_url('index.php/validasi/login/logout'); ?>"><i class="fas fa-power-off fa-fw"></i> Logout</a>
    </li>
</ul>