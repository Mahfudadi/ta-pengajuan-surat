<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image" style="margin-left: 15px">
                        <img src="<?php echo base_url('assets/images/icon/user.png') ?>" class="img-circle elevation-2" alt="User Image" style="width: 50px">
                    </div>
                    <div class="">
                        <div class="info" style="margin-left: 12px">
                            <a href="<?php echo base_url('Kades/Profil/Profil/index/'.$this->session->userdata('ses_id'))?>" class="d-block text-light" style="margin-bottom:0px"><span class="badge badge-info"><?php echo $user['id_perangkat']; ?></a>
                            <p class="d-block text-light"><span class="badge" style="background-color:indigo;"><?php echo $user['nip']; ?></p>
                        </div>
                    </div>
                </div>
                <a class="nav-link" href="<?php echo base_url ('Kades/Beranda')?>">
                    <div class="sb-nav-link-icon"><i class="fas fa-home"></i></div>
                    Beranda
                </a>
                <a class="nav-link" href="<?php echo base_url ('Kades/Surat/SuratAjuan_Kades')?>">
                    <div class="sb-nav-link-icon"><i class="fas fa-envelope"></i></div>
                    Surat Ajuan
                </a>
                <a class="nav-link" href="<?php echo base_url('Kades/Laporan/LaporanKades')?>">
                    <div class="sb-nav-link-icon"><i class="fas fa-list-alt"></i></div>
                    Laporan
                </a>
                
            </div>
        </div>
        <div class="sb-sidenav-footer">
            <div class="small">Pemerintah</div>
            Desa Gurah
        </div>
    </nav>
</div>