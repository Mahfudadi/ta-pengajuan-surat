<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("Kades/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("Kades/_Partials/Header.php") ?>
            <?php 
            $koneksi = mysqli_connect("localhost","root","","surat_desagurah");

            // Check connection
            if (mysqli_connect_errno()){
                echo "Koneksi database gagal : " . mysqli_connect_error();
            }?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("Kades/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-2">Beranda</h1>
                        <div class="alert alert-secondary" role="alert">
                            <div class="row">
                                <div class="col-md-8">
                                    <h5>Selamat Datang, <strong>Kepala Desa Gurah</strong></h5>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-secondary btn-sm float-right ml-1" data-toggle="modal" data-target="#ubah-pass">Ubah Password</button>
                                    <button type="button" class="btn btn-secondary btn-sm float-right ml-1" data-toggle="modal" data-target="#profile">Ubah Profil</button>
                                    <a type="button" href="<?php echo base_url('Kades/Profil/Profil/ubah_ttd')?>" class="btn btn-secondary btn-sm float-right">Ubah TTD</a>
                                </div>
                            </div>
                        </div>
                        <div class="flashdata" data-flashdata="<?= $this->session->flashdata('pesan') ?>"> </div>
                        <div class="flashdata2" data-flashdata2="<?= $this->session->flashdata('pesangagal') ?>"></div>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active"><a href="beranda.php">Beranda</li></a>
                        </ol>
                        <div class="row mt-2">
                        <?php 
                        $data = mysqli_query($koneksi,"SELECT SUM(semua) as semua, SUM(process) as diproses, SUM(finish) as selesai, SUM(reject) as ditolak FROM (
                            SELECT COUNT(*) as semua, COUNT(IF(id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_biodata  UNION ALL
                            SELECT COUNT(*) as semua, COUNT(IF(id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_domisili  UNION ALL
                            SELECT COUNT(*) as semua, COUNT(IF(id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_ijin_keramaian  UNION ALL
                            SELECT COUNT(*) as semua, COUNT(IF(id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kelahiran  UNION ALL
                            SELECT COUNT(*) as semua, COUNT(IF(id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kematian  UNION ALL
                            SELECT COUNT(*) as semua, COUNT(IF(id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kip  UNION ALL
                            SELECT COUNT(*) as semua, COUNT(IF(id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kuasa  UNION ALL
                            SELECT COUNT(*) as semua, COUNT(IF(id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_permohonan_ktp  UNION ALL
                            SELECT COUNT(*) as semua, COUNT(IF(id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_perubahan_kk  UNION ALL
                            SELECT COUNT(*) as semua, COUNT(IF(id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_skck  UNION ALL
                            SELECT COUNT(*) as semua, COUNT(IF(id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_sktm  UNION ALL
                            SELECT COUNT(*) as semua, COUNT(IF(id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_sptjm 
                        )X");
                        while($total = mysqli_fetch_array($data)){ ?>
                            <div class="col-xl-4">
                                <div class="card bg-info text-white">
                                    <div class="card-body"><h1><?php echo $total['diproses']; ?></h1><span><h4>Total Surat Selesai TTD</h4></span></div>
                                </div>
                            </div>
                            <div class="col-xl-4">
                                <div class="card bg-success text-white">
                                    <div class="card-body"><h1><?php echo $total['selesai']; ?></h1><span><h4>Total Surat Selesai TTD</h4></span></div>
                                </div>
                            </div>
                            <div class="col-xl-4">
                                <div class="card bg-danger text-white">
                                    <div class="card-body"><h1><?php echo $total['ditolak']; ?></h1><span><h4>Total Surat Ditolak</h4></span></div>
                                </div>
                            </div>
                        </div>
                    <?php } ?> 
                        <br>
                            <div class="modal fade" id="profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Ubah Profil</h5>
                            </div>
                            <div class="modal-body">
                                <form action="<?php echo base_url('Kades/Profil/Profil/update_profil'); ?>" method="post"> 
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Kepala Desa</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" name="nama" value="<?php echo $user['id_perangkat']; ?>" hidden>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">NIP</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" name="id_user" value="<?php echo $this->session->userdata('ses_id') ?>" hidden>
                                        <input type="text" class="form-control" name="nip" value="<?php echo $user['nip']; ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Nama Lengkap</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" name="nama_rt" value="<?php echo $user['nama']; ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Alamat</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" name="alamat" value="<?php echo $user['alamat']; ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <button type="submit" class="btn btn-success btn-block"><i class="fas fa-check"> </i> Simpan Perubahan </button>
                                    </div>
                                    <div class="col-sm-6">
                                        <button type="button" class="btn btn-danger btn-block"data-dismiss="modal"><i class="fas fa-times"> </i> Batal</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="ubah-pass">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Ubah Password</h4>
                            </div>
                            <div class="modal-body">
                                <div class="box-body">
                                    <form action="<?php echo base_url('Kades/Beranda/update_password'); ?>" method="post">
                                        <div class="form-group">
                                            <label for="current_password">Password Lama</label>
                                            <input type="password" class="form-control" id="current_password" name="current_password" placeholder="Password lama" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="new_password1">Password Baru</label>
                                            <div class="input-group">
                                                <input type="password" class="form-control" id="new_password1" name="new_password1" placeholder="Password baru" required>
                                                <div class="input-group-append">
                            
                                                    <!-- kita pasang onclick untuk merubah icon buka/tutup mata setiap diklik  -->
                                                    <span id="mybutton" onclick="change()" class="input-group-text">
                            
                                                        <!-- icon mata bawaan bootstrap  -->
                                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor"
                                                            xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z" />
                                                            <path fill-rule="evenodd"
                                                                d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z" />
                                                        </svg>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="new_password2">Ulang Password Baru</label>
                                            <div class="input-group">
                                                <input type="password" class="form-control" id="new_password2" name="new_password2" placeholder="Password baru" required>
                                                <div class="input-group-append">
                            
                                                    <!-- kita pasang onclick untuk merubah icon buka/tutup mata setiap diklik  -->
                                                    <span id="mybutton2" onclick="change2()" class="input-group-text">
                            
                                                        <!-- icon mata bawaan bootstrap  -->
                                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor"
                                                            xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z" />
                                                            <path fill-rule="evenodd"
                                                                d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z" />
                                                        </svg>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="col">
                                                    <button type="submit" class="btn btn-outline-success btn-block" >Simpan </button>
                                                </div>
                                                <div class="col">
                                                    <button type="button" class="btn btn-outline-danger btn-block btn-outline" data-dismiss="modal">Batal</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("Kades/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("Kades/_Partials/Js.php") ?>
</html>
