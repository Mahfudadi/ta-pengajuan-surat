<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("Kades/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("Kades/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("Kades/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4 text-left" >Surat Ajuan</h1>
                        <ol class="breadcrumb" style="margin-bottom:4px">
                            <li class="breadcrumb-item active">Surat</li>
                            <li class="breadcrumb-item active">Surat Ajuan</li>
                            <li class="breadcrumb-item active">Kades</li>
                        </ol>
                        <form class="form-horizontal style-form" action="<?php echo base_url().'Kades/Surat/SuratAjuan_Kades/index' ?>" method="post">
                        <div class="form-group">
                            <label for="Filter" class="col-form-label"><b>Filter</b></label>
                            <div class="input-group mb-3">
                                <label for="bulan" class="col-form-label mr-2"><b>Jenis Surat</b></label>
                                <select class="custom-select d-block col-md-3 mr-5" id="jenis_surat" name="jenis_surat" required>
                                    <option value="0">Semua Data</option>
                                    <option value="1">Surat Kuasa</option>
                                    <option value="2">Surat Keterangan Domisili</option>
                                    <option value="3">Surat Pengajuan Kartu Indonesia Pintar</option>
                                    <option value="4">Surat Keterangan Tidak Mampu</option>
                                    <option value="5">Surat Ijin Keramaian</option>
                                    <option value="6">Surat Keterangan Biodata Penduduk</option>
                                    <option value="7">Surat Pernyataan Tanggung Jawab Mutlak (SPTJM)</option>
                                    <option value="8">Surat Permohonan Pengantar SKCK</option>
                                    <option value="9">Surat Kelahiran</option>
                                    <option value="10">Surat Permohonan Perubahan KK</option>
                                    <option value="11">Surat Keterangan Kematian</option>
                                    <option value="12">Surat Permohonan E-KTP WNI</option>
                                </select>
                                <label for="bulan" class="col-form-label mr-2"><b>Bulan</b></label>
                                <select class="custom-select d-block col-md-3" id="bulan" name="bulan" required>
                                    <option value="0">--Pilih Bulan--</option>
                                    <option value="01">Januari</option>
                                    <option value="02">Februari</option>
                                    <option value="03">Maret</option>
                                    <option value="04">April</option>
                                    <option value="05">Mei</option>
                                    <option value="06">Juni</option>
                                    <option value="07">Juli</option>
                                    <option value="08">Agustus</option>
                                    <option value="09">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Desember</option>
                                </select>
                                <button type="submit" class="btn btn-md btn-outline-secondary ml-4" >
                                    <i class="fas fa-search-plus"></i> Tampilkan
                                </button>
                            </div>
                        </div>
                        </form>
                        <?= $this->session->flashdata('msg') ?>
                        <div class="flashdata" data-flashdata="<?= $this->session->flashdata('pesan') ?>"> </div>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                Data Surat Ajuan Kades
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead class="text-center">
                                            <tr>
                                                <th>Jenis Surat</th>
                                                <th>No. Surat</th>
                                                <th>Nama Lengkap</th>
                                                <th>Tanggal Surat</th>
                                                <th>Status Surat</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php
                                                $no=1;
                                                foreach ($data_surat as $ds):?>
                                                <tr>
                                                    <?php if(substr($ds->id_surat,0,7) == 'biodata'){ ?>
                                                        <td>Surat Keterangan Biodata</td>
                                                    <?php }elseif(substr($ds->id_surat,0,8) == 'domisili'){ ?>
                                                        <td>Surat Keterangan Domisili</td>
                                                    <?php }elseif(substr($ds->id_surat,0,9) == 'keramaian') { ?>
                                                        <td>Surat Ijin Keramaian</td>
                                                    <?php }elseif(substr($ds->id_surat,0,9) == 'kelahiran'){ ?>
                                                        <td>Surat Kelahiran</td>
                                                    <?php }elseif(substr($ds->id_surat,0,8) == 'kematian'){ ?>
                                                        <td>Surat Keterangan Kematian</td>
                                                    <?php }elseif(substr($ds->id_surat,0,3) == 'kip'){ ?>
                                                        <td>Surat Pengajuan KIP</td>
                                                    <?php }elseif(substr($ds->id_surat,0,5) == 'Kuasa'){ ?>
                                                        <td>Surat Kuasa</td>
                                                    <?php }elseif(substr($ds->id_surat,0,3) == 'ktp'){ ?>
                                                        <td>Surat Permohonan EKTP</td>
                                                    <?php }elseif(substr($ds->id_surat,0,2) == 'kk'){ ?>
                                                        <td>Surat Permohonan Perubahan KK</td>
                                                    <?php }elseif(substr($ds->id_surat,0,4) == 'skck'){ ?>
                                                        <td>Surat Pengantar SKCK</td>
                                                    <?php }elseif(substr($ds->id_surat,0,4) == 'sktm'){ ?>
                                                        <td>Surat Pengajuan SKTM</td>
                                                    <?php }elseif(substr($ds->id_surat,0,5) == 'sptjm'){ ?>
                                                        <td>Surat Pengajuan SPTJM</td>
                                                    <?php }else{echo "data gagal";} ?>
                                                    <td><?php echo $ds->no_surat ?></td>
                                                    <td><?php echo $ds->nama_lengkap ?></td>
                                                    <td><?php echo format_indo($ds->tgl_surat) ?></td>
                                                    <td class="text-center"><?php echo $ds->status_monitoring?></td>
                                                    <td class="text-center">
                                                    <?php if(substr($ds->id_surat,0,7) == 'biodata'){ ?>
                                                        <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Detail" 
                                                        href="<?php echo base_url('Kades/Surat/SuratAjuan_Kades/detail_biodata/'.$ds->id_surat) ?>">
                                                            <i class="fas fa-file"></i>
                                                        </a>   
                                                    <?php }elseif(substr($ds->id_surat,0,8) == 'domisili'){ ?>
                                                        <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Detail" 
                                                        href="<?php echo base_url('Kades/Surat/SuratAjuan_Kades\/detail_domisili/'.$ds->id_surat) ?>">
                                                            <i class="fas fa-file"></i>
                                                        </a>   
                                                    <?php }elseif(substr($ds->id_surat,0,9) == 'keramaian') { ?>
                                                        <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Detail" 
                                                        href="<?php echo base_url('Kades/Surat/SuratAjuan_Kades/detail_keramaian/'.$ds->id_surat) ?>">
                                                            <i class="fas fa-file"></i>
                                                        </a>   
                                                    <?php }elseif(substr($ds->id_surat,0,9) == 'kelahiran'){ ?>
                                                        <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Detail" 
                                                        href="<?php echo base_url('Kades/Surat/SuratAjuan_Kades/detail_kelahiran/'.$ds->id_surat) ?>">
                                                            <i class="fas fa-file"></i>
                                                        </a>   
                                                    <?php }elseif(substr($ds->id_surat,0,8) == 'kematian'){ ?>
                                                        <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Detail" 
                                                        href="<?php echo base_url('Kades/Surat/SuratAjuan_Kades/detail_kematian/'.$ds->id_surat) ?>">
                                                            <i class="fas fa-file"></i>
                                                        </a>   
                                                    <?php }elseif(substr($ds->id_surat,0,3) == 'kip'){ ?>
                                                        <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Detail" 
                                                        href="<?php echo base_url('Kades/Surat/SuratAjuan_Kades/detail_kip/'.$ds->id_surat) ?>">
                                                            <i class="fas fa-file"></i>
                                                        </a>   
                                                    <?php }elseif(substr($ds->id_surat,0,5) == 'Kuasa'){ ?>
                                                        <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Detail" 
                                                        href="<?php echo base_url('Kades/Surat/SuratAjuan_Kades/detail_kuasa/'.$ds->id_surat) ?>">
                                                            <i class="fas fa-file"></i>
                                                        </a>   
                                                    <?php }elseif(substr($ds->id_surat,0,3) == 'ktp'){ ?>
                                                        <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Detail" 
                                                        href="<?php echo base_url('Kades/Surat/SuratAjuan_Kades/detail_ktp/'.$ds->id_surat) ?>">
                                                            <i class="fas fa-file"></i>
                                                        </a>    
                                                    <?php }elseif(substr($ds->id_surat,0,2) == 'kk'){ ?>
                                                        <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Detail" 
                                                        href="<?php echo base_url('Kades/Surat/SuratAjuan_Kades/detail_kk/'.$ds->id_surat) ?>">
                                                            <i class="fas fa-file"></i>
                                                        </a>   
                                                    <?php }elseif(substr($ds->id_surat,0,4) == 'skck'){ ?>
                                                        <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Detail" 
                                                        href="<?php echo base_url('Kades/Surat/SuratAjuan_Kades/detail_skck/'.$ds->id_surat) ?>">
                                                            <i class="fas fa-file"></i>
                                                        </a>   
                                                    <?php }elseif(substr($ds->id_surat,0,4) == 'sktm'){ ?>
                                                        <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Detail" 
                                                        href="<?php echo base_url('Kades/Surat/SuratAjuan_Kades/detail_sktm/'.$ds->id_surat) ?>">
                                                            <i class="fas fa-file"></i>
                                                        </a>   
                                                    <?php }elseif(substr($ds->id_surat,0,5) == 'sptjm'){ ?>
                                                        <a class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="top" title="Detail" 
                                                        href="<?php echo base_url('Kades/Surat/SuratAjuan_Kades/detail_sptjm/'.$ds->id_surat) ?>">
                                                            <i class="fas fa-file"></i>
                                                        </a>   
                                                    <?php }else{echo "data gagal";} ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("Kades/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("Kades/_Partials/Js.php") ?>
        <script>
            $(document).ready(function(){
                $("#jenis_surat").change(function(){
                    let a = $(this).val();
                    console.log(a);
                });
            });
        </script>
    </body>
</html>