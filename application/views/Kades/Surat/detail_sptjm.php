<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("Kades/_Partials/Head.php") ?>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <?php $this->load->view("Kades/_Partials/Header.php") ?>
        </nav>
        <div id="layoutSidenav">
            <?php $this->load->view("Kades/_Partials/Sidebar.php") ?>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4 text-center ">Detail Data Ajuan Surat Pernyataan Tanggung Jawab Mutlak Kebenaran Sebagai Pasangan Suami Istri</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Surat Ajuan</li>
                            <li class="breadcrumb-item active" >SPTJM</li>
                            <li class="breadcrumb-item active">Detail Data Ajuan SPTJM</li>
                        </ol>
                        <a class="btn btn-danger rounded" href="<?php echo base_url('Monitoring/Surat/Ajuan_sptjm') ?>"><i class="fas fa-reply"></i> Kembali</a>
                        <hr>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-user mr-1"></i>
                                <?php foreach ($detailsptjm as $sptjm) : ?>
                                <?php echo $sptjm->nama_lengkap ?>
                                <?php endforeach;?>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table-form" width="30%" cellpadding="5" cellspacing="0">
                                        <?php
                                        $no=1;
                                        foreach ($detailsptjm as $sptjm) : ?>
                                            <tr>
                                                <td>Nomor Surat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sptjm->no_surat ?></td>
                                            </tr>
                                            <tr>
                                                <td >Tanggal Surat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo tgl_indo($sptjm->tgl_surat) ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA PEMOHON</b></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap </td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sptjm->nama_lengkap ?></td>
                                            </tr>
                                            <tr>
                                                <td>NIK</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sptjm->nik ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tempat, Tanggal Lahir</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sptjm->tempat_lahir ?>, <?php echo tgl_indo($sptjm->tanggal_lahir) ?></td>
                                            </tr>
                                            <tr>
                                                <td>Pekerjaan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sptjm->pekerjaan ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sptjm->alamat ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA SUAMI/ISTRI I</b></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap </td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sptjm->nama_a ?></td>
                                            </tr>
                                            <tr>
                                                <td>NIK</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sptjm->nik_a ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tempat, Tanggal Lahir</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sptjm->tempat_lahir_a ?>, <?php echo tgl_indo($sptjm->tgl_lahir_a) ?></td>
                                            </tr>
                                            <tr>
                                                <td>Pekerjaan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sptjm->pekerjaan_a ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sptjm->alamat_a ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA SUAMI/ISTRI II</b></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap </td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sptjm->nama_b ?></td>
                                            </tr>
                                            <tr>
                                                <td>NIK</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sptjm->nik_b ?></td>
                                            </tr>
                                            <tr>
                                                <td>Tempat, Tanggal Lahir</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sptjm->tempat_lahir_b ?>, <?php echo tgl_indo($sptjm->tgl_lahir_b) ?></td>
                                            </tr>
                                            <tr>
                                                <td>Pekerjaan</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sptjm->pekerjaan_b ?></td>
                                            </tr>
                                            <tr>
                                                <td>Alamat</td>
                                                <td width="1%">:</td>
                                                <td><?php echo $sptjm->alamat_b ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>DATA SAKSI</b></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap Saksi I</td>
                                                <td width="1%">:</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>NIK Saksi I</td>
                                                <td width="1%">:</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Nama Lengkap Saksi II</td>
                                                <td width="1%">:</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>NIK Saksi II</td>
                                                <td width="1%">:</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td><b>BERKAS PERSYARATAN</b></td>
                                            </tr>
                                            <tr>
                                                <td>Scan KK</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_SPTJM/'.$sptjm->scan_kk ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_SPTJM/'.$sptjm->scan_kk ?>" alt="Scan KK">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan E-KTP</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_SPTJM/'.$sptjm->scan_ektp ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_SPTJM/'.$sptjm->scan_ektp ?>" alt="Scan EKTP">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Scan Pajak</td>
                                                <td width="1%">:</td>
                                                <td>
                                                    <a data-fancybox="gallery" href="<?php echo base_url().'assets/upload/Surat_SPTJM/'.$sptjm->scan_pajak ?>">
                                                        <img id="myImg" class="img-scan" src="<?php echo base_url().'assets/upload/Surat_SPTJM/'.$sptjm->scan_pajak ?>" alt="Scan Pajak">
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php endforeach;?>
                                        </table>
                                        <hr>
                                        <?php echo form_open('Kades/Surat/SuratAjuan_Kades/update_sptjm')?>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-sm-left">Status Monitoring</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <select class="custom-select d-block mr-4" id="id_status" name="id_status" onchange="if (this.selectedIndex==1){ document.getElementById('tampil_alasan').style.display = 'inline' }else { document.getElementById('tampil_tanggal').style.display = 'none' };">
                                                        <option value="5">TTD</option>
                                                        <option value="7">Ditolak</option>
                                                    </select>
                                                    <span id="tampil_alasan" style="display:none;">
                                                    <label>Alasan Ditolak</label>
                                                    <input type="text"  name="ditolak" class="form-control">
                                                    </span>
                                                </div>
                                                <input name="tgl_riwayat" id="tgl_riwayat" class="form-control" value="<?php echo date('Y-m-d H:i:s');?>" hidden>
                                            </div>
                                            <input name="id_surat" class="form-control" value="<?php echo $sptjm->id_surat ?>" hidden>
                                            <button class="btn btn-success btn-block" type="submit" data-toggle="tooltip" data-placement="top" title="TTD" >
                                                <i class="fa fa-check-square"></i> Setujui
                                            </button>
                                        <?php echo form_close()?>
                                        <br>
                                </div>
                            </div>
                        </div>
                    </div>

                </main>
                <footer class="py-4 bg-light mt-auto">
                    <?php $this->load->view("Kades/_Partials/Footer.php") ?>
                </footer>
            </div>
        </div>
        <?php $this->load->view("Kades/_Partials/Js.php") ?>
    </body>
</html>