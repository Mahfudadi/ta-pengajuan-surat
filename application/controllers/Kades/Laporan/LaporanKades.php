<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LaporanKades extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('LaporanKades_model');
	}

	public function index()
    {
        $data['user'] = $this->db->get_where('tb_perangkat_desa', ['id_perangkat' => $this->session->userdata('ses_id')])->row_array();
        $jenis = $this->input->post('jenis_surat');
        $bulan = $this->input->post('bulan');
        $tahun = $this->input->post('tahun');

        if( $jenis == 0 && $bulan == 0 && $tahun == 0)
        {
            $data['data'] = $this->LaporanKades_model->tampil_semua();
            $this->load->view('Kades/Laporan/laporan',$data);
        } elseif ( $jenis == 1 && $bulan == 0 && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Kuasa</div>');
            $data['data'] = $this->LaporanKades_model->tampil_kuasa();
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 2 && $bulan == 0 && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Domisili</div>');
            $data['data'] = $this->LaporanKades_model->tampil_domisili();
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 3 && $bulan == 0 && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Pengajuan Kartu Indonesia Pintar (KIP)</div>');
            $data['data'] = $this->LaporanKades_model->tampil_kip();
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 4 && $bulan == 0 && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Tidak Mampu (SKTM)</div>');
            $data['data'] = $this->LaporanKades_model->tampil_sktm();
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 5 && $bulan == 0 && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Ijin Keramaian</div>');
            $data['data'] = $this->LaporanKades_model->tampil_keramaian();
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 6 && $bulan == 0 && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Biodata Penduduk</div>');
            $data['data'] = $this->LaporanKades_model->tampil_biodata();
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 7 && $bulan == 0 && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Pernyataan Tanggung Jawab Mutlak (SPTJM)</div>');
            $data['data'] = $this->LaporanKades_model->tampil_sptjm();
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 8 && $bulan == 0 && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Permohonan Pengantar SKCK</div>');
            $data['data'] = $this->LaporanKades_model->tampil_skck();
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 9 && $bulan == 0 && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Kelahiran</div>');
            $data['data'] = $this->LaporanKades_model->tampil_kelahiran();
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 10 && $bulan == 0 && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Permohonan Perubahan KK</div>');
            $data['data'] = $this->LaporanKades_model->tampil_kk();
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 11 && $bulan == 0 && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Kematian</div>');
            $data['data'] = $this->LaporanKades_model->tampil_kematian();
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 12 && $bulan == 0 && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Permohonan E-KTP WNI</div>');
            $data['data'] = $this->LaporanKades_model->tampil_ktp();
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif( $jenis == 0 && $bulan == $bulan && $tahun == 0)
        {
            $data['data'] = $this->LaporanKades_model->filter_bulan_semua($bulan);
            $this->load->view('Kades/Laporan/laporan',$data);
        } elseif ( $jenis == 1 && $bulan == $bulan && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Kuasa</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_kuasa($bulan);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 2 && $bulan == $bulan && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Domisili</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_domisili($bulan);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 3 && $bulan == $bulan && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Pengajuan Kartu Indonesia Pintar (KIP)</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_kip($bulan);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 4 && $bulan == $bulan && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Tidak Mampu (SKTM)</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_sktm($bulan);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 5 && $bulan == $bulan && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Ijin Keramaian</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_keramaian($bulan);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 6 && $bulan == $bulan && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Biodata Penduduk</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_biodata($bulan);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 7 && $bulan == $bulan && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Pernyataan Tanggung Jawab Mutlak (SPTJM)</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_sptjm($bulan);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 8 && $bulan == $bulan && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Permohonan Pengantar SKCK</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_skck($bulan);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 9 && $bulan == $bulan && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Kelahiran</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_kelahiran($bulan);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 10 && $bulan == $bulan && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Permohonan Perubahan KK</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_kk($bulan);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 11 && $bulan == $bulan && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Kematian</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_kematian($bulan);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 12 && $bulan == $bulan && $tahun == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Permohonan E-KTP WNI</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_ktp($bulan);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif( $jenis == 0 && $bulan == 0 && $tahun == $tahun)
        {
            $data['data'] = $this->LaporanKades_model->filter_tahun_semua($tahun);
            $this->load->view('Kades/Laporan/laporan',$data);
        } elseif ( $jenis == 1 && $bulan == 0 && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Kuasa</div>');
            $data['data'] = $this->LaporanKades_model->filter_tahun_kuasa($tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 2 && $bulan == 0 && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Domisili</div>');
            $data['data'] = $this->LaporanKades_model->filter_tahun_domisili($tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 3 && $bulan == 0 && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Pengajuan Kartu Indonesia Pintar (KIP)</div>');
            $data['data'] = $this->LaporanKades_model->filter_tahun_kip($tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 4 && $bulan == 0 && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Tidak Mampu (SKTM)</div>');
            $data['data'] = $this->LaporanKades_model->filter_tahun_sktm($tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 5 && $bulan == 0 && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Ijin Keramaian</div>');
            $data['data'] = $this->LaporanKades_model->filter_tahun_keramaian($tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 6 && $bulan == 0 && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Biodata Penduduk</div>');
            $data['data'] = $this->LaporanKades_model->filter_tahun_biodata($tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 7 && $bulan == 0 && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Pernyataan Tanggung Jawab Mutlak (SPTJM)</div>');
            $data['data'] = $this->LaporanKades_model->filter_tahun_sptjm($tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 8 && $bulan == 0 && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Permohonan Pengantar SKCK</div>');
            $data['data'] = $this->LaporanKades_model->filter_tahun_skck($tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 9 && $bulan == 0 && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Kelahiran</div>');
            $data['data'] = $this->LaporanKades_model->filter_tahun_kelahiran($tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 10 && $bulan == 0 && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Permohonan Perubahan KK</div>');
            $data['data'] = $this->LaporanKades_model->filter_tahun_kk($tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 11 && $bulan == 0 && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Kematian</div>');
            $data['data'] = $this->LaporanKades_model->filter_tahun_kematian($tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 12 && $bulan == 0 && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Permohonan E-KTP WNI</div>');
            $data['data'] = $this->LaporanKades_model->filter_tahun_ktp($tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif( $jenis == 0 && $bulan == $bulan && $tahun == $tahun)
        {
            $data['data'] = $this->LaporanKades_model->filter_bulan_tahun_semua($bulan, $tahun);
            $this->load->view('Kades/Laporan/laporan',$data);
        } elseif ( $jenis == 1 && $bulan == $bulan && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Kuasa</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_tahun_kuasa($bulan, $tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 2 && $bulan == $bulan && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Domisili</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_tahun_domisili($bulan, $tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 3 && $bulan == $bulan && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Pengajuan Kartu Indonesia Pintar (KIP)</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_tahun_kip($bulan, $tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 4 && $bulan == $bulan && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Tidak Mampu (SKTM)</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_tahun_sktm($bulan, $tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 5 && $bulan == $bulan && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Ijin Keramaian</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_tahun_keramaian($bulan, $tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 6 && $bulan == $bulan && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Biodata Penduduk</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_tahun_biodata($bulan, $tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 7 && $bulan == $bulan && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Pernyataan Tanggung Jawab Mutlak (SPTJM)</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_tahun_sptjm($bulan, $tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 8 && $bulan == $bulan && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Permohonan Pengantar SKCK</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_tahun_skck($bulan, $tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 9 && $bulan == $bulan && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Kelahiran</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_tahun_kelahiran($bulan, $tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 10 && $bulan == $bulan && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Permohonan Perubahan KK</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_tahun_kk($bulan, $tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 11 && $bulan == $bulan && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Kematian</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_tahun_kematian($bulan, $tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } elseif ( $jenis == 12 && $bulan == $bulan && $tahun == $tahun)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Permohonan E-KTP WNI</div>');
            $data['data'] = $this->LaporanKades_model->filter_bulan_tahun_ktp($bulan, $tahun);
            $this->load->view('Kades/Laporan/laporan', $data);
        } else
        {
            $data['data'] = $this->LaporanKades_model->tampil_semua();
            $this->load->view('Kades/Laporan/laporan',$data);
        }
    }
}