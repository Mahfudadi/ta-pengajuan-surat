<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Kades_model');
	}

	public function index()
	{
		
		$data['ttd'] = $this->Kades_model->total_ttd();
		$data['belum'] = $this->Kades_model->total_belumttd();

		$data['user'] = $this->db->get_where('tb_perangkat_desa', ['id_perangkat' => $this->session->userdata('ses_id')])->row_array();
		$data['list_user'] = $this->db->get('tb_perangkat_desa')->result_array();

		$this->load->view('Kades/beranda',$data);
	}

	
    public function update_profil()
    {
        $id['id_user'] = $this->session->userdata('ses_id');
        $data = array(
            'nip'      		=> $this->input->post('nip'),
            'nama'    => $this->input->post('nama'),
            'alamat'    => $this->input->post('alamat')
        ); 

        $this->session->set_flashdata('pesan', 'Diubah');
        $this->KetuaRT_model->update_rt($data, $id);
        redirect('Kades/Beranda');
    }
	

	public function update_password()
	{
		$current_password = $this->input->post('current_password');
		$new_password1 = $this->input->post('new_password1');
		$new_password2 = $this->input->post('new_password2');

		if ($current_password == $new_password1) {
			$this->session->set_flashdata('msg', '<div class="alert alert-danger font-weight-bolder text-center" role="alert">Ubah Password Gagal !! <br> Password baru tidak boleh sama dengan password lama </div>');
			redirect('RT/BerandaRT');
		} elseif($new_password1 != $new_password2){
			$this->session->set_flashdata('msg', '<div class="alert alert-danger font-weight-bolder text-center" role="alert">Ubah Password Gagal !! <br> Password yang anda masukkan tidak sama</div>');
			redirect('Kades/Beranda');
		} else {
			$this->db->set('password', $new_password1);
			$this->db->where('id_user', $this->session->userdata('ses_id'));
			$this->db->update('tb_user');
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder text-center" role="alert">Ubah Password Sukses !</div>');
			redirect('Kades/Beranda');
		}
	}
	
}
