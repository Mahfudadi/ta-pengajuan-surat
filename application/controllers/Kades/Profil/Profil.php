<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {

	function __construct(){
		parent::__construct();
        $this->load->model('Kades_model');
	}

	public function index($id)
    {
        $data['user'] = $this->db->get_where('tb_perangkat_desa', ['id_perangkat' => $this->session->userdata('ses_id')])->row_array();
        $data['profil'] = $this->Kades_model->profil($id);
        //Menampilkan view dan juga data yang diambil dari model
        $this->load->view('Kades/Profil/profil',$data);
    }
    public function ubah_ttd()
    {
        $data['user'] = $this->db->get_where('tb_perangkat_desa', ['id_perangkat' => $this->session->userdata('ses_id')])->row_array();
        $this->load->view("Kades/Profil/ubah_ttd", $data);
    }
    Public function insert_single_signature()
	{

	$img = $this->input->post('image');
	$img = str_replace('data:image/png;base64,', '', $img);
	$img = str_replace(' ', '+', $img);
	$data = base64_decode($img);
	$file = 'assets/images/ttd_kades/' . uniqid() . '.png';
	$success = file_put_contents($file, $data);
	$image=str_replace('./','',$file);

    $this->Kades_model->insert_single_signature($image);
	 echo '<img src="'.base_url().$image.'">';

     
	}
    public function edit_profil($id)
    {
        $data['editprofil'] = $this->Admin_model->edit_kades($id);
        $this->load->view("Kades/Beranda",$data);
    }
    public function update_profil()
    {
        $id['id_perangkat'] = $this->input->post('id_user');
        $data = array(
            'nip'      => $this->input->post('nip'),
            'nama'      => $this->input->post('nama'),
            'alamat'      => $this->input->post('alamat'),
        ); 
        $this->session->set_flashdata('message', 'Simpan Perubahan');
        $this->Kades_model->update_kades($data, $id);
        redirect('Kades/Beranda');
    }
}