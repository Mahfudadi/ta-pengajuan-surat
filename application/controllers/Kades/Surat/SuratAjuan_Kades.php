<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SuratAjuan_Kades extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('SuratAjuanKades_model');
		$this->load->model('Suratajuan_model');
	}
    //surat sktm
    public function index()
    {
        $data['user'] = $this->db->get_where('tb_perangkat_desa', ['id_perangkat' => $this->session->userdata('ses_id')])->row_array();
        $jenis = $this->input->post('jenis_surat');
        $bulan = $this->input->post('bulan');

        if( $jenis == 0 && $bulan == 0)
        {
            $data['data_surat'] = $this->SuratAjuanKades_model->semua_data();
            $this->load->view('Kades/Surat/surat_ajuan',$data);
        } elseif ( $jenis == 1 && $bulan == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Kuasa</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->tampil_kuasa();
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } elseif ( $jenis == 2 && $bulan == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Domisili</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->tampil_domisili();
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } elseif ( $jenis == 3 && $bulan == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Pengajuan Kartu Indonesia Pintar (KIP)</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->tampil_kip();
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } elseif ( $jenis == 4 && $bulan == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Tidak Mampu (SKTM)</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->tampil_sktm();
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } elseif ( $jenis == 5 && $bulan == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Ijin Keramaian</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->tampil_keramaian();
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } elseif ( $jenis == 6 && $bulan == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Biodata Penduduk</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->tampil_biodata();
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } elseif ( $jenis == 7 && $bulan == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Pernyataan Tanggung Jawab Mutlak (SPTJM)</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->tampil_sptjm();
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } elseif ( $jenis == 8 && $bulan == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Permohonan Pengantar SKCK</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->tampil_skck();
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } elseif ( $jenis == 9 && $bulan == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Kelahiran</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->tampil_kelahiran();
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } elseif ( $jenis == 10 && $bulan == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Permohonan Perubahan KK</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->tampil_kk();
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } elseif ( $jenis == 11 && $bulan == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Kematian</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->tampil_kematian();
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } elseif ( $jenis == 12 && $bulan == 0)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Permohonan E-KTP WNI</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->tampil_ktp();
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } elseif( $jenis == 0 && $bulan == $bulan)
        {
            $data['data_surat'] = $this->SuratAjuanKades_model->filter_semua_data($bulan);
            $this->load->view('Kades/Surat/surat_ajuan',$data);
        } elseif ( $jenis == 1 && $bulan == $bulan)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Kuasa</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->filter_kuasa($bulan);
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } elseif ( $jenis == 2 && $bulan == $bulan)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Domisili</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->filter_domisili($bulan);
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } elseif ( $jenis == 3 && $bulan == $bulan)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Pengajuan Kartu Indonesia Pintar (KIP)</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->filter_kip($bulan);
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } elseif ( $jenis == 4 && $bulan == $bulan)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Tidak Mampu (SKTM)</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->filter_sktm($bulan);
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } elseif ( $jenis == 5 && $bulan == $bulan)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Ijin Keramaian</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->filter_keramaian($bulan);
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } elseif ( $jenis == 6 && $bulan == $bulan)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Biodata Penduduk</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->filter_biodata($bulan);
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } elseif ( $jenis == 7 && $bulan == $bulan)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Pernyataan Tanggung Jawab Mutlak (SPTJM)</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->filter_sptjm($bulan);
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } elseif ( $jenis == 8 && $bulan == $bulan)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Permohonan Pengantar SKCK</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->filter_skck($bulan);
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } elseif ( $jenis == 9 && $bulan == $bulan)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Kelahiran</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->filter_kelahiran($bulan);
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } elseif ( $jenis == 10 && $bulan == $bulan)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Permohonan Perubahan KK</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->filter_kk($bulan);
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } elseif ( $jenis == 11 && $bulan == $bulan)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Keterangan Kematian</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->filter_kematian($bulan);
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } elseif ( $jenis == 12 && $bulan == $bulan)
        {
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder" role="alert">Data Surat Permohonan E-KTP WNI</div>');
            $data['data_surat'] = $this->SuratAjuanKades_model->filter_ktp($bulan);
            $this->load->view('Kades/Surat/surat_ajuan', $data);
        } else
        {
            $data['data_surat'] = $this->SuratAjuanKades_model->semua_data($bulan);
            $this->load->view('Kades/Surat/surat_ajuan',$data);
        }
    }
    public function update_biodata()
    {
        $id['id_surat'] = $this->input->post('id_surat');
        $data = array(
            'id_status'         => $this->input->post('id_status'),
            'ket_tolak'         => $this->input->post('ditolak')

        ); 
        $data2 = array(
            'id_surat'                  => $this->input->post('id_surat'),
            'tgl_riwayat'               => $this->input->post('tgl_riwayat'),
            'riwayat_status'            => $this->input->post('id_status')
        ); 
        $this->Suratajuan_model->insert_riwayat($data2,'tb_riwayat');
        $this->Suratajuan_model->update_biodata($data, $id);
        $this->session->set_flashdata('pesan', 'Diterima');
        redirect('Kades/Surat/SuratAjuan_Kades/index');
    }
    public function update_domisili()
    {
        $id['id_surat'] = $this->input->post('id_surat');
        $data = array(
            'id_status'         => $this->input->post('id_status'),
            'ket_tolak'         => $this->input->post('ditolak')

        ); 
        $data2 = array(
            'id_surat'                  => $this->input->post('id_surat'),
            'tgl_riwayat'               => $this->input->post('tgl_riwayat'),
            'riwayat_status'            => $this->input->post('id_status')
        ); 
        $this->Suratajuan_model->insert_riwayat($data2,'tb_riwayat');
        $this->Suratajuan_model->update_domisili($data, $id);
        $this->session->set_flashdata('pesan', 'Diterima');
        redirect('Kades/Surat/SuratAjuan_Kades/index');
    }
    public function update_kelahiran()
    {
        $id['id_surat'] = $this->input->post('id_surat');
        $data = array(
            'id_status'         => $this->input->post('id_status'),
            'ket_tolak'         => $this->input->post('ditolak')
        ); 
        $data2 = array(
            'id_surat'                  => $this->input->post('id_surat'),
            'tgl_riwayat'               => $this->input->post('tgl_riwayat'),
            'riwayat_status'            => $this->input->post('id_status')
        ); 
        $this->Suratajuan_model->insert_riwayat($data2,'tb_riwayat');
        $this->Suratajuan_model->update_kelahiran($data, $id);
        $this->session->set_flashdata('pesan', 'Diterima');
        redirect('Kades/Surat/SuratAjuan_Kades/index');
    }
    public function update_kematian()
    {
        $id['id_surat'] = $this->input->post('id_surat');
        $data = array(
            'id_status'         => $this->input->post('id_status'),
            'ket_tolak'         => $this->input->post('ditolak')
        ); 
        $data2 = array(
            'id_surat'                  => $this->input->post('id_surat'),
            'tgl_riwayat'               => $this->input->post('tgl_riwayat'),
            'riwayat_status'            => $this->input->post('id_status')
        ); 
        $this->Suratajuan_model->insert_riwayat($data2,'tb_riwayat');
        $this->Suratajuan_model->update_kematian($data, $id);
        $this->session->set_flashdata('pesan', 'Diterima');
        redirect('Kades/Surat/SuratAjuan_Kades/index');
    }
    public function update_keramaian()
    {
        $id['id_surat'] = $this->input->post('id_surat');
        $data = array(
            'id_status'         => $this->input->post('id_status'),
            'ket_tolak'         => $this->input->post('ditolak')
        ); 
        $data2 = array(
            'id_surat'                  => $this->input->post('id_surat'),
            'tgl_riwayat'               => $this->input->post('tgl_riwayat'),
            'riwayat_status'            => $this->input->post('id_status')
        ); 
        $this->Suratajuan_model->insert_riwayat($data2,'tb_riwayat');
        $this->Suratajuan_model->update_keramaian($data, $id);
        $this->session->set_flashdata('pesan', 'Diterima');
        redirect('Kades/Surat/SuratAjuan_Kades/index');
    }
    public function update_kip()
    {
        $id['id_surat'] = $this->input->post('id_surat');
        $data = array(
            'id_status'         => $this->input->post('id_status'),
            'ket_tolak'         => $this->input->post('ditolak')
        ); 
        $data2 = array(
            'id_surat'                  => $this->input->post('id_surat'),
            'tgl_riwayat'               => $this->input->post('tgl_riwayat'),
            'riwayat_status'            => $this->input->post('id_status')
        ); 
        $this->Suratajuan_model->insert_riwayat($data2,'tb_riwayat');
        $this->Suratajuan_model->update_kip($data, $id);
        $this->session->set_flashdata('pesan', 'Diterima');
        redirect('Kades/Surat/SuratAjuan_Kades/index');
    }
    public function update_kk()
    {
        $id['id_surat'] = $this->input->post('id_surat');
        $data = array(
            'id_status'         => $this->input->post('id_status'),
            'ket_tolak'         => $this->input->post('ditolak')
        ); 
        $data2 = array(
            'id_surat'                  => $this->input->post('id_surat'),
            'tgl_riwayat'               => $this->input->post('tgl_riwayat'),
            'riwayat_status'            => $this->input->post('id_status')
        ); 
        $this->Suratajuan_model->insert_riwayat($data2,'tb_riwayat');
        $this->Suratajuan_model->update_kk($data, $id);
        $this->session->set_flashdata('pesan', 'Diterima');
        redirect('Kades/Surat/SuratAjuan_Kades/index');
    }
    public function update_ktp()
    {
        $id['id_surat'] = $this->input->post('id_surat');
        $data = array(
            'id_status'         => $this->input->post('id_status'),
            'ket_tolak'         => $this->input->post('ditolak')
        ); 
        $data2 = array(
            'id_surat'                  => $this->input->post('id_surat'),
            'tgl_riwayat'               => $this->input->post('tgl_riwayat'),
            'riwayat_status'            => $this->input->post('id_status')
        ); 
        $this->Suratajuan_model->insert_riwayat($data2,'tb_riwayat');
        $this->Suratajuan_model->update_ktp($data, $id);
        $this->session->set_flashdata('pesan', 'Diterima');
        redirect('Kades/Surat/SuratAjuan_Kades/index');
    }
    public function update_kuasa()
    {
        $id['id_surat'] = $this->input->post('id_surat');
        $data = array(
            'id_status'         => $this->input->post('id_status'),
            'ket_tolak'         => $this->input->post('ditolak')
        ); 
        $data2 = array(
            'id_surat'                  => $this->input->post('id_surat'),
            'tgl_riwayat'               => $this->input->post('tgl_riwayat'),
            'riwayat_status'            => $this->input->post('id_status')
        ); 
        $this->Suratajuan_model->insert_riwayat($data2,'tb_riwayat');
        $this->Suratajuan_model->update_kuasa($data, $id);
        $this->session->set_flashdata('pesan', 'Diterima');
        redirect('Kades/Surat/SuratAjuan_Kades/index');
    }
    public function update_skck()
    {
        $id['id_surat'] = $this->input->post('id_surat');
        $data = array(
            'id_status'         => $this->input->post('id_status'),
            'ket_tolak'         => $this->input->post('ditolak')
        ); 
        $data2 = array(
            'id_surat'                  => $this->input->post('id_surat'),
            'tgl_riwayat'               => $this->input->post('tgl_riwayat'),
            'riwayat_status'            => $this->input->post('id_status')
        ); 
        $this->Suratajuan_model->insert_riwayat($data2,'tb_riwayat');
        $this->Suratajuan_model->update_skck($data, $id);
        $this->session->set_flashdata('pesan', 'Diterima');
        redirect('Kades/Surat/SuratAjuan_Kades/index');
    }
    public function update_sktm()
    {
        $id['id_surat'] = $this->input->post('id_surat');
        $data = array(
            'id_status'         => $this->input->post('id_status'),
            'ket_tolak'         => $this->input->post('ditolak')
        ); 
        $data2 = array(
            'id_surat'                  => $this->input->post('id_surat'),
            'tgl_riwayat'               => $this->input->post('tgl_riwayat'),
            'riwayat_status'            => $this->input->post('id_status')
        ); 
        $this->Suratajuan_model->insert_riwayat($data2,'tb_riwayat');
        $this->Suratajuan_model->update_sktm($data, $id);
        $this->session->set_flashdata('pesan', 'Diterima');
        redirect('Kades/Surat/SuratAjuan_Kades/index');
    }
    public function update_sptjm()
    {
        $id['id_surat'] = $this->input->post('id_surat');
        $data = array(
            'id_status'         => $this->input->post('id_status'),
            'ket_tolak'         => $this->input->post('ditolak')
        ); 
        $data2 = array(
            'id_surat'                  => $this->input->post('id_surat'),
            'tgl_riwayat'               => $this->input->post('tgl_riwayat'),
            'riwayat_status'            => $this->input->post('id_status')
        ); 
        $this->Suratajuan_model->insert_riwayat($data2,'tb_riwayat');
        $this->Suratajuan_model->update_sptjm($data, $id);
        $this->session->set_flashdata('pesan', 'Diterima');
        redirect('Kades/Surat/SuratAjuan_Kades/index');
    }

    //detail
    public function detail_biodata($id)
    {
        $data['user'] = $this->db->get_where('tb_perangkat_desa', ['id_perangkat' => $this->session->userdata('ses_id')])->row_array();
        $data['detailbiodata'] = $this->Suratajuan_model->detail_biodata($id);
        $this->load->view('Kades/Surat/detail_biodata',$data);
    }
    public function detail_domisili($id)
    {
        $data['user'] = $this->db->get_where('tb_perangkat_desa', ['id_perangkat' => $this->session->userdata('ses_id')])->row_array();
        $data['detaildomisili'] = $this->Suratajuan_model->detail_domisili($id);
        $this->load->view('Kades/Surat/detail_domisili',$data);
    }
    public function detail_kelahiran($id)
    {
        $data['user'] = $this->db->get_where('tb_perangkat_desa', ['id_perangkat' => $this->session->userdata('ses_id')])->row_array();
        $data['detailkelahiran'] = $this->Suratajuan_model->detail_kelahiran($id);
        $this->load->view('Kades/Surat/detail_kelahiran',$data);
    }
    public function detail_kematian($id)
    {
        $data['user'] = $this->db->get_where('tb_perangkat_desa', ['id_perangkat' => $this->session->userdata('ses_id')])->row_array();
        $data['detailkematian'] = $this->Suratajuan_model->detail_kematian($id);
        $this->load->view('Kades/Surat/detail_kematian',$data);
    }
    public function detail_keramaian($id)
    {
        $data['user'] = $this->db->get_where('tb_perangkat_desa', ['id_perangkat' => $this->session->userdata('ses_id')])->row_array();
        $data['detailkeramaian'] = $this->Suratajuan_model->detail_keramaian($id);
        $this->load->view('Kades/Surat/detail_keramaian',$data);
    }
    public function detail_kip($id)
    {
        $data['user'] = $this->db->get_where('tb_perangkat_desa', ['id_perangkat' => $this->session->userdata('ses_id')])->row_array();
        $data['detailkip'] = $this->Suratajuan_model->detail_kip($id);
        $this->load->view('Kades/Surat/detail_kip',$data);
    }
    public function detail_kk($id)
    {
        $data['user'] = $this->db->get_where('tb_perangkat_desa', ['id_perangkat' => $this->session->userdata('ses_id')])->row_array();
        $data['detailkk'] = $this->Suratajuan_model->detail_kk($id);
        $this->load->view('Kades/Surat/detail_kk',$data);
    }
    public function detail_ktp($id)
    {
        $data['user'] = $this->db->get_where('tb_perangkat_desa', ['id_perangkat' => $this->session->userdata('ses_id')])->row_array();
        $data['detailktp'] = $this->Suratajuan_model->detail_ktp($id);
        $this->load->view('Kades/Surat/detail_ktp',$data);
    }
    public function detail_kuasa($id)
    {
        $data['user'] = $this->db->get_where('tb_perangkat_desa', ['id_perangkat' => $this->session->userdata('ses_id')])->row_array();
        $data['detailkuasa'] = $this->Suratajuan_model->detail_kuasa($id);
        $this->load->view('Kades/Surat/detail_kuasa',$data);
    }
    public function detail_skck($id)
    {
        $data['user'] = $this->db->get_where('tb_perangkat_desa', ['id_perangkat' => $this->session->userdata('ses_id')])->row_array();
        $data['detailskck'] = $this->Suratajuan_model->detail_skck($id);
        $this->load->view('Kades/Surat/detail_skck',$data);
    }
    public function detail_sktm($id)
    {
        $data['user'] = $this->db->get_where('tb_perangkat_desa', ['id_perangkat' => $this->session->userdata('ses_id')])->row_array();
        $data['detailsktm'] = $this->Suratajuan_model->detail_sktm($id);
        $this->load->view('Kades/Surat/detail_sktm',$data);
    }
    public function detail_sptjm($id)
    {
        $data['user'] = $this->db->get_where('tb_perangkat_desa', ['id_perangkat' => $this->session->userdata('ses_id')])->row_array();
        $data['detailsptjm'] = $this->Suratajuan_model->detail_sptjm($id);
        $this->load->view('Kades/Surat/detail_sptjm',$data);
    }
}