<?php
class Login extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('Login_model');
	}

	function index(){
		$this->load->view('Beranda/index');
	}

	function auth(){
        $username=htmlspecialchars($this->input->post('username',TRUE),ENT_QUOTES);
        $password=htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);

        $cek=$this->Login_model->auth_login($username,$password);

        if($cek->num_rows() > 0){ //jika login sebagai dosen
				$data=$cek->row_array();
        		$this->session->set_userdata('masuk',TRUE);
		         if($data['level']=='warga'){ //Akses admin
		            $this->session->set_userdata('akses','warga');
		            $this->session->set_userdata('ses_id',$data['id_user']);
		            $this->session->set_userdata('username',$data['username']);
		            $this->session->set_userdata('password',$data['password']);
		            $this->session->set_userdata('ses_level',$data['level']);
		            redirect('validasi/page');

		         }elseif($data['level']=='rt'){ //Akses rt
		            $this->session->set_userdata('akses','rt');
		            $this->session->set_userdata('ses_id',$data['id_user']);
		            $this->session->set_userdata('username',$data['username']);
		            $this->session->set_userdata('password',$data['password']);
		            $this->session->set_userdata('ses_level',$data['level']);
		            redirect('RT/BerandaRT');

		         }elseif($data['level']=='admin'){ //Akses rt
		            $this->session->set_userdata('akses','admin');
		            $this->session->set_userdata('ses_id',$data['id_user']);
		            $this->session->set_userdata('username',$data['username']);
		            $this->session->set_userdata('password',$data['password']);
		            $this->session->set_userdata('ses_level',$data['level']);
		            redirect('Monitoring/beranda');
					
		         }elseif($data['level']=='kades'){ //Akses rt
		            $this->session->set_userdata('akses','kades');
		            $this->session->set_userdata('ses_id',$data['id_user']);
		            $this->session->set_userdata('username',$data['username']);
		            $this->session->set_userdata('password',$data['password']);
		            $this->session->set_userdata('ses_level',$data['level']);
		            redirect('Kades/beranda');
		         }else{
		         	redirect('Beranda/index');
		         }


        }else{// jika username dan password tidak ditemukan atau salah
            $url=base_url();
            echo $this->session->set_flashdata('msg','<div class="alert alert-danger text-center mb-0" role="alert">Username Atau Password Anda Salah</div>');
            redirect($url);
        }
    }

    function logout(){
        $this->session->sess_destroy();
        $url=base_url('');
        redirect($url);
	}
	
	function belum_login(){
		$url=base_url();
		echo $this->session->set_flashdata('psn','Anda belum login');
		redirect($url);
	}
	public function search(){
		$keyword = $this->input->post('no_surat');
		$data['hasil']=$this->Login_model->search($keyword);
		$this->load->view('Beranda/index',$data);
	}

}