<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('Login_model');
	}

	public function index()
	{
		
		$this->load->view('login');
	}
	public function cari(){
		
		$data['hasil']=$this->Login_model->search($keyword);
		$this->load->view('search');
	}

	public function search(){
		$keyword = $this->input->post('no_surat');
		$data['hasil']=$this->Login_model->search($keyword);
		$data['hasil2']=$this->Login_model->riwayat($keyword);
		$this->load->view('search',$data);
	}
}
