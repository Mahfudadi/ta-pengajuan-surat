<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beranda extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Admin_model');
	}

	public function index()
	{
		$data['semua'] = $this->Admin_model->total_semua();
		$data['diproses'] = $this->Admin_model->total_diproses();
		$data['selesai'] = $this->Admin_model->total_selesai();
		$data['ditolak'] = $this->Admin_model->total_ditolak();
		$data['menunggu'] = $this->Admin_model->total_menunggu();
		$data['bulanan'] = $this->Admin_model->total_bulanan();
		$data['tahunan'] = $this->Admin_model->total_tahunan();
		$data['monthly'] = $this->Admin_model->totalByLetterName(null, date('m'));
		$data['chartPie'] = $this->Admin_model->totalByLetterName();
		$data['chartPieYearly'] = $this->Admin_model->totalLetterByYear();
		
		$Y = date('Y');
		$data['average'] = $this->Admin_model->presentase($Y);

		$data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();

		$this->load->view('Monitoring/beranda', $data);
	}
	public function update_profil()
	{
		$id['id_user'] = $this->session->userdata('ses_id');
		$data = array(
			'nip'      		=> $this->input->post('nip'),
			'nama_admin'    => $this->input->post('nama_admin')
		);

		$this->session->set_flashdata('pesan', 'Diubah');
		$this->Admin_model->update_profil($data, $id);
		redirect('Monitoring/Beranda');
	}

	public function update_password()
	{
		$current_password = $this->input->post('current_password');
		$new_password1 = $this->input->post('new_password1');
		$new_password2 = $this->input->post('new_password2');

		if ($current_password == $new_password1) {
			$this->session->set_flashdata('pesangagal', 'Password baru tidak boleh sama dengan password lama !');
			redirect('Monitoring/Beranda');
		} elseif ($new_password1 != $new_password2) {
			$this->session->set_flashdata('pesangagal', 'Password yang anda masukkan tidak sama !');
			redirect('Monitoring/Beranda');
		} else {
			$this->db->set('password', $new_password1);
			$this->db->where('id_user', $this->session->userdata('ses_id'));
			$this->db->update('tb_user');
			$this->session->set_flashdata('pesan', 'Diubah');
			redirect('Monitoring/Beranda');
		}
	}

	public function fetchDataByFilter($letter = null, $month = null)
	{
		if ($letter === null && $month === null) {
			$data = $this->Admin_model->totalByLetterName();
		} elseif ($letter != null && $month == null) {
			$data = $this->Admin_model->totalByLetterName($letter);
		} else {
			$data = $this->Admin_model->totalByLetterName($letter, $month);
		}
		echo json_encode($data);
	}

	public function fetchDataByMonth($month)
	{
		$data = $this->Admin_model->totalByLetterName(null, $month);
		echo json_encode($data);
	}

	public function fetchDataByYear($year, $letter = null)
	{
		if ($letter === null) {
			$data = $this->Admin_model->totalLetterByYear($year);
		} else {
			$data = $this->Admin_model->totalLetterByYear($year, $letter);
		}
		echo json_encode($data);
	}
}
