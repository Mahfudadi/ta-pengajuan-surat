<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_warga extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Warga_model');
	}
	public function index()
	{
		$data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();
        //Mengambil data dari model dan di jadikan variabel
        $data['warga'] = $this->Warga_model->tampil_data();

        //Menampilkan view dan juga data yang diambil dari model
		$this->load->view('Monitoring/Master/data_warga',$data);
    }
    public function tambahwarga()
	{
		$data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();
        $this->load->view("Monitoring/master/tambah_warga",$data);
	}

    public function tambah_datawarga()
	{
		$id_user 			= $this->input->post('id_user');
		$no_kk 				= $this->input->post('no_kk');
		$nik 				= $this->input->post('nik');
		$nama_lengkap 		= $this->input->post('nama');
		$tempat_lahir 		= $this->input->post('tempat');
		$tanggal_lahir 		= $this->input->post('tanggal');
		$jenis_kelamin 		= $this->input->post('jenis_kelamin');
		$umur 				= $this->input->post('umur');
		$gol_darah			= $this->input->post('golongan');
		$penyandang_cacat 	= $this->input->post('penyandang');
		$agama	 			= $this->input->post('agama');
		$status_kawin 		= $this->input->post('status');
		$pendidikan 		= $this->input->post('pendidikan');
		$pekerjaan 			= $this->input->post('pekerjaan');
		$alamat 			= $this->input->post('alamat');
		$dusun 				= $this->input->post('dusun');
		$rw 				= $this->input->post('rw');
		$rt 				= $this->input->post('rt');
		$desa 				= $this->input->post('desa');
		$kecamatan 			= $this->input->post('kecamatan');
		$kab_kota 			= $this->input->post('kab_kota');
		$prov 				= $this->input->post('prov');
		$warga_negara 		= $this->input->post('warga_negara');
		$data 			= array(
			'id_user'			=> $id_user,
			'no_kk'				=> $no_kk,
			'nik'				=> $nik,
			'nama_lengkap'		=> $nama_lengkap,
			'tempat_lahir'		=> $tempat_lahir,
			'tanggal_lahir'		=> $tanggal_lahir,
			'jenis_kelamin'		=> $jenis_kelamin,
			'umur'				=> $umur,
			'gol_darah'			=> $gol_darah,
			'penyandang_cacat'	=> $penyandang_cacat,
			'agama'				=> $agama,
			'status_kawin'		=> $status_kawin,
			'pendidikan'		=> $pendidikan,
			'pekerjaan'			=> $pekerjaan,
			'alamat'			=> $alamat,
			'dusun'				=> $dusun,
			'rw'				=> $rw,
			'rt'				=> $rt,
			'desa'				=> $desa,
			'kecamatan'			=> $kecamatan,
			'kab_kota'			=> $kab_kota,
			'prov'				=> $prov,
			'warga_negara'		=> $warga_negara
		);
		$data2 = array (
			'id_user'			=> $id_user,
			'username'			=> $nik,
			'password'			=> $nik, 
			'level'				=> "warga"
		);
		$this->Warga_model->input_warga($data,'tb_warga');
		$this->Warga_model->input_warga($data2,'tb_user');

		$this->session->set_flashdata('pesan', 'Ditambahkan');
		redirect("Monitoring/Master/Data_warga");
	}

	public function edit($id)
	{
		$data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();
        $data['ewarga'] = $this->Warga_model->edit_warga($id);
		$this->load->view("Monitoring/Master/edit_warga",$data);
	}
	public function update_warga()
	{
		$id['id_user']	= $this->input->post('id_user');
		$data = array(
			'no_kk'				=> $this->input->post('no_kk'),
			'nik'				=> $this->input->post('nik'),
			'nama_lengkap'		=> $this->input->post('nama'),
			'tempat_lahir'		=> $this->input->post('tempat'),
			'tanggal_lahir'		=> $this->input->post('tanggal'),
			'jenis_kelamin'		=> $this->input->post('jenis_kelamin'),
			'umur'				=> $this->input->post('umur'),
			'gol_darah'			=> $this->input->post('golongan'),
			'penyandang_cacat'	=> $this->input->post('penyandang'),
			'agama'				=> $this->input->post('agama'),
			'status_kawin'		=> $this->input->post('status'),
			'pendidikan'		=> $this->input->post('pendidikan'),
			'pekerjaan'			=> $this->input->post('pekerjaan'),
			'alamat'			=> $this->input->post('alamat'),
			'dusun'				=> $this->input->post('dusun'),
			'rw'				=> $this->input->post('rw'),
			'rt'				=> $this->input->post('rt'),
			'desa'				=> $this->input->post('desa'),
			'kecamatan'			=> $this->input->post('kecamatan'),
			'kab_kota'			=> $this->input->post('kab_kota'),
			'prov'				=> $this->input->post('prov'),
			'warga_negara'		=> $this->input->post('warga_negara')
		); 
		$this->session->set_flashdata('message', 'Simpan Perubahan');
		$this->Warga_model->update_warga($data, $id);

		$this->session->set_flashdata('pesan', 'Diubah');
		redirect('Monitoring/Master/Data_warga');
	}
	public function get_warga()
	{
		$id_user = $this->input->post('id_user');
		echo json_encode($this->db->get_where('tb_warga', ['id_user' => $id_user])->row_array());
	}
	public function hapus($id)
    {
		$this->Warga_model->delete_user($id); 
		$this->Warga_model->delete_warga($id);  

		$this->session->set_flashdata('pesan', 'Dihapus');
		redirect("Monitoring/Master/Data_warga");
    }
    public function detail($id)
	{
        $data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();
        $data['detail'] = $this->Warga_model->detail_warga($id);
		$this->load->view('Monitoring/Master/detail_warga',$data);
    }
}