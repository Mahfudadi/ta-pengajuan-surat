<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_perangkat extends CI_Controller {

	function __construct(){
		parent::__construct();
        $this->load->model('KetuaRt_model');
        $this->load->model('PerangkatDesa_model');
	}

	public function index()
    {
        $data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();

        // $data['rw'] = $this->KetuaRt_model->tampil_rw();
        $data['rt'] = $this->KetuaRt_model->tampil_data();
        $data['kades'] = $this->PerangkatDesa_model->tampil_kepala_desa();
        $data['camat'] = $this->PerangkatDesa_model->tampil_camat();
        $data['kapolsek'] = $this->PerangkatDesa_model->tampil_kapolsek();

        //Menampilkan view dan juga data yang diambil dari model
        $this->load->view('Monitoring/Master/data_perangkat',$data);
    }
    public function detail_rt($id)
    {
        $data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();
        $data['detail'] = $this->KetuaRt_model->detail_rt($id);
        $this->load->view('Monitoring/Master/detail_rt',$data);
    }
    public function edit_rt($id)
    {
        $data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();
        $data['editrt'] = $this->KetuaRt_model->edit_rt($id);
        $this->load->view("Monitoring/Master/edit_rt",$data);
    }
    public function update_rt()
    {
        $id['id_rt'] = $this->input->post('id_rt');
        $data = array(
            'nik'      => $this->input->post('nik'),
            'nama_rt'      => $this->input->post('nama_rt'),
            'ketua_rt'          => $this->input->post('ketua_rt'),
            'alamat_rt'          => $this->input->post('alamat_rt')
        ); 
        $this->session->set_flashdata('message', 'Simpan Perubahan');
        $this->KetuaRt_model->update_rt($data, $id);
        $this->session->set_flashdata('pesan', 'Diubah');
        redirect('Monitoring/Master/data_perangkat');
    }
    //kades
    public function edit_kades($id)
    {
        $data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();
        $data['kades'] = $this->PerangkatDesa_model->edit_kades($id);
        $this->load->view("Monitoring/Master/edit_kades",$data);
    }
    //camat
    public function edit_camat($id)
    {
        $data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();
        $data['camat'] = $this->PerangkatDesa_model->edit_camat($id);
        $this->load->view("Monitoring/Master/edit_camat",$data);
    }
    //kapolsek
    public function edit_kapolsek($id)
    {
        $data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();
        $data['kapolsek'] = $this->PerangkatDesa_model->edit_kapolsek($id);
        $this->load->view("Monitoring/Master/edit_kapolsek",$data);
    }
    public function update()
    {
        $id['id_perangkat']  = $this->input->post('id_perangkat');
        $data = array(
            'nip'            => $this->input->post('nip'),
            'nama'                => $this->input->post('nama'),
            'alamat'                => $this->input->post('alamat')
        ); 
        $this->session->set_flashdata('message', 'Simpan Perubahan');
        $this->PerangkatDesa_model->update($data, $id);
        $this->session->set_flashdata('pesan', 'Diubah');
        redirect('Monitoring/Master/data_perangkat');
    }

}