<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajuan_kelahiran extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Suratajuan_model');
	}
    public function index()
    {        

        $data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();
        $data['data_surat'] = $this->Suratajuan_model->tampil_kelahiran();

        $this->load->view('Monitoring/Surat/tampil_kelahiran',$data);
    }
    public function filter_kelahiran()
    {   
        
        $status = $this->input->post('status');
        $bulan = $this->input->post('bulan');

        $data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();


        if($status=='0'&&$bulan=='0')
        {
            redirect ("Monitoring/Surat/Ajuan_kelahiran");

        }elseif($status=='0'&&$bulan==$bulan)
        {
            $data['data_surat'] = $this->Suratajuan_model->cari_bulan_lahir($bulan);
            $this->load->view('Monitoring/Surat/tampil_kelahiran',$data);

        }elseif($status==$status&&$bulan=='0')
        {
            $data['data_surat'] = $this->Suratajuan_model->cari_status_lahir($status);
            $this->load->view('Monitoring/Surat/tampil_kelahiran',$data);

        }elseif($status==$status&&$bulan==$bulan)
        {
            $data['data_surat'] = $this->Suratajuan_model->cari_lahir($bulan, $status);
            $this->load->view('Monitoring/Surat/tampil_kelahiran',$data);

        }else{
            redirect ("Monitoring/Surat/Ajuan_kelahiran");
        }
    }
    public function detail($id)
    {
        $data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();
        $data['detailkelahiran'] = $this->Suratajuan_model->detail_kelahiran($id);
        $this->load->view('Monitoring/Surat/detail_kelahiran',$data);
    }
    public function edit($id)
    {
        $data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();
        $data['editkelahiran'] = $this->Suratajuan_model->edit_kelahiran($id);
        $data['statuskelahiran'] = $this->Suratajuan_model->status();
        $this->load->view("Monitoring/Surat/edit_kelahiran",$data);
    }
    public function update_kelahiran()
    {
        $id['id_surat'] = $this->input->post('id_surat');
        $data = array(
            'no_surat'          => $this->input->post('no_surat'),
            'id_status'         => $this->input->post('id_status'),
            'ket_tolak'         => $this->input->post('ditolak')
        ); 
        $data2 = array(
            'id_surat'                  => $this->input->post('id_surat'),
            'tgl_riwayat'               => $this->input->post('tgl_riwayat'),
            'riwayat_status'            => $this->input->post('id_status')
        );
        $this->Suratajuan_model->update_kelahiran($data, $id);
        $this->Suratajuan_model->insert_riwayat($data2,'tb_riwayat');
        $this->session->set_flashdata('pesan', 'Diubah');
        redirect('Monitoring/Surat/Ajuan_kelahiran');
    }
    
    public function cetak($id){

        $status = '6';
        $tgl = date('Y-m-d');
        $id2['id_surat'] = $id;
        $data1 = array(
            'id_status'         => $status
        ); 
        $data2 = array(
            'id_surat'                  => $id,
            'tgl_riwayat'               => $tgl,
            'riwayat_status'            => $status
        ); 

        $data['cetak'] = $this->Suratajuan_model->detail_kelahiran($id);
        $data['kades'] = $this->Suratajuan_model->kades();

        $this->load->library('pdf');
        $this->pdf->setPaper('F4', 'potrait');
        $this->Suratajuan_model->insert_riwayat($data2,'tb_riwayat');
        $this->Suratajuan_model->update_kelahiran($data1, $id2);
        $this->session->set_flashdata('pesan', 'Diunduh');
        $this->pdf->load_view('Monitoring/Surat/cetak_kelahiran',$data);

    }
}