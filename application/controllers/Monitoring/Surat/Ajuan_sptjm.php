<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajuan_sptjm extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Suratajuan_model');
	}
    public function index()
    {        
        
        $data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();
        $data['data_surat'] = $this->Suratajuan_model->tampil_sptjm();

        $this->load->view('Monitoring/Surat/tampil_sptjm',$data);
    }
    public function filter_sptjm()
    {   
        $status = $this->input->post('status');
        $bulan = $this->input->post('bulan');

        $data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();

        if($status=='0'&&$bulan=='0')
        {
            redirect ("Monitoring/Surat/Ajuan_sptjm");

        }elseif($status=='0'&&$bulan==$bulan)
        {
            $data['data_surat'] = $this->Suratajuan_model->cari_bulan_sptjm($bulan);
            $this->load->view('Monitoring/Surat/tampil_sptjm',$data);

        }elseif($status==$status&&$bulan=='0')
        {
            $data['data_surat'] = $this->Suratajuan_model->cari_status_sptjm($status);
            $this->load->view('Monitoring/Surat/tampil_sptjm',$data);

        }elseif($status==$status&&$bulan==$bulan)
        {
            $data['data_surat'] = $this->Suratajuan_model->cari_sptjm($bulan, $status);
            $this->load->view('Monitoring/Surat/tampil_sptjm',$data);

        }else{
            redirect ("Monitoring/Surat/Ajuan_sptjm");
        }
    }
    public function detail($id)
    {
        $data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();
        $data['detailsptjm'] = $this->Suratajuan_model->detail_sptjm($id);
        $this->load->view('Monitoring/Surat/detail_sptjm',$data);
    }
    public function edit($id)
    {
        $data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();
        $data['editsptjm'] = $this->Suratajuan_model->edit_sptjm($id);
        $data['statussptjm'] = $this->Suratajuan_model->status();
        $this->load->view("Monitoring/Surat/edit_sptjm",$data);
    }
    public function update_sptjm()
    {
        $id['id_surat'] = $this->input->post('id_surat');
        $data = array(
            'no_surat'          => $this->input->post('no_surat'),
            'id_status'         => $this->input->post('id_status'),
            'ket_tolak'         => $this->input->post('ditolak')
        ); 
        $data2 = array(
            'id_surat'                  => $this->input->post('id_surat'),
            'tgl_riwayat'               => $this->input->post('tgl_riwayat'),
            'riwayat_status'            => $this->input->post('id_status')
        ); 
        $this->Suratajuan_model->insert_riwayat($data2,'tb_riwayat');
        $this->Suratajuan_model->update_sptjm($data, $id);
        $this->session->set_flashdata('pesan', 'Diubah');
        redirect('Monitoring/Surat/Ajuan_sptjm');
    }
    public function cetak($id){

        $status = '6';
        $tgl = date('Y-m-d');
        $id2['id_surat'] = $id;
        $data1 = array(
            'id_status'         => $status
        ); 
        $data2 = array(
            'id_surat'                  => $id,
            'tgl_riwayat'               => $tgl,
            'riwayat_status'            => $status
        ); 

        $data['cetak'] = $this->Suratajuan_model->detail_sptjm($id);
        $data['kades'] = $this->Suratajuan_model->kades();

        $this->load->library('pdf');
        $this->pdf->setPaper('F4', 'potrait');
        $this->Suratajuan_model->insert_riwayat($data2,'tb_riwayat');
        $this->Suratajuan_model->update_sptjm($data1, $id2);
        $this->session->set_flashdata('pesan', 'Diubah');
        $this->pdf->load_view('Monitoring/Surat/cetak_sptjm',$data);
    }
}