<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Semua_surat extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Suratajuan_model');
	}

    public function index()
    {   
        $data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();
        $data['data_surat'] = $this->Suratajuan_model->tampil_semua();

        $this->load->view('Monitoring/Surat/tampil_semua',$data);
    }
    public function filter_semua()
    {   
        $status = $this->input->post('status');
        $bulan = $this->input->post('bulan');

        $data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();

        if($status=='0'&&$bulan=='0')
        {
            redirect ("Monitoring/Surat/Semua_surat");

        }elseif($status=='0'&&$bulan==$bulan)
        {
            $data['data_surat'] = $this->Suratajuan_model->tampil_semua_bulan($bulan);
            $this->load->view('Monitoring/Surat/tampil_semua',$data);

        }elseif($status==$status&&$bulan=='0')
        {
            $data['data_surat'] = $this->Suratajuan_model->tampil_semua_status($status);
            $this->load->view('Monitoring/Surat/tampil_semua',$data);

        }elseif($status==$status&&$bulan==$bulan)
        {
            $data['data_surat'] = $this->Suratajuan_model->tampil_semua_status_bulan($bulan, $status);
            $this->load->view('Monitoring/Surat/tampil_semua',$data);

        }else{
            redirect ("Monitoring/Surat/Semua_surat");
        }
    }
}