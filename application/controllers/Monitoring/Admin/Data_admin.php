<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_admin extends CI_Controller {

	function __construct(){
		parent::__construct();
        $this->load->model('Admin_model');
	}

	public function index()
    {
        $data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();
        $data['profil'] = $this->Admin_model->tampil_data();
        //Menampilkan view dan juga data yang diambil dari model
        $this->load->view('Monitoring/Admin/profil_admin',$data);
    }
    public function edit_profil($id)
    {
        $data['editprofil'] = $this->Admin_model->edit_rt($id);
        $this->load->view("Monitoring/Beranda",$data);
    }
    public function update_profil()
    {
        $id['id_user'] = $this->input->post('id_user');
        $data = array(
            'nip'      => $this->input->post('nip'),
            'nama_admin'      => $this->input->post('nama_admin'),
        ); 
        $this->session->set_flashdata('message', 'Simpan Perubahan');
        $this->KetuaRt_model->update_profil($data, $id);
        redirect('Monitoring/Admin/Data_admin');
    }
}