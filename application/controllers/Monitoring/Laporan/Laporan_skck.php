<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_skck extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('LaporanSurat_model');
	}
	public function index()
	{
		$data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();
        //Mengambil data dari model dan di jadikan variabel
        $data['laporan'] = $this->LaporanSurat_model->tampil_skck();

        //Menampilkan view dan juga data yang diambil dari model
		$this->load->view('Monitoring/Laporan/laporan_skck',$data);
    }
    public function filter_skck()
	{
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$status = $this->input->post('status');

		$data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();
        //Mengambil data dari model dan di jadikan variabel

		if($bulan=='0'&&$tahun=='0'&&$status=='0')
		{
			redirect ("Monitoring/Laporan/Laporan_skck");
		}elseif($status==$status&&$bulan=='0'&&$tahun=='0')
		{
			$data['laporan'] = $this->LaporanSurat_model->filter_status_skck($status);
			$this->load->view('Monitoring/Laporan/laporan_skck',$data);
		}elseif($status=='0'&&$bulan==$bulan&&$tahun=='0')
		{
			$data['laporan'] = $this->LaporanSurat_model->filter_bulan_skck($bulan);
			$this->load->view('Monitoring/Laporan/laporan_skck',$data);
		}elseif($status=='0'&&$bulan=='0'&&$tahun==$tahun)
		{
			$data['laporan'] = $this->LaporanSurat_model->filter_tahun_skck($tahun);
			$this->load->view('Monitoring/Laporan/laporan_skck',$data);
		}elseif($status==$status&&$bulan==$bulan&&$tahun=='0')
		{
			$data['laporan'] = $this->LaporanSurat_model->filter_status_bulan_skck($status, $bulan);
			$this->load->view('Monitoring/Laporan/laporan_skck',$data);
		}elseif($status==$status&&$bulan=='0'&&$tahun==$tahun)
		{
			$data['laporan'] = $this->LaporanSurat_model->filter_status_tahun_skck($status, $tahun);
			$this->load->view('Monitoring/Laporan/laporan_skck',$data);
		}elseif($status=='0'&&$bulan==$bulan&&$tahun==$tahun)
		{
			$data['laporan'] = $this->LaporanSurat_model->filter_bulan_tahun_skck($bulan, $tahun);
			$this->load->view('Monitoring/Laporan/laporan_skck',$data);
		}else
		{
		$data['laporan'] = $this->LaporanSurat_model->filter_semua_skck($status, $bulan, $tahun);
		$this->load->view('Monitoring/Laporan/laporan_skck',$data);
		}
    }     

}