<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_kelahiran extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('LaporanSurat_model');
	}
	public function index()
	{
		$data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();
        //Mengambil data dari model dan di jadikan variabel
        $data['laporan'] = $this->LaporanSurat_model->tampil_kelahiran();

        //Menampilkan view dan juga data yang diambil dari model
		$this->load->view('Monitoring/Laporan/laporan_kelahiran',$data);
    }
    
    public function filter_kelahiran()
	{
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$status = $this->input->post('status');

		$data['user'] = $this->db->get_where('tb_admin', ['id_user' => $this->session->userdata('ses_id')])->row_array();
        //Mengambil data dari model dan di jadikan variabel

		if($bulan=='0'&&$tahun=='0'&&$status=='0')
		{
			redirect ("Monitoring/Laporan/Laporan_kelahiran");
		}elseif($status==$status&&$bulan=='0'&&$tahun=='0')
		{
			$data['laporan'] = $this->LaporanSurat_model->filter_status_kelahiran($status);
			$this->load->view('Monitoring/Laporan/laporan_kelahiran',$data);
		}elseif($status=='0'&&$bulan==$bulan&&$tahun=='0')
		{
			$data['laporan'] = $this->LaporanSurat_model->filter_bulan_kelahiran($bulan);
			$this->load->view('Monitoring/Laporan/laporan_kelahiran',$data);
		}elseif($status=='0'&&$bulan=='0'&&$tahun==$tahun)
		{
			$data['laporan'] = $this->LaporanSurat_model->filter_tahun_kelahiran($tahun);
			$this->load->view('Monitoring/Laporan/laporan_kelahiran',$data);
		}elseif($status==$status&&$bulan==$bulan&&$tahun=='0')
		{
			$data['laporan'] = $this->LaporanSurat_model->filter_status_bulan_kelahiran($status, $bulan);
			$this->load->view('Monitoring/Laporan/laporan_kelahiran',$data);
		}elseif($status==$status&&$bulan=='0'&&$tahun==$tahun)
		{
			$data['laporan'] = $this->LaporanSurat_model->filter_status_tahun_kelahiran($status, $tahun);
			$this->load->view('Monitoring/Laporan/laporan_kelahiran',$data);
		}elseif($status=='0'&&$bulan==$bulan&&$tahun==$tahun)
		{
			$data['laporan'] = $this->LaporanSurat_model->filter_bulan_tahun_kelahiran($bulan, $tahun);
			$this->load->view('Monitoring/Laporan/laporan_kelahiran',$data);
		}else
		{
		$data['laporan'] = $this->LaporanSurat_model->filter_semua_kelahiran($status, $bulan, $tahun);
		$this->load->view('Monitoring/Laporan/laporan_kelahiran',$data);
		}
    }

}