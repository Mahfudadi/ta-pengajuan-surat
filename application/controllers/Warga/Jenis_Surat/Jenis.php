<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('WargaSuratAjuan_model');
	}

	public function index()
	{
        $id = $this->session->userdata('ses_id');
        // memanggil model berdasarkan id user yang login
        $data['biodata'] = $this->WargaSuratAjuan_model->biodata($id);
        $data['domisili'] = $this->WargaSuratAjuan_model->domisili($id);
        $data['keramaian'] = $this->WargaSuratAjuan_model->keramaian($id);
        $data['kelahiran'] = $this->WargaSuratAjuan_model->kelahiran($id);
        $data['kematian'] = $this->WargaSuratAjuan_model->kematian($id);
        $data['kip'] = $this->WargaSuratAjuan_model->kip($id);
        $data['kuasa'] = $this->WargaSuratAjuan_model->kuasa($id);
        $data['ektp'] = $this->WargaSuratAjuan_model->ktp($id);
        $data['kk'] = $this->WargaSuratAjuan_model->kk($id);
        $data['skck'] = $this->WargaSuratAjuan_model->skck($id);
        $data['sktm'] = $this->WargaSuratAjuan_model->sktm($id);
        $data['sptjm'] = $this->WargaSuratAjuan_model->sptjm($id);

		$this->load->view('Warga/Jenis_surat/jenis', $data);
	}
}
