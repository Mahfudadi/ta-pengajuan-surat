<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProfilWarga extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('WargaSuratAjuan_model');
	}

	public function index($id)
	{
        $data['profil'] = $this->WargaSuratAjuan_model->edit_warga($id);
		$this->load->view('Warga/Profil_Warga/detail_profil',$data);
	}
	public function edit_warga($id)
    {
        $data['profil'] = $this->WargaSuratAjuan_model->edit_warga($id);
        $this->load->view("Warga/Profil_Warga/ubah_profil",$data);
    }
    public function update()
    {
        $id['id_user']	= $this->input->post('id_user');
		$data = array(
			'no_kk'				=> $this->input->post('no_kk'),
			'nik'				=> $this->input->post('nik'),
			'nama_lengkap'		=> $this->input->post('nama'),
			'tempat_lahir'		=> $this->input->post('tempat_lahir'),
			'tanggal_lahir'		=> $this->input->post('tgl_lahir'),
			'jenis_kelamin'		=> $this->input->post('jenkel'),
			'umur'				=> $this->input->post('umur'),
			'gol_darah'			=> $this->input->post('gol_darah'),
			'penyandang_cacat'	=> $this->input->post('cacat'),
			'agama'				=> $this->input->post('agama'),
			'status_kawin'		=> $this->input->post('status'),
			'pendidikan'		=> $this->input->post('pendidikan'),
			'pekerjaan'			=> $this->input->post('pekerjaan'),
			'alamat'			=> $this->input->post('alamat'),
			'dusun'				=> $this->input->post('dusun'),
			'rw'				=> $this->input->post('rw'),
			'rt'				=> $this->input->post('rt'),
			'desa'				=> $this->input->post('desa'),
			'kecamatan'			=> $this->input->post('kecamatan'),
			'kab_kota'			=> $this->input->post('kabupaten'),
			'prov'				=> $this->input->post('prov'),
			'warga_negara'		=> $this->input->post('warga_negara')
        ); 
        $this->WargaSuratAjuan_model->update_warga($data, $id);
        $this->session->set_flashdata('pesan', 'Diubah');
        redirect('Warga/Profil_Warga/ProfilWarga/index/'.$this->session->userdata('ses_id'));
    }
    public function edit_pw($id)
    {
        $data['profil'] = $this->WargaSuratAjuan_model->edit_pw($id);
        $this->load->view("Warga/Profil_Warga/ubah_password",$data);
    }
    public function update_password()
	{
		$current_password = $this->input->post('password_lama');
		$new_password1 = $this->input->post('password_baru');
		$new_password2 = $this->input->post('password_baru2');

		if ($current_password == $new_password1) {
			$this->session->set_flashdata('pesangagal', 'Password baru tidak boleh sama dengan password lama !');
			redirect('Warga/Profil_Warga/ProfilWarga/edit_pw/'.$this->session->userdata('ses_id'));
		} elseif($new_password1 != $new_password2){
			$this->session->set_flashdata('pesangagal', 'Password yang anda masukkan tidak sama !');
			redirect('Warga/Profil_Warga/ProfilWarga/edit_pw/'.$this->session->userdata('ses_id'));
		} else {
			$this->db->set('password', $new_password1);
			$this->db->where('id_user', $this->session->userdata('ses_id'));
			$this->db->update('tb_user');
			$this->session->set_flashdata('pesan', 'Diubah');
			redirect('Warga/Profil_Warga/ProfilWarga/index/'.$this->session->userdata('ses_id'));
		}
	}
}
