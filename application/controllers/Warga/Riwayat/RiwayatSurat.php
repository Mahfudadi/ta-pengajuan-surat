<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RiwayatSurat extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Riwayat_warga_model');
	}

	public function index()
	{
		$id = $this->session->userdata('ses_id');
        $data['surat'] = $this->Riwayat_warga_model->tampil($id);
        //Mengambil data dari model dan di jadikan variabel
		$this->load->view('Warga/Riwayat/riwayat',$data);
	}
}
