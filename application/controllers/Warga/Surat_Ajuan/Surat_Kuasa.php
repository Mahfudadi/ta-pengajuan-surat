<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surat_Kuasa extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('WargaSuratAjuan_model');
		$this->load->helper('string');
	}

    public function index()
	{
        //mengambil session id yang login
        $id = $this->session->userdata('ses_id');
        // memanggil model berdasarkan id user yang login
        $data['warga'] = $this->WargaSuratAjuan_model->warga($id);

		$this->load->view('Warga/Surat_ajuan/kuasa', $data);
	}
    public function tambah()
	{
        if(isset($_POST['submit']))
        {
            $this->form_validation->set_rules('name', 'Name', 'required');

            $config['upload_path'] = 'assets/upload/Surat_Kuasa/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']  = '2048';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            //file 1
            if(!empty($_FILES['scan_kk']['name'])) {
                $this->upload->do_upload('scan_kk');
                $data1 = $this->upload->data();
                $file1 = $data1['file_name'];
            }
            //file 2
            if(!empty($_FILES['scan_ektp']['name'])) {
                $this->upload->do_upload('scan_ektp');
                $data2 = $this->upload->data();
                $file2 = $data2['file_name'];
            }
            //file 3
            if(!empty($_FILES['scan_kk_kedua']['name'])) {
                $this->upload->do_upload('scan_kk_kedua');
                $data3 = $this->upload->data();
                $file3 = $data3['file_name'];
            }
            //file 4
            if(!empty($_FILES['scan_ektp_kedua']['name'])) {
                $this->upload->do_upload('scan_ektp_kedua');
                $data4 = $this->upload->data();
                $file4 = $data4['file_name'];
            }
            if(!empty($_FILES['scan_pajak']['name'])) {
                $this->upload->do_upload('scan_pajak');
                $data5 = $this->upload->data();
                $file5 = $data5['file_name'];
            }

            $id_surat           = $this->input->post('id_surat');
            $id_user            = $this->input->post('id_user');
            $id_status          = $this->input->post('id_status');
            $no_surat           = $this->input->post('no_surat');
            $tgl_surat          = $this->input->post('tgl_surat');
            $nama_pihak_kedua   = $this->input->post('nama_pihak_kedua');
            $nik_pihak_kedua    = $this->input->post('nik_pihak_kedua');
            $tempat_lahir_pihak_kedua  = $this->input->post('tempat_lahir_pihak_kedua');
            $tgl_lahir_pihak_kedua     = $this->input->post('tgl_lahir_pihak_kedua');
            $jenkel_pihak_kedua        = $this->input->post('jenkel_pihak_kedua');
            $status_kawin_pihak_kedua  = $this->input->post('status_kawin_pihak_kedua');
            $pekerjaan_pihak_kedua     = $this->input->post('pekerjaan_pihak_kedua');
            $alamat_pihak_kedua        = $this->input->post('alamat_pihak_kedua');
            $kuasa        = $this->input->post('kuasa');
            
            $data = array(
                'id_surat' 	            => $id_surat,
                'id_user' 	            => $id_user,
                'id_status' 	        => $id_status,
                'no_surat'              => $no_surat,
                'tgl_surat'             => $tgl_surat,
                'nama_pihak_kedua'      => $nama_pihak_kedua,
                'nik_pihak_kedua'       => $nik_pihak_kedua,
                'tempat_lahir_pihak_kedua'  => $tempat_lahir_pihak_kedua,
                'tgl_lahir_pihak_kedua'     => $tgl_lahir_pihak_kedua,
                'jenkel_pihak_kedua'        => $jenkel_pihak_kedua,
                'status_kawin_pihak_kedua'  => $status_kawin_pihak_kedua,
                'pekerjaan_pihak_kedua'     => $pekerjaan_pihak_kedua,
                'alamat_pihak_kedua' 	=> $alamat_pihak_kedua,
                'kuasa' 	            => $kuasa,
                'scan_kk'               => $file1,
                'scan_ektp'             => $file2,
                'scan_kk_pihak_kedua'   => $file3,
                'scan_ektp_pihak_kedua' => $file4,
                'scan_pajak'            => $file5
            );
            $this->WargaSuratAjuan_model->input_surat($data,'surat_kuasa');
            $this->session->set_flashdata('pesan', 'Diajukan');
            redirect("index.php/Warga/Jenis_Surat/Jenis");
        }
        else
        {
            echo "Gagal Insert";
        }
	}

}