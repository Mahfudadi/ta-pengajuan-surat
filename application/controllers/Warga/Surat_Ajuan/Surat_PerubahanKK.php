<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surat_PerubahanKK extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('WargaSuratAjuan_model');
	}

    public function index()
	{
        //mengambil session id yang login
        $id = $this->session->userdata('ses_id');
        // memanggil model berdasarkan id user yang login
        $data['warga'] = $this->WargaSuratAjuan_model->warga($id);

		$this->load->view('Warga/Surat_ajuan/perubahankk', $data);
	}

    public function tambah()
	{
        if(isset($_POST['submit']))
        {
            $this->form_validation->set_rules('name', 'Name', 'required');

            $config['upload_path'] = 'assets/upload/Surat_KK/';
            $config['allowed_types'] = 'jpg|png|jpeg|pdf';
            $config['max_size']  = '10000';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            //file 1
            if(!empty($_FILES['scan_kk']['name'])) {
                $this->upload->do_upload('scan_kk');
                $data1 = $this->upload->data();
                $file1 = $data1['file_name'];
            }
            //file 2
            if(!empty($_FILES['scan_ektp']['name'])) {
                $this->upload->do_upload('scan_ektp');
                $data2 = $this->upload->data();
                $file2 = $data2['file_name'];
            }
            //file 1
            if(!empty($_FILES['scan_ijazah']['name'])) {
                $this->upload->do_upload('scan_ijazah');
                $data3 = $this->upload->data();
                $file3 = $data1['file_name'];
            }
            //file 2
            if(!empty($_FILES['scan_nikah']['name'])) {
                $this->upload->do_upload('scan_nikah');
                $data4 = $this->upload->data();
                $file4 = $data4['file_name'];
            }
            if(!empty($_FILES['scan_pajak']['name'])) {
                $this->upload->do_upload('scan_pajak');
                $data5 = $this->upload->data();
                $file5 = $data5['file_name'];
            }

            $id_surat       = $this->input->post('id_surat');
            $id_user        = $this->input->post('id_user');
            $no_surat       = $this->input->post('no_surat');
            $id_status      = $this->input->post('id_status');
            $tgl_surat      = $this->input->post('tgl_surat');
            $kepala_keluarga      = $this->input->post('kepala_keluarga');
            $alasan         = $this->input->post('alasan');
            $jumlah_anggota   = $this->input->post('jumlah_anggota');
            $nik_anggota_a      = $this->input->post('nik_anggota_a');
            $nik_anggota_b            = $this->input->post('nik_anggota_b');
            $nama_anggota_a        = $this->input->post('nama_anggota_a');
            $nama_anggota_b       = $this->input->post('nama_anggota_b');

            $data = array(
                'id_surat' 	        => $id_surat,
                'id_user' 	        => $id_user,
                'no_surat' 	        => $no_surat,
                'id_status' 	    => $id_status,
                'tgl_surat' 	    => $tgl_surat,
                'kepala_keluarga' 	=> $kepala_keluarga,
                'alasan' 	        => $alasan,
                'jumlah_anggota'    => $jumlah_anggota,
                'nik_anggota_a' 	=> $nik_anggota_a,
                'nik_anggota_b' 	=> $nik_anggota_b,
                'nama_anggota_a' 	=> $nama_anggota_a,
                'nama_anggota_b' 	=> $nama_anggota_b,
                'scan_kk'           => $file1,
                'scan_ektp'         => $file2,
                'ijazah_terakhir'   => $file3,
                'surat_nikah_cerai' => $file4,
                'scan_pajak'        => $file5
            );

            $this->WargaSuratAjuan_model->input_surat($data,'surat_perubahan_kk');
            $this->session->set_flashdata('pesan', 'Diajukan');
            redirect("index.php/Warga/Jenis_Surat/Jenis");
        }
        else
        {
            echo "Gagal Insert";
        }
	}
}