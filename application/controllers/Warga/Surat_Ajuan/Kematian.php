<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kematian extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('WargaSuratAjuan_model');
	}

    public function index()
	{
        //mengambil session id yang login
        $id = $this->session->userdata('ses_id');
        // memanggil model berdasarkan id user yang login
        $data['warga'] = $this->WargaSuratAjuan_model->warga($id);

		$this->load->view('Warga/Surat_ajuan/kematian', $data);
	}

    public function tambah()
	{
        if(isset($_POST['submit']))
        {
            $this->form_validation->set_rules('name', 'Name', 'required');

            $config['upload_path'] = 'assets/upload/Surat_Kuasa/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']  = '2048';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            //file 1
            if(!empty($_FILES['scan_kk']['name'])) {
                $this->upload->do_upload('scan_kk');
                $data1 = $this->upload->data();
                $file1 = $data1['file_name'];
            }
            //file 2
            if(!empty($_FILES['scan_ektp_mati']['name'])) {
                $this->upload->do_upload('scan_ektp_mati');
                $data2 = $this->upload->data();
                $file2 = $data2['file_name'];
            }
            //file 1
            if(!empty($_FILES['scan_ektp_saksi_a']['name'])) {
                $this->upload->do_upload('scan_ektp_saksi_a');
                $data3 = $this->upload->data();
                $file3 = $data3['file_name'];
            }
            //file 2
            if(!empty($_FILES['scan_ektp_saksi_b']['name'])) {
                $this->upload->do_upload('scan_ektp_saksi_b');
                $data4 = $this->upload->data();
                $file4 = $data4['file_name'];
            }
            //file 1
            if(!empty($_FILES['scan_suket_rs']['name'])) {
                $this->upload->do_upload('scan_suket_rs');
                $data5 = $this->upload->data();
                $file5 = $data5['file_name'];
            }
            if(!empty($_FILES['scan_pajak']['name'])) {
                $this->upload->do_upload('scan_pajak');
                $data6 = $this->upload->data();
                $file6 = $data6['file_name'];
            }


            $id_user            = $this->input->post('id_user');
        $no_surat           = $this->input->post('no_surat');
        $id_surat           = $this->input->post('id_surat');
        $tgl_surat          = $this->input->post('tgl_surat');
        $id_status          = $this->input->post('id_status');
        $nama_kepala_keluarga        = $this->input->post('nama_kepala_keluarga');
        $no_kk_kepala_keluarga      = $this->input->post('no_kk_kepala_keluarga');
        $nik_jenazah          = $this->input->post('nik_jenazah');
        $nama_jenazah        = $this->input->post('nama_jenazah');
        $jenkel_jenazah  = $this->input->post('jenkel_jenazah');
        $tempat_lahir_jenazah   = $this->input->post('tempat_lahir_jenazah');
        $tgl_lahir_jenazah   = $this->input->post('tgl_lahir_jenazah');
        $umur_jenazah       = $this->input->post('umur_jenazah');
        $agama_jenazah               = $this->input->post('agama_jenazah');
        $pekerjaan_jenazah     = $this->input->post('pekerjaan_jenazah');
        $alamat_jenazah              = $this->input->post('alamat_jenazah');
        $kewarganegaraan_jenazah           = $this->input->post('kewarganegaraan_jenazah');
        $anak_ke         = $this->input->post('anak_ke');
        $tgl_kematian       = $this->input->post('tgl_kematian');
        $pukul            = $this->input->post('pukul');
        $sebab_kematian           = $this->input->post('sebab_kematian');
        $tempat_kematian      = $this->input->post('tempat_kematian');
        $yang_menerangkan           = $this->input->post('yang_menerangkan');
        $nik_ibu      = $this->input->post('nik_ibu');
        $nama_ibu         = $this->input->post('nama_ibu');
        $tgl_lahir_ibu  = $this->input->post('tgl_lahir_ibu');
        $umur_ibu  = $this->input->post('umur_ibu');
        $pekerjaan_ibu  = $this->input->post('pekerjaan_ibu');
        $alamat_ibu           = $this->input->post('alamat_ibu');
        $nik_ayah          = $this->input->post('nik_ayah');
        $nama_ayah     = $this->input->post('nama_ayah');
        $tgl_lahir_ayah          = $this->input->post('tgl_lahir_ayah');
        $umur_ayah     = $this->input->post('umur_ayah');
        $pekerjaan_ayah        = $this->input->post('pekerjaan_ayah');
        $alamat_ayah = $this->input->post('alamat_ayah');
        $nik_saksi_a        = $this->input->post('nik_saksi_a');
        $nama_saksi_a       = $this->input->post('nama_saksi_a');
        $tgl_lahir_saksi_a       = $this->input->post('tgl_lahir_saksi_a');
        $umur_saksi_a       = $this->input->post('umur_saksi_a');
        $pekerjaan_saksi_a  = $this->input->post('pekerjaan_saksi_a');
        $alamat_saksi_a     = $this->input->post('alamat_saksi_a');;
        $nik_saksi_b        = $this->input->post('nik_saksi_b');
        $nama_saksi_b       = $this->input->post('nama_saksi_b');
        $tgl_lahir_saksi_b       = $this->input->post('tgl_lahir_saksi_b');
        $umur_saksi_b       = $this->input->post('umur_saksi_b');
        $pekerjaan_saksi_b  = $this->input->post('pekerjaan_saksi_b');
        $alamat_saksi_b     = $this->input->post('alamat_saksi_b');

        $data = array(
            'id_surat'              => $id_surat,
            'id_user'               => $id_user,
            'no_surat'              => $no_surat,
            'id_status'             => $id_status,
            'tgl_surat'             => $tgl_surat,
            'nama_kepala_keluarga'       => $nama_kepala_keluarga,
            'no_kk_kepala_keluarga'        => $no_kk_kepala_keluarga,
            'nik_jenazah'             => $nik_jenazah,
            'nama_jenazah'           => $nama_jenazah,
            'jenkel_jenazah'     => $jenkel_jenazah,
            'tempat_lahir_jenazah'      => $tempat_lahir_jenazah,
            'tgl_lahir_jenazah'       => $tgl_lahir_jenazah,
            'umur_jenazah'          => $umur_jenazah,
            'agama_jenazah'                  => $agama_jenazah,
            'pekerjaan_jenazah'               => $pekerjaan_jenazah,
            'alamat_jenazah'                 => $alamat_jenazah,
            'kewarganegaraan_jenazah'    => $kewarganegaraan_jenazah,
            'anak_ke'            => $anak_ke,
            'tgl_kematian'          => $tgl_kematian,
            'pukul'               => $pukul,
            'sebab_kematian'              => $sebab_kematian,
            'tempat_kematian'         => $tempat_kematian,
            'yang_menerangkan'              => $yang_menerangkan,
            'nik_ibu'         => $nik_ibu,
            'nama_ibu'            => $nama_ibu,
            'tgl_lahir_ibu'   => $tgl_lahir_ibu,
            'alamat_ibu'        => $alamat_ibu,
            'nik_ayah'              => $nik_ayah,
            'nama_ayah'             => $nama_ayah,
            'tgl_lahir_ayah'        => $tgl_lahir_ayah,
            'umur_ayah'             => $umur_ayah,
            'pekerjaan_ayah'        => $pekerjaan_ayah,
            'alamat_ayah'           => $alamat_ayah,
            'nik_saksi_a'           => $nik_saksi_a,
            'nama_saksi_a'          => $nama_saksi_a,
            'tgl_lahir_saksi_a'     => $tgl_lahir_saksi_a,
            'umur_saksi_a'          => $umur_saksi_a,
            'pekerjaan_saksi_a'     => $pekerjaan_saksi_a,
            'alamat_saksi_a'        => $alamat_saksi_a,
            'nik_saksi_b'           => $nik_saksi_b,
            'nama_saksi_b'          => $nama_saksi_b,
            'tgl_lahir_saksi_b'     => $tgl_lahir_saksi_b,
            'umur_saksi_b'          => $umur_saksi_b,
            'pekerjaan_saksi_b'     => $pekerjaan_saksi_b,
            'alamat_saksi_b'        => $alamat_saksi_b,
            'scan_kk'               => $file1,
            'scan_ektp_meninggal'               => $file2,
            'scan_ektp_saksi_a'               => $file3,
            'scan_ektp_saksi_b'               => $file4,
            'suket_meninggal'               => $file5,
            'scan_pajak'               => $file6
            );

            $this->WargaSuratAjuan_model->input_surat($data,'surat_kematian');
            $this->session->set_flashdata('pesan', 'Diajukan');
            redirect("Warga/Jenis_Surat/Jenis");
        }
        else
        {
            echo "Gagal Insert";
        }
	}

}