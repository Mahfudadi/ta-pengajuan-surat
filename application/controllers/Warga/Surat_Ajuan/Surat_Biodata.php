<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surat_Biodata extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('WargaSuratAjuan_model');
	}

    public function index()
	{
        //mengambil session id yang login
        $id = $this->session->userdata('ses_id');
        // memanggil model berdasarkan id user yang login
        $data['warga'] = $this->WargaSuratAjuan_model->warga($id);

		$this->load->view('Warga/Surat_ajuan/biodata', $data);
	}

    public function tambah()
	{
        if(isset($_POST['submit']))
        {
            $this->form_validation->set_rules('name', 'Name', 'required');

            $config['upload_path'] = 'assets/upload/Surat_Biodata/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']  = '2048';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            //file 1
            if(!empty($_FILES['scan_kk']['name'])) {
                $this->upload->do_upload('scan_kk');
                $data1 = $this->upload->data();
                $file1 = $data1['file_name'];
            }
            //file 2
            if(!empty($_FILES['scan_ektp']['name'])) {
                $this->upload->do_upload('scan_ektp');
                $data2 = $this->upload->data();
                $file2 = $data2['file_name'];
            }
            if(!empty($_FILES['scan_pajak']['name'])) {
                $this->upload->do_upload('scan_pajak');
                $data3 = $this->upload->data();
                $file3 = $data3['file_name'];
            }

            $id_surat       = $this->input->post('id_surat');
            $id_user        = $this->input->post('id_user');
            $no_surat       = $this->input->post('no_surat');
            $id_status      = $this->input->post('id_status');
            $tgl_surat      = $this->input->post('tgl_surat');
            $hub_keluarga      = $this->input->post('hub_keluarga');
            $nik_ibu        = $this->input->post('nik_ibu');
            $nama_ibu   = $this->input->post('nama_ibu');
            $nik_ayah      = $this->input->post('nik_ayah');
            $nama_ayah           = $this->input->post('nama_ayah');
            $no_paspor        = $this->input->post('no_paspor');
            $tenggang_paspor       = $this->input->post('tenggang_paspor');
            $no_akta_kelahiran        = $this->input->post('no_akta_kelahiran');
            $no_akta_kawin     = $this->input->post('no_akta_kawin');
            $tgl_kawin      = $this->input->post('tgl_kawin');
            $no_akta_cerai      = $this->input->post('no_akta_cerai');
            $tgl_cerai      = $this->input->post('tgl_cerai');
            $alamat_sekarang      = $this->input->post('alamat_sekarang');
            $rt_sekarang      = $this->input->post('rt_sekarang');
            $rw_sekarang      = $this->input->post('rw_sekarang');
            $dusun_sekarang      = $this->input->post('dusun_sekarang');

            $data = array(
                'id_surat' 	        => $id_surat,
                'id_user' 	        => $id_user,
                'no_surat' 	        => $no_surat,
                'id_status' 	    => $id_status,
                'tgl_surat' 	    => $tgl_surat,
                'hub_keluarga' 	    => $hub_keluarga,
                'nik_ibu' 	        => $nik_ibu,
                'nama_ibu'          => $nama_ibu,
                'nik_ayah' 	        => $nik_ayah,
                'nama_ayah' 	    => $nama_ayah,
                'no_paspor' 	    => $no_paspor,
                'tenggang_paspor' 	=> $tenggang_paspor,
                'no_akta_kelahiran' => $no_akta_kelahiran,
                'no_akta_kawin' 	=> $no_akta_kawin,
                'tgl_kawin' 	    => $tgl_kawin,
                'no_akta_cerai'     => $no_akta_cerai,
                'tgl_cerai'         => $tgl_cerai,
                'alamat_sekarang'   => $alamat_sekarang,
                'rt_sekarang'       => $rt_sekarang,
                'rw_sekarang'       => $rw_sekarang,
                'dusun_sekarang'    => $dusun_sekarang,
                'scan_kk'           => $file1,
                'scan_ektp'         => $file2,
                'scan_pajak'         => $file3
            );
            $this->WargaSuratAjuan_model->input_surat($data,'surat_biodata');
            $this->session->set_flashdata('pesan', 'Diajukan');
            redirect("Warga/Jenis_Surat/Jenis");
        }
        else
        {
            echo "Gagal Insert";
        }
	}

}