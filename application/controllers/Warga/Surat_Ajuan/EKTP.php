<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EKTP extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('WargaSuratAjuan_model');
	}

    public function index()
	{
        //mengambil session id yang login
        $id = $this->session->userdata('ses_id');
        // memanggil model berdasarkan id user yang login
        $data['warga'] = $this->WargaSuratAjuan_model->warga($id);

		$this->load->view('Warga/Surat_ajuan/ektp', $data);
	}

    public function tambah()
	{
        if(isset($_POST['submit']))
        {
            $this->form_validation->set_rules('name', 'Name', 'required');

            $config['upload_path'] = 'assets/upload/Surat_KTP/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']  = '5000';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            //file 1
            if(!empty($_FILES['scan_kk']['name'])) {
                $this->upload->do_upload('scan_kk');
                $data1 = $this->upload->data();
                $file1 = $data1['file_name'];
            }
            //file 2
            if(!empty($_FILES['ektp_lama']['name'])) {
                $this->upload->do_upload('ektp_lama');
                $data2 = $this->upload->data();
                $file2 = $data2['file_name'];
            }//file 1
            if(!empty($_FILES['ijazah_akhir']['name'])) {
                $this->upload->do_upload('ijazah_akhir');
                $data3 = $this->upload->data();
                $file3 = $data3['file_name'];
            }
            //file 2
            if(!empty($_FILES['akta']['name'])) {
                $this->upload->do_upload('akta');
                $data4 = $this->upload->data();
                $file4 = $data4['file_name'];
            }//file 1
            if(!empty($_FILES['surat_nikah_cerai']['name'])) {
                $this->upload->do_upload('surat_nikah_cerai');
                $data5 = $this->upload->data();
                $file5 = $data5['file_name'];
            }
            //file 2
            if(!empty($_FILES['foto']['name'])) {
                $this->upload->do_upload('foto');
                $data6 = $this->upload->data();
                $file6 = $data6['file_name'];
            }
            if(!empty($_FILES['scan_pajak']['name'])) {
                $this->upload->do_upload('scan_pajak');
                $data7 = $this->upload->data();
                $file7 = $data7['file_name'];
            }

            $id_surat       = $this->input->post('id_surat');
            $id_user        = $this->input->post('id_user');
            $no_surat       = $this->input->post('no_surat');
            $id_status      = $this->input->post('id_status');
            $tgl_surat      = $this->input->post('tgl_surat');
            $tipe_permohonan      = $this->input->post('tipe_permohonan');

            $file = $this->upload->data();
			$gambar = $file['file_name'];

            $data = array(
                'id_surat' 	        => $id_surat,
                'id_user' 	        => $id_user,
                'no_surat' 	        => $no_surat,
                'id_status' 	    => $id_status,
                'tgl_surat' 	    => $tgl_surat,
                'tipe_permohonan' 	=> $tipe_permohonan,
                'scan_kk'           => $file1,
                'ektp_lama'           => $file2,
                'ijazah_akhir'           => $file3,
                'akta'           => $file4,
                'surat_nikah_cerai'           => $file5,
                'foto'           => $file6,
                'scan_pajak'           => $file7
            );

            $this->WargaSuratAjuan_model->input_surat($data,'surat_permohonan_ktp');
            $this->session->set_flashdata('pesan', 'Diajukan');
            redirect("index.php/Warga/Jenis_Surat/Jenis");
        }
        else
        {
            echo "Gagal Insert";
        }
	}

}