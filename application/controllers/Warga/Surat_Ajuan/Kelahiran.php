<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelahiran extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('WargaSuratAjuan_model');
	}

    public function index()
    {
        //mengambil session id yang login
        $id = $this->session->userdata('ses_id');
        // memanggil model berdasarkan id user yang login
        $data['warga'] = $this->WargaSuratAjuan_model->warga($id);

        $this->load->view('Warga/Surat_ajuan/kelahiran', $data);
    }

    public function tambah()
	{
        if(isset($_POST['submit']))
        {
            $this->form_validation->set_rules('name', 'Name', 'required');

            $config['upload_path'] = 'assets/upload/Surat_Kelahiran/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']  = '10000';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            //file 1
            if(!empty($_FILES['scan_kk']['name'])) {
                $this->upload->do_upload('scan_kk');
                $data1 = $this->upload->data();
                $file1 = $data1['file_name'];
            }
            //file 2
            if(!empty($_FILES['scan_ektp_ayah']['name'])) {
                $this->upload->do_upload('scan_ektp_ayah');
                $data2 = $this->upload->data();
                $file2 = $data2['file_name'];
            }
            if(!empty($_FILES['scan_ektp_ibu']['name'])) {
                $this->upload->do_upload('scan_ektp_ibu');
                $data3 = $this->upload->data();
                $file3 = $data3['file_name'];
            }
            //file 2
            if(!empty($_FILES['scan_ektp_saksi_a']['name'])) {
                $this->upload->do_upload('scan_ektp_saksi_a');
                $data4 = $this->upload->data();
                $file4 = $data4['file_name'];
            }

            if(!empty($_FILES['scan_ektp_saksi_b']['name'])) {
                $this->upload->do_upload('scan_ektp_saksi_b');
                $data5 = $this->upload->data();
                $file5 = $data5['file_name'];
            }
            //file 2
            if(!empty($_FILES['scan_buku_nikah']['name'])) {
                $this->upload->do_upload('scan_buku_nikah');
                $data6 = $this->upload->data();
                $file6 = $data6['file_name'];
            }
            //file 2
            if(!empty($_FILES['scan_surat_rs']['name'])) {
                $this->upload->do_upload('scan_surat_rs');
                $data7 = $this->upload->data();
                $file7 = $data7['file_name'];
            }
            if(!empty($_FILES['scan_pajak']['name'])) {
                $this->upload->do_upload('scan_pajak');
                $data8 = $this->upload->data();
                $file8 = $data8['file_name'];
            }

            $id_user            = $this->input->post('id_user');
            $no_surat           = $this->input->post('no_surat');
            $id_surat           = $this->input->post('id_surat');
            $tgl_surat          = $this->input->post('tgl_surat');
            $id_status          = $this->input->post('id_status');
            $kepala_keluarga    = $this->input->post('kepala_keluarga');
            $no_kk_kepala       = $this->input->post('no_kk_kepala');
            $nama_bayi          = $this->input->post('nama_bayi');
            $jenkel_bayi        = $this->input->post('jenkel_bayi');
            $tempat_dilahirkan  = $this->input->post('tempat_dilahirkan');
            $tempat_kelahiran   = $this->input->post('tempat_kelahiran');
            $jenis_kelahiran    = $this->input->post('jenis_kelahiran');
            $kelahiran_ke       = $this->input->post('kelahiran_ke');
            $hari               = $this->input->post('hari');
            $tgl_lahir_bayi     = $this->input->post('tgl_lahir_bayi');
            $pukul              = $this->input->post('pukul');
            $penolong           = $this->input->post('penolong');
            $berat_bayi         = $this->input->post('berat_bayi');
            $panjang_bayi       = $this->input->post('panjang_bayi');
            $nik_ibu            = $this->input->post('nik_ibu');
            $nama_ibu           = $this->input->post('nama_ibu');
            $tgl_lahir_ibu      = $this->input->post('tgl_lahir_ibu');
            $umur_ibu           = $this->input->post('umur_ibu');
            $pekerjaan_ibu      = $this->input->post('pekerjaan_ibu');
            $alamat_ibu         = $this->input->post('alamat_ibu');
            $kewarganegaraan_ibu  = $this->input->post('kewarganegaraan_ibu');
            $kebangsaan_ibu  = $this->input->post('kebangsaan_ibu');
            $tgl_kawin       = $this->input->post('tgl_kawin');
            $nik_ayah           = $this->input->post('nik_ayah');
            $nama_ayah          = $this->input->post('nama_ayah');
            $tgl_lahir_ayah     = $this->input->post('tgl_lahir_ayah');
            $umur_ayah          = $this->input->post('umur_ayah');
            $pekerjaan_ayah     = $this->input->post('pekerjaan_ayah');
            $alamat_ayah        = $this->input->post('alamat_ayah');
            $kewarganegaraan_ayah = $this->input->post('kewarganegaraan_ayah');
            $kebangsaan_ayah    = $this->input->post('kebangsaan_ayah');
            $nik_saksi_a        = $this->input->post('nik_saksi_a');
            $nama_saksi_a       = $this->input->post('nama_saksi_a');
            $umur_saksi_a       = $this->input->post('umur_saksi_a');
            $pekerjaan_saksi_a  = $this->input->post('pekerjaan_saksi_a');
            $alamat_saksi_a     = $this->input->post('alamat_saksi_a');;
            $nik_saksi_b        = $this->input->post('nik_saksi_b');
            $nama_saksi_b       = $this->input->post('nama_saksi_b');
            $umur_saksi_b       = $this->input->post('umur_saksi_b');
            $pekerjaan_saksi_b  = $this->input->post('pekerjaan_saksi_b');
            $alamat_saksi_b     = $this->input->post('alamat_saksi_b');

            $data = array(
                'id_surat' 	            => $id_surat,
                'id_user' 	            => $id_user,
                'no_surat' 	            => $no_surat,
                'id_status' 	        => $id_status,
                'tgl_surat' 	        => $tgl_surat,
                'kepala_keluarga' 	    => $kepala_keluarga,
                'no_kk_keluarga' 	    => $no_kk_kepala,
                'nama_bayi' 	        => $nama_bayi,
                'jenkel_bayi' 	        => $jenkel_bayi,
                'tempat_dilahirkan' 	=> $tempat_dilahirkan,
                'tempat_kelahiran' 	    => $tempat_kelahiran,
                'jenis_kelahiran' 	    => $jenis_kelahiran,
                'kelahiran_ke' 	        => $kelahiran_ke,
                'hari' 	                => $hari,
                'tanggal' 	            => $tgl_lahir_bayi,
                'pukul' 	            => $pukul,
                'penolong_kelahiran' 	=> $penolong,
                'berat_bayi' 	        => $berat_bayi,
                'panjang_bayi' 	        => $panjang_bayi,
                'nik_ibu' 	            => $nik_ibu,
                'nama_ibu' 	            => $nama_ibu,
                'tgl_lahir_ibu' 	    => $tgl_lahir_ibu,
                'umur_ibu' 	            => $umur_ibu,
                'pekerjaan_ibu' 	    => $pekerjaan_ibu,
                'alamat_ibu' 	        => $alamat_ibu,
                'kewarganegaraan_ibu' 	=> $kewarganegaraan_ibu,
                'kebangsaan_ibu'        => $kebangsaan_ibu,
                'tgl_kawin'             => $tgl_kawin,
                'nik_ayah' 	            => $nik_ayah,
                'nama_ayah' 	        => $nama_ayah,
                'tgl_lahir_ayah' 	    => $tgl_lahir_ayah,
                'umur_ayah' 	        => $umur_ayah,
                'pekerjaan_ayah' 	    => $pekerjaan_ayah,
                'alamat_ayah' 	        => $alamat_ayah,
                'kewarganegaraan_ayah'  => $kewarganegaraan_ayah,
                'kebangsaan_ayah'       => $kebangsaan_ayah,
                'nik_saksi_a' 	        => $nik_saksi_a,
                'nama_saksi_a' 	        => $nama_saksi_a,
                'umur_saksi_a' 	        => $umur_saksi_a,
                'pekerjaan_saksi_a' 	=> $pekerjaan_saksi_a,
                'alamat_saksi_a' 	    => $alamat_saksi_a,
                'nik_saksi_b' 	        => $nik_saksi_b,
                'nama_saksi_b' 	        => $nama_saksi_b,
                'umur_saksi_b' 	        => $umur_saksi_b,
                'pekerjaan_saksi_b'     => $pekerjaan_saksi_b,
                'alamat_saksi_b' 	    => $alamat_saksi_b,
                'scan_kk'               => $file1,
                'scan_ktp_ayah'         => $file2,
                'scan_ktp_ibu'          => $file3,
                'scan_ktp_saksi_a'      => $file4,
                'scan_ktp_saksi_b'      => $file5,
                'scan_buku_nikah'       => $file6,
                'surat_keterangan_rs'    => $file7,
                'scan_pajak'    => $file8
            );
            $this->WargaSuratAjuan_model->input_surat($data,'surat_kelahiran');
            $this->session->set_flashdata('pesan', 'Diajukan');
            redirect("Warga/Jenis_Surat/Jenis");
        }
        else
        {
            echo "Gagal Insert";
        }
	}
}