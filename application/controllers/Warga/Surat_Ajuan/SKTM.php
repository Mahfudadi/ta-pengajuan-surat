<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SKTM extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('WargaSuratAjuan_model');
	}

    public function index()
	{
        //mengambil session id yang login
        $id = $this->session->userdata('ses_id');
        // memanggil model berdasarkan id user yang login
        $data['warga'] = $this->WargaSuratAjuan_model->warga($id);

		$this->load->view('Warga/Surat_ajuan/sktm', $data);
	}

    public function tambah()
	{
        if(isset($_POST['submit']))
        {
            $this->form_validation->set_rules('name', 'Name', 'required');

            $config['upload_path'] = 'assets/upload/Surat_SKTM/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']  = '2048';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            //file 1
            if(!empty($_FILES['scan_kk']['name'])) {
                $this->upload->do_upload('scan_kk');
                $data1 = $this->upload->data();
                $file1 = $data1['file_name'];
            }
            //file 2
            if(!empty($_FILES['scan_ektp']['name'])) {
                $this->upload->do_upload('scan_ektp');
                $data2 = $this->upload->data();
                $file2 = $data2['file_name'];
            }
            //file 3
            if(!empty($_FILES['scan_pajak']['name'])) {
                $this->upload->do_upload('scan_pajak');
                $data3 = $this->upload->data();
                $file3 = $data3['file_name'];
            }

            $id_surat       = $this->input->post('id_surat');
            $id_user        = $this->input->post('id_user');
            $no_surat       = $this->input->post('no_surat');
            $id_status      = $this->input->post('id_status');
            $tgl_surat      = $this->input->post('tgl_surat');
            $nama_anak      = $this->input->post('nama_anak');
            $jenkel         = $this->input->post('jenkel_anak');
            $tempat_lahir   = $this->input->post('tempat_lahir_anak');
            $tgl_lahir      = $this->input->post('tgl_lahir_anak');
            $nik            = $this->input->post('nik_anak');
            $sekolah        = $this->input->post('sekolah_anak');
            $semester       = $this->input->post('semester_anak');
            $nimnisn        = $this->input->post('nim_anak');
            $keterangan     = $this->input->post('keterangan');
            $keperluan      = $this->input->post('keperluan');
            
            $data = array(
                'id_surat' 	        => $id_surat,
                'id_user' 	        => $id_user,
                'no_surat' 	        => $no_surat,
                'id_status' 	    => $id_status,
                'tgl_surat' 	    => $tgl_surat,
                'nama_anak' 	    => $nama_anak,
                'jenkel' 	        => $jenkel,
                'tempat_lahir_anak' => $tempat_lahir,
                'tgl_lahir_anak' 	=> $tgl_lahir,
                'nik_anak' 	        => $nik,
                'sekolah' 	        => $sekolah,
                'semester' 	        => $semester,
                'nim_nisn' 	        => $nimnisn,
                'keterangan' 	    => $keterangan,
                'keperluan' 	    => $keperluan,
                'scan_kk'           => $file1,
                'scan_ektp'           => $file2,
                'scan_pajak'           => $file3
            );

            $this->WargaSuratAjuan_model->input_surat($data,'surat_sktm');
            $this->session->set_flashdata('pesan', 'Diajukan');
            redirect("index.php/Warga/Jenis_Surat/Jenis");
        }
        else
        {
            echo "Gagal Insert";
        }
	}

}