<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SKCK extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('WargaSuratAjuan_model');
	}

    public function index()
    {
        //mengambil session id yang login
        $id = $this->session->userdata('ses_id');
        // memanggil model berdasarkan id user yang login
        $data['warga'] = $this->WargaSuratAjuan_model->warga($id);

        $this->load->view('Warga/Surat_ajuan/skck', $data);
    }

    public function tambah()
	{
        if(isset($_POST['submit']))
        {
            $this->form_validation->set_rules('name', 'Name', 'required');

            $config['upload_path'] = 'assets/upload/Surat_SKCK/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']  = '2048';
            $config['encrypt_name'] = TRUE;
            $this->load->library('upload', $config);

            //file 1
            if(!empty($_FILES['scan_kk']['name'])) {
                $this->upload->do_upload('scan_kk');
                $data1 = $this->upload->data();
                $file1 = $data1['file_name'];
            }
            //file 2
            if(!empty($_FILES['scan_ektp']['name'])) {
                $this->upload->do_upload('scan_ektp');
                $data2 = $this->upload->data();
                $file2 = $data2['file_name'];
            }
            if(!empty($_FILES['foto']['name'])) {
                $this->upload->do_upload('foto');
                $data3 = $this->upload->data();
                $file3 = $data3['file_name'];
            }
            if(!empty($_FILES['scan_pajak']['name'])) {
                $this->upload->do_upload('scan_pajak');
                $data4 = $this->upload->data();
                $file4 = $data4['file_name'];
            }

            $id_surat   = $this->input->post('id_surat');
            $id_user    = $this->input->post('id_user');
            $id_status  = $this->input->post('id_status');
            $no_surat   = $this->input->post('no_surat');
            $tgl_surat  = $this->input->post('tgl_surat');
            $keterangan = $this->input->post('keterangan');
            $kegunaan   = $this->input->post('kegunaan');
            $catatan    = $this->input->post('catatan');

            $data = array(
                'id_surat' 	    => $id_surat,
                'id_user'       => $id_user,
                'id_status' 	=> $id_status,
                'no_surat'      => $no_surat,
                'tgl_surat' 	=> $tgl_surat,
                'keterangan' 	=> $keterangan,
                'kegunaan'      => $kegunaan,
                'catatan'       => $catatan,
                'scan_kk'       => $file1,
                'scan_ektp'     => $file2,
                'foto'          => $file3,
                'scan_pajak'          => $file4
            );
            $this->WargaSuratAjuan_model->input_surat($data,'surat_skck');
            $this->session->set_flashdata('pesan', 'Diajukan');
            redirect("index.php/Warga/Jenis_Surat/Jenis");
        }
        else
        {
            echo "Gagal Insert";
        }
	}
}