<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Monitoring_warga_model');
	}

	public function index()
	{
		$id = $this->session->userdata('ses_id');
        // Mengambil data dari model dan di jadikan variabel
        $data['surat'] = $this->Monitoring_warga_model->tampil($id);
		$this->load->view('Warga/Monitor/monitor',$data);
	}
}
