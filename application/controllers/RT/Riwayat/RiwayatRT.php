<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RiwayatRT extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('RiwayatRT_model');
	}
	public function index($id)
	{
        //Mengambil data dari model dan di jadikan variabel
        $data['riwayat'] = $this->RiwayatRT_model->tampil($id);
		$data['user'] = $this->db->get_where('tb_rt', ['id_user' => $this->session->userdata('ses_id')])->row_array();
        //Menampilkan view dan juga data yang diambil dari model
		$this->load->view('RT/Riwayat/riwayat',$data);
    }
    

}