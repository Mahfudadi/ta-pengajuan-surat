<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BerandaRT extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('KetuaRT_model');
	}

	public function index()
	{
		
		$data['disetujui'] = $this->KetuaRT_model->total_disetujui();
		$data['belum'] = $this->KetuaRT_model->total_belum();

		$data['user'] = $this->db->get_where('tb_rt', ['id_user' => $this->session->userdata('ses_id')])->row_array();
		$data['list_user'] = $this->db->get('tb_rt')->result_array();

		$this->load->view('RT/beranda',$data);
	}

	
    public function update_profil()
    {
        $id['id_user'] = $this->session->userdata('ses_id');
        $data = array(
            'nik'      		=> $this->input->post('nik'),
            'nama_rt'    => $this->input->post('nama_rt'),
            'alamat_rt'    => $this->input->post('alamat')
        ); 

        $this->KetuaRT_model->update_rt($data, $id);
        $this->session->set_flashdata('pesan', 'Diubah');
        redirect('RT/BerandaRT');
    }
	

	public function update_password()
	{
		$current_password = $this->input->post('current_password');
		$new_password1 = $this->input->post('new_password1');
		$new_password2 = $this->input->post('new_password2');

		if ($current_password == $new_password1) {
			$this->session->set_flashdata('msg', '<div class="alert alert-danger font-weight-bolder text-center" role="alert">Ubah Password Gagal !! <br> Password baru tidak boleh sama dengan password lama </div>');
			redirect('RT/BerandaRT');
		} elseif($new_password1 != $new_password2){
			$this->session->set_flashdata('msg', '<div class="alert alert-danger font-weight-bolder text-center" role="alert">Ubah Password Gagal !! <br> Password yang anda masukkan tidak sama</div>');
			redirect('RT/BerandaRT');
		} else {
			$this->db->set('password', $new_password1);
			$this->db->where('id_user', $this->session->userdata('ses_id'));
			$this->db->update('tb_user');
			$this->session->set_flashdata('msg', '<div class="alert alert-success font-weight-bolder text-center" role="alert">Ubah Password Sukses !</div>');
			redirect('RT/BerandaRT');
		}
	}
	
}
