<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_rt extends CI_Controller {

	function __construct(){
		parent::__construct();
        $this->load->model('KetuaRt_model');
	}

	public function index($id)
    {
        $data['user'] = $this->db->get_where('tb_rt', ['id_user' => $this->session->userdata('ses_id')])->row_array();
        $data['profil'] = $this->KetuaRt_model->profil($id);
        //Menampilkan view dan juga data yang diambil dari model
        $this->load->view('RT/Profil/profil_rt',$data);
    }
    public function edit_profil($id)
    {
        $data['editprofil'] = $this->Admin_model->edit_rt($id);
        $this->load->view("RT/BerandaRT",$data);
    }
    public function update_profil()
    {
        $id['id_user'] = $this->input->post('id_user');
        $data = array(
            'nik'      => $this->input->post('nik'),
            'nama_rt'      => $this->input->post('nama_rt'),
            'alamat_rt'      => $this->input->post('alamat_rt'),
        ); 
        $this->session->set_flashdata('message', 'Simpan Perubahan');
        $this->KetuaRt_model->update_profil($data, $id);
        redirect('RT/Profil/Data_rt');
    }
}