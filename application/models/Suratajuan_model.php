<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Suratajuan_model extends CI_Model {
    //surat sktm
    public function tampil_sktm()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_sktm k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status = "2" or k.id_status = "3" or k.id_status = "4"');
        $this->db->order_by('tgl_surat','ASC');
        return $this->db->get()->result();
    }
    public function detail_sktm($id)
    {
        $this->db->select('w.id_user,nama_lengkap,jenis_kelamin,tempat_lahir,tanggal_lahir,warga_negara,agama,status_kawin,pekerjaan,nik,alamat,rt,rw,dusun,desa,kecamatan,kab_kota,s.id_surat,no_surat,tgl_surat,nama_anak,jenkel,tempat_lahir_anak,tgl_lahir_anak,nik_anak,sekolah,semester,nim_nisn,keterangan,keperluan,scan_kk,scan_ektp,t.id_status,status_monitoring, scan_pajak');
        $this->db->from('surat_sktm s');
        $this->db->join('tb_warga w','s.id_user = w.id_user');
        $this->db->join('tb_status t','s.id_status = t.id_status');
        $this->db->where('s.id_surat',$id);
        return $this->db->get()->result();
    }
    public function edit_sktm($id)
    {
        $this->db->select('*,no_surat');
        $this->db->from('surat_sktm');
        $this->db->join('tb_warga w','surat_sktm.id_user = w.id_user');
        $this->db->join('tb_status t','surat_sktm.id_status = t.id_status');
        $this->db->where('id_surat',$id);
        return $this->db->get()->result();
    }

    public function update_sktm($data, $id)
    {
        $this->db->update("surat_sktm", $data, $id);
    }
    public function cari_bulan($bulan)//query search
    { 
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_sktm k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND k.id_status IN ('2','3','4')");
        return $this->db->get()->result();
    }
    public function cari_status($status)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_sktm k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function cari_sktm($status, $bulan)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_sktm k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    //surat kelahiran
    public function tampil_kelahiran()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_kelahiran k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status = "2" or k.id_status = "3" or k.id_status = "4"');
        $this->db->order_by('tgl_surat','ASC');
        return $this->db->get()->result();
    }
    public function detail_kelahiran($id)
    {
        $this->db->select('w.id_user,nik,nama_lengkap,umur,pekerjaan,jenis_kelamin,alamat,s.id_surat,no_surat,tgl_surat,kepala_keluarga,no_kk_keluarga,nama_bayi,jenkel_bayi,tempat_dilahirkan,tempat_kelahiran,hari,tanggal,pukul,jenis_kelahiran,kelahiran_ke,penolong_kelahiran,berat_bayi,panjang_bayi,nik_ibu,nama_ibu,tgl_lahir_ibu,umur_ibu,pekerjaan_ibu,alamat_ibu,kewarganegaraan_ibu,kebangsaan_ibu,tgl_kawin,nik_ayah,nama_ayah,tgl_lahir_ayah,umur_ayah,pekerjaan_ayah,alamat_ayah,kewarganegaraan_ayah,kebangsaan_ayah,nik_saksi_a,nama_saksi_a,umur_saksi_a,pekerjaan_saksi_a,alamat_saksi_a,nik_saksi_b,nama_saksi_b,umur_saksi_b,pekerjaan_saksi_b,alamat_saksi_b,scan_kk,scan_ktp_ibu,scan_ktp_ayah,scan_ktp_saksi_a,scan_ktp_saksi_b, scan_pajak,scan_buku_nikah,surat_keterangan_rs,t.id_status,status_monitoring');
        $this->db->from('surat_kelahiran s');
        $this->db->join('tb_warga w','s.id_user = w.id_user');
        $this->db->join('tb_status t','s.id_status = t.id_status');
        $this->db->where('s.id_surat',$id);
        $this->db->limit(1);
        return $this->db->get()->result();
    }
    public function edit_kelahiran($id)
    {
        $this->db->select('*,no_surat');
        $this->db->from('surat_kelahiran');
        $this->db->join('tb_warga w','surat_kelahiran.id_user = w.id_user');
        $this->db->join('tb_status t','surat_kelahiran.id_status = t.id_status');
        $this->db->where('id_surat',$id);
        return $this->db->get()->result();
    }
    public function update_kelahiran($data, $id)
    {
        $this->db->update("surat_kelahiran", $data, $id);
    }
    public function cari_bulan_lahir($bulan)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_kelahiran k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND k.id_status IN ('2','3','4')");
        return $this->db->get()->result();
    }
    public function cari_status_lahir($status)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_kelahiran k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function cari_lahir($bulan, $status)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_kelahiran k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    //surat domisili
    public function tampil_domisili()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_domisili k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status = "2" or k.id_status = "3" or k.id_status = "4"');
        $this->db->order_by('tgl_surat','ASC');
        return $this->db->get()->result();
    }
    public function detail_domisili($id)
    {
        $this->db->select('w.id_user,nama_lengkap,jenis_kelamin,tempat_lahir,tanggal_lahir,warga_negara,agama,status_kawin,pekerjaan,pendidikan,nik,alamat,rt,rw,dusun,s.id_surat,no_surat,tgl_surat,keterangan,scan_kk,scan_ektp, scan_pajak,t.id_status,status_monitoring');
        $this->db->from('surat_domisili s');
        $this->db->join('tb_warga w','s.id_user = w.id_user');
        $this->db->join('tb_status t','s.id_status = t.id_status');
        $this->db->where('s.id_surat',$id);
        $this->db->limit(1);
        return $this->db->get()->result();
    }
    public function edit_domisili($id)
    {
        $this->db->select('*,no_surat');
        $this->db->from('surat_domisili');
        $this->db->join('tb_warga w','surat_domisili.id_user = w.id_user');
        $this->db->join('tb_status t','surat_domisili.id_status = t.id_status');
        $this->db->where('id_surat',$id);
        return $this->db->get()->result();
    }
    public function update_domisili($data, $id)
    {
        $this->db->update("surat_domisili", $data, $id);
    }

    public function cari_bulan_domisili($bulan)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_domisili k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND k.id_status IN ('2','3','4')");
        return $this->db->get()->result();
    }
    public function cari_status_domisili($status)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_domisili k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function cari_domisili($status, $bulan)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_domisili k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    //surat kip
    public function tampil_kip()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_kip k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status = "2" or k.id_status = "3" or k.id_status = "4"');
        $this->db->order_by('tgl_surat','ASC');
        return $this->db->get()->result();
    }
    public function detail_kip($id)
    {
        $this->db->select('w.id_user,nama_lengkap,jenis_kelamin,tempat_lahir,tanggal_lahir,warga_negara,agama,status_kawin,pekerjaan,nik,alamat,rt,rw,dusun,desa,kecamatan,kab_kota,s.id_surat,no_surat,tgl_surat,nama_anak,jenkel,tempat_lahir_anak,tgl_lahir_anak,nik_anak,sekolah,semester,nim_nisn,keterangan,keperluan,scan_kk,scan_ektp, scan_sktm, scan_pajak,t.id_status,status_monitoring');
        $this->db->from('surat_kip s');
        $this->db->join('tb_warga w','s.id_user = w.id_user');
        $this->db->join('tb_status t','s.id_status = t.id_status');
        $this->db->where('s.id_surat',$id);
        $this->db->limit(1);
        return $this->db->get()->result();
    }
    public function edit_kip($id)
    {
        $this->db->select('*,no_surat');
        $this->db->from('surat_kip');
        $this->db->join('tb_warga w','surat_kip.id_user = w.id_user');
        $this->db->join('tb_status t','surat_kip.id_status = t.id_status');
        $this->db->where('id_surat',$id);
        return $this->db->get()->result();
    }
    public function update_kip($data, $id)
    {
        $this->db->update("surat_kip", $data, $id);
    }

    public function cari_bulan_kip($bulan)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_kip k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND k.id_status IN ('2','3','4')");
        return $this->db->get()->result();
    }
    public function cari_status_kip($status)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_kip k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function cari_kip($status, $bulan)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_kip k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    //surat kk
    public function tampil_kk()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_perubahan_kk k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status = "2" or k.id_status = "3" or k.id_status = "4"');
        $this->db->order_by('tgl_surat','ASC');
        return $this->db->get()->result();
    }
    public function detail_kk($id)
    {
        $this->db->select('w.id_user,nama_lengkap,nik,no_kk,alamat,rt,rw,dusun,s.id_surat,no_surat,tgl_surat,kepala_keluarga,alasan,jumlah_anggota,nik_anggota_a,nik_anggota_b,nama_anggota_a,nama_anggota_b,scan_kk,scan_ektp, scan_pajak,ijazah_terakhir,surat_nikah_cerai,t.id_status,status_monitoring');
        $this->db->from('surat_perubahan_kk s');
        $this->db->join('tb_warga w','s.id_user = w.id_user');
        $this->db->join('tb_status t','s.id_status = t.id_status');
        $this->db->where('s.id_surat',$id);
        $this->db->limit(1);
        return $this->db->get()->result();
    }
    public function edit_kk($id)
    {
        $this->db->select('*,no_surat');
        $this->db->from('surat_perubahan_kk');
        $this->db->join('tb_warga w','surat_perubahan_kk.id_user = w.id_user');
        $this->db->join('tb_status t','surat_perubahan_kk.id_status = t.id_status');
        $this->db->where('id_surat',$id);
        return $this->db->get()->result();
    }
    public function update_kk($data, $id)
    {
        $this->db->update("surat_perubahan_kk", $data, $id);
    }
    public function cari_bulan_kk($bulan)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_perubahan_kk k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND k.id_status IN ('2','3','4')");
        return $this->db->get()->result();
    }
    public function cari_status_kk($status)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_perubahan_kk k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function cari_kk($status, $bulan)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_perubahan_kk k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    //surat ktp
    public function tampil_ktp()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_permohonan_ktp k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status = "2" or k.id_status = "3" or k.id_status = "4"');
        $this->db->order_by('tgl_surat','ASC');
        return $this->db->get()->result();
    }
    public function detail_ktp($id)
    {
        $this->db->select('w.id_user,nama_lengkap,nik,no_kk,alamat,rt,rw,s.id_surat,no_surat,tgl_surat,tipe_permohonan,scan_kk,ektp_lama,ijazah_akhir,akta,surat_nikah_cerai,foto, scan_pajak,t.id_status,status_monitoring');
        $this->db->from('surat_permohonan_ktp s');
        $this->db->join('tb_warga w','s.id_user = w.id_user');
        $this->db->join('tb_status t','s.id_status = t.id_status');
        $this->db->where('s.id_surat',$id);
        $this->db->limit(1);
        return $this->db->get()->result();
    }
    public function edit_ktp($id)
    {
        $this->db->select('*,no_surat');
        $this->db->from('surat_permohonan_ktp');
        $this->db->join('tb_warga w','surat_permohonan_ktp.id_user = w.id_user');
        $this->db->join('tb_status t','surat_permohonan_ktp.id_status = t.id_status');
        $this->db->where('id_surat',$id);
        return $this->db->get()->result();
    }
    public function update_ktp($data, $id)
    {
        $this->db->update("surat_permohonan_ktp", $data, $id);
    }

    public function cari_bulan_ktp($bulan)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_permohonan_ktp k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND k.id_status IN ('2','3','4')");
        return $this->db->get()->result();
    }
    public function cari_status_ktp($status)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_permohonan_ktp k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function cari_ktp($status, $bulan)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_permohonan_ktp k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    //surat sptjm
    public function tampil_sptjm()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_sptjm k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status = "2" or k.id_status = "3" or k.id_status = "4"');
        $this->db->order_by('tgl_surat','ASC');
        return $this->db->get()->result();
    }
    public function detail_sptjm($id)
    {
        $this->db->select('w.id_user,nama_lengkap,nik,tempat_lahir,tanggal_lahir,pekerjaan,alamat,s.id_surat,no_surat,tgl_surat,nama_a,nik_a,tempat_lahir_a,tgl_lahir_a,pekerjaan_a,alamat_a,nama_b,nik_b,tempat_lahir_b,tgl_lahir_b,pekerjaan_b,alamat_b,scan_kk,scan_ektp, scan_pajak,t.id_status,status_monitoring');
        $this->db->from('surat_sptjm s');
        $this->db->join('tb_warga w','s.id_user = w.id_user');
        $this->db->join('tb_status t','s.id_status = t.id_status');
        $this->db->where('s.id_surat',$id);
        $this->db->limit(1);
        return $this->db->get()->result();
    }
    public function edit_sptjm($id)
    {
        $this->db->select('*,no_surat');
        $this->db->from('surat_sptjm');
        $this->db->join('tb_warga w','surat_sptjm.id_user = w.id_user');
        $this->db->join('tb_status t','surat_sptjm.id_status = t.id_status');
        $this->db->where('id_surat',$id);
        return $this->db->get()->result();
    }
    public function update_sptjm($data, $id)
    {
        $this->db->update("surat_sptjm", $data, $id);
    }

    public function cari_bulan_sptjm($bulan)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_sptjm k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND k.id_status IN ('2','3','4')");
        return $this->db->get()->result();
    }
    public function cari_status_sptjm($status)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_sptjm k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function cari_sptjm($status, $bulan)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_sptjm k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    //surat kuasa
    public function tampil_kuasa()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_kuasa k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status = "2" or k.id_status = "3" or k.id_status = "4"');
        $this->db->order_by('tgl_surat','ASC');
        return $this->db->get()->result();
    }
    public function detail_kuasa($id)
    {
        $this->db->select('w.id_user,nama_lengkap,nik,tempat_lahir,tanggal_lahir,jenis_kelamin,status_kawin,pekerjaan,alamat,rt,rw,dusun,desa,kecamatan,kab_kota,s.id_surat,no_surat,tgl_surat,nama_pihak_kedua,nik_pihak_kedua,tempat_lahir_pihak_kedua,tgl_lahir_pihak_kedua,jenkel_pihak_kedua,status_kawin_pihak_kedua,pekerjaan_pihak_kedua,alamat_pihak_kedua,kuasa,scan_kk,scan_ektp,scan_kk_pihak_kedua,scan_ektp_pihak_kedua, scan_pajak,t.id_status,status_monitoring');
        $this->db->from('surat_kuasa s');
        $this->db->join('tb_warga w','s.id_user = w.id_user');
        $this->db->join('tb_status t','s.id_status = t.id_status');
        $this->db->where('s.id_surat',$id);
        $this->db->limit(1);
        return $this->db->get()->result();
    }
    public function edit_kuasa($id)
    {
        $this->db->select('*,no_surat');
        $this->db->from('surat_kuasa');
        $this->db->join('tb_warga w','surat_kuasa.id_user = w.id_user');
        $this->db->join('tb_status t','surat_kuasa.id_status = t.id_status');
        $this->db->where('id_surat',$id);
        return $this->db->get()->result();
    }
    public function update_kuasa($data, $id)
    {
        $this->db->update("surat_kuasa", $data, $id);
    }

    public function cari_bulan_kuasa($bulan)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_kuasa k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND k.id_status IN ('2','3','4')");
        return $this->db->get()->result();
    }
    public function cari_status_kuasa($status)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_kuasa k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function cari_kuasa($status, $bulan)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_kuasa k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    //surat ijin keramaian
    public function tampil_keramaian()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_ijin_keramaian k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status = "2" or k.id_status = "3" or k.id_status = "4"');
        $this->db->order_by('tgl_surat','ASC');
        return $this->db->get()->result();
    }
    public function detail_keramaian($id)
    {
        $this->db->select('w.id_user,nama_lengkap,tempat_lahir,tanggal_lahir,umur,pekerjaan,nik,alamat,rt,rw,dusun,desa,kecamatan,kab_kota,s.id_surat,no_surat,tgl_surat,jenis_kegiatan,penanggungjawab,scan_kk,scan_ektp, scan_pajak,t.id_status,status_monitoring');
        $this->db->from('surat_ijin_keramaian s');
        $this->db->join('tb_warga w','s.id_user = w.id_user');
        $this->db->join('tb_status t','s.id_status = t.id_status');
        $this->db->where('s.id_surat',$id);
        $this->db->limit(1);
        return $this->db->get()->result();
    }
    public function edit_keramaian($id)
    {
        $this->db->select('*,no_surat');
        $this->db->from('surat_ijin_keramaian');
        $this->db->join('tb_warga w','surat_ijin_keramaian.id_user = w.id_user');
        $this->db->join('tb_status t','surat_ijin_keramaian.id_status = t.id_status');
        $this->db->where('id_surat',$id);
        return $this->db->get()->result();
    }
    public function update_keramaian($data, $id)
    {
        $this->db->update("surat_ijin_keramaian", $data, $id);
    }
    public function cari_bulan_ramai($bulan)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_ijin_keramaian k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND k.id_status IN ('2','3','4')");
        return $this->db->get()->result();
    }
    public function cari_status_ramai($status)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_ijin_keramaian k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function cari_ramai($status, $bulan)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_ijin_keramaian k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    //surat kematian
    public function tampil_kematian()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_kematian k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status = "2" or k.id_status = "3" or k.id_status = "4"');
        $this->db->order_by('tgl_surat','ASC');
        return $this->db->get()->result();
    }
    public function detail_kematian($id)
    {
        $this->db->select('w.id_user,nik,nama_lengkap,tanggal_lahir,umur,pekerjaan,alamat,rt,rw,dusun,s.id_surat,no_surat,tgl_surat,nama_kepala_keluarga,no_kk_kepala_keluarga,nik_jenazah,nama_jenazah,jenkel_jenazah,tempat_lahir_jenazah,tgl_lahir_jenazah,umur_jenazah,agama_jenazah,pekerjaan_jenazah,alamat_jenazah,kewarganegaraan_jenazah,anak_ke,tgl_kematian,pukul,sebab_kematian,tempat_kematian,yang_menerangkan,nik_ibu,nama_ibu,tgl_lahir_ibu,umur_ibu,pekerjaan_ibu,alamat_ibu,nik_ayah,nama_ayah,tgl_lahir_ayah,umur_ayah,pekerjaan_ayah,alamat_ayah,nik_saksi_a,nama_saksi_a,tgl_lahir_saksi_a,umur_saksi_a,pekerjaan_saksi_a,alamat_saksi_a,nik_saksi_b,nama_saksi_b,tgl_lahir_saksi_b,umur_saksi_b,pekerjaan_saksi_b,alamat_saksi_b,scan_kk, scan_pajak,scan_ektp_meninggal,scan_ektp_saksi_a,scan_ektp_saksi_b,suket_meninggal,t.id_status,status_monitoring');
        $this->db->from('surat_kematian s');
        $this->db->join('tb_warga w','s.id_user = w.id_user');
        $this->db->join('tb_status t','s.id_status = t.id_status');
        $this->db->where('s.id_surat',$id);
        $this->db->limit(1);
        return $this->db->get()->result();
    }
    public function edit_kematian($id)
    {
        $this->db->select('*,no_surat');
        $this->db->from('surat_kematian');
        $this->db->join('tb_warga w','surat_kematian.id_user = w.id_user');
        $this->db->join('tb_status t','surat_kematian.id_status = t.id_status');
        $this->db->where('id_surat',$id);
        return $this->db->get()->result();
    }
    public function update_kematian($data, $id)
    {
        $this->db->update("surat_kematian", $data, $id);
    }
    public function cari_bulan_kematian($bulan)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_kematian k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND k.id_status IN ('2','3','4')");
        return $this->db->get()->result();
    }
    public function cari_status_kematian($status)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_kematian k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status',$status);
        $this->db->where('k.id_status = "2" or k.id_status = "3" or k.id_status = "4"');
        return $this->db->get()->result();
    }
    //surat biodata
    public function tampil_biodata()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_biodata k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status = "2" or k.id_status = "3" or k.id_status = "4"');
        $this->db->order_by('tgl_surat','ASC');
        return $this->db->get()->result();
    }
    public function detail_biodata($id)
    {
        $this->db->select('w.id_user,nama_lengkap,nik,no_kk,tempat_lahir,tanggal_lahir,jenis_kelamin,agama,pekerjaan,pendidikan,gol_darah,penyandang_cacat,status_kawin,alamat,rt,rw,dusun,desa,kecamatan,kab_kota,s.id_surat,no_surat,tgl_surat,hub_keluarga,nik_ibu,nama_ibu,nik_ayah,nama_ayah,no_paspor,tenggang_paspor,no_akta_kelahiran,no_akta_kawin,tgl_kawin,no_akta_cerai,tgl_cerai,alamat_sekarang,rt_sekarang,rw_sekarang,dusun_sekarang,scan_kk,scan_ektp, scan_pajak,t.id_status,status_monitoring');
        $this->db->from('surat_biodata s');
        $this->db->join('tb_warga w','s.id_user = w.id_user');
        $this->db->join('tb_status t','s.id_status = t.id_status');
        $this->db->where('s.id_surat',$id);
        $this->db->limit(1);
        return $this->db->get()->result();
    }
    public function edit_biodata($id)
    {
        $this->db->select('*,no_surat');
        $this->db->from('surat_biodata');
        $this->db->join('tb_warga w','surat_biodata.id_user = w.id_user');
        $this->db->join('tb_status t','surat_biodata.id_status = t.id_status');
        $this->db->where('id_surat',$id);
        return $this->db->get()->result();
    }
    public function update_biodata($data, $id)
    {
        $this->db->update("surat_biodata", $data, $id);
    }

    public function cari_bulan_bio($bulan)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_biodata k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND k.id_status IN ('2','3','4')");
        return $this->db->get()->result();
    }
    public function cari_status_bio($status)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_biodata k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function cari_bio($status, $bulan)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_biodata k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    //surat SKCK
    public function tampil_skck()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_skck k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status = "2" or k.id_status = "3" or k.id_status = "4"');
        $this->db->order_by('tgl_surat','ASC');
        return $this->db->get()->result();
    }
    public function detail_skck($id)
    {
        $this->db->select('w.id_user,nama_lengkap,jenis_kelamin,tempat_lahir,tanggal_lahir,agama,status_kawin,pekerjaan,pendidikan,nik,alamat,rt,rw,dusun,desa,kecamatan,kab_kota,s.id_surat,no_surat,tgl_surat,keterangan,kegunaan,catatan,scan_kk,scan_ektp,foto, scan_pajak,t.id_status,status_monitoring');
        $this->db->from('surat_skck s');
        $this->db->join('tb_warga w','s.id_user = w.id_user');
        $this->db->join('tb_status t','s.id_status = t.id_status');
        $this->db->where('s.id_surat',$id);
        $this->db->limit(1);
        return $this->db->get()->result();
    }
    public function edit_skck($id)
    {
        $this->db->select('*,no_surat');
        $this->db->from('surat_skck');
        $this->db->join('tb_warga w','surat_skck.id_user = w.id_user');
        $this->db->join('tb_status t','surat_skck.id_status = t.id_status');
        $this->db->where('id_surat',$id);
        return $this->db->get()->result();
    }
    public function update_skck($data, $id)
    {
        $this->db->update("surat_skck", $data, $id);
    }

    public function insert_riwayat($data,$table){
        $this->db->insert($table,$data);
    }

    public function cari_bulan_skck($bulan)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_skck k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND k.id_status IN ('2','3','4')");
        return $this->db->get()->result();
    }
    public function cari_status_skck($status)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_skck k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function cari_skck($status, $bulan)//query search
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap, w.rt');
        $this->db->from('surat_skck k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    //Data kosong
    public function tampil_semua()
    {
        $query = $this->db->query("SELECT id_surat, no_surat, id_user, nama_lengkap, tgl_surat, id_status, status_monitoring FROM (
            SELECT id_surat, no_surat, surat_biodata.id_user, nama_lengkap, tgl_surat, surat_biodata.id_status, status_monitoring FROM surat_biodata JOIN tb_warga ON surat_biodata.id_user = tb_warga.id_user JOIN tb_status ON surat_biodata.id_status = tb_status.id_status WHERE surat_biodata.id_status IN ('2','3','4') UNION ALL
            SELECT id_surat, no_surat, surat_domisili.id_user, nama_lengkap, tgl_surat, surat_domisili.id_status, status_monitoring FROM surat_domisili JOIN tb_warga ON surat_domisili.id_user = tb_warga.id_user JOIN tb_status ON surat_domisili.id_status = tb_status.id_status WHERE surat_domisili.id_status IN ('2','3','4') UNION ALL
            SELECT id_surat, no_surat, surat_ijin_keramaian.id_user, nama_lengkap, tgl_surat, surat_ijin_keramaian.id_status, status_monitoring FROM surat_ijin_keramaian JOIN tb_warga ON surat_ijin_keramaian.id_user = tb_warga.id_user JOIN tb_status ON surat_ijin_keramaian.id_status = tb_status.id_status WHERE surat_ijin_keramaian.id_status IN ('2','3','4') UNION ALL
            SELECT id_surat, no_surat, surat_kelahiran.id_user, nama_lengkap, tgl_surat, surat_kelahiran.id_status, status_monitoring FROM surat_kelahiran JOIN tb_warga ON surat_kelahiran.id_user = tb_warga.id_user JOIN tb_status ON surat_kelahiran.id_status = tb_status.id_status WHERE surat_kelahiran.id_status IN ('2','3','4') UNION ALL
            SELECT id_surat, no_surat, surat_kematian.id_user, nama_lengkap, tgl_surat, surat_kematian.id_status, status_monitoring FROM surat_kematian JOIN tb_warga ON surat_kematian.id_user = tb_warga.id_user JOIN tb_status ON surat_kematian.id_status = tb_status.id_status WHERE surat_kematian.id_status IN ('2','3','4') UNION ALL
            SELECT id_surat, no_surat, surat_kip.id_user, nama_lengkap, tgl_surat, surat_kip.id_status, status_monitoring FROM surat_kip JOIN tb_warga ON surat_kip.id_user = tb_warga.id_user JOIN tb_status ON surat_kip.id_status = tb_status.id_status WHERE surat_kip.id_status IN ('2','3','4') UNION ALL
            SELECT id_surat, no_surat, surat_kuasa.id_user, nama_lengkap, tgl_surat, surat_kuasa.id_status, status_monitoring FROM surat_kuasa JOIN tb_warga ON surat_kuasa.id_user = tb_warga.id_user JOIN tb_status ON surat_kuasa.id_status = tb_status.id_status WHERE surat_kuasa.id_status IN ('2','3','4') UNION ALL
            SELECT id_surat, no_surat, surat_permohonan_ktp.id_user, nama_lengkap, tgl_surat, surat_permohonan_ktp.id_status, status_monitoring FROM surat_permohonan_ktp JOIN tb_warga ON surat_permohonan_ktp.id_user = tb_warga.id_user JOIN tb_status ON surat_permohonan_ktp.id_status = tb_status.id_status WHERE surat_permohonan_ktp.id_status IN ('2','3','4') UNION ALL
            SELECT id_surat, no_surat, surat_perubahan_kk.id_user, nama_lengkap, tgl_surat, surat_perubahan_kk.id_status, status_monitoring FROM surat_perubahan_kk JOIN tb_warga ON surat_perubahan_kk.id_user = tb_warga.id_user JOIN tb_status ON surat_perubahan_kk.id_status = tb_status.id_status WHERE surat_perubahan_kk.id_status IN ('2','3','4') UNION ALL
            SELECT id_surat, no_surat, surat_skck.id_user, nama_lengkap, tgl_surat, surat_skck.id_status, status_monitoring FROM surat_skck JOIN tb_warga ON surat_skck.id_user = tb_warga.id_user JOIN tb_status ON surat_skck.id_status = tb_status.id_status WHERE surat_skck.id_status IN ('2','3','4') UNION ALL
            SELECT id_surat, no_surat, surat_sktm.id_user, nama_lengkap, tgl_surat, surat_sktm.id_status, status_monitoring FROM surat_sktm JOIN tb_warga ON surat_sktm.id_user = tb_warga.id_user JOIN tb_status ON surat_sktm.id_status = tb_status.id_status WHERE surat_sktm.id_status IN ('2','3','4') UNION ALL
            SELECT id_surat, no_surat, surat_sptjm.id_user, nama_lengkap, tgl_surat, surat_sptjm.id_status, status_monitoring FROM surat_sptjm JOIN tb_warga ON surat_sptjm.id_user = tb_warga.id_user JOIN tb_status ON surat_sptjm.id_status = tb_status.id_status WHERE surat_sptjm.id_status IN ('2','3','4')
        )X  ORDER BY tgl_surat ASC");
        return $query->result();
    }
    public function tampil_semua_status($status)
    {
        $query = $this->db->query("SELECT id_surat, no_surat, id_user, nama_lengkap, tgl_surat, id_status, status_monitoring FROM (
            SELECT id_surat, no_surat, surat_biodata.id_user, nama_lengkap, tgl_surat, surat_biodata.id_status, status_monitoring FROM surat_biodata JOIN tb_warga ON surat_biodata.id_user = tb_warga.id_user JOIN tb_status ON surat_biodata.id_status = tb_status.id_status WHERE surat_biodata.id_status = '$status' UNION ALL
            SELECT id_surat, no_surat, surat_domisili.id_user, nama_lengkap, tgl_surat, surat_domisili.id_status, status_monitoring FROM surat_domisili JOIN tb_warga ON surat_domisili.id_user = tb_warga.id_user JOIN tb_status ON surat_domisili.id_status = tb_status.id_status WHERE surat_domisili.id_status = '$status' UNION ALL
            SELECT id_surat, no_surat, surat_ijin_keramaian.id_user, nama_lengkap, tgl_surat, surat_ijin_keramaian.id_status, status_monitoring FROM surat_ijin_keramaian JOIN tb_warga ON surat_ijin_keramaian.id_user = tb_warga.id_user JOIN tb_status ON surat_ijin_keramaian.id_status = tb_status.id_status WHERE surat_ijin_keramaian.id_status = '$status' UNION ALL
            SELECT id_surat, no_surat, surat_kelahiran.id_user, nama_lengkap, tgl_surat, surat_kelahiran.id_status, status_monitoring FROM surat_kelahiran JOIN tb_warga ON surat_kelahiran.id_user = tb_warga.id_user JOIN tb_status ON surat_kelahiran.id_status = tb_status.id_status WHERE surat_kelahiran.id_status = '$status' UNION ALL
            SELECT id_surat, no_surat, surat_kematian.id_user, nama_lengkap, tgl_surat, surat_kematian.id_status, status_monitoring FROM surat_kematian JOIN tb_warga ON surat_kematian.id_user = tb_warga.id_user JOIN tb_status ON surat_kematian.id_status = tb_status.id_status WHERE surat_kematian.id_status = '$status' UNION ALL
            SELECT id_surat, no_surat, surat_kip.id_user, nama_lengkap, tgl_surat, surat_kip.id_status, status_monitoring FROM surat_kip JOIN tb_warga ON surat_kip.id_user = tb_warga.id_user JOIN tb_status ON surat_kip.id_status = tb_status.id_status WHERE surat_kip.id_status = '$status' UNION ALL
            SELECT id_surat, no_surat, surat_kuasa.id_user, nama_lengkap, tgl_surat, surat_kuasa.id_status, status_monitoring FROM surat_kuasa JOIN tb_warga ON surat_kuasa.id_user = tb_warga.id_user JOIN tb_status ON surat_kuasa.id_status = tb_status.id_status WHERE surat_kuasa.id_status = '$status' UNION ALL
            SELECT id_surat, no_surat, surat_permohonan_ktp.id_user, nama_lengkap, tgl_surat, surat_permohonan_ktp.id_status, status_monitoring FROM surat_permohonan_ktp JOIN tb_warga ON surat_permohonan_ktp.id_user = tb_warga.id_user JOIN tb_status ON surat_permohonan_ktp.id_status = tb_status.id_status WHERE surat_permohonan_ktp.id_status = '$status' UNION ALL
            SELECT id_surat, no_surat, surat_perubahan_kk.id_user, nama_lengkap, tgl_surat, surat_perubahan_kk.id_status, status_monitoring FROM surat_perubahan_kk JOIN tb_warga ON surat_perubahan_kk.id_user = tb_warga.id_user JOIN tb_status ON surat_perubahan_kk.id_status = tb_status.id_status WHERE surat_perubahan_kk.id_status = '$status' UNION ALL
            SELECT id_surat, no_surat, surat_skck.id_user, nama_lengkap, tgl_surat, surat_skck.id_status, status_monitoring FROM surat_skck JOIN tb_warga ON surat_skck.id_user = tb_warga.id_user JOIN tb_status ON surat_skck.id_status = tb_status.id_status WHERE surat_skck.id_status = '$status' UNION ALL
            SELECT id_surat, no_surat, surat_sktm.id_user, nama_lengkap, tgl_surat, surat_sktm.id_status, status_monitoring FROM surat_sktm JOIN tb_warga ON surat_sktm.id_user = tb_warga.id_user JOIN tb_status ON surat_sktm.id_status = tb_status.id_status WHERE surat_sktm.id_status = '$status' UNION ALL
            SELECT id_surat, no_surat, surat_sptjm.id_user, nama_lengkap, tgl_surat, surat_sptjm.id_status, status_monitoring FROM surat_sptjm JOIN tb_warga ON surat_sptjm.id_user = tb_warga.id_user JOIN tb_status ON surat_sptjm.id_status = tb_status.id_status WHERE surat_sptjm.id_status = '$status'
        )X  ORDER BY tgl_surat ASC");
        return $query->result();
    }
    public function tampil_semua_bulan($bulan)
    {
        $query = $this->db->query("SELECT id_surat, no_surat, id_user, nama_lengkap, tgl_surat, id_status, status_monitoring FROM (
            SELECT id_surat, no_surat, surat_biodata.id_user, nama_lengkap, tgl_surat, surat_biodata.id_status, status_monitoring FROM surat_biodata JOIN tb_warga ON surat_biodata.id_user = tb_warga.id_user JOIN tb_status ON surat_biodata.id_status = tb_status.id_status WHERE surat_biodata.id_status IN ('2','3','4') AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_domisili.id_user, nama_lengkap, tgl_surat, surat_domisili.id_status, status_monitoring FROM surat_domisili JOIN tb_warga ON surat_domisili.id_user = tb_warga.id_user JOIN tb_status ON surat_domisili.id_status = tb_status.id_status WHERE surat_domisili.id_status IN ('2','3','4') AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_ijin_keramaian.id_user, nama_lengkap, tgl_surat, surat_ijin_keramaian.id_status, status_monitoring FROM surat_ijin_keramaian JOIN tb_warga ON surat_ijin_keramaian.id_user = tb_warga.id_user JOIN tb_status ON surat_ijin_keramaian.id_status = tb_status.id_status WHERE surat_ijin_keramaian.id_status IN ('2','3','4') AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_kelahiran.id_user, nama_lengkap, tgl_surat, surat_kelahiran.id_status, status_monitoring FROM surat_kelahiran JOIN tb_warga ON surat_kelahiran.id_user = tb_warga.id_user JOIN tb_status ON surat_kelahiran.id_status = tb_status.id_status WHERE surat_kelahiran.id_status IN ('2','3','4') AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_kematian.id_user, nama_lengkap, tgl_surat, surat_kematian.id_status, status_monitoring FROM surat_kematian JOIN tb_warga ON surat_kematian.id_user = tb_warga.id_user JOIN tb_status ON surat_kematian.id_status = tb_status.id_status WHERE surat_kematian.id_status IN ('2','3','4') AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_kip.id_user, nama_lengkap, tgl_surat, surat_kip.id_status, status_monitoring FROM surat_kip JOIN tb_warga ON surat_kip.id_user = tb_warga.id_user JOIN tb_status ON surat_kip.id_status = tb_status.id_status WHERE surat_kip.id_status IN ('2','3','4') AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_kuasa.id_user, nama_lengkap, tgl_surat, surat_kuasa.id_status, status_monitoring FROM surat_kuasa JOIN tb_warga ON surat_kuasa.id_user = tb_warga.id_user JOIN tb_status ON surat_kuasa.id_status = tb_status.id_status WHERE surat_kuasa.id_status IN ('2','3','4') AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_permohonan_ktp.id_user, nama_lengkap, tgl_surat, surat_permohonan_ktp.id_status, status_monitoring FROM surat_permohonan_ktp JOIN tb_warga ON surat_permohonan_ktp.id_user = tb_warga.id_user JOIN tb_status ON surat_permohonan_ktp.id_status = tb_status.id_status WHERE surat_permohonan_ktp.id_status IN ('2','3','4') AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_perubahan_kk.id_user, nama_lengkap, tgl_surat, surat_perubahan_kk.id_status, status_monitoring FROM surat_perubahan_kk JOIN tb_warga ON surat_perubahan_kk.id_user = tb_warga.id_user JOIN tb_status ON surat_perubahan_kk.id_status = tb_status.id_status WHERE surat_perubahan_kk.id_status IN ('2','3','4') AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_skck.id_user, nama_lengkap, tgl_surat, surat_skck.id_status, status_monitoring FROM surat_skck JOIN tb_warga ON surat_skck.id_user = tb_warga.id_user JOIN tb_status ON surat_skck.id_status = tb_status.id_status WHERE surat_skck.id_status IN ('2','3','4') AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_sktm.id_user, nama_lengkap, tgl_surat, surat_sktm.id_status, status_monitoring FROM surat_sktm JOIN tb_warga ON surat_sktm.id_user = tb_warga.id_user JOIN tb_status ON surat_sktm.id_status = tb_status.id_status WHERE surat_sktm.id_status IN ('2','3','4') AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_sptjm.id_user, nama_lengkap, tgl_surat, surat_sptjm.id_status, status_monitoring FROM surat_sptjm JOIN tb_warga ON surat_sptjm.id_user = tb_warga.id_user JOIN tb_status ON surat_sptjm.id_status = tb_status.id_status WHERE surat_sptjm.id_status IN ('2','3','4') AND MONTH(tgl_surat) = '$bulan'
        )X  ORDER BY tgl_surat ASC");
        return $query->result();
    }
    public function tampil_semua_status_bulan($status, $bulan)
    {
        $query = $this->db->query("SELECT id_surat, no_surat, id_user, nama_lengkap, tgl_surat, id_status, status_monitoring FROM (
            SELECT id_surat, no_surat, surat_biodata.id_user, nama_lengkap, tgl_surat, surat_biodata.id_status, status_monitoring FROM surat_biodata JOIN tb_warga ON surat_biodata.id_user = tb_warga.id_user JOIN tb_status ON surat_biodata.id_status = tb_status.id_status WHERE surat_biodata.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_domisili.id_user, nama_lengkap, tgl_surat, surat_domisili.id_status, status_monitoring FROM surat_domisili JOIN tb_warga ON surat_domisili.id_user = tb_warga.id_user JOIN tb_status ON surat_domisili.id_status = tb_status.id_status WHERE surat_domisili.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_ijin_keramaian.id_user, nama_lengkap, tgl_surat, surat_ijin_keramaian.id_status, status_monitoring FROM surat_ijin_keramaian JOIN tb_warga ON surat_ijin_keramaian.id_user = tb_warga.id_user JOIN tb_status ON surat_ijin_keramaian.id_status = tb_status.id_status WHERE surat_ijin_keramaian.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_kelahiran.id_user, nama_lengkap, tgl_surat, surat_kelahiran.id_status, status_monitoring FROM surat_kelahiran JOIN tb_warga ON surat_kelahiran.id_user = tb_warga.id_user JOIN tb_status ON surat_kelahiran.id_status = tb_status.id_status WHERE surat_kelahiran.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_kematian.id_user, nama_lengkap, tgl_surat, surat_kematian.id_status, status_monitoring FROM surat_kematian JOIN tb_warga ON surat_kematian.id_user = tb_warga.id_user JOIN tb_status ON surat_kematian.id_status = tb_status.id_status WHERE surat_kematian.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_kip.id_user, nama_lengkap, tgl_surat, surat_kip.id_status, status_monitoring FROM surat_kip JOIN tb_warga ON surat_kip.id_user = tb_warga.id_user JOIN tb_status ON surat_kip.id_status = tb_status.id_status WHERE surat_kip.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_kuasa.id_user, nama_lengkap, tgl_surat, surat_kuasa.id_status, status_monitoring FROM surat_kuasa JOIN tb_warga ON surat_kuasa.id_user = tb_warga.id_user JOIN tb_status ON surat_kuasa.id_status = tb_status.id_status WHERE surat_kuasa.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_permohonan_ktp.id_user, nama_lengkap, tgl_surat, surat_permohonan_ktp.id_status, status_monitoring FROM surat_permohonan_ktp JOIN tb_warga ON surat_permohonan_ktp.id_user = tb_warga.id_user JOIN tb_status ON surat_permohonan_ktp.id_status = tb_status.id_status WHERE surat_permohonan_ktp.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_perubahan_kk.id_user, nama_lengkap, tgl_surat, surat_perubahan_kk.id_status, status_monitoring FROM surat_perubahan_kk JOIN tb_warga ON surat_perubahan_kk.id_user = tb_warga.id_user JOIN tb_status ON surat_perubahan_kk.id_status = tb_status.id_status WHERE surat_perubahan_kk.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_skck.id_user, nama_lengkap, tgl_surat, surat_skck.id_status, status_monitoring FROM surat_skck JOIN tb_warga ON surat_skck.id_user = tb_warga.id_user JOIN tb_status ON surat_skck.id_status = tb_status.id_status WHERE surat_skck.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_sktm.id_user, nama_lengkap, tgl_surat, surat_sktm.id_status, status_monitoring FROM surat_sktm JOIN tb_warga ON surat_sktm.id_user = tb_warga.id_user JOIN tb_status ON surat_sktm.id_status = tb_status.id_status WHERE surat_sktm.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_sptjm.id_user, nama_lengkap, tgl_surat, surat_sptjm.id_status, status_monitoring FROM surat_sptjm JOIN tb_warga ON surat_sptjm.id_user = tb_warga.id_user JOIN tb_status ON surat_sptjm.id_status = tb_status.id_status WHERE surat_sptjm.id_status = '$status' AND MONTH(tgl_surat) = '$bulan'
        )X  ORDER BY tgl_surat ASC");
        return $query->result();
    }

    public function status()
    {
        $this->db->select('*');
        $this->db->from('tb_status');
        return $this->db->get()->result();
    }
    public function kades()
    {
        $this->db->select('*');
        $this->db->from('tb_perangkat_desa');
        $this->db->where('id_perangkat','kades01');
        return $this->db->get()->result();
    }

}