<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SuratAjuanKades_model extends CI_Model {

    public function tampil_kuasa()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_kuasa k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','4');
        return $this->db->get()->result();
    }
    public function tampil_domisili()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_domisili k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','4');
        return $this->db->get()->result();
    }
    public function tampil_kip()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_kip k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','4');
        return $this->db->get()->result();
    }
    public function tampil_sktm()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_sktm k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','4');
        return $this->db->get()->result();
    }
    public function tampil_keramaian()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_ijin_keramaian k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','4');
        return $this->db->get()->result();
    }
    public function tampil_biodata()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_biodata k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','4');
        
        return $this->db->get()->result();
    }
    public function tampil_sptjm()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_sptjm k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','4');
        return $this->db->get()->result();
    }
    public function tampil_skck()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_skck k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','4');
        
        return $this->db->get()->result();
    }
    public function tampil_kelahiran()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_kelahiran k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','4');
        
        return $this->db->get()->result();
    }
    public function tampil_kk()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_perubahan_kk k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','4');
       
        return $this->db->get()->result();
    }
    public function tampil_kematian()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_kematian k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','4');
        
        return $this->db->get()->result();
    }
    public function tampil_ktp()
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_permohonan_ktp k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','4');
        return $this->db->get()->result();
    }
    public function semua_data()
    {
        $query = $this->db->query("SELECT id_surat, no_surat, id_user, nama_lengkap, tgl_surat, id_status, status_monitoring FROM (
                    SELECT id_surat, no_surat, surat_biodata.id_user, nama_lengkap,  tgl_surat, surat_biodata.id_status, status_monitoring FROM surat_biodata JOIN tb_warga ON surat_biodata.id_user = tb_warga.id_user JOIN tb_status ON surat_biodata.id_status = tb_status.id_status WHERE surat_biodata.id_status='4'  UNION ALL
                    SELECT id_surat, no_surat, surat_domisili.id_user, nama_lengkap,  tgl_surat, surat_domisili.id_status, status_monitoring FROM surat_domisili JOIN tb_warga ON surat_domisili.id_user = tb_warga.id_user JOIN tb_status ON surat_domisili.id_status = tb_status.id_status WHERE surat_domisili.id_status='4'  UNION ALL
                    SELECT id_surat, no_surat, surat_ijin_keramaian.id_user, nama_lengkap,  tgl_surat, surat_ijin_keramaian.id_status, status_monitoring FROM surat_ijin_keramaian JOIN tb_warga ON surat_ijin_keramaian.id_user = tb_warga.id_user JOIN tb_status ON surat_ijin_keramaian.id_status = tb_status.id_status WHERE surat_ijin_keramaian.id_status='4'  UNION ALL
                    SELECT id_surat, no_surat, surat_kelahiran.id_user, nama_lengkap,  tgl_surat, surat_kelahiran.id_status, status_monitoring FROM surat_kelahiran JOIN tb_warga ON surat_kelahiran.id_user = tb_warga.id_user JOIN tb_status ON surat_kelahiran.id_status = tb_status.id_status WHERE surat_kelahiran.id_status='4' UNION ALL
                    SELECT id_surat, no_surat, surat_kematian.id_user, nama_lengkap,  tgl_surat, surat_kematian.id_status, status_monitoring FROM surat_kematian JOIN tb_warga ON surat_kematian.id_user = tb_warga.id_user JOIN tb_status ON surat_kematian.id_status = tb_status.id_status WHERE surat_kematian.id_status='4'  UNION ALL
                    SELECT id_surat, no_surat, surat_kip.id_user, nama_lengkap,  tgl_surat, surat_kip.id_status, status_monitoring FROM surat_kip JOIN tb_warga ON surat_kip.id_user = tb_warga.id_user JOIN tb_status ON surat_kip.id_status = tb_status.id_status WHERE surat_kip.id_status='4'  UNION ALL
                    SELECT id_surat, no_surat, surat_kuasa.id_user, nama_lengkap,  tgl_surat, surat_kuasa.id_status, status_monitoring FROM surat_kuasa JOIN tb_warga ON surat_kuasa.id_user = tb_warga.id_user JOIN tb_status ON surat_kuasa.id_status = tb_status.id_status WHERE surat_kuasa.id_status='4'  UNION ALL
                    SELECT id_surat, no_surat, surat_permohonan_ktp.id_user, nama_lengkap,  tgl_surat, surat_permohonan_ktp.id_status, status_monitoring FROM surat_permohonan_ktp JOIN tb_warga ON surat_permohonan_ktp.id_user = tb_warga.id_user JOIN tb_status ON surat_permohonan_ktp.id_status = tb_status.id_status WHERE surat_permohonan_ktp.id_status='4'  UNION ALL
                    SELECT id_surat, no_surat, surat_perubahan_kk.id_user, nama_lengkap, tgl_surat, surat_perubahan_kk.id_status, status_monitoring FROM surat_perubahan_kk JOIN tb_warga ON surat_perubahan_kk.id_user = tb_warga.id_user JOIN tb_status ON surat_perubahan_kk.id_status = tb_status.id_status WHERE surat_perubahan_kk.id_status='4'  UNION ALL
                    SELECT id_surat, no_surat, surat_skck.id_user, nama_lengkap,  tgl_surat, surat_skck.id_status, status_monitoring FROM surat_skck JOIN tb_warga ON surat_skck.id_user = tb_warga.id_user JOIN tb_status ON surat_skck.id_status = tb_status.id_status WHERE surat_skck.id_status='4'  UNION ALL
                    SELECT id_surat, no_surat, surat_sktm.id_user, nama_lengkap,  tgl_surat, surat_sktm.id_status, status_monitoring FROM surat_sktm JOIN tb_warga ON surat_sktm.id_user = tb_warga.id_user JOIN tb_status ON surat_sktm.id_status = tb_status.id_status WHERE surat_sktm.id_status='4'  UNION ALL
                    SELECT id_surat, no_surat, surat_sptjm.id_user, nama_lengkap,  tgl_surat, surat_sptjm.id_status, status_monitoring FROM surat_sptjm JOIN tb_warga ON surat_sptjm.id_user = tb_warga.id_user JOIN tb_status ON surat_sptjm.id_status = tb_status.id_status WHERE surat_sptjm.id_status='4' 
                    )X  ORDER BY tgl_surat ASC");
        return $query->result();
    }
    public function filter_kuasa($bulan)
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_kuasa k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','4');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        return $this->db->get()->result();
    }
    public function filter_domisili($bulan)
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_domisili k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','4');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        return $this->db->get()->result();
    }
    public function filter_kip($bulan)
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_kip k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','1');
        return $this->db->get()->result();
    }
    public function filter_sktm($bulan)
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_sktm k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','4');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        return $this->db->get()->result();
    }
    public function filter_keramaian($bulan)
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_ijin_keramaian k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','4');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        return $this->db->get()->result();
    }
    public function filter_biodata($bulan)
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_biodata k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','4');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        
        return $this->db->get()->result();
    }
    public function filter_sptjm($bulan)
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_sptjm k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','4');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        return $this->db->get()->result();
    }
    public function filter_skck($bulan)
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_skck k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','4');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        
        return $this->db->get()->result();
    }
    public function filter_kelahiran($bulan)
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_kelahiran k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','4');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        
        return $this->db->get()->result();
    }
    public function filter_kk($bulan)
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_perubahan_kk k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','4');
        $this->db->where('MONTH(tgl_surat)',$bulan);
       
        return $this->db->get()->result();
    }
    public function filter_kematian($bulan)
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_kematian k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','4');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        
        return $this->db->get()->result();
    }
    public function filter_ktp($bulan)
    {
        $this->db->select('k.id_surat, k.id_user, k.id_status, no_surat, tgl_surat, s.status_monitoring, w.nama_lengkap');
        $this->db->from('surat_permohonan_ktp k');
        $this->db->join('tb_status s','k.id_status = s.id_status');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->where('k.id_status','4');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        return $this->db->get()->result();
    }
    public function filter_semua_data($bulan)
    {
        $query = $this->db->query("SELECT id_surat, no_surat, id_user, nama_lengkap, tgl_surat, id_status, status_monitoring FROM (
            SELECT id_surat, no_surat, surat_biodata.id_user, nama_lengkap,  tgl_surat, surat_biodata.id_status, status_monitoring FROM surat_biodata JOIN tb_warga ON surat_biodata.id_user = tb_warga.id_user JOIN tb_status ON surat_biodata.id_status = tb_status.id_status WHERE surat_biodata.id_status='4' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_domisili.id_user, nama_lengkap,  tgl_surat, surat_domisili.id_status, status_monitoring FROM surat_domisili JOIN tb_warga ON surat_domisili.id_user = tb_warga.id_user JOIN tb_status ON surat_domisili.id_status = tb_status.id_status WHERE surat_domisili.id_status='4' AND MONTH(tgl_surat) = '$bulan'  UNION ALL
            SELECT id_surat, no_surat, surat_ijin_keramaian.id_user, nama_lengkap,  tgl_surat, surat_ijin_keramaian.id_status, status_monitoring FROM surat_ijin_keramaian JOIN tb_warga ON surat_ijin_keramaian.id_user = tb_warga.id_user JOIN tb_status ON surat_ijin_keramaian.id_status = tb_status.id_status WHERE surat_ijin_keramaian.id_status='4' AND MONTH(tgl_surat) = '$bulan'  UNION ALL
            SELECT id_surat, no_surat, surat_kelahiran.id_user, nama_lengkap,  tgl_surat, surat_kelahiran.id_status, status_monitoring FROM surat_kelahiran JOIN tb_warga ON surat_kelahiran.id_user = tb_warga.id_user JOIN tb_status ON surat_kelahiran.id_status = tb_status.id_status WHERE surat_kelahiran.id_status='4' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT id_surat, no_surat, surat_kematian.id_user, nama_lengkap,  tgl_surat, surat_kematian.id_status, status_monitoring FROM surat_kematian JOIN tb_warga ON surat_kematian.id_user = tb_warga.id_user JOIN tb_status ON surat_kematian.id_status = tb_status.id_status WHERE surat_kematian.id_status='4' AND MONTH(tgl_surat) = '$bulan'  UNION ALL
            SELECT id_surat, no_surat, surat_kip.id_user, nama_lengkap,  tgl_surat, surat_kip.id_status, status_monitoring FROM surat_kip JOIN tb_warga ON surat_kip.id_user = tb_warga.id_user JOIN tb_status ON surat_kip.id_status = tb_status.id_status WHERE surat_kip.id_status='4' AND MONTH(tgl_surat) = '$bulan'  UNION ALL
            SELECT id_surat, no_surat, surat_kuasa.id_user, nama_lengkap,  tgl_surat, surat_kuasa.id_status, status_monitoring FROM surat_kuasa JOIN tb_warga ON surat_kuasa.id_user = tb_warga.id_user JOIN tb_status ON surat_kuasa.id_status = tb_status.id_status WHERE surat_kuasa.id_status='4' AND MONTH(tgl_surat) = '$bulan'  UNION ALL
            SELECT id_surat, no_surat, surat_permohonan_ktp.id_user, nama_lengkap,  tgl_surat, surat_permohonan_ktp.id_status, status_monitoring FROM surat_permohonan_ktp JOIN tb_warga ON surat_permohonan_ktp.id_user = tb_warga.id_user JOIN tb_status ON surat_permohonan_ktp.id_status = tb_status.id_status WHERE surat_permohonan_ktp.id_status='4' AND MONTH(tgl_surat) = '$bulan'  UNION ALL
            SELECT id_surat, no_surat, surat_perubahan_kk.id_user, nama_lengkap, tgl_surat, surat_perubahan_kk.id_status, status_monitoring FROM surat_perubahan_kk JOIN tb_warga ON surat_perubahan_kk.id_user = tb_warga.id_user JOIN tb_status ON surat_perubahan_kk.id_status = tb_status.id_status WHERE surat_perubahan_kk.id_status='4' AND MONTH(tgl_surat) = '$bulan'  UNION ALL
            SELECT id_surat, no_surat, surat_skck.id_user, nama_lengkap,  tgl_surat, surat_skck.id_status, status_monitoring FROM surat_skck JOIN tb_warga ON surat_skck.id_user = tb_warga.id_user JOIN tb_status ON surat_skck.id_status = tb_status.id_status WHERE surat_skck.id_status='4' AND MONTH(tgl_surat) = '$bulan'  UNION ALL
            SELECT id_surat, no_surat, surat_sktm.id_user, nama_lengkap,  tgl_surat, surat_sktm.id_status, status_monitoring FROM surat_sktm JOIN tb_warga ON surat_sktm.id_user = tb_warga.id_user JOIN tb_status ON surat_sktm.id_status = tb_status.id_status WHERE surat_sktm.id_status='4' AND MONTH(tgl_surat) = '$bulan'  UNION ALL
            SELECT id_surat, no_surat, surat_sptjm.id_user, nama_lengkap,  tgl_surat, surat_sptjm.id_status, status_monitoring FROM surat_sptjm JOIN tb_warga ON surat_sptjm.id_user = tb_warga.id_user JOIN tb_status ON surat_sptjm.id_status = tb_status.id_status WHERE surat_sptjm.id_status='4' AND MONTH(tgl_surat) = '$bulan' 
            )X  ORDER BY tgl_surat ASC");
        return $query->result();
    }


    public function kades($where)
    {
        $query=$this->db->query("SELECT *
        FROM tb_perangkat_desa
        WHERE id_perangkat='$where'");
        return $query;
    }

} 