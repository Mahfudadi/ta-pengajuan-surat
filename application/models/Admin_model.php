<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model
{

	public function tampil_data()
	{

		$this->db->select('*');
		$this->db->from('tb_admin');
		return $this->db->get()->result();
	}
	public function profil($id)
	{
		$this->db->select('*');
		$this->db->from('tb_admin');

		return $this->db->get()->result();
	}
	public function edit_profil($id)
	{
		$this->db->select('*');
		$this->db->from('tb_admin');
		$this->db->where('id_user', $id);
		return $this->db->get()->result();
	}
	public function update_profil($data, $id)
	{
		$this->db->update("tb_admin", $data, $id);
	}
	public function total_selesai()
	{
		$query = $this->db->query("SELECT SUM(total) AS total FROM (
            SELECT COUNT('id_status') AS total FROM surat_biodata WHERE id_status IN ('5','6') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_domisili WHERE id_status IN ('5','6') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_ijin_keramaian WHERE id_status IN ('5','6') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kelahiran WHERE id_status IN ('5','6') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kematian WHERE id_status IN ('5','6') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kip WHERE id_status IN ('5','6') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kuasa WHERE id_status IN ('5','6') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_permohonan_ktp WHERE id_status IN ('5','6') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_perubahan_kk WHERE id_status IN ('5','6') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_skck WHERE id_status IN ('5','6') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_sktm WHERE id_status IN ('5','6') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_sptjm WHERE id_status IN ('5','6')
        )X");
		return $query->result();
	}
	public function total_ditolak()
	{
		$query = $this->db->query("SELECT SUM(total) AS total FROM (
            SELECT COUNT('id_status') AS total FROM surat_biodata WHERE id_status = '7' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_domisili WHERE id_status = '7' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_ijin_keramaian WHERE id_status = '7' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kelahiran WHERE id_status = '7' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kematian WHERE id_status = '7' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kip WHERE id_status = '7' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kuasa WHERE id_status = '7' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_permohonan_ktp WHERE id_status = '7' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_perubahan_kk WHERE id_status = '7' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_skck WHERE id_status = '7' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_sktm WHERE id_status = '7' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_sptjm WHERE id_status = '7'
        )X");
		return $query->result();
	}
	public function total_diproses()
	{
		$query = $this->db->query("SELECT SUM(total) AS total FROM (
            SELECT COUNT('id_status') AS total FROM surat_biodata WHERE id_status IN ('2','3','4') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_domisili WHERE id_status IN ('2','3','4') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_ijin_keramaian WHERE id_status IN ('2','3','4') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kelahiran WHERE id_status IN ('2','3','4') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kematian WHERE id_status IN ('2','3','4') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kip WHERE id_status IN ('2','3','4') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kuasa WHERE id_status IN ('2','3','4') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_permohonan_ktp WHERE id_status IN ('2','3','4') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_perubahan_kk WHERE id_status IN ('2','3','4') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_skck WHERE id_status IN ('2','3','4') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_sktm WHERE id_status IN ('2','3','4') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_sptjm WHERE id_status IN ('2','3','4')
        )X");
		return $query->result();
	}
	public function total_semua()
	{
		$query = $this->db->query("SELECT SUM(total) AS total FROM (
            SELECT COUNT('id_status') AS total FROM surat_biodata WHERE id_status IN ('2','3','4','5','6','7') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_domisili WHERE id_status IN ('2','3','4','5','6','7') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_ijin_keramaian WHERE id_status IN ('2','3','4','5','6','7') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kelahiran WHERE id_status IN ('2','3','4','5','6','7') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kematian WHERE id_status IN ('2','3','4','5','6','7') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kip WHERE id_status IN ('2','3','4','5','6','7') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kuasa WHERE id_status IN ('2','3','4','5','6','7') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_permohonan_ktp WHERE id_status IN ('2','3','4','5','6','7') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_perubahan_kk WHERE id_status IN ('2','3','4','5','6','7') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_skck WHERE id_status IN ('2','3','4','5','6','7') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_sktm WHERE id_status IN ('2','3','4','5','6','7') UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_sptjm WHERE id_status IN ('2','3','4','5','6','7')
        )X");
		return $query->result();
	}
	public function total_menunggu()
	{
		$query = $this->db->query("SELECT SUM(total) AS total FROM (
            SELECT COUNT('id_status') AS total FROM surat_biodata WHERE id_status = '2' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_domisili WHERE id_status = '2' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_ijin_keramaian WHERE id_status = '2' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kelahiran WHERE id_status = '2' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kematian WHERE id_status = '2' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kip WHERE id_status = '2' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kuasa WHERE id_status = '2' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_permohonan_ktp WHERE id_status = '2' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_perubahan_kk WHERE id_status = '2' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_skck WHERE id_status = '2' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_sktm WHERE id_status = '2' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_sptjm WHERE id_status = '2'
        )X");
		return $query->result();
	}

	public function total_bulanan()
	{
		$query = $this->db->query("SELECT MONTHNAME(month) as bulan, SUM(semua) as semua, SUM(process) as diproses, SUM(finish) as selesai, SUM(reject) as ditolak FROM (
			SELECT tgl_surat as month, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_biodata WHERE tgl_surat >= DATE_SUB(NOW(), INTERVAL 5 MONTH) GROUP BY month UNION ALL
			SELECT tgl_surat as month, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_domisili WHERE tgl_surat >= DATE_SUB(NOW(), INTERVAL 5 MONTH) GROUP BY month UNION ALL
			SELECT tgl_surat as month, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_ijin_keramaian WHERE tgl_surat >= DATE_SUB(NOW(), INTERVAL 5 MONTH) GROUP BY month UNION ALL
			SELECT tgl_surat as month, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kelahiran WHERE tgl_surat >= DATE_SUB(NOW(), INTERVAL 5 MONTH) GROUP BY month UNION ALL
			SELECT tgl_surat as month, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kematian WHERE tgl_surat >= DATE_SUB(NOW(), INTERVAL 5 MONTH) GROUP BY month UNION ALL
			SELECT tgl_surat as month, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kip WHERE tgl_surat >= DATE_SUB(NOW(), INTERVAL 5 MONTH) GROUP BY month UNION ALL
			SELECT tgl_surat as month, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kuasa WHERE tgl_surat >= DATE_SUB(NOW(), INTERVAL 5 MONTH) GROUP BY month UNION ALL
			SELECT tgl_surat as month, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_permohonan_ktp WHERE tgl_surat >= DATE_SUB(NOW(), INTERVAL 5 MONTH) GROUP BY month UNION ALL
			SELECT tgl_surat as month, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_perubahan_kk WHERE tgl_surat >= DATE_SUB(NOW(), INTERVAL 5 MONTH) GROUP BY month UNION ALL
			SELECT tgl_surat as month, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_skck WHERE tgl_surat >= DATE_SUB(NOW(), INTERVAL 5 MONTH) GROUP BY month UNION ALL
			SELECT tgl_surat as month, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_sktm WHERE tgl_surat >= DATE_SUB(NOW(), INTERVAL 5 MONTH) GROUP BY month UNION ALL
			SELECT tgl_surat as month, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_sptjm WHERE tgl_surat >= DATE_SUB(NOW(), INTERVAL 5 MONTH) GROUP BY month
			)X
			GROUP BY bulan ORDER BY month ASC");
		return $query->result_array();
	}

	public function total_tahunan()
	{
		$query = $this->db->query("SELECT YEAR(thn) as tahun, SUM(semua) as semua, SUM(process) as diproses, SUM(finish) as selesai, SUM(reject) as ditolak FROM (
			SELECT tgl_surat as thn, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_biodata WHERE YEAR(tgl_surat) >= YEAR(DATE_SUB(NOW(), INTERVAL 5 YEAR)) GROUP BY thn UNION ALL
			SELECT tgl_surat as thn, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_domisili WHERE YEAR(tgl_surat) >= YEAR(DATE_SUB(NOW(), INTERVAL 5 YEAR)) GROUP BY thn UNION ALL
			SELECT tgl_surat as thn, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_ijin_keramaian WHERE YEAR(tgl_surat) >= YEAR(DATE_SUB(NOW(), INTERVAL 5 YEAR)) GROUP BY thn UNION ALL
			SELECT tgl_surat as thn, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kelahiran WHERE YEAR(tgl_surat) >= YEAR(DATE_SUB(NOW(), INTERVAL 5 YEAR)) GROUP BY thn UNION ALL
			SELECT tgl_surat as thn, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kematian WHERE YEAR(tgl_surat) >= YEAR(DATE_SUB(NOW(), INTERVAL 5 YEAR)) GROUP BY thn UNION ALL
			SELECT tgl_surat as thn, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kip WHERE YEAR(tgl_surat) >= YEAR(DATE_SUB(NOW(), INTERVAL 5 YEAR)) GROUP BY thn UNION ALL
			SELECT tgl_surat as thn, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kuasa WHERE YEAR(tgl_surat) >= YEAR(DATE_SUB(NOW(), INTERVAL 5 YEAR)) GROUP BY thn UNION ALL
			SELECT tgl_surat as thn, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_permohonan_ktp WHERE YEAR(tgl_surat) >= YEAR(DATE_SUB(NOW(), INTERVAL 5 YEAR)) GROUP BY thn UNION ALL
			SELECT tgl_surat as thn, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_perubahan_kk WHERE YEAR(tgl_surat) >= YEAR(DATE_SUB(NOW(), INTERVAL 5 YEAR)) GROUP BY thn UNION ALL
			SELECT tgl_surat as thn, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_skck WHERE YEAR(tgl_surat) >= YEAR(DATE_SUB(NOW(), INTERVAL 5 YEAR)) GROUP BY thn UNION ALL
			SELECT tgl_surat as thn, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_sktm WHERE YEAR(tgl_surat) >= YEAR(DATE_SUB(NOW(), INTERVAL 5 YEAR)) GROUP BY thn UNION ALL
			SELECT tgl_surat as thn, COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_sptjm WHERE YEAR(tgl_surat) >= YEAR(DATE_SUB(NOW(), INTERVAL 5 YEAR)) GROUP BY thn
			)X
			GROUP BY tahun ORDER BY thn ASC");
		return $query->result_array();
	}

	public function totalByLetterName($letter = null, $month = null)
	{
		if ($letter === null && $month === null) {
			$query = $this->db->query("SELECT SUM(semua) as semua, SUM(process) as diproses, SUM(finish) as selesai, SUM(reject) as ditolak FROM (
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_biodata UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_domisili UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_ijin_keramaian UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kelahiran UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kematian UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kip UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kuasa UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_permohonan_ktp UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_perubahan_kk UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_skck UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_sktm UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_sptjm
			)X");
			return $query->row_array();
		} elseif ($letter != null && $month == null) {
			$query = $this->db->query(
				"SELECT COUNT(*) as semua, 
				COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as diproses, 
				COUNT(IF(id_status = 6, id_surat, null)) as selesai, 
				COUNT(IF(id_status = 7, id_surat, null)) as ditolak 
				FROM $letter"
			);
			return $query->row_array();
		} elseif ($letter == null && $month != null) {
			$query = $this->db->query("SELECT SUM(semua) as semua, SUM(process) as diproses, SUM(finish) as selesai, SUM(reject) as ditolak FROM (
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_biodata WHERE MONTH(tgl_surat) = $month UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_domisili WHERE MONTH(tgl_surat) = $month UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_ijin_keramaian WHERE MONTH(tgl_surat) = $month UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kelahiran WHERE MONTH(tgl_surat) = $month UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kematian WHERE MONTH(tgl_surat) = $month UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kip WHERE MONTH(tgl_surat) = $month UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kuasa WHERE MONTH(tgl_surat) = $month UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_permohonan_ktp WHERE MONTH(tgl_surat) = $month UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_perubahan_kk WHERE MONTH(tgl_surat) = $month UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_skck WHERE MONTH(tgl_surat) = $month UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_sktm WHERE MONTH(tgl_surat) = $month UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_sptjm WHERE MONTH(tgl_surat) = $month
			)X");
			return $query->row_array();
		} else {
			$query = $this->db->query(
				"SELECT COUNT(*) as semua, 
				COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as diproses, 
				COUNT(IF(id_status = 6, id_surat, null)) as selesai, 
				COUNT(IF(id_status = 7, id_surat, null)) as ditolak 
				FROM $letter WHERE MONTH(tgl_surat) = $month"
			);
			return $query->row_array();
		}
	}
	public function totalLetterByYear($year = null, $letter = null)
	{
		if ($year === null && $year === null) {
			$query = $this->db->query("SELECT SUM(semua) as semua, SUM(process) as diproses, SUM(finish) as selesai, SUM(reject) as ditolak FROM (
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_biodata WHERE YEAR(tgl_surat) = YEAR(NOW()) UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_domisili WHERE YEAR(tgl_surat) = YEAR(NOW()) UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_ijin_keramaian WHERE YEAR(tgl_surat) = YEAR(NOW()) UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kelahiran WHERE YEAR(tgl_surat) = YEAR(NOW()) UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kematian WHERE YEAR(tgl_surat) = YEAR(NOW()) UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kip WHERE YEAR(tgl_surat) = YEAR(NOW()) UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kuasa WHERE YEAR(tgl_surat) = YEAR(NOW()) UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_permohonan_ktp WHERE YEAR(tgl_surat) = YEAR(NOW()) UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_perubahan_kk WHERE YEAR(tgl_surat) = YEAR(NOW()) UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_skck WHERE YEAR(tgl_surat) = YEAR(NOW()) UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_sktm WHERE YEAR(tgl_surat) = YEAR(NOW()) UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_sptjm WHERE YEAR(tgl_surat) = YEAR(NOW())
			)X");
			return $query->row_array();
		} else if ($year != null && $letter == null) {
			$query = $this->db->query("SELECT SUM(semua) as semua, SUM(process) as diproses, SUM(finish) as selesai, SUM(reject) as ditolak FROM (
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_biodata WHERE YEAR(tgl_surat) = $year UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_domisili WHERE YEAR(tgl_surat) = $year UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_ijin_keramaian WHERE YEAR(tgl_surat) = $year UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kelahiran WHERE YEAR(tgl_surat) = $year UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kematian WHERE YEAR(tgl_surat) = $year UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kip WHERE YEAR(tgl_surat) = $year UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kuasa WHERE YEAR(tgl_surat) = $year UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_permohonan_ktp WHERE YEAR(tgl_surat) = $year UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_perubahan_kk WHERE YEAR(tgl_surat) = $year UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_skck WHERE YEAR(tgl_surat) = $year UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_sktm WHERE YEAR(tgl_surat) = $year UNION ALL
				SELECT COUNT(*) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_sptjm WHERE YEAR(tgl_surat) = $year
			)X");
			return $query->row_array();
		} else {
			$query = $this->db->query(
				"SELECT COUNT(*) as semua, 
				COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as diproses, 
				COUNT(IF(id_status = 6, id_surat, null)) as selesai, 
				COUNT(IF(id_status = 7, id_surat, null)) as ditolak 
				FROM $letter WHERE YEAR(tgl_surat) = $year"
			);
			return $query->row_array();
		}
	}
	
	public function presentase($Y)
	{
		$query = $this->db->query("SELECT SUM(semua) as semua, SUM(process) as diproses, SUM(finish) as selesai, SUM(reject) as ditolak FROM (
			SELECT COUNT(IF(id_status IN (2,3,4,5,6,7), id_surat, null)) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5 OR id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_biodata WHERE YEAR(tgl_surat) = $Y UNION ALL
			SELECT COUNT(IF(id_status IN (2,3,4,5,6,7), id_surat, null)) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5 OR id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_domisili WHERE YEAR(tgl_surat) = $Y UNION ALL
			SELECT COUNT(IF(id_status IN (2,3,4,5,6,7), id_surat, null)) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5 OR id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_ijin_keramaian WHERE YEAR(tgl_surat) = $Y UNION ALL
			SELECT COUNT(IF(id_status IN (2,3,4,5,6,7), id_surat, null)) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5 OR id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kelahiran WHERE YEAR(tgl_surat) = $Y UNION ALL
			SELECT COUNT(IF(id_status IN (2,3,4,5,6,7), id_surat, null)) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5 OR id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kematian WHERE YEAR(tgl_surat) = $Y UNION ALL
			SELECT COUNT(IF(id_status IN (2,3,4,5,6,7), id_surat, null)) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5 OR id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kip WHERE YEAR(tgl_surat) = $Y UNION ALL
			SELECT COUNT(IF(id_status IN (2,3,4,5,6,7), id_surat, null)) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5 OR id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_kuasa WHERE YEAR(tgl_surat) = $Y UNION ALL
			SELECT COUNT(IF(id_status IN (2,3,4,5,6,7), id_surat, null)) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5 OR id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_permohonan_ktp WHERE YEAR(tgl_surat) = $Y UNION ALL
			SELECT COUNT(IF(id_status IN (2,3,4,5,6,7), id_surat, null)) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5 OR id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_perubahan_kk WHERE YEAR(tgl_surat) = $Y UNION ALL
			SELECT COUNT(IF(id_status IN (2,3,4,5,6,7), id_surat, null)) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5 OR id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_skck WHERE YEAR(tgl_surat) = $Y UNION ALL
			SELECT COUNT(IF(id_status IN (2,3,4,5,6,7), id_surat, null)) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5 OR id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_sktm WHERE YEAR(tgl_surat) = $Y UNION ALL
			SELECT COUNT(IF(id_status IN (2,3,4,5,6,7), id_surat, null)) as semua, COUNT(IF(id_status = 3 OR id_status = 4, id_surat, null)) as process, COUNT(IF(id_status = 5 OR id_status = 6, id_surat, null)) as finish, COUNT(IF(id_status = 7, id_surat, null)) as reject FROM surat_sptjm WHERE YEAR(tgl_surat) = $Y
		)X");
		return $query->row_array();
	}
}
