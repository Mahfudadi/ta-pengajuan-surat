<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class WargaSuratAjuan_model extends CI_Model {

    public function input_surat($data,$table){
        $this->db->insert($table,$data);
    }

    // mengambil data warga
    public function warga($id)
    {
        $this->db->select('*');
        $this->db->from('tb_warga');
        $this->db->where('id_user',$id);
        return $this->db->get()->result();
    }
    public function detail_profil($id)
    {
        $this->db->select('tb_warga.id_user,no_kk,nik, nama_lengkap,tempat_lahir,tanggal_lahir,jenis_kelamin,umur,gol_darah,penyandang_cacat,agama,status_kawin,pendidikan,pekerjaan,alamat,dusun,rw,rt,desa,kecamatan,kab_kota,prov,warga_negara');
        $this->db->from('tb_warga');
        $this->db->where('tb_warga.id_user',$id);
        $this->db->limit(1);
        return $this->db->get()->result();
    }
    public function edit_warga($id)
    {
        $this->db->select('*');
        $this->db->from('tb_warga');
        $this->db->where('id_user',$id);
        return $this->db->get()->result();
    }
    public function update_warga($data, $id)
    {
        $this->db->update("tb_warga", $data, $id);
    }
    public function edit_pw($id)
    {
        $this->db->select('*');
        $this->db->from('tb_user');
        $this->db->where('id_user',$id);
        return $this->db->get()->result();
    }
    public function update_pw($data, $id)
    {
        $this->db->update("tb_user", $data, $id);
    }
    
    public function biodata($id)
    {
        $this->db->select('COUNT(id_status) AS aktif');
        $this->db->from('surat_biodata');
        $this->db->where("id_status IN ('1','2','3','4','5')");
        $this->db->where('id_user',$id);
        return $this->db->get()->result();
    }
    public function domisili($id)
    {
        $this->db->select('COUNT(id_status) AS aktif');
        $this->db->from('surat_domisili');
        $this->db->where("id_status IN ('1','2','3','4','5')");
        $this->db->where('id_user',$id);
        return $this->db->get()->result();
    }
    public function keramaian($id)
    {
        $this->db->select('COUNT(id_status) AS aktif');
        $this->db->from('surat_ijin_keramaian');
        $this->db->where("id_status IN ('1','2','3','4','5')");
        $this->db->where('id_user',$id);
        return $this->db->get()->result();
    }
    public function kelahiran($id)
    {
        $this->db->select('COUNT(id_status) AS aktif');
        $this->db->from('surat_kelahiran');
        $this->db->where("id_status IN ('1','2','3','4','5')");
        $this->db->where('id_user',$id);
        return $this->db->get()->result();
    }
    public function kematian($id)
    {
        $this->db->select('COUNT(id_status) AS aktif');
        $this->db->from('surat_kematian');
        $this->db->where("id_status IN ('1','2','3','4','5')");
        $this->db->where('id_user',$id);
        return $this->db->get()->result();
    }
    public function kip($id)
    {
        $this->db->select('COUNT(id_status) AS aktif');
        $this->db->from('surat_kip');
        $this->db->where("id_status IN ('1','2','3','4','5')");
        $this->db->where('id_user',$id);
        return $this->db->get()->result();
    }
    public function kuasa($id)
    {
        $this->db->select('COUNT(id_status) AS aktif');
        $this->db->from('surat_kuasa');
        $this->db->where("id_status IN ('1','2','3','4','5')");
        $this->db->where('id_user',$id);
        return $this->db->get()->result();
    }
    public function ktp($id)
    {
        $this->db->select('COUNT(id_status) AS aktif');
        $this->db->from('surat_permohonan_ktp');
        $this->db->where("id_status IN ('1','2','3','4','5')");
        $this->db->where('id_user',$id);
        return $this->db->get()->result();
    }
    public function kk($id)
    {
        $this->db->select('COUNT(id_status) AS aktif');
        $this->db->from('surat_perubahan_kk');
        $this->db->where("id_status IN ('1','2','3','4','5')");
        $this->db->where('id_user',$id);
        return $this->db->get()->result();
    }
    public function skck($id)
    {
        $this->db->select('COUNT(id_status) AS aktif');
        $this->db->from('surat_skck');
        $this->db->where("id_status IN ('1','2','3','4','5')");
        $this->db->where('id_user',$id);
        return $this->db->get()->result();
    }
    public function sktm($id)
    {
        $this->db->select('COUNT(id_status) AS aktif');
        $this->db->from('surat_sktm');
        $this->db->where("id_status IN ('1','2','3','4','5')");
        $this->db->where('id_user',$id);
        return $this->db->get()->result();
    }
    public function sptjm($id)
    {
        $this->db->select('COUNT(id_status) AS aktif');
        $this->db->from('surat_sptjm');
        $this->db->where("id_status IN ('1','2','3','4','5')");
        $this->db->where('id_user',$id);
        return $this->db->get()->result();
    }
} 