<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class LaporanSurat_model extends CI_Model {

    public function tampil_biodata()
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_biodata k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status = "6" OR k.id_status = "7"');
        return $this->db->get()->result();
    }
    public function filter_status_biodata($status)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_biodata k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_biodata($bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_biodata k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        return $this->db->get()->result();
    }
    public function filter_tahun_biodata($tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_biodata k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    public function filter_status_bulan_biodata($status, $bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_biodata k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_status_tahun_biodata($status, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_biodata k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_tahun_biodata($bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_biodata k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    
    public function filter_semua_biodata($status, $bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_biodata k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' AND  k.id_status = '$status'");
        return $this->db->get()->result();
    }
    //====================batas surat==========================//
    public function tampil_domisili()
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_domisili k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status = "6" or k.id_status = "7"');
        return $this->db->get()->result();
    }
    public function filter_status_domisili($status)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_domisili k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_domisili($bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_domisili k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        return $this->db->get()->result();
    }
    public function filter_tahun_domisili($tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_domisili k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    public function filter_status_bulan_domisili($status, $bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_domisili k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_status_tahun_domisili($status, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_domisili k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_tahun_domisili($bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_domisili k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    
    public function filter_semua_domisili($status, $bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_domisili k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' AND  k.id_status = '$status'");
        return $this->db->get()->result();
    }
    // kelahiran
    public function tampil_kelahiran()
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kelahiran k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status = "6" or k.id_status = "7"');
        return $this->db->get()->result();
    }
    public function filter_status_kelahiran($status)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak, r.tgl_riwayat, r.riwayat_status, k.ket_tolak
');
        $this->db->from('surat_kelahiran k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_kelahiran($bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kelahiran k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        return $this->db->get()->result();
    }
    public function filter_tahun_kelahiran($tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kelahiran k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    public function filter_status_bulan_kelahiran($status, $bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kelahiran k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_status_tahun_kelahiran($status, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kelahiran k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_tahun_kelahiran($bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kelahiran k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    
    public function filter_semua_kelahiran($status, $bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kelahiran k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' AND  k.id_status = '$status'");
        return $this->db->get()->result();
    }
    //kematian
    public function tampil_kematian()
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak, r.tgl_riwayat, r.riwayat_status, k.ket_tolak
');
        $this->db->from('surat_kematian k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status = "6" or k.id_status = "7"');
        return $this->db->get()->result();
    }
    public function filter_status_kematian($status)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kematian k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_kematian($bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kematian k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        return $this->db->get()->result();
    }
    public function filter_tahun_kematian($tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kematian k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    public function filter_status_bulan_kematian($status, $bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kematian k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_status_tahun_kematian($status, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kematian k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_tahun_kematian($bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kematian k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    
    public function filter_semua_kematian($status, $bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kematian k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' AND  k.id_status = '$status'");
        return $this->db->get()->result();
    }
    //keramaian
    public function tampil_keramaian()
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_ijin_keramaian k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status = "6" or k.id_status = "7"');
        return $this->db->get()->result();
    }
    public function filter_status_keramaian($status)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_ijin_keramaian k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_keramaian($bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_ijin_keramaian k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        return $this->db->get()->result();
    }
    public function filter_tahun_keramaian($tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_ijin_keramaian k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    public function filter_status_bulan_keramaian($status, $bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_ijin_keramaian k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_status_tahun_keramaian($status, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_ijin_keramaian k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_tahun_keramaian($bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_ijin_keramaian k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    
    public function filter_semua_keramaian($status, $bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_ijin_keramaian k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' AND  k.id_status = '$status'");
        return $this->db->get()->result();
    }
    //kip
    public function tampil_kip()
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kip k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status = "6" or k.id_status = "7"');
        return $this->db->get()->result();
    }
    public function filter_status_kip($status)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kip k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_kip($bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kip k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        return $this->db->get()->result();
    }
    public function filter_tahun_kip($tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kip k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    public function filter_status_bulan_kip($status, $bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kip k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_status_tahun_kip($status, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kip k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_tahun_kip($bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kip k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    
    public function filter_semua_kip($status, $bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kip k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' AND  k.id_status = '$status'");
        return $this->db->get()->result();
    }
    // kk
    public function tampil_kk()
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak, r.tgl_riwayat, r.riwayat_status, k.ket_tolak
');
        $this->db->from('surat_perubahan_kk k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status = "6" or k.id_status = "7"');
        return $this->db->get()->result();
    }
    public function filter_status_kk($status)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_perubahan_kk k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_kk($bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_perubahan_kk k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        return $this->db->get()->result();
    }
    public function filter_tahun_kk($tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_perubahan_kk k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    public function filter_status_bulan_kk($status, $bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_perubahan_kk k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_status_tahun_kk($status, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_perubahan_kk k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_tahun_kk($bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_perubahan_kk k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    
    public function filter_semua_kk($status, $bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_perubahan_kk k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' AND  k.id_status = '$status'");
        return $this->db->get()->result();
    }
    //ktp
    public function tampil_ktp()
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak, r.tgl_riwayat, r.riwayat_status, k.ket_tolak
');
        $this->db->from('surat_permohonan_ktp k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status = "6" or k.id_status = "7"');
        return $this->db->get()->result();
    }
    public function filter_status_ktp($status)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_permohonan_ktp k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_ktp($bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_permohonan_ktp k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        return $this->db->get()->result();
    }
    public function filter_tahun_ktp($tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_permohonan_ktp k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    public function filter_status_bulan_ktp($status, $bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_permohonan_ktp k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_status_tahun_ktp($status, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_permohonan_ktp k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_tahun_ktp($bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_permohonan_ktp k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    
    public function filter_semua_ktp($status, $bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_permohonan_ktp k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' AND  k.id_status = '$status'");
        return $this->db->get()->result();
    }
    //kuasa
    public function tampil_kuasa()
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak, r.tgl_riwayat, r.riwayat_status, k.ket_tolak
');
        $this->db->from('surat_kuasa k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status = "6" or k.id_status = "7"');
        return $this->db->get()->result();
    }
    public function filter_status_kuasa($status)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kuasa k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_kuasa($bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kuasa k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        return $this->db->get()->result();
    }
    public function filter_tahun_kuasa($tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kuasa k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    public function filter_status_bulan_kuasa($status, $bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kuasa k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_status_tahun_kuasa($status, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kuasa k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_tahun_kuasa($bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kuasa k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    
    public function filter_semua_kuasa($status, $bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_kuasa k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' AND  k.id_status = '$status'");
        return $this->db->get()->result();
    }
    //skck
    public function tampil_skck()
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak, r.tgl_riwayat, r.riwayat_status, k.ket_tolak
');
        $this->db->from('surat_skck k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status = "6" or k.id_status = "7"');
        return $this->db->get()->result();
    }
    public function filter_status_skck($status)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_skck k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_skck($bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_skck k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        return $this->db->get()->result();
    }
    public function filter_tahun_skck($tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_skck k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    public function filter_status_bulan_skck($status, $bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_skck k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_status_tahun_skck($status, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_skck k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_tahun_skck($bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_skck k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    
    public function filter_semua_skck($status, $bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_skck k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' AND  k.id_status = '$status'");
        return $this->db->get()->result();
    }
    //sktm
    public function tampil_sktm()
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak, r.tgl_riwayat, r.riwayat_status, k.ket_tolak
');
        $this->db->from('surat_sktm k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status = "6" or k.id_status = "7"');
        return $this->db->get()->result();
    }
    public function filter_status_sktm($status)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_sktm k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_sktm($bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_sktm k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        return $this->db->get()->result();
    }
    public function filter_tahun_sktm($tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_sktm k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    public function filter_status_bulan_sktm($status, $bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_sktm k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_status_tahun_sktm($status, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_sktm k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_tahun_sktm($bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_sktm k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    
    public function filter_semua_sktm($status, $bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_sktm k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' AND  k.id_status = '$status'");
        return $this->db->get()->result();
    }
    //sptjm
    public function tampil_sptjm()
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak, r.tgl_riwayat, r.riwayat_status, k.ket_tolak
');
        $this->db->from('surat_sptjm k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status = "6" or k.id_status = "7"');
        return $this->db->get()->result();
    }
    public function filter_status_sptjm($status)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_sptjm k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_sptjm($bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_sptjm k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        return $this->db->get()->result();
    }
    public function filter_tahun_sptjm($tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_sptjm k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    public function filter_status_bulan_sptjm($status, $bulan)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_sptjm k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_status_tahun_sptjm($status, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_sptjm k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('YEAR(tgl_surat)',$tahun);
        $this->db->where('k.id_status',$status);
        return $this->db->get()->result();
    }
    public function filter_bulan_tahun_sptjm($bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_sptjm k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where('MONTH(tgl_surat)',$bulan);
        $this->db->where('YEAR(tgl_surat)',$tahun);
        return $this->db->get()->result();
    }
    
    public function filter_semua_sptjm($status, $bulan, $tahun)
    {
        $this->db->select('k.id_surat, nik, k.id_user, k.id_status, no_surat, tgl_surat, w.nama_lengkap, r.tgl_riwayat, r.riwayat_status, k.ket_tolak');
        $this->db->from('surat_sptjm k');
        $this->db->join('tb_warga w','k.id_user = w.id_user');
        $this->db->join('tb_riwayat r','k.id_surat = r.id_surat');
        $this->db->where("MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' AND  k.id_status = '$status'");
        return $this->db->get()->result();
    }

    //Laporan Semua Surat
    public function tampil_semua()
    {
        $query = $this->db->query("SELECT id_surat, no_surat, id_user, nik, nama_lengkap, tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM (
            SELECT surat_biodata.id_surat, no_surat, surat_biodata.id_user, nik, nama_lengkap, surat_biodata.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_biodata JOIN tb_warga ON surat_biodata.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_biodata.id_surat = tb_riwayat.id_surat WHERE surat_biodata.id_status IN ('6','7') UNION ALL
            SELECT surat_domisili.id_surat, no_surat, surat_domisili.id_user, nik, nama_lengkap, surat_domisili.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_domisili JOIN tb_warga ON surat_domisili.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_domisili.id_surat = tb_riwayat.id_surat WHERE surat_domisili.id_status IN ('6','7') UNION ALL
            SELECT surat_ijin_keramaian.id_surat, no_surat, surat_ijin_keramaian.id_user, nik, nama_lengkap, surat_ijin_keramaian.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_ijin_keramaian JOIN tb_warga ON surat_ijin_keramaian.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_ijin_keramaian.id_surat = tb_riwayat.id_surat WHERE surat_ijin_keramaian.id_status IN ('6','7') UNION ALL
            SELECT surat_kelahiran.id_surat, no_surat, surat_kelahiran.id_user, nik, nama_lengkap, surat_kelahiran.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kelahiran JOIN tb_warga ON surat_kelahiran.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kelahiran.id_surat = tb_riwayat.id_surat WHERE surat_kelahiran.id_status IN ('6','7') UNION ALL
            SELECT surat_kematian.id_surat, no_surat, surat_kematian.id_user, nik, nama_lengkap, surat_kematian.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kematian JOIN tb_warga ON surat_kematian.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kematian.id_surat = tb_riwayat.id_surat WHERE surat_kematian.id_status IN ('6','7') UNION ALL
            SELECT surat_kip.id_surat, no_surat, surat_kip.id_user, nik, nama_lengkap, surat_kip.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kip JOIN tb_warga ON surat_kip.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kip.id_surat = tb_riwayat.id_surat WHERE surat_kip.id_status IN ('6','7') UNION ALL
            SELECT surat_kuasa.id_surat, no_surat, surat_kuasa.id_user, nik, nama_lengkap, surat_kuasa.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kuasa JOIN tb_warga ON surat_kuasa.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kuasa.id_surat = tb_riwayat.id_surat WHERE surat_kuasa.id_status IN ('6','7') UNION ALL
            SELECT surat_permohonan_ktp.id_surat, no_surat, surat_permohonan_ktp.id_user, nik, nama_lengkap, surat_permohonan_ktp.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_permohonan_ktp JOIN tb_warga ON surat_permohonan_ktp.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_permohonan_ktp.id_surat = tb_riwayat.id_surat WHERE surat_permohonan_ktp.id_status IN ('6','7') UNION ALL
            SELECT surat_perubahan_kk.id_surat, no_surat, surat_perubahan_kk.id_user, nik, nama_lengkap, surat_perubahan_kk.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_perubahan_kk JOIN tb_warga ON surat_perubahan_kk.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_perubahan_kk.id_surat = tb_riwayat.id_surat WHERE surat_perubahan_kk.id_status IN ('6','7') UNION ALL
            SELECT surat_skck.id_surat, no_surat, surat_skck.id_user, nik, nama_lengkap, surat_skck.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_skck JOIN tb_warga ON surat_skck.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_skck.id_surat = tb_riwayat.id_surat WHERE surat_skck.id_status IN ('6','7') UNION ALL
            SELECT surat_sktm.id_surat, no_surat, surat_sktm.id_user, nik, nama_lengkap, surat_sktm.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_sktm JOIN tb_warga ON surat_sktm.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_sktm.id_surat = tb_riwayat.id_surat WHERE surat_sktm.id_status IN ('6','7') UNION ALL
            SELECT surat_sptjm.id_surat, no_surat, surat_sptjm.id_user, nik, nama_lengkap, surat_sptjm.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_sptjm JOIN tb_warga ON surat_sptjm.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_sptjm.id_surat = tb_riwayat.id_surat WHERE surat_sptjm.id_status IN ('6','7')
        )X  ORDER BY tgl_surat ASC");
        return $query->result();
    }
    public function filter_status($status)
    {
        $query = $this->db->query("SELECT id_surat, no_surat, id_user, nik, nama_lengkap, tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM (
            SELECT surat_biodata.id_surat, no_surat, surat_biodata.id_user, nik, nama_lengkap, surat_biodata.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_biodata JOIN tb_warga ON surat_biodata.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_biodata.id_surat = tb_riwayat.id_surat WHERE surat_biodata.id_status = '$status' UNION ALL
            SELECT surat_domisili.id_surat, no_surat, surat_domisili.id_user, nik, nama_lengkap, surat_domisili.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_domisili JOIN tb_warga ON surat_domisili.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_domisili.id_surat = tb_riwayat.id_surat WHERE surat_domisili.id_status = '$status' UNION ALL
            SELECT surat_ijin_keramaian.id_surat, no_surat, surat_ijin_keramaian.id_user, nik, nama_lengkap, surat_ijin_keramaian.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_ijin_keramaian JOIN tb_warga ON surat_ijin_keramaian.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_ijin_keramaian.id_surat = tb_riwayat.id_surat WHERE surat_ijin_keramaian.id_status = '$status' UNION ALL
            SELECT surat_kelahiran.id_surat, no_surat, surat_kelahiran.id_user, nik, nama_lengkap, surat_kelahiran.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kelahiran JOIN tb_warga ON surat_kelahiran.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kelahiran.id_surat = tb_riwayat.id_surat WHERE surat_kelahiran.id_status = '$status' UNION ALL
            SELECT surat_kematian.id_surat, no_surat, surat_kematian.id_user, nik, nama_lengkap, surat_kematian.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kematian JOIN tb_warga ON surat_kematian.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kematian.id_surat = tb_riwayat.id_surat WHERE surat_kematian.id_status = '$status' UNION ALL
            SELECT surat_kip.id_surat, no_surat, surat_kip.id_user, nik, nama_lengkap, surat_kip.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kip JOIN tb_warga ON surat_kip.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kip.id_surat = tb_riwayat.id_surat WHERE surat_kip.id_status = '$status' UNION ALL
            SELECT surat_kuasa.id_surat, no_surat, surat_kuasa.id_user, nik, nama_lengkap, surat_kuasa.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kuasa JOIN tb_warga ON surat_kuasa.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kuasa.id_surat = tb_riwayat.id_surat WHERE surat_kuasa.id_status = '$status' UNION ALL
            SELECT surat_permohonan_ktp.id_surat, no_surat, surat_permohonan_ktp.id_user, nik, nama_lengkap, surat_permohonan_ktp.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_permohonan_ktp JOIN tb_warga ON surat_permohonan_ktp.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_permohonan_ktp.id_surat = tb_riwayat.id_surat WHERE surat_permohonan_ktp.id_status = '$status' UNION ALL
            SELECT surat_perubahan_kk.id_surat, no_surat, surat_perubahan_kk.id_user, nik, nama_lengkap, surat_perubahan_kk.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_perubahan_kk JOIN tb_warga ON surat_perubahan_kk.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_perubahan_kk.id_surat = tb_riwayat.id_surat WHERE surat_perubahan_kk.id_status = '$status' UNION ALL
            SELECT surat_skck.id_surat, no_surat, surat_skck.id_user, nik, nama_lengkap, surat_skck.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_skck JOIN tb_warga ON surat_skck.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_skck.id_surat = tb_riwayat.id_surat WHERE surat_skck.id_status = '$status' UNION ALL
            SELECT surat_sktm.id_surat, no_surat, surat_sktm.id_user, nik, nama_lengkap, surat_sktm.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_sktm JOIN tb_warga ON surat_sktm.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_sktm.id_surat = tb_riwayat.id_surat WHERE surat_sktm.id_status = '$status' UNION ALL
            SELECT surat_sptjm.id_surat, no_surat, surat_sptjm.id_user, nik, nama_lengkap, surat_sptjm.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_sptjm JOIN tb_warga ON surat_sptjm.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_sptjm.id_surat = tb_riwayat.id_surat WHERE surat_sptjm.id_status = '$status'
        )X  ORDER BY tgl_surat ASC");
        return $query->result();
    }
    public function filter_bulan($bulan)
    {
        $query = $this->db->query("SELECT id_surat, no_surat, id_user, nik, nama_lengkap, tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM (
            SELECT surat_biodata.id_surat, no_surat, surat_biodata.id_user, nik, nama_lengkap, surat_biodata.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_biodata JOIN tb_warga ON surat_biodata.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_biodata.id_surat = tb_riwayat.id_surat WHERE surat_biodata.id_status IN ('6','7') AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT surat_domisili.id_surat, no_surat, surat_domisili.id_user, nik, nama_lengkap, surat_domisili.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_domisili JOIN tb_warga ON surat_domisili.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_domisili.id_surat = tb_riwayat.id_surat WHERE surat_domisili.id_status IN ('6','7') AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT surat_ijin_keramaian.id_surat, no_surat, surat_ijin_keramaian.id_user, nik, nama_lengkap, surat_ijin_keramaian.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_ijin_keramaian JOIN tb_warga ON surat_ijin_keramaian.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_ijin_keramaian.id_surat = tb_riwayat.id_surat WHERE surat_ijin_keramaian.id_status IN ('6','7') AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT surat_kelahiran.id_surat, no_surat, surat_kelahiran.id_user, nik, nama_lengkap, surat_kelahiran.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kelahiran JOIN tb_warga ON surat_kelahiran.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kelahiran.id_surat = tb_riwayat.id_surat WHERE surat_kelahiran.id_status IN ('6','7') AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT surat_kematian.id_surat, no_surat, surat_kematian.id_user, nik, nama_lengkap, surat_kematian.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kematian JOIN tb_warga ON surat_kematian.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kematian.id_surat = tb_riwayat.id_surat WHERE surat_kematian.id_status IN ('6','7') AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT surat_kip.id_surat, no_surat, surat_kip.id_user, nik, nama_lengkap, surat_kip.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kip JOIN tb_warga ON surat_kip.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kip.id_surat = tb_riwayat.id_surat WHERE surat_kip.id_status IN ('6','7') AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT surat_kuasa.id_surat, no_surat, surat_kuasa.id_user, nik, nama_lengkap, surat_kuasa.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kuasa JOIN tb_warga ON surat_kuasa.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kuasa.id_surat = tb_riwayat.id_surat WHERE surat_kuasa.id_status IN ('6','7') AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT surat_permohonan_ktp.id_surat, no_surat, surat_permohonan_ktp.id_user, nik, nama_lengkap, surat_permohonan_ktp.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_permohonan_ktp JOIN tb_warga ON surat_permohonan_ktp.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_permohonan_ktp.id_surat = tb_riwayat.id_surat WHERE surat_permohonan_ktp.id_status IN ('6','7') AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT surat_perubahan_kk.id_surat, no_surat, surat_perubahan_kk.id_user, nik, nama_lengkap, surat_perubahan_kk.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_perubahan_kk JOIN tb_warga ON surat_perubahan_kk.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_perubahan_kk.id_surat = tb_riwayat.id_surat WHERE surat_perubahan_kk.id_status IN ('6','7') AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT surat_skck.id_surat, no_surat, surat_skck.id_user, nik, nama_lengkap, surat_skck.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_skck JOIN tb_warga ON surat_skck.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_skck.id_surat = tb_riwayat.id_surat WHERE surat_skck.id_status IN ('6','7') AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT surat_sktm.id_surat, no_surat, surat_sktm.id_user, nik, nama_lengkap, surat_sktm.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_sktm JOIN tb_warga ON surat_sktm.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_sktm.id_surat = tb_riwayat.id_surat WHERE surat_sktm.id_status IN ('6','7') AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT surat_sptjm.id_surat, no_surat, surat_sptjm.id_user, nik, nama_lengkap, surat_sptjm.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_sptjm JOIN tb_warga ON surat_sptjm.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_sptjm.id_surat = tb_riwayat.id_surat WHERE surat_sptjm.id_status IN ('6','7') AND MONTH(tgl_surat) = '$bulan'
        )X  ORDER BY tgl_surat ASC");
        return $query->result();
    }
    public function filter_tahun($tahun)
    {
        $query = $this->db->query("SELECT id_surat, no_surat, id_user, nik, nama_lengkap, tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM (
            SELECT surat_biodata.id_surat, no_surat, surat_biodata.id_user, nik, nama_lengkap, surat_biodata.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_biodata JOIN tb_warga ON surat_biodata.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_biodata.id_surat = tb_riwayat.id_surat WHERE surat_biodata.id_status IN ('6','7') AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_domisili.id_surat, no_surat, surat_domisili.id_user, nik, nama_lengkap, surat_domisili.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_domisili JOIN tb_warga ON surat_domisili.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_domisili.id_surat = tb_riwayat.id_surat WHERE surat_domisili.id_status IN ('6','7') AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_ijin_keramaian.id_surat, no_surat, surat_ijin_keramaian.id_user, nik, nama_lengkap, surat_ijin_keramaian.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_ijin_keramaian JOIN tb_warga ON surat_ijin_keramaian.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_ijin_keramaian.id_surat = tb_riwayat.id_surat WHERE surat_ijin_keramaian.id_status IN ('6','7') AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_kelahiran.id_surat, no_surat, surat_kelahiran.id_user, nik, nama_lengkap, surat_kelahiran.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kelahiran JOIN tb_warga ON surat_kelahiran.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kelahiran.id_surat = tb_riwayat.id_surat WHERE surat_kelahiran.id_status IN ('6','7') AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_kematian.id_surat, no_surat, surat_kematian.id_user, nik, nama_lengkap, surat_kematian.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kematian JOIN tb_warga ON surat_kematian.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kematian.id_surat = tb_riwayat.id_surat WHERE surat_kematian.id_status IN ('6','7') AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_kip.id_surat, no_surat, surat_kip.id_user, nik, nama_lengkap, surat_kip.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kip JOIN tb_warga ON surat_kip.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kip.id_surat = tb_riwayat.id_surat WHERE surat_kip.id_status IN ('6','7') AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_kuasa.id_surat, no_surat, surat_kuasa.id_user, nik, nama_lengkap, surat_kuasa.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kuasa JOIN tb_warga ON surat_kuasa.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kuasa.id_surat = tb_riwayat.id_surat WHERE surat_kuasa.id_status IN ('6','7') AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_permohonan_ktp.id_surat, no_surat, surat_permohonan_ktp.id_user, nik, nama_lengkap, surat_permohonan_ktp.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_permohonan_ktp JOIN tb_warga ON surat_permohonan_ktp.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_permohonan_ktp.id_surat = tb_riwayat.id_surat WHERE surat_permohonan_ktp.id_status IN ('6','7') AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_perubahan_kk.id_surat, no_surat, surat_perubahan_kk.id_user, nik, nama_lengkap, surat_perubahan_kk.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_perubahan_kk JOIN tb_warga ON surat_perubahan_kk.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_perubahan_kk.id_surat = tb_riwayat.id_surat WHERE surat_perubahan_kk.id_status IN ('6','7') AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_skck.id_surat, no_surat, surat_skck.id_user, nik, nama_lengkap, surat_skck.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_skck JOIN tb_warga ON surat_skck.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_skck.id_surat = tb_riwayat.id_surat WHERE surat_skck.id_status IN ('6','7') AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_sktm.id_surat, no_surat, surat_sktm.id_user, nik, nama_lengkap, surat_sktm.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_sktm JOIN tb_warga ON surat_sktm.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_sktm.id_surat = tb_riwayat.id_surat WHERE surat_sktm.id_status IN ('6','7') AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_sptjm.id_surat, no_surat, surat_sptjm.id_user, nik, nama_lengkap, surat_sptjm.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_sptjm JOIN tb_warga ON surat_sptjm.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_sptjm.id_surat = tb_riwayat.id_surat WHERE surat_sptjm.id_status IN ('6','7') AND YEAR(tgl_surat) = '$tahun'
        )X  ORDER BY tgl_surat ASC");
        return $query->result();
    }
    public function filter_status_bulan($status, $bulan)
    {
        $query = $this->db->query("SELECT id_surat, no_surat, id_user, nik, nama_lengkap, tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM (
            SELECT surat_biodata.id_surat, no_surat, surat_biodata.id_user, nik, nama_lengkap, surat_biodata.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_biodata JOIN tb_warga ON surat_biodata.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_biodata.id_surat = tb_riwayat.id_surat WHERE surat_biodata.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT surat_domisili.id_surat, no_surat, surat_domisili.id_user, nik, nama_lengkap, surat_domisili.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_domisili JOIN tb_warga ON surat_domisili.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_domisili.id_surat = tb_riwayat.id_surat WHERE surat_domisili.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT surat_ijin_keramaian.id_surat, no_surat, surat_ijin_keramaian.id_user, nik, nama_lengkap, surat_ijin_keramaian.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_ijin_keramaian JOIN tb_warga ON surat_ijin_keramaian.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_ijin_keramaian.id_surat = tb_riwayat.id_surat WHERE surat_ijin_keramaian.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT surat_kelahiran.id_surat, no_surat, surat_kelahiran.id_user, nik, nama_lengkap, surat_kelahiran.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kelahiran JOIN tb_warga ON surat_kelahiran.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kelahiran.id_surat = tb_riwayat.id_surat WHERE surat_kelahiran.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT surat_kematian.id_surat, no_surat, surat_kematian.id_user, nik, nama_lengkap, surat_kematian.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kematian JOIN tb_warga ON surat_kematian.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kematian.id_surat = tb_riwayat.id_surat WHERE surat_kematian.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT surat_kip.id_surat, no_surat, surat_kip.id_user, nik, nama_lengkap, surat_kip.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kip JOIN tb_warga ON surat_kip.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kip.id_surat = tb_riwayat.id_surat WHERE surat_kip.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT surat_kuasa.id_surat, no_surat, surat_kuasa.id_user, nik, nama_lengkap, surat_kuasa.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kuasa JOIN tb_warga ON surat_kuasa.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kuasa.id_surat = tb_riwayat.id_surat WHERE surat_kuasa.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT surat_permohonan_ktp.id_surat, no_surat, surat_permohonan_ktp.id_user, nik, nama_lengkap, surat_permohonan_ktp.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_permohonan_ktp JOIN tb_warga ON surat_permohonan_ktp.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_permohonan_ktp.id_surat = tb_riwayat.id_surat WHERE surat_permohonan_ktp.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT surat_perubahan_kk.id_surat, no_surat, surat_perubahan_kk.id_user, nik, nama_lengkap, surat_perubahan_kk.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_perubahan_kk JOIN tb_warga ON surat_perubahan_kk.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_perubahan_kk.id_surat = tb_riwayat.id_surat WHERE surat_perubahan_kk.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT surat_skck.id_surat, no_surat, surat_skck.id_user, nik, nama_lengkap, surat_skck.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_skck JOIN tb_warga ON surat_skck.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_skck.id_surat = tb_riwayat.id_surat WHERE surat_skck.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT surat_sktm.id_surat, no_surat, surat_sktm.id_user, nik, nama_lengkap, surat_sktm.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_sktm JOIN tb_warga ON surat_sktm.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_sktm.id_surat = tb_riwayat.id_surat WHERE surat_sktm.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' UNION ALL
            SELECT surat_sptjm.id_surat, no_surat, surat_sptjm.id_user, nik, nama_lengkap, surat_sptjm.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_sptjm JOIN tb_warga ON surat_sptjm.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_sptjm.id_surat = tb_riwayat.id_surat WHERE surat_sptjm.id_status = '$status' AND MONTH(tgl_surat) = '$bulan'
        )X  ORDER BY tgl_surat ASC");
        return $query->result();
    }
    public function filter_status_tahun($status, $tahun)
    {
        $query = $this->db->query("SELECT id_surat, no_surat, id_user, nik, nama_lengkap, tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM (
            SELECT surat_biodata.id_surat, no_surat, surat_biodata.id_user, nik, nama_lengkap, surat_biodata.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_biodata JOIN tb_warga ON surat_biodata.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_biodata.id_surat = tb_riwayat.id_surat WHERE surat_biodata.id_status = '$status' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_domisili.id_surat, no_surat, surat_domisili.id_user, nik, nama_lengkap, surat_domisili.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_domisili JOIN tb_warga ON surat_domisili.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_domisili.id_surat = tb_riwayat.id_surat WHERE surat_domisili.id_status = '$status' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_ijin_keramaian.id_surat, no_surat, surat_ijin_keramaian.id_user, nik, nama_lengkap, surat_ijin_keramaian.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_ijin_keramaian JOIN tb_warga ON surat_ijin_keramaian.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_ijin_keramaian.id_surat = tb_riwayat.id_surat WHERE surat_ijin_keramaian.id_status = '$status' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_kelahiran.id_surat, no_surat, surat_kelahiran.id_user, nik, nama_lengkap, surat_kelahiran.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kelahiran JOIN tb_warga ON surat_kelahiran.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kelahiran.id_surat = tb_riwayat.id_surat WHERE surat_kelahiran.id_status = '$status' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_kematian.id_surat, no_surat, surat_kematian.id_user, nik, nama_lengkap, surat_kematian.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kematian JOIN tb_warga ON surat_kematian.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kematian.id_surat = tb_riwayat.id_surat WHERE surat_kematian.id_status = '$status' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_kip.id_surat, no_surat, surat_kip.id_user, nik, nama_lengkap, surat_kip.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kip JOIN tb_warga ON surat_kip.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kip.id_surat = tb_riwayat.id_surat WHERE surat_kip.id_status = '$status' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_kuasa.id_surat, no_surat, surat_kuasa.id_user, nik, nama_lengkap, surat_kuasa.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kuasa JOIN tb_warga ON surat_kuasa.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kuasa.id_surat = tb_riwayat.id_surat WHERE surat_kuasa.id_status = '$status' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_permohonan_ktp.id_surat, no_surat, surat_permohonan_ktp.id_user, nik, nama_lengkap, surat_permohonan_ktp.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_permohonan_ktp JOIN tb_warga ON surat_permohonan_ktp.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_permohonan_ktp.id_surat = tb_riwayat.id_surat WHERE surat_permohonan_ktp.id_status = '$status' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_perubahan_kk.id_surat, no_surat, surat_perubahan_kk.id_user, nik, nama_lengkap, surat_perubahan_kk.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_perubahan_kk JOIN tb_warga ON surat_perubahan_kk.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_perubahan_kk.id_surat = tb_riwayat.id_surat WHERE surat_perubahan_kk.id_status = '$status' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_skck.id_surat, no_surat, surat_skck.id_user, nik, nama_lengkap, surat_skck.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_skck JOIN tb_warga ON surat_skck.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_skck.id_surat = tb_riwayat.id_surat WHERE surat_skck.id_status = '$status' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_sktm.id_surat, no_surat, surat_sktm.id_user, nik, nama_lengkap, surat_sktm.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_sktm JOIN tb_warga ON surat_sktm.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_sktm.id_surat = tb_riwayat.id_surat WHERE surat_sktm.id_status = '$status' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_sptjm.id_surat, no_surat, surat_sptjm.id_user, nik, nama_lengkap, surat_sptjm.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_sptjm JOIN tb_warga ON surat_sptjm.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_sptjm.id_surat = tb_riwayat.id_surat WHERE surat_sptjm.id_status = '$status' AND YEAR(tgl_surat) = '$tahun'
        )X  ORDER BY tgl_surat ASC");
        return $query->result();
    }
    public function filter_bulan_tahun($bulan, $tahun)
    {
        $query = $this->db->query("SELECT id_surat, no_surat, id_user, nik, nama_lengkap, tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM (
            SELECT surat_biodata.id_surat, no_surat, surat_biodata.id_user, nik, nama_lengkap, surat_biodata.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_biodata JOIN tb_warga ON surat_biodata.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_biodata.id_surat = tb_riwayat.id_surat WHERE MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_domisili.id_surat, no_surat, surat_domisili.id_user, nik, nama_lengkap, surat_domisili.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_domisili JOIN tb_warga ON surat_domisili.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_domisili.id_surat = tb_riwayat.id_surat WHERE MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_ijin_keramaian.id_surat, no_surat, surat_ijin_keramaian.id_user, nik, nama_lengkap, surat_ijin_keramaian.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_ijin_keramaian JOIN tb_warga ON surat_ijin_keramaian.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_ijin_keramaian.id_surat = tb_riwayat.id_surat WHERE MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_kelahiran.id_surat, no_surat, surat_kelahiran.id_user, nik, nama_lengkap, surat_kelahiran.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kelahiran JOIN tb_warga ON surat_kelahiran.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kelahiran.id_surat = tb_riwayat.id_surat WHERE MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_kematian.id_surat, no_surat, surat_kematian.id_user, nik, nama_lengkap, surat_kematian.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kematian JOIN tb_warga ON surat_kematian.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kematian.id_surat = tb_riwayat.id_surat WHERE MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_kip.id_surat, no_surat, surat_kip.id_user, nik, nama_lengkap, surat_kip.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kip JOIN tb_warga ON surat_kip.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kip.id_surat = tb_riwayat.id_surat WHERE MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_kuasa.id_surat, no_surat, surat_kuasa.id_user, nik, nama_lengkap, surat_kuasa.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kuasa JOIN tb_warga ON surat_kuasa.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kuasa.id_surat = tb_riwayat.id_surat WHERE MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_permohonan_ktp.id_surat, no_surat, surat_permohonan_ktp.id_user, nik, nama_lengkap, surat_permohonan_ktp.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_permohonan_ktp JOIN tb_warga ON surat_permohonan_ktp.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_permohonan_ktp.id_surat = tb_riwayat.id_surat WHERE MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_perubahan_kk.id_surat, no_surat, surat_perubahan_kk.id_user, nik, nama_lengkap, surat_perubahan_kk.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_perubahan_kk JOIN tb_warga ON surat_perubahan_kk.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_perubahan_kk.id_surat = tb_riwayat.id_surat WHERE MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_skck.id_surat, no_surat, surat_skck.id_user, nik, nama_lengkap, surat_skck.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_skck JOIN tb_warga ON surat_skck.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_skck.id_surat = tb_riwayat.id_surat WHERE MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_sktm.id_surat, no_surat, surat_sktm.id_user, nik, nama_lengkap, surat_sktm.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_sktm JOIN tb_warga ON surat_sktm.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_sktm.id_surat = tb_riwayat.id_surat WHERE MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_sptjm.id_surat, no_surat, surat_sptjm.id_user, nik, nama_lengkap, surat_sptjm.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_sptjm JOIN tb_warga ON surat_sptjm.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_sptjm.id_surat = tb_riwayat.id_surat WHERE MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun'
        )X  ORDER BY tgl_surat ASC");
        return $query->result();
    }
    public function filter_semua($status, $bulan, $tahun)
    {
        $query = $this->db->query("SELECT id_surat, no_surat, id_user, nik, nama_lengkap, tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM (
            SELECT surat_biodata.id_surat, no_surat, surat_biodata.id_user, nik, nama_lengkap, surat_biodata.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_biodata JOIN tb_warga ON surat_biodata.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_biodata.id_surat = tb_riwayat.id_surat WHERE surat_biodata.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_domisili.id_surat, no_surat, surat_domisili.id_user, nik, nama_lengkap, surat_domisili.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_domisili JOIN tb_warga ON surat_domisili.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_domisili.id_surat = tb_riwayat.id_surat WHERE surat_domisili.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_ijin_keramaian.id_surat, no_surat, surat_ijin_keramaian.id_user, nik, nama_lengkap, surat_ijin_keramaian.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_ijin_keramaian JOIN tb_warga ON surat_ijin_keramaian.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_ijin_keramaian.id_surat = tb_riwayat.id_surat WHERE surat_ijin_keramaian.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_kelahiran.id_surat, no_surat, surat_kelahiran.id_user, nik, nama_lengkap, surat_kelahiran.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kelahiran JOIN tb_warga ON surat_kelahiran.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kelahiran.id_surat = tb_riwayat.id_surat WHERE surat_kelahiran.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_kematian.id_surat, no_surat, surat_kematian.id_user, nik, nama_lengkap, surat_kematian.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kematian JOIN tb_warga ON surat_kematian.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kematian.id_surat = tb_riwayat.id_surat WHERE surat_kematian.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_kip.id_surat, no_surat, surat_kip.id_user, nik, nama_lengkap, surat_kip.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kip JOIN tb_warga ON surat_kip.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kip.id_surat = tb_riwayat.id_surat WHERE surat_kip.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_kuasa.id_surat, no_surat, surat_kuasa.id_user, nik, nama_lengkap, surat_kuasa.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_kuasa JOIN tb_warga ON surat_kuasa.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_kuasa.id_surat = tb_riwayat.id_surat WHERE surat_kuasa.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_permohonan_ktp.id_surat, no_surat, surat_permohonan_ktp.id_user, nik, nama_lengkap, surat_permohonan_ktp.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_permohonan_ktp JOIN tb_warga ON surat_permohonan_ktp.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_permohonan_ktp.id_surat = tb_riwayat.id_surat WHERE surat_permohonan_ktp.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_perubahan_kk.id_surat, no_surat, surat_perubahan_kk.id_user, nik, nama_lengkap, surat_perubahan_kk.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_perubahan_kk JOIN tb_warga ON surat_perubahan_kk.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_perubahan_kk.id_surat = tb_riwayat.id_surat WHERE surat_perubahan_kk.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_skck.id_surat, no_surat, surat_skck.id_user, nik, nama_lengkap, surat_skck.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_skck JOIN tb_warga ON surat_skck.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_skck.id_surat = tb_riwayat.id_surat WHERE surat_skck.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_sktm.id_surat, no_surat, surat_sktm.id_user, nik, nama_lengkap, surat_sktm.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_sktm JOIN tb_warga ON surat_sktm.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_sktm.id_surat = tb_riwayat.id_surat WHERE surat_sktm.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun' UNION ALL
            SELECT surat_sptjm.id_surat, no_surat, surat_sptjm.id_user, nik, nama_lengkap, surat_sptjm.tgl_surat, tgl_riwayat, riwayat_status, ket_tolak FROM surat_sptjm JOIN tb_warga ON surat_sptjm.id_user = tb_warga.id_user JOIN tb_riwayat ON surat_sptjm.id_surat = tb_riwayat.id_surat WHERE surat_sptjm.id_status = '$status' AND MONTH(tgl_surat) = '$bulan' AND YEAR(tgl_surat) = '$tahun'
        )X  ORDER BY tgl_surat ASC");
        return $query->result();
    }

}