<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PerangkatDesa_model extends CI_Model {

    //kades
    public function tampil_kepala_desa()
    {

        $this->db->select('*');
        $this->db->from('tb_perangkat_desa');
        $this->db->where('jabatan','kades');
        return $this->db->get()->result();

    }
    public function tampil_camat()
    {

        $this->db->select('*');
        $this->db->from('tb_perangkat_desa');
        $this->db->where('jabatan','camat');
        return $this->db->get()->result();

    }
    public function tampil_kapolsek()
    {

        $this->db->select('*');
        $this->db->from('tb_perangkat_desa');
        $this->db->where('jabatan','kapolsek');
        return $this->db->get()->result();

    }
    public function edit_kades($id)
    {
        $this->db->select('*');
        $this->db->from('tb_perangkat_desa');
        $this->db->where('id_perangkat',$id);
        return $this->db->get()->result();
    }
    //camat
    public function edit_camat($id)
    {
        $this->db->select('*');
        $this->db->from('tb_perangkat_desa');
        $this->db->where('id_perangkat',$id);
        return $this->db->get()->result();
    }
    //kapolsek
    
    public function edit_kapolsek($id)
    {
        $this->db->select('*');
        $this->db->from('tb_perangkat_desa');
        $this->db->where('id_perangkat',$id);
        return $this->db->get()->result();
    }
    
    public function update($data, $id)
    {
        $this->db->update("tb_perangkat_desa", $data, $id);
    }
}