<?php
class Login_model extends CI_Model{
    public function auth_login($username,$password){
        $query=$this->db->query("SELECT u.id_user, username, PASSWORD, level
        FROM tb_user u
        WHERE username='$username' AND PASSWORD = '$password'");
        return $query;
    }
    public function search($keyword)
    {
        $query = $this->db->query("SELECT id_surat, nik, no_surat, id_user, nama_lengkap, tgl_surat, id_status, status_monitoring FROM (
            SELECT id_surat,nik, no_surat, surat_biodata.id_user, nama_lengkap, tgl_surat, surat_biodata.id_status, status_monitoring FROM surat_biodata JOIN tb_warga ON surat_biodata.id_user = tb_warga.id_user JOIN tb_status ON surat_biodata.id_status = tb_status.id_status WHERE surat_biodata.no_surat LIKE  '$keyword' UNION ALL
            SELECT id_surat,nik, no_surat, surat_domisili.id_user, nama_lengkap, tgl_surat, surat_domisili.id_status, status_monitoring FROM surat_domisili JOIN tb_warga ON surat_domisili.id_user = tb_warga.id_user JOIN tb_status ON surat_domisili.id_status = tb_status.id_status WHERE surat_domisili.no_surat LIKE  '$keyword' UNION ALL
            SELECT id_surat,nik, no_surat, surat_ijin_keramaian.id_user, nama_lengkap, tgl_surat, surat_ijin_keramaian.id_status, status_monitoring FROM surat_ijin_keramaian JOIN tb_warga ON surat_ijin_keramaian.id_user = tb_warga.id_user JOIN tb_status ON surat_ijin_keramaian.id_status = tb_status.id_status WHERE surat_ijin_keramaian.no_surat LIKE  '$keyword' UNION ALL
            SELECT id_surat,nik, no_surat, surat_kelahiran.id_user, nama_lengkap, tgl_surat, surat_kelahiran.id_status, status_monitoring FROM surat_kelahiran JOIN tb_warga ON surat_kelahiran.id_user = tb_warga.id_user JOIN tb_status ON surat_kelahiran.id_status = tb_status.id_status WHERE surat_kelahiran.no_surat LIKE  '$keyword' UNION ALL
            SELECT id_surat,nik, no_surat, surat_kematian.id_user, nama_lengkap, tgl_surat, surat_kematian.id_status, status_monitoring FROM surat_kematian JOIN tb_warga ON surat_kematian.id_user = tb_warga.id_user JOIN tb_status ON surat_kematian.id_status = tb_status.id_status WHERE surat_kematian.no_surat LIKE  '$keyword' UNION ALL
            SELECT id_surat,nik, no_surat, surat_kip.id_user, nama_lengkap, tgl_surat, surat_kip.id_status, status_monitoring FROM surat_kip JOIN tb_warga ON surat_kip.id_user = tb_warga.id_user JOIN tb_status ON surat_kip.id_status = tb_status.id_status WHERE surat_kip.no_surat LIKE  '$keyword' UNION ALL
            SELECT id_surat,nik, no_surat, surat_kuasa.id_user, nama_lengkap, tgl_surat, surat_kuasa.id_status, status_monitoring FROM surat_kuasa JOIN tb_warga ON surat_kuasa.id_user = tb_warga.id_user JOIN tb_status ON surat_kuasa.id_status = tb_status.id_status WHERE surat_kuasa.no_surat LIKE  '$keyword' UNION ALL
            SELECT id_surat,nik, no_surat, surat_permohonan_ktp.id_user, nama_lengkap, tgl_surat, surat_permohonan_ktp.id_status, status_monitoring FROM surat_permohonan_ktp JOIN tb_warga ON surat_permohonan_ktp.id_user = tb_warga.id_user JOIN tb_status ON surat_permohonan_ktp.id_status = tb_status.id_status WHERE surat_permohonan_ktp.no_surat LIKE  '$keyword' UNION ALL
            SELECT id_surat,nik, no_surat, surat_perubahan_kk.id_user, nama_lengkap, tgl_surat, surat_perubahan_kk.id_status, status_monitoring FROM surat_perubahan_kk JOIN tb_warga ON surat_perubahan_kk.id_user = tb_warga.id_user JOIN tb_status ON surat_perubahan_kk.id_status = tb_status.id_status WHERE surat_perubahan_kk.no_surat LIKE  '$keyword' UNION ALL
            SELECT id_surat,nik, no_surat, surat_skck.id_user, nama_lengkap, tgl_surat, surat_skck.id_status, status_monitoring FROM surat_skck JOIN tb_warga ON surat_skck.id_user = tb_warga.id_user JOIN tb_status ON surat_skck.id_status = tb_status.id_status WHERE surat_skck.no_surat LIKE  '$keyword' UNION ALL
            SELECT id_surat,nik, no_surat, surat_sktm.id_user, nama_lengkap, tgl_surat, surat_sktm.id_status, status_monitoring FROM surat_sktm JOIN tb_warga ON surat_sktm.id_user = tb_warga.id_user JOIN tb_status ON surat_sktm.id_status = tb_status.id_status WHERE surat_sktm.no_surat LIKE  '$keyword' UNION ALL
            SELECT id_surat,nik, no_surat, surat_sptjm.id_user, nama_lengkap, tgl_surat, surat_sptjm.id_status, status_monitoring FROM surat_sptjm JOIN tb_warga ON surat_sptjm.id_user = tb_warga.id_user JOIN tb_status ON surat_sptjm.id_status = tb_status.id_status WHERE surat_sptjm.no_surat LIKE  '$keyword'
        )X");
        return $query->result();
    }
    public function riwayat($keyword)
    {
        $this->db->select('r.id_surat, no_surat, tgl_riwayat, riwayat_status, tb_status.status_monitoring');
        $this->db->from('tb_riwayat r');
        $this->db->join('search','r.id_surat = search.id_surat');
        $this->db->join('tb_status','r.riwayat_status = tb_status.id_status');
        $this->db->where('no_surat',$keyword);
        $this->db->order_by('tgl_riwayat DESC');
        return $this->db->get()->result();
    }
}