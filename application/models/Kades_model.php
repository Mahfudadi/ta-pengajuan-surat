<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kades_model extends CI_Model {

    public function tampil_data()
    {

        $this->db->select('*');
        $this->db->from('tb_perangkat_desa');
        return $this->db->get()->result();
    }
    public function detail_kades($id)
    {
        $this->db->select('*');
        $this->db->from('tb_perangkat_desa r');
        $this->db->where('r.id_perangkat',$id);
        return $this->db->get()->result();
    }
    public function edit_kades($id)
    {
        $this->db->select('*');
        $this->db->from('tb_perangkat_desa');
        $this->db->where('id_perangkat',$id);
        return $this->db->get()->result();
    }
    public function update_kades($data, $id)
    {
        $this->db->update("tb_perangkat_desa", $data, $id);
    }
    public function profil($id)
    {
        $this->db->select('*');
        $this->db->from('tb_perangkat_desa');
        $this->db->where('id_perangkat',$id);

        
        return $this->db->get()->result();
    }
    public function edit_profil($id)
    {
        $this->db->select('*');
        $this->db->from('tb_perangkat_desa');
        $this->db->where('id_perangkat',$id);
        return $this->db->get()->result();
    }
    public function total_ttd()
    {
        $query = $this->db->query("SELECT SUM(total) AS total FROM (
            SELECT COUNT('id_status') AS total FROM surat_biodata WHERE id_status = '5' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_domisili WHERE id_status = '5' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_ijin_keramaian WHERE id_status = '5' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kelahiran WHERE id_status = '5' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kematian WHERE id_status = '5' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kip WHERE id_status = '5' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kuasa WHERE id_status = '5' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_permohonan_ktp WHERE id_status = '5' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_perubahan_kk WHERE id_status = '5' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_skck WHERE id_status = '5' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_sktm WHERE id_status = '5' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_sptjm WHERE id_status = '5'
        )X");
        return $query->result();
    }
    public function total_belumttd()
    {
        $query = $this->db->query("SELECT SUM(total) AS total FROM (
            SELECT COUNT('id_status') AS total FROM surat_biodata WHERE id_status = '4' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_domisili WHERE id_status = '4' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_ijin_keramaian WHERE id_status = '4' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kelahiran WHERE id_status = '4' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kematian WHERE id_status = '4' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kip WHERE id_status = '4' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kuasa WHERE id_status = '4' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_permohonan_ktp WHERE id_status = '4' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_perubahan_kk WHERE id_status = '4' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_skck WHERE id_status = '4' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_sktm WHERE id_status = '4' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_sptjm WHERE id_status = '4'
        )X");
        return $query->result();
    }

    Public function insert_signature($image)
{
	$check=$this->get_signs();
	if($check==0)
	{
		$data=array(
			'name'=>$this->input->post('signname'),
			'img'=>$image,
			'rowno'=>$this->input->post('rowno'),
			'append'=>$this->input->post('appendcount')
			);

		$this->db->insert('signature', $data);
	}
	else
	{
		$data=array(
			'name'=>$this->input->post('signname'),
			'img'=>$image,		
			);
		$this->db
		      ->where('rowno',$this->input->post('rowno'))
		      ->where('append',$this->input->post('appendcount'))
		     ->update('signature', $data);
	}

	return ($this->db->affected_rows()!=1)?false:true;
}



Public function insert_single_signature($image)
{

	$check=$this->get_single_signs();
	if($check==0)
	{
		$data=array(			
			'img'=>$image,
			'rowno'=>$this->input->post('rowno')		
			);

		$this->db->insert('signature', $data);
	}
	else
	{

		$data=array('img'=>$image);

		$this->db
		      ->where('rowno',$this->input->post('rowno'))		
		     ->update('signature', $data);



	}

	return ($this->db->affected_rows()!=1)?false:true;
}

public function get_signs()
{
	$datas=array(
			'rowno'=>$this->input->post('rowno'),
			'append'=>$this->input->post('appendcount')
		);

	return $this->db->get_where('signature',$datas)->num_rows();
	

}

public function get_single_signs()
{
	$datas=array(
			'rowno'=>$this->input->post('rowno')			
		);

	return $this->db->get_where('signature',$datas)->num_rows();
	

}
}