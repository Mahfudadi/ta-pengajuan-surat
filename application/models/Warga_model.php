<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Warga_model extends CI_Model {

    public function tampil_data()
    {

        $this->db->select('*');
        $this->db->from('tb_warga');
        return $this->db->get()->result();

    }
    public function input_warga($data,$table){
		$this->db->insert($table,$data);
    }
    public function edit_warga($id)
    {
        $this->db->select('*');
        $this->db->from('tb_warga');
        $this->db->where('id_user',$id);
        return $this->db->get()->result();
    }
    public function update_warga($data, $id)
    {
        $this->db->update("tb_warga", $data, $id);
    }
    public function delete_warga($id)
    {    
        $this->db->where('id_user',$id);    
        $this->db->delete('tb_warga');
    }
    public function delete_user($id)
    {    
        $this->db->where('id_user',$id);    
        $this->db->delete('tb_user');
    }
    public function detail_warga($id)
    {
        $this->db->select('tb_warga.id_user,no_kk,nik, nama_lengkap,tempat_lahir,tanggal_lahir,jenis_kelamin,umur,gol_darah,penyandang_cacat,agama,status_kawin,pendidikan,pekerjaan,alamat,dusun,rw,rt,desa,kecamatan,kab_kota,prov,warga_negara');
        $this->db->from('tb_warga');
        $this->db->where('tb_warga.id_user',$id);
        $this->db->limit(1);
        return $this->db->get()->result();
    }
}