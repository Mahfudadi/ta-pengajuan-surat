<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class KetuaRt_model extends CI_Model {

	public function tampil_data()
    {

        $this->db->select('*');
        $this->db->from('tb_rt');
        return $this->db->get()->result();
    }
    public function detail_rt($id)
    {
        $this->db->select('*');
        $this->db->from('tb_rt r');
        $this->db->where('r.id_rt',$id);
        return $this->db->get()->result();
    }
    public function edit_rt($id)
    {
        $this->db->select('*');
        $this->db->from('tb_rt');
        $this->db->where('id_rt',$id);
        return $this->db->get()->result();
    }
    public function update_rt($data, $id)
    {
        $this->db->update("tb_rt", $data, $id);
    }
    public function profil($id)
    {
        $this->db->select('*');
        $this->db->from('tb_rt');
        $this->db->where('id_user',$id);

        
        return $this->db->get()->result();
    }
    public function edit_profil($id)
    {
        $this->db->select('*');
        $this->db->from('tb_rt');
        $this->db->where('id_user',$id);
        return $this->db->get()->result();
    }
    // public function update_profil($data, $id)
    // {
    //     $this->db->update("tb_rt", $data, $id);
    // }
    //rw
    // public function tampil_rw()
    // {

    //     $this->db->select('*');
    //     $this->db->from('tb_rw');
    //     return $this->db->get()->result();

    // }
    // public function detail_rw($id)
    // {
    //     $this->db->select('*');
    //     $this->db->from('tb_rw r');
    //     $this->db->where('r.id_rw',$id);
    //     return $this->db->get()->result();
    // }
    // public function edit_rw($id)
    // {
    //     $this->db->select('*');
    //     $this->db->from('tb_rw');
    //     $this->db->where('id_rw',$id);
    //     return $this->db->get()->result();
    // }
    // public function update_rt($data, $id)
    // {
    //     $this->db->update("tb_rt", $data, $id);
    // }
    public function total_disetujui()
    {
        $query = $this->db->query("SELECT SUM(total) AS total FROM (
            SELECT COUNT('id_status') AS total FROM surat_biodata WHERE id_status = '2' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_domisili WHERE id_status = '2' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_ijin_keramaian WHERE id_status = '2' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kelahiran WHERE id_status = '2' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kematian WHERE id_status = '2' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kip WHERE id_status = '2' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kuasa WHERE id_status = '2' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_permohonan_ktp WHERE id_status = '2' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_perubahan_kk WHERE id_status = '2' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_skck WHERE id_status = '2' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_sktm WHERE id_status = '2' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_sptjm WHERE id_status = '2'
        )X");
        return $query->result();
    }
    public function total_belum()
    {
        $query = $this->db->query("SELECT SUM(total) AS total FROM (
            SELECT COUNT('id_status') AS total FROM surat_biodata WHERE id_status = '1' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_domisili WHERE id_status = '1' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_ijin_keramaian WHERE id_status = '1' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kelahiran WHERE id_status = '1' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kematian WHERE id_status = '1' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kip WHERE id_status = '1' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_kuasa WHERE id_status = '1' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_permohonan_ktp WHERE id_status = '1' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_perubahan_kk WHERE id_status = '1' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_skck WHERE id_status = '1' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_sktm WHERE id_status = '1' UNION ALL
            SELECT COUNT('id_status') AS total FROM surat_sptjm WHERE id_status = '1'
        )X");
        return $query->result();
    }
}